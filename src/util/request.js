import axios from 'axios';
import get from 'lodash/get';
import local from './local';
import appInstance from '../index';
axios.defaults.baseURL = `${process.env.REACT_APP_URL || ""}/api`;

export default (
  url,
  options = {
    method: "get",
    headers: {}
  },
  noti = false
) => {
  let headers = {};
  if (local.get('token')) {
    headers = {
      'Authorization': `Bearer ${local.get('token')}`
    };
  }
  if (!options.method) {
    options.method = "get";
  }
  if (
    options.method.toLowerCase() === 'post' ||
    options.method.toLowerCase() === 'put' ||
    options.method.toLowerCase() === 'patch' ||
    options.method.toLowerCase() === 'delete'
  ) {
    if (options.data instanceof FormData) {
      headers = Object.assign(headers, {
        ...options.headers,
        // 'Accept': 'application/json',
      });
    } else {
      headers = Object.assign(headers, {
        ...options.headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      });
      // options.data = JSON.stringify(options.data);
    }
  } /* else if (options.params && typeof options.params == 'object'){
    options.params = JSON.stringify(options.params);
  } */

  return axios({
    url,
    /* transformRequest: [function (data, headers) {
      return data;
    }],
    transformResponse: [function (data) {
        return data;
    }], */
    timeout: 1000 * 60 * 5, // current: 5' default is `0` (no timeout)
    ...options,
    headers
  })
    .then(res => res)
    .catch(function (error) {
      if (axios.isCancel(error)) {
        console.log('Request canceled', error.message);
        throw error;
      } else {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          if (error.response.status == 401) {
            // redirect login
            appInstance._store.dispatch({
              type: 'auth/userSignOut',
            });
            return;
            // appInstance._store.dispatch(routerRedux.replace({ pathname: '/exception/403' }));
          }
          let msg = 'Đã có lỗi xảy ra';
          try {
            msg = JSON.parse(error.response.data).message;
          } catch (err) {
            msg = get(error, "response.data.message", 'Đã có lỗi xảy ra');
          }
          throw new Error(msg);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
          throw new Error('Hệ thống backend xảy ra lỗi');
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
          throw new Error(error.message || 'Lỗi gọi api lấy dữ liệu');
        }
        console.log(error.config);
        throw error;
      }
    });
};
