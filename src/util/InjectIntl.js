import React from 'react';
import { injectIntl } from "react-intl";

const InjectIntl = ({ intl }) => Component => {
  return <Component intl={intl} />;
};

export default injectIntl(InjectIntl);
