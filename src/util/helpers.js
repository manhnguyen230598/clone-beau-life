import React, { useEffect, useRef } from 'react';
import { parse, stringify } from 'qs';
import dayjs from 'dayjs';
import XLSX from 'xlsx';
import { get, omit, pick, assign, cloneDeep } from 'lodash';

export function fromExcelDate(excelDate, date1904) {
  const daysIn4Years = 1461;
  const daysIn70years = Math.round(25567.5 + 1); // +1 because of the leap-year bug
  const daysFrom1900 = excelDate + (date1904 ? daysIn4Years + 1 : 0);
  const daysFrom1970 = daysFrom1900 - daysIn70years;
  const secondsFrom1970 = daysFrom1970 * (3600 * 24);
  const utc = new Date(secondsFrom1970 * 1000);
  return !isNaN(utc) ? utc : null;
}

export function toExcelDate(date, date1904) {
  if (isNaN(date)) return null;
  const daysIn4Years = 1461;
  const daysIn70years = Math.round(25567.5 + 1); // +1 because of the leap-year bug
  const daysFrom1970 = date.getTime() / 1000 / 3600 / 24;
  const daysFrom1900 = daysFrom1970 + daysIn70years;
  const daysFrom1904Jan2nd = daysFrom1900 - daysIn4Years - 1;
  return Math.round(date1904 ? daysFrom1904Jan2nd : daysFrom1900);
}

export const usePrevious = (state) => {
  const ref = useRef();

  useEffect(() => {
    ref.current = state;
  });

  return ref.current;
};

/* list of supported file types */
export const SheetJSFT = [
  "xlsx", "xlsb", "xlsm", "xls", "xml", "csv", "txt", "ods", "fods", "uos", "sylk", "dif", "dbf", "prn", "qpw", "123", "wb*", "wq*", "html", "htm"
].map(x => `.${x}`).join(",");

/* generate an array of column objects */
export const make_cols = refstr => {
  let o = [], C = XLSX.utils.decode_range(refstr).e.c + 1;
  for (let i = 0; i < C; ++i) o[i] = { name: XLSX.utils.encode_col(i), key: i };
  return o;
};

const pad = (n) => (n < 10 ? `0${n}` : n);

export const formatTimeClock = (t) => {
  const hours = t.getUTCHours();
  const minutes = t.getUTCMinutes();
  const seconds = t.getUTCSeconds();
  return `${pad(hours)}:${pad(minutes)}:${pad(seconds)}`;
};

export const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

export const formatDate = (value) => {
  return dayjs(value).format("DD-MM-YYYY");
};

export const camelToPascalCase = (v) => {
  let ret = '';
  let i = 0;
  for (let s of v) {
    s = i === 0 ? s.toUpperCase() : s;
    ret += s;
    i += 1;
  }
  return ret;
};

export function fixedZero(val) {
  return val * 1 < 10 ? `0${val}` : val;
}

export function getTimeDistance(type) {
  const now = new Date();
  const oneDay = 1000 * 60 * 60 * 24;

  if (type === 'today') {
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    return [dayjs(now), dayjs(now.getTime() + (oneDay - 1000))];
  }

  if (type === 'week') {
    let day = now.getDay();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);

    if (day === 0) {
      day = 6;
    } else {
      day -= 1;
    }

    const beginTime = now.getTime() - day * oneDay;

    return [dayjs(beginTime), dayjs(beginTime + (7 * oneDay - 1000))];
  }

  if (type === 'month') {
    const year = now.getFullYear();
    const month = now.getMonth();
    const nextDate = dayjs(now).add(1, 'months');
    const nextYear = nextDate.year();
    const nextMonth = nextDate.month();

    return [
      dayjs(`${year}-${fixedZero(month + 1)}-01 00:00:00`),
      dayjs(dayjs(`${nextYear}-${fixedZero(nextMonth + 1)}-01 00:00:00`).valueOf() - 1000),
    ];
  }

  const year = now.getFullYear();
  return [dayjs(`${year}-01-01 00:00:00`), dayjs(`${year}-12-31 23:59:59`)];
}

export function getPageQuery() {
  return parse(window.location.href.split('?')[1]);
}

export function getQueryPath(path = '', query = {}) {
  const search = stringify(query);
  if (search.length) {
    return `${path}?${search}`;
  }
  return path;
}

/* eslint no-useless-escape:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;

export function isUrl(path) {
  return reg.test(path);
}

export function formatVnd(val) {
  const v = val * 1;
  if (!v || Number.isNaN(v)) return '';

  let result = val;
  if (val > 10000) {
    result = Math.floor(val / 10000);
    result = (
      <span>
        {result}
        <span
          style={{
            position: 'relative',
            top: -2,
            fontSize: 14,
            fontStyle: 'normal',
            marginLeft: 2,
          }}
        >
          đ
        </span>
      </span>
    );
  }
  return result;
}

export const formatNumber = (value) => {
  // eslint-disable-next-line no-param-reassign
  if (Number.isNaN(value)) return 0;
  value += '';
  const list = value.split('.');
  const prefix = list[0].charAt(0) === '-' ? '-' : '';
  let num = prefix ? list[0].slice(1) : list[0];
  let result = '';
  while (num.length > 3) {
    result = `,${num.slice(-3)}${result}`;
    num = num.slice(0, num.length - 3);
  }
  if (num) {
    result = num + result;
  }
  return `${prefix}${result}${list[1] ? `.${list[1]}` : ''}`;
};

export const fnKhongDau = (str) => {
  let strReturn = str;
  strReturn = strReturn.toLowerCase();
  strReturn = strReturn.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  strReturn = strReturn.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  strReturn = strReturn.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  strReturn = strReturn.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  strReturn = strReturn.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  strReturn = strReturn.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  strReturn = strReturn.replace(/đ/g, "d");
  // strReturn = strReturn.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
  // strReturn = strReturn.replace(/-+-/g, " ");
  // strReturn = strReturn.replace(/^\-+|\-+$/g, "");
  // strReturn = strReturn.replace('-', ' ');
  return strReturn;
};

export const textToDash = (str) => {
  let strReturn = str;
  strReturn = strReturn.toLowerCase();
  strReturn = strReturn.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  strReturn = strReturn.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  strReturn = strReturn.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  strReturn = strReturn.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  strReturn = strReturn.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  strReturn = strReturn.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  strReturn = strReturn.replace(/đ/g, "d");
  strReturn = strReturn.replace(/!|@|\$|%|\”|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
  // strReturn = strReturn.replace(/-+-/g, "-");
  strReturn = strReturn.replace(/^\-+|\-+$/g, "-");
  strReturn = strReturn.replace(/-+/, '-');
  return strReturn;
};

export const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

export const camelCaseToDash = (v) => {
  let ret = '', prevLowercase = false, prevIsNumber = false;
  for (let s of v) {
    const isUppercase = s.toUpperCase() === s;
    const isNumber = !isNaN(s);
    if (isNumber) {
      if (prevLowercase) {
        ret += '-';
      }
    } else {
      if (isUppercase && (prevLowercase || prevIsNumber)) {
        ret += '-';
      }
    }
    ret += s;
    prevLowercase = !isUppercase;
    prevIsNumber = isNumber;
  }
  return ret.replace(/-+/g, '-').toLowerCase();
};

const baseUrlUpload = process.env.REACT_APP_URL
  ? process.env.REACT_APP_URL
  : window.location.origin;

export const uploadImage = async (file) => {
  // const arrType = file.type.split('/');
  let fro = new FormData();
  fro.append("images", file);
  let getFileName = await fetch(baseUrlUpload + "/api/file/upload-image", {
    method: "POST",
    body: fro
  });
  if (getFileName.status !== 200) throw getFileName;
  return getFileName.json();
};


export const setSearchParam = (key, value) => {
  if (!window.history.pushState) {
    return;
  }

  if (!key) {
    return;
  }

  let url = new URL(window.location.href);
  let params = new window.URLSearchParams(window.location.search);
  if (value === undefined || value === null) {
    params.delete(key);
  } else {
    params.set(key, value);
  }

  url.search = params;
  url = url.toString();
  // window.history.replaceState({ url: url }, null, url);
};

export const getTableChange = (resource) => {
  const tableChangeStr = sessionStorage.getItem(`${resource}_tableChange`);
  if (tableChangeStr) return JSON.parse(tableChangeStr);
  return JSON.parse("{}");
};

export const setTableChange = (resource, obj) => {
  sessionStorage.setItem(`${resource}_tableChange`, JSON.stringify({ ...obj }));
};

export const tranformClassRoom = (record) => {
  let data = cloneDeep(record);
  if (data.projectId && typeof data.projectId == "object") {
    data.projectId = data.projectId.id;
  }
  if (data.agencyId && typeof data.agencyId == "object") {
    data.agencyId = data.agencyId.id;
  }
  if (data.agencyIds && Array.isArray(data.agencyIds)) {
    data.agencyIds = data.agencyIds.map(i => i.id);
  }
  if (data.teacher && typeof data.teacher == "object") {
    data.teacher = data.teacher.id;
  }
  if (data.startDate && dayjs.isDayjs(data.startDate)) {
    data.startDate = dayjs(data.startDate).valueOf();
  }
  if (data.endDate && dayjs.isDayjs(data.endDate)) {
    data.endDate = dayjs(data.endDate).valueOf();
  }
  if (data.thumbnail && typeof data.thumbnail == "object") {
    data.thumbnail = get(data, "thumbnail[0].url", "");
  }
  if (data.isActive && typeof data.isActive == "string") {
    data.isActive = Boolean(data.isActive);
  }
  return data;
};

export const tranformClassLesson = (data, classId) => {
  const lessons = get(data, "classlessons", []);
  return lessons.map(item => {
    item.classId = classId;
    if (item.thumbnail && typeof item.thumbnail == "object") {
      item.thumbnail = get(item, "thumbnail[0].url", "");
    }
    if (item.documents) {
      if (item.documents.link && typeof item.documents.link == "object") {
        item.documents.link = get(item, "link.documents.url", "");
      }
      if (item.documents.type) {
        item.documents.type = "pdf";
      } else {
        item.documents.type = "video";
      }
      if (typeof item.documents == "object") {
        item.documents = [item.documents];
      }
    }
    return item;
  });
};

export const tranformClassExam = (data, classId) => {
  if (data.startDate && dayjs.isDayjs(data.startDate)) {
    data.startDate = dayjs(data.startDate).valueOf();
  }
  if (data.endDate && dayjs.isDayjs(data.endDate)) {
    data.endDate = dayjs(data.endDate).valueOf();
  }
  if (data.isActive && typeof data.isActive == "string") {
    data.isActive = Boolean(data.isActive);
  }
  if (data.questions && Array.isArray(data.questions)) {
    data.questions.map(item => {
      if (item.correctAnswer && typeof item.correctAnswer == "string") {
        item.correctAnswer = item.correctAnswer.split(',');
      }
      return item;
    });
  }
  let exam = omit(data, ["xlsx"]);
  exam = pick(data, ["name", "startDate", "endDate", "minCorrectAnswer", "questions", "timeTestText", "timeTest", "totalQuestion", "isActive"]);
  exam = assign(exam, { classId });
  return exam;
};

export const tranformClassUser = (data, classId) => {
  if (data.type) {
    data.type = "agency";
  } else {
    data.type = "users";
  }
  return assign(data, { classId });
};

export function useOutsideMenu(ref, callback) {
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        callback();
      }
    }

    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
}
