import request from 'util/request';

export async function getList(params = {}, notification) {
  return request(`/role`, {
    params
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/role/${id}`, { params }, notification);
  }
  return request(`/role/${id}`, {}, notification);
}

export async function create(data, notification) {
  return request(`/role`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/role/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function getRoleMenu(params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/admin/menu/get-role-menu`, { params }, notification);
  }
  return request(`/admin/menu/get-role-menu`, {}, notification);
}
