import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

// export async function getList(params = {}, notification) {
//   let queryInput;
//   let query = omit(params, "queryInput", {});
//   if (params.queryInput){
//     queryInput = JSON.stringify(params.queryInput);

//   }
//   if (queryInput){
//     return request(`/stackingplandata?queryInput=${queryInput}&${stringify(query)}`, {
//     }, notification);
//   }
//   return request(`/stackingplandata?${stringify(query)}`, {
//   }, notification);
// }

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/stackingplandata?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/stackingplandata?queryInput=${JSON.stringify({ id: Number(id) })}`, {}, notification);
}

export async function create(data, notification) {
  return request(`/stackingplandata`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/stackingplandata/${id}`, {
    method: 'patch',
    data,
  }, notification);
}


export async function getList(params = {}, notification) {
  let query = omit(params, "queryInput", {});
  return request(`/admin/stackingplan/get-data-by-project?${stringify(query)}`, {
  }, notification);
}

export async function updateMulti(data, notification) {
  return request(`/admin/stackingplan/approve-multi`, {
    method: 'post',
    data,
  }, notification);
}