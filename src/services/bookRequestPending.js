import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  return request(`admin/dashboard-project/request-pending?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`????queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`????queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`???`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`???/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function browserBooking( data, notification) {
  return request(`admin/apartment/approve-book?status=${data.status}&id=${data.id}`, {
    method: 'post',
    data,
  }, notification);
}