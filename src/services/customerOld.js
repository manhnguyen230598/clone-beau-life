import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput){
    return request(`/customerold?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/customerold?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/customerold?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/customerold?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`/customerold`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/customerold/${id}`, {
    method: 'patch',
    data,
  }, notification);
}
export async function importData(data, notification) {
  let form = new FormData();
  form.append("file", data.file.file);
  let res = await new Promise((rs, rj) => {
    fetch(process.env.REACT_APP_URL + "/api/admin/customerold/import-from-file", {
      method: "POST",
      body: form,
      headers : {
        Authorization : `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then(response => response.json())
      .then(data => rs(data));
  });
  return res;
}
