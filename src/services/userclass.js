import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);
  }
  if (queryInput){
    return request(`/userclass?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/userclass?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/userclass?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/userclass?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`/userclass`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  // delete data.id
  return request(`/userclass/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function appendBroker(data, notification) {
  return request(`/admin/class/approve-broker`, {
    method: 'post',
    data,
  }, notification);
}

export async function getUserNotInClass(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);
  }
  if (queryInput) {
    return request(`/admin/user-class/get-user-not-in-class?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/admin/user-class/get-user-not-in-class?${stringify(query)}`, {
  }, notification);
}
