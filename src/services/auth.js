import request from "util/request";
import {
  auth
  // facebookAuthProvider,
  // githubAuthProvider,
  // googleAuthProvider,
  // twitterAuthProvider
} from "../firebase/firebase";

export const signInUserWithUserPasswordRequest = async (username, password) =>
  await request(`admin/login`, {
    method: "post",
    data: {
      username,
      password
    }
  })
    .then(authUser => authUser)
    .catch(error => error);
export const getLogin = () => {
  return request(`/admin/get-login`, {
    method: "get"
  });
};

export const signInUserWithEmailPasswordRequest = async (email, password) =>
  await auth
    .signInWithEmailAndPassword(email, password)
    .then(authUser => authUser)
    .catch(error => error);

export const signOutRequest = async () =>
  await auth
    .signOut()
    .then(authUser => authUser)
    .catch(error => error);
export const resetTools = data => {
  return request(`/admin/reset-tools`, {
    method: "post",
    data
  });
};
