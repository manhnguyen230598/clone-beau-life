import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput){
    return request(`/saletarget?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/saletarget?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/saletarget?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/saletarget?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`/saletarget`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  // return request(`/saletarget/${id}`, {
  //   method: 'patch',
  //   data,
  // }, notification);
  let body = {
    id,
    amount : data.amount || 0
  }
  return request(`/admin/saletarget/update`, {
      method: 'post',
      data : body,
    }, notification);
}

export async function createMulti(data, notification) {
  return request(`/admin/saletarget/create`, {
    method: 'post',
    data,
  }, notification);
}

export async function report3Months(data, notification) {
  return request(`/admin/saletarget/get-three-month`, {
    method: 'post',
    data,
  }, notification);
}
export async function getReportRangeData(data, notification) {
  return request(`/admin/saletarget/report-data`, {
    method: 'post',
    data,
  }, notification);
}