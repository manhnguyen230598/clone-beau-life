import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput){
    return request(`/projectvisitingcustomer?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/projectvisitingcustomer?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/projectvisitingcustomer?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/projectvisitingcustomer?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`/projectvisitingcustomer`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/projectvisitingcustomer/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function importList(data, notification) {
  return request(`/admin/customer/import-project`, {
    method: 'post',
    data,
    headers:{
      "Accept":"application/vnd.ms-excel"
    }
  }, notification);
}
