import { stringify } from "qs";
import omit from "lodash/omit";
import request from "util/request";

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);
  }
  if (queryInput) {
    return request(
      `/agencytarget?queryInput=${queryInput}&${stringify(query)}`,
      {},
      notification
    );
  }
  return request(`/agencytarget?${stringify(query)}`, {}, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(
      `/agencytarget?queryInput=${JSON.stringify({ id: Number(id) })}`,
      { params },
      notification
    );
  }
  return request(
    `/agencytarget?queryInput=${JSON.stringify({ id: Number(id) })}`,
    {},
    notification
  );
}

export async function create(data, notification) {
  return request(
    `/admin/agencysale/create`,
    {
      method: "post",
      data
    },
    notification
  );
}

export async function update(id, data, notification) {
  return request(
    `/agencytarget/${id}`,
    {
      method: "patch",
      data
    },
    notification
  );
}
export async function deleteRecord(id, notification) {
  return request(
    `/agencytarget/${id}`,
    {
      method: "delete"
    },
    notification
  );
}
export async function getListAgency(id, notification) {
  return request(
    `/admin/agencysale/get-list-agency`,
    {
      method: "post",
      data: {
        id
      }
    },
    notification
  );
}
export async function updateAmount(id, amount, notification) {
  return request(
    `/agencysaledata/${id}`,
    {
      method: "patch",
      data: {
        amount
      }
    },
    notification
  );
}
