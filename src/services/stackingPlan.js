import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  console.log(`🚀 ~ file: stackingPlan.js ~ line 6 ~ getList ~ params`, params);
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput) {
    return request(`/stackingplan?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/stackingplan?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/stackingplan?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/stackingplan?queryInput=${JSON.stringify({ id: Number(id) })}`, {}, notification);
}

export async function create(data, notification) {
  return request(`/stackingplan`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/stackingplan/${id}`, {
    method: 'patch',
    data,
  }, notification);
}
