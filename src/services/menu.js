import request from 'util/request';

export const getMenu = async () =>
  await request(`/admin/menu/get-meta`, {})
    .then(res => res)
    .catch(error => error);
