import request from "util/request";
import { stringify } from "qs";
import omit from "lodash/omit";

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);
  }
  if (queryInput) {
    return request(
      `/user?queryInput=${queryInput}&${stringify(query)}`,
      {},
      notification
    );
  }
  return request(`/user?${stringify(query)}`, {}, notification);
}

export async function get(params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/user`, { params }, notification);
  }
  return request(`/user`, { params }, notification);
}

export async function create(data, notification) {
  return request(
    `/user/create-user`,
    {
      method: "post",
      data
    },
    notification
  );
}

export async function update(id, data, notification) {
  return request(
    `admin/user/update-info`,
    {
      method: "post",
      data : {
        id,
        info : data
      }
    },
    notification
  );
}
export async function importData(data, notification) {
  let form = new FormData();
  form.append("agencyId", data.agencyId.id);
  form.append("file", data.file.file);
  // let res = await fetch(
  //   process.env.REACT_APP_URL + "/api/admin/user/import-from-file",
  //   {
  //     method: "POST",
  //     body: form
  //   }
  // );
  let res = await new Promise((rs, rj) => {
    fetch(process.env.REACT_APP_URL + "/api/admin/user/import-from-file", {
      method: "POST",
      body: form
    })
      .then(response => response.json())
      .then(data => rs(data));
  });
  return res;
}
export async function changePass(data, notification) {
  return request(
    `/admin/user/set-password`,
    {
      method: "patch",
      data
    },
    notification
  );
}
export async function approveUser(data, notification) {
  return request(
    `/admin/user/approve-user`,
    {
      method: "post",
      data
    },
    notification
  );
}
export async function setUserRole(data, notification) {
  return request(
    `/admin/user/set-user-role`,
    {
      method: "post",
      data
    },
    notification
  );
}
