import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';


export async function getList(data, notification) {
  return request(`/admin/apartment/report-agency-sale`, {
    method: 'post',
    data,
  }, notification);
}

