import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput){
    return request(`/roleuserprojectjob?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/roleuserprojectjob?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/roleuserprojectjob?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/roleuserprojectjob?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`/roleuserprojectjob`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/roleuserprojectjob/${id}`, {
    method: 'patch',
    data,
  }, notification);
}


export async function getUserNotInClass(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);
  }
  if (queryInput) {
    return request(`/admin/user-checklist/get-user-not-in?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/admin/user-checklist/get-user-not-in?${stringify(query)}`, {
  }, notification);
}

export async function approveStaff(data, notification) {
  return request(
    `admin/user-checklist/import`,
    {
      method: "POST",
      data
    },
    notification
  );
}