import { stringify } from "qs";
import omit from "lodash/omit";
import request from "util/request";
export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);
  }
  if (queryInput) {
    return request(
      `/businessman?queryInput=${queryInput}&${stringify(query)}`,
      {},
      notification
    );
  }
  return request(`/businessman?${stringify(query)}`, {}, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(
      `businessman?queryInput=${JSON.stringify({ id: Number(id) })}`,
      { params },
      notification
    );
  }
  return request(
    `businessman?queryInput=${JSON.stringify({ id: Number(id) })}`,
    {},
    notification
  );
}

export async function create(data, notification) {
  return request(
    `businessman`,
    {
      method: "post",
      data
    },
    notification
  );
}

export async function update(id, data, notification) {
  return request(
    `businessman/${id}`,
    {
      method: "patch",
      data
    },
    notification
  );
}

export async function approveStaff(data, notification) {
  return request(
    `admin/businessman/approve-staff`,
    {
      method: "POST",
      data
    },
    notification
  );
}
export async function approveAgency(data, notification) {
  return request(
    `admin/businessman/approve-agency`,
    {
      method: "POST",
      data
    },
    notification
  );
}
export async function importExcel(data, notification) {
  let formData = new FormData();
  formData.append("projectId",data.projectId)
  formData.append("file",data.file[0])
  return request(
    `admin/businessman/import-excel`,
    {
      method: "POST",
      data : formData,
      headers:{
        "Accept":"application/vnd.ms-excel"
      }
    },
    
    notification
  );
}
export async function getUserNotInClass(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);
  }
  if (queryInput) {
    return request(`/admin/businessman/get-staff-not-in?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/admin/businessman/get-staff-not-in?${stringify(query)}`, {
  }, notification);
}
