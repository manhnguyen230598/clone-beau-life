import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput){
    return request(`--?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`--?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`--?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`--?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`--`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/report-user-class/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function reportClass(id, data, notification) {
  return request(`admin/report/report-user-class?classId=${id}`, {
    method: 'get',
    data,
  }, notification);
}

export async function reportUser( data, notification) {
  return request(`admin/report/report-user`, {
    method: 'get',
    data,
  }, notification);
}

export async function countHome( data, notification) {
  return request(`admin/report/count-active`, {
    method: 'get',
    data,
  }, notification);
}

export async function dataProjectHome(id, data, notification) {
  return request(`admin/report/report-user-project?projectId=${id}`, {
    method: 'get',
    data,
  }, notification);
}

export async function dataHistoryUser(day, data, notification) {
  return request(`admin/report/user-login?range=${day}days`, {
    method: 'get',
    data,
  }, notification);
}
export async function dataProjectStaff(data, notification) {
  return request(`admin/report/chart-project-staff`, {
    method: 'post',
    data,
  }, notification);
}