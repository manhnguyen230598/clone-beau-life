import { stringify } from 'qs';
import omit from 'lodash/omit';
import forIn from 'lodash/forIn';
import cloneDeep from 'lodash/cloneDeep';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput) {
    return request(`/job?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/job?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/job?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/job?queryInput=${JSON.stringify({ id: Number(id) })}`, {}, notification);
}

export async function create(data, notification) {
  return request(`/job`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/job/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function updateJob(id, data, notification) {
  let dataCrud = cloneDeep(data);
  forIn(data, (key, value) => {
    if(value == null || value == undefined){
      dataCrud = omit(dataCrud, [key]);
    }
  });
  return request(`/admin/job/update`, {
    method: 'post',
    data: {
      id,
      info: {
        ...(omit(dataCrud, "id"))
      }
    },
  }, notification);
}
export async function updateSequenceTimeline(data, notification) {
  return request(`/admin/job/update-sequence-timeline`, {
    method: 'post',
    data,
  }, notification);
}
export async function getHistoryChange(data, notification) {
  return request(`/admin/job/get-history-change`, {
    method: 'post',
    data,
  }, notification);
}
export async function getById(id, notification) {
  return request(`admin/job/get-job-info`, {
    method: 'post',
    data : {
      jobId : id
    },
  }, notification);
}

