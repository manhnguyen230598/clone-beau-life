import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput){
    return request(`/apartment?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/apartment?${stringify(query)}`, {
  }, notification);
}
export async function getListManage(params = {}, notification) {
  return request(`/admin/project/manage-project?${stringify(params)}`, {
  }, notification);
}
export async function getListTemp(params = {}, notification) {
  return request(`/admin/apartment/get-booking-temp?${stringify(params)}`, {
  }, notification);
}


export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/apartment?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/apartment?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`/apartment`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/apartment/${id}`, {
    method: 'patch',
    data,
  }, notification);
}


export async function importLowApartment(data, notification) {
  return request(`/admin/apartment/import-low-apartment`, {
    method: 'post',
    data,
    headers:{
      "Accept":"application/vnd.ms-excel"
    }
  }, notification);
}

export async function importHighApartment(data, notification) {
  return request(`/admin/apartment/import-high-apartment`, {
    method: 'post',
    data,
    headers:{
      "Accept":"application/vnd.ms-excel"
    }
  }, notification);
}

export async function updateStatus(data, notification) {
  return request(`/admin/apartment/update-status`, {
    method: 'post',
    data,
  }, notification);
}
export async function updateStatusMulti(data, notification) {
  return request(`/admin/apartment/update-multi-status`, {
    method: 'post',
    data,
  }, notification);
}
