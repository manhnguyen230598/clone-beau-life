import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput){
    return request(`/checklist?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/checklist?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/checklist?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/checklist?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`/checklist`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/checklist/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function updateJob(data, notification) {
  return request(`/admin/job/update`, {
    method: 'post',
    data,
  }, notification);
}

export async function updateSequence(data, notification) {
  return request(`/admin/job/update-sequence`, {
    method: 'post',
    data,
  }, notification);
}

export async function checkInfoProjectChecklist(params = {}, notification) {
  return request(`/admin/checklist/get-initial?${stringify(params)}`, {}, notification);
}

export async function getListProject(notification) {
  return request(`/admin/checklist/get-list-project`, { }, notification);
}

export async function getByProject(params = {}, opts = {}, notification) {
  return request(`/admin/checklist/get-by-project?${stringify(params)}`, {
    ...opts
  }, notification);
}

export async function canCreate(params = {}, notification) {
  return request(`/admin/checklist/check-can-create-checklist?${stringify(params)}`, {}, notification);
}

export async function createChecklist(data, notification) {
  return request(`/admin/checklist/create`, {
    method: 'post',
    data,
  }, notification);
}

export async function startChecklist(data, notification) {
  return request(`/admin/checklist/start-checklist`, {
    method: 'post',
    data,
  }, notification);
}

export async function exportExcel(projectId, token, notification) {
  return request(`/admin/checklist/export?projectId=${projectId}&accesstoken=${token}`, {
    method: 'get',
    responseType: 'arraybuffer'
  }, notification);
}
