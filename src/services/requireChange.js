import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput){
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput){
    return request(`jobrequirechange?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`jobrequirechange?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`jobrequirechange?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`jobrequirechange?queryInput=${JSON.stringify({ id: Number(id) })}`, { }, notification);
}

export async function create(data, notification) {
  return request(`jobrequirechange`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`jobrequirechange/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function getChangeDeadline(id, data, notification) {
  return request(`admin/job/check-deadline-change?requestId=${id}`, {
    method: 'get',
    data,
  }, notification);
}

export async function approveChange( data, notification) {
  return request(`admin/job/approve-change-deadline?status=${data.status}&id=${data.id}`, {
    method: 'post',
    data,
  }, notification);
}
