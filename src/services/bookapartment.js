import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  return request(`/admin/apartment/get-list-apartment?${stringify(params)}`, {
  }, notification);
}

export async function get(data, notification) {
  // return request(`/admin/apartment/get-apartment-info?projectId=${payload.projectId}&fullCode=${payload.fullCode}`, notification);
  return request(`/admin/apartment/get-apartment-info`, {
    method: 'post',
    data,
  }, notification);
}
export async function book(data, notification) {
  return request(`/admin/apartment/book-apartment`, {
    method: 'post',
    data,
  }, notification);
}


export async function create(data, notification) {
  return request(`//`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`///${id}`, {
    method: 'patch',
    data,
  }, notification);
}
export async function approveBook2(data, notification) {
  return request(`/admin/apartment/approve-book-2`, {
    method: 'post',
    data,
  }, notification);
}
