import { stringify } from 'qs';
import omit from 'lodash/omit';
import request from 'util/request';

export async function getList(params = {}, notification) {
  let queryInput;
  let query = omit(params, "queryInput", {});
  if (params.queryInput) {
    queryInput = JSON.stringify(params.queryInput);

  }
  if (queryInput) {
    return request(`/project?queryInput=${queryInput}&${stringify(query)}`, {
    }, notification);
  }
  return request(`/project?${stringify(query)}`, {
  }, notification);
}

export async function get(id, params = {}, notification) {
  if (params !== null && Object.keys(params).length > 0) {
    return request(`/project?queryInput=${JSON.stringify({ id: Number(id) })}`, { params }, notification);
  }
  return request(`/project?queryInput=${JSON.stringify({ id: Number(id) })}`, {}, notification);
}

export async function create(data, notification) {
  return request(`/project`, {
    method: 'post',
    data,
  }, notification);
}

export async function update(id, data, notification) {
  return request(`/project/${id}`, {
    method: 'patch',
    data,
  }, notification);
}

export async function customProject(id, month , year, data, notification) {
  return request(`admin/customer/statistics-by-month?projectId=${id}&month=${month}&year=${year}`, {
    method: 'get',
    data,
  }, notification);
}

export async function customerVisit(id, month , year, data, notification) {
  return request(`admin/customer/statistics-by-agency?projectId=${id}&month=${month}&year=${year}`, {
    method: 'get',
    data,
  }, notification);
}

export async function reportSale(id, startYear, endYear,startmonth , endmonth, data, notification) {
  return request(`admin/apartment/report-total-saled?projectId=${id}&startYear=${startYear}&endYear=${endYear}&startMonth=${startmonth}&endMonth=${endmonth}`, {
    method: 'get',
    data,
  }, notification);
}

export async function completedReport(id, startYear, endYear,startmonth , endmonth, data, notification) {
  return request(`admin/apartment/chart-sale-target?projectId=${id}&startYear=${startYear}&endYear=${endYear}&startMonth=${startmonth}&endMonth=${endmonth}`, {
    method: 'get',
    data,
  }, notification);
}


export async function reportVisit(id, data, notification) {
  return request(`admin/customer/report-portrait-visit?projectId=${id}`, {
    method: 'get',
    data,
  }, notification);
}

export async function reportMedia(id, data, notification) {
  return request(`admin/customer/report-portrait-media?projectId=${id}`, {
    method: 'get',
    data,
  }, notification);
}

export async function addStackingPlan(data, notification) {
  return request(`/admin/stackingplan/create-by-project`, {
    method: 'post',
    data,
  }, notification);
}
export async function countBooking(id, data, notification) {
  return request(`admin/dashboard-project/count-request-booking?projectId=${id}`, {
    method: 'get',
    data,
  }, notification);
}

export async function notYetBooking(id, skip, limit , data, notification) {
  return request(`admin/dashboard-project/request-pending?projectId=${id}&skip=${skip}&limit=${limit}`, {
    method: 'get',
    data,
  }, notification);
}


export async function customerBought(id, month , year , data, notification) {
  return request(`admin/customer/report-portrait-saled?projectId=${id}&month=${month}&year=${year}`, {
    method: 'get',
    data,
  }, notification);
}

export async function historyActivity(id, skip , limit , data, notification) {
  return request(`admin/dashboard-project/log-project?projectId=${id}&skip=${skip}&limit=${limit}`, {
    method: 'get',
    data,
  }, notification);
}
export async function exportExcel( id,token, data, notification) {
  return request(`admin/apartment/dowload-report-fund?projectId=${id}&accesstoken=${token}`, {
    method: 'get',
    data,
  }, notification);
}


export async function getListForBooking(notification) {
  return request(`admin/project/get-list-to-book`, {
    method: 'get',
  }, notification);
}
export async function cloneProject(data,notification) {
  return request(`admin/project/clone`, {
    method: 'post',
    data
  }, notification);
}

