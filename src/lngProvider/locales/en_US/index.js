import en_US from './en_US.json';
import user from './user/user_en_EN.json';
import classroom from './classroom/classroom_en_US.json';
// không xóa dòng dưới
/* PLOP_INJECT_IMPORT */
// import customerOld from './customerOld/customerOld_en_US.json';
import loginImage from './loginImage/loginImage_en_US.json';
import actionLog from './actionLog/actionLog_en_US.json';
import historyChangeJob from './historyChangeJob/historyChangeJob_en_US.json';
import eventApp from './eventApp/eventApp_en_US.json';
import popupApp from './popupApp/popupApp_en_US.json';
import bannerApp from './bannerApp/bannerApp_en_US.json';
import dealerTarget from './dealerTarget/dealerTarget_en_US.json';
import notification from './notification/notification_en_US.json';
import bannerHome from './bannerHome/bannerHome_en_US.json';
import requireChange from './requireChange/requireChange_en_US.json';
import relatedTasks from './relatedTasks/relatedTasks_en_US.json';
import groupJob from './groupJob/groupJob_en_US.json';
import tagJob from './tagJob/tagJob_en_US.json';
import roleProjectJob from './roleProjectJob/roleProjectJob_en_US.json';
import employees from './employees/employees_en_US.json';
import targetsDealer from './targetsDealer/targetsDealer_en_US.json';
import businessStaff from './businessStaff/businessStaff_en_US.json';
import salePolicy from './salePolicy/salePolicy_en_US.json';
import reportFund from './reportFund/reportFund_en_US.json';
import procedureProject from './procedureProject/procedureProject_en_US.json';
import paymentTransaction from './paymentTransaction/paymentTransaction_en_US.json';
import saleTarget from './saleTarget/saleTarget_en_US.json';
import bookRequestPending from './bookRequestPending/bookRequestPending_en_US.json';
import customer from './customer/customer_en_US.json';
import apartment from './apartment/apartment_en_US.json';
import projectMediaCustomer from './projectMediaCustomer/projectMediaCustomer_en_US.json';
import projectVisitingCustomer from './projectVisitingCustomer/projectVisitingCustomer_en_US.json';
import reportclass from './reportclass/reportclass_en_US.json';
import userDoExamLog from './userDoExamLog/userDoExamLog_en_US.json';
import agency from './agency/agency_en_US.json';

import building from './building/building_en_US.json';
import examination from './examination/examination_en_US.json';
import feedbackclass from './feedbackclass/feedbackclass_en_US.json';
import userclass from './userclass/userclass_en_US.json';
import checklist from './checklist/checklist_en_US.json';
import documentProject from './documentProject/documentProject_en_US.json';
import customProject from './customproject/customproject_en_US.json';
export default {
  ...en_US,
  ...user,
  ...classroom,
  // không xóa dòng dưới
  /* PLOP_INJECT_EXPORT */
// ...customerOld,
...loginImage,
...actionLog,
...historyChangeJob,
...eventApp,
...popupApp,
...bannerApp,
...dealerTarget,
...notification,
...bannerHome,
...requireChange,
...relatedTasks,
...groupJob,
...tagJob,
...roleProjectJob,
...employees,
...targetsDealer,
...businessStaff,
...salePolicy,
  ...reportFund,
  ...procedureProject,
  ...paymentTransaction,
  ...saleTarget,
  ...customer,
  ...saleTarget,
  ...bookRequestPending,
  ...apartment,
  ...projectMediaCustomer,
  ...projectVisitingCustomer,
  ...agency,
  ...building,
  ...examination,
  ...feedbackclass,
  ...reportclass,
  ...userDoExamLog,
  ...userclass,
  ...checklist,
  ...documentProject,
  ...customProject,
};
