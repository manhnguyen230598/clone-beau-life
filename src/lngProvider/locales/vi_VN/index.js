import vi_VN from './vi_VN.json';
import user from './user/user_vi_VN.json';
import classroom from './classroom/classroom_vi_VN.json';
/* PLOP_INJECT_IMPORT */
// import customerOld from './customerOld/customerOld_vi_VN.json';
import loginImage from './loginImage/loginImage_vi_VN.json';
import actionLog from './actionLog/actionLog_vi_VN.json';
import historyChangeJob from './historyChangeJob/historyChangeJob_vi_VN.json';
import eventApp from './eventApp/eventApp_vi_VN.json';
import popupApp from './popupApp/popupApp_vi_VN.json';
import bannerApp from './bannerApp/bannerApp_vi_VN.json';
import dealerTarget from './dealerTarget/dealerTarget_vi_VN.json';
import notification from './notification/notification_vi_VN.json';
import bannerHome from './bannerHome/bannerHome_vi_VN.json';
import requireChange from './requireChange/requireChange_vi_VN.json';
import relatedTasks from './relatedTasks/relatedTasks_vi_VN.json';
import groupJob from './groupJob/groupJob_vi_VN.json';
import tagJob from './tagJob/tagJob_vi_VN.json';
import roleProjectJob from './roleProjectJob/roleProjectJob_vi_VN.json';
import employees from './employees/employees_vi_VN.json';
import targetsDealer from './targetsDealer/targetsDealer_vi_VN.json';
import businessStaff from './businessStaff/businessStaff_vi_VN.json';
import salePolicy from './salePolicy/salePolicy_vi_VN.json';
import reportFund from './reportFund/reportFund_vi_VN.json';
import saleTarget from './saleTarget/saleTarget_vi_VN.json';
import procedureProject from './procedureProject/procedureProject_vi_VN.json';
import paymentTransaction from './paymentTransaction/paymentTransaction_vi_VN.json';
import bookRequestPending from './bookRequestPending/bookRequestPending_vi_VN.json';
import customer from './customer/customer_vi_VN.json';
import apartment from './apartment/apartment_vi_VN.json';
import projectMediaCustomer from './projectMediaCustomer/projectMediaCustomer_vi_VN.json';
import projectVisitingCustomer from './projectVisitingCustomer/projectVisitingCustomer_vi_VN.json';
import reportclass from './reportclass/reportclass_vi_VN.json';
import userDoExamLog from './userDoExamLog/userDoExamLog_vi_VN.json';
import agency from './agency/agency_vi_VN.json';

import building from './building/building_vi_VN.json';
import examination from './examination/examination_vi_VN.json';
import feedbackclass from './feedbackclass/feedbackclass_vi_VN.json';
import userclass from './userclass/userclass_vi_VN.json';
import checklist from './checklist/checklist_vi_VN.json';
import documentProject from './documentProject/documentProject_vi_VN.json';
import customProject from './customproject/customproject_vi_VN.json';

export default {
  ...vi_VN,
  ...user,
  ...classroom,
  /* PLOP_INJECT_EXPORT */
// ...customerOld,
...loginImage,
...actionLog,
...historyChangeJob,
...eventApp,
...popupApp,
...bannerApp,
...dealerTarget,
...notification,
...bannerHome,
...requireChange,
...relatedTasks,
...groupJob,
...tagJob,
...roleProjectJob,
...employees,
...targetsDealer,
...businessStaff,
...salePolicy,
  ...reportFund,
  ...saleTarget,
  ...procedureProject,
  ...paymentTransaction,
  ...bookRequestPending,
  ...customer,
  ...apartment,
  ...projectMediaCustomer,
  ...projectVisitingCustomer,
  ...agency,
  ...building,
  ...examination,
  ...feedbackclass,
  ...reportclass,
  ...userDoExamLog,
  ...userclass,
  ...checklist,
  ...documentProject,
  ...customProject,
};
