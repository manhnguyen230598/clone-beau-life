import dayjs from 'dayjs';
import { message } from 'antd';
import get from 'lodash/get';
import * as userServices from 'services/user';

const RESOURCE = 'user';
const transfromDocument = (record) => {

  if (record.createdAt) {
    const createdAt = record.createdAt;
    record.createdAt = dayjs(createdAt);
  }
  if (record.updatedAt) {
    const updatedAt = record.updatedAt;
    record.updatedAt = dayjs(updatedAt);
  }
  if (record.birth) {
    const birth = record.birth;
    record.birth = dayjs(birth);
  }
  if (record.cmnd_issue_date) {
    const cmnd_issue_date = record.cmnd_issue_date;
    record.cmnd_issue_date = dayjs(cmnd_issue_date);
  }
  if (record.avatar && Array.isArray(record.avatar)) {
    record.avatar = record.avatar && record.avatar.length > 0
      ? record.avatar.filter(i => i && i !== '').map((i, idx) => ({
        uid: `${record.id}${idx + 1}`,
        name: `${record.name}-${idx}` || '',
        status: 'done',
        url: i,
      }))
      : [];
  } else if (record.avatar && typeof record.avatar === 'string') {
    record.avatar = [{
      uid: `${record.id}`,
      name: `${record.name}` || '',
      status: 'done',
      url: record.avatar,
    }];
  }
  if (record.isApprove) {
    record.isApprove = `${record.isApprove}`;
  }

  return record;
};

export default {
  namespace: `${RESOURCE}`,

  state: {
    data: {
      list: [],
      pagination: {
        total: 0,
        totalPages: 0,
        page: 1,
        pageSize: parseInt(process.env.PAGE_SIZE, 10)
      },
    },
    submitting: false,
    formTitle: '',
    formID: '',
    formVisible: false,
    formData: {},
  },

  effects: {
    *fetch({ payload, callback }, { call, put }) {
      const pageSize = get(payload, ["pageSize"], parseInt(process.env.PAGE_SIZE || 10, 10));
      let payloadRet = {};
      let list = [];
      try {
        const { status, data = {} } = yield call(userServices.getList, payload);
        if (status == 200) {
          let list = [];
          if (data.data) {
            list = get(data, "data", []).map(item => {
              transfromDocument(item);
              return item;
            });
          }
          payloadRet = {
            list,
            pagination: {
              pageSize,
              total: get(data, "count", 0),
              totalPages: Math.floor((get(data, "count", 0) + pageSize - 1) / pageSize),
              page: get(payload, "page", 1),
              current: get(payload, "current", 1),
              skip: get(payload, "skip", 0),
              limit: get(payload, "limit", pageSize),
            }
          };
          yield put({
            type: 'save',
            payload: payloadRet
          });
        }
      } catch (error) {
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(list);
      }
    },
    *loadForm({ payload, callback }, { put }) {
      yield put({
        type: 'changeFormVisible',
        payload: true,
      });

      yield [
        put({
          type: 'saveFormType',
          payload: payload.type,
        }),
        put({
          type: 'saveFormID',
          payload: '',
        })
      ];
      if (payload.type === 'A') {
        yield [
          put({
            type: 'saveFormTitle',
            payload: 'Thêm mới',
          }),
          put({
            type: 'saveFormData',
            payload: {
              gender: 'Nam',
              experienceYear: 0,
              soldSuccessCount: 0,
              projectJoin: ''
            },
          }),
        ];
      }
      else if (payload.type === 'E') {
        yield [
          put({
            type: 'saveFormTitle',
            payload: `Chỉnh sửa ${payload.id}`,
          }),
          put({
            type: 'saveFormID',
            payload: payload.id,
          }),
          put({
            type: 'fetchForm',
            payload: {
              id: payload.id
            },
          }),
        ];
      }
      if (callback) {
        callback();
      }
    },
    *fetchForm({ payload, callback }, { call, put }) {
      let response;
      let record;
      try {
        response = yield call(userServices.get, {
          queryInput: {
            id: payload.id
          }
        }, payload);

        if (response && response.status == 200 && response.data) {
          record = transfromDocument(response.data.data[0]);
        }
        yield [
          put({
            type: 'saveFormData',
            payload: record,
          }),
        ];
      } catch (error) {
        console.log('*fetch -> error', error);
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(record);
      }
    },
    *submit({ payload, callback }, { call, put, select }) {
      yield put({
        type: 'changeSubmitting',
        payload: true,
      });

      const params = { ...payload };
      const formType = yield select(state => state[`${RESOURCE}`].formType);

      let success = false;
      let response;
      let errorObj;
      try {
        if (formType === 'E') {
          const id = yield select(state => state[`${RESOURCE}`].formID);
          if (!params.id) {
            params.id = id;
          }
          response = yield call(userServices.update, id, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        } else {
          response = yield call(userServices.create, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        }
      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
      }

      yield put({
        type: 'changeSubmitting',
        payload: false,
      });

      if (success) {
        message.success('Thành công');
        yield put({
          type: 'changeFormVisible',
          payload: false,
        });
        // yield put({
        //   type: 'fetch',
        // });
        if (callback) {
          callback(response);
        }
      } else {
        message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    },
    *updateState({ payload, callback }, { call, put, select }) {
      const params = { ...payload };
      try {
        const response = yield call(userServices.approveUser, params);
        console.log(response);
        if (response.status === 200) {
          message.success(response.data.message);
        } else {
          message.error(response.data.message);
        }
      } catch (error) { }

      callback();
    },
    *importUser({ payload, callback }, { call, put, select }) {
      const params = { ...payload };
      // let id = params.id;
      delete params.id;
      try {
        const response = yield call(userServices.importData, params);
        if (response.code === 0) {
          callback(response);
          // message.success("Nhập dữ liệu người dùng thành công");
        } else {
          message.error("Nhập dữ liệu người dùng thất bại");
        }
      } catch (error) { }
    },
    *changePass({ payload, callback }, { call, put, select }) {
      const params = { ...payload };
      try {
        const response = yield call(userServices.changePass, params);
        if (response.status === 200) {
          message.success("Đổi mật khẩu người dùng thành công");
        } else {
          message.error("Đổi mật khẩu người dùng thất bại");
        }
      } catch (error) { }
    },
    *updateMultiApprove({ payload, callback }, { call, put, select, all }) {
      let success = false;
      let params = { ...payload };
      let response;
      let promise = [];
      try {
        (params.id || []).map(id => {
          // promise.push(call(userServices.update, id, { isApprove: params.isApprove }));
          promise.push(call(userServices.approveUser, { userId: id, check: params.isApprove }));
        });
        // eslint-disable-next-line no-unused-vars
        response = yield all(promise);
        success = true;
      } catch (error) {
        success = false;
      }

      if (success) {
        message.success('Thành công');
        // yield put({
        //   type: 'fetch',
        // });
        if (callback) {
          callback({
            code: 0
          });
        }
      } else {
        message.error('Cập nhật thất bại');
        if (callback) {
          callback({ code: 1, error: {
            message: 'Cập nhật thất bại'
          } });
        }
      }

    },

  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          ...state.data,
          list: action.payload.list,
          pagination: action.payload.pagination
        },
      };
    },
    changeFormVisible(state, { payload }) {
      return { ...state, formVisible: payload };
    },
    saveFormTitle(state, { payload }) {
      return { ...state, formTitle: payload };
    },
    saveFormType(state, { payload }) {
      return { ...state, formType: payload };
    },
    saveFormID(state, { payload }) {
      return { ...state, formID: payload };
    },
    saveFormData(state, { payload }) {
      return { ...state, formData: payload };
    },
    changeSubmitting(state, { payload }) {
      return { ...state, submitting: payload };
    },
  },

  subscription: {

  }
};
