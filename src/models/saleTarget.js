import dayjs from 'dayjs';
import { message } from 'antd';
import get from 'lodash/get';
import pick from 'lodash/pick';
import groupBy from 'lodash/groupBy';
import forIn from 'lodash/forIn';
import * as saleTargetServices from 'services/saleTarget';

const RESOURCE = 'saleTarget';
const transfromDocument = (record) => {

  if (record.createdAt) {
    const createdAt = record.createdAt;
    record.createdAt = dayjs(createdAt);
  }
  if (record.updatedAt) {
    const updatedAt = record.updatedAt;
    record.updatedAt = dayjs(updatedAt);
  }
  if (record.isActive) {
    record.isActive = `${record.isActive}`;
  }

  return record;
};

const transformReport = (data) => {
  const dataGroup = groupBy(data, function (item) {
    return item.name;
  });
  console.log("$$dataGroup",dataGroup)
  let datafinal = [];
  forIn(dataGroup, function (value, key) {
    const chitieuobj = value.reduce((acc, cur) => {
      const dataobj = pick(cur, ["group", "name","week","month","year"]);
      return {
        ...acc,
        ...dataobj,
        [`${cur.year}_${cur.month}_${cur.week}`]:{week:cur.week,month:cur.month,year:cur.year,id:cur.id,amount:cur.amount}
      };
    }, {});
    datafinal.push({
      key: key,
      ...chitieuobj
    });
    console.log("$$dataFinal",datafinal)
  });

  const dataYm = groupBy(data, function (item) {
    return `${item.month}/${item.year}`;
  });
  let dataCol = [];
  forIn(dataYm, function (value, key) {
    const colobj = groupBy(value, (item) => {
      return `${item.year}_${item.month}_${item.week}`;
    });
    let colChild = [];
    forIn(colobj, function (value, key) {
      colChild.push({
        title: key.split('_')[key.split('_').length - 1],
        key: key,
        dataIndex: key
      });
    });
    dataCol.push({
      title: `Tháng ${key}`,
      children: colChild
    });
  });
  return {
    data: datafinal,
    columns: dataCol
  };
};

export default {
  namespace: `${RESOURCE}`,

  state: {
    data: {
      list: [],
      pagination: {
        total: 0,
        totalPages: 0,
        page: 1,
        current: 1,
        pageSize: parseInt(process.env.PAGE_SIZE || 10, 10)
      },
    },
    submitting: false,
    formTitle: '',
    formID: '',
    formVisible: false,
    formData: {},
    report: {
      data: [],
      columns: []
    }
  },

  effects: {
    *fetch({ payload, callback }, { call, put }) {
      const params = { ...payload };
      let success = false;
      let response;
      let errorObj;
      try {
        response = yield call(saleTargetServices.getList, params);
        if (response && response.status == 200 && response.data) {
          success = true;
          yield put({
            type: 'saveReport',
            payload: transformReport(get(response, "data.data", []) || [])
          });
        }
      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
        message.error(error.message);
        // throw error;
      }
      if (success) {
        if (callback) {
          callback(response);
        }
      } else {
        // message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    },
    *loadForm({ payload, callback }, { put, all }) {
      yield put({
        type: 'changeFormVisible',
        payload: true,
      });

      yield all([
        put({
          type: 'saveFormType',
          payload: payload.type,
        }),
        put({
          type: 'saveFormID',
          payload: '',
        })
      ]);
      if (payload.type === 'A') {
        yield all([
          put({
            type: 'saveFormTitle',
            payload: 'Thêm mới',
          }),
          put({
            type: 'saveFormData',
            payload: {
              status: "true"
            },
          }),
        ]);
      }
      else if (payload.type === 'E') {
        yield all([
          put({
            type: 'saveFormTitle',
            payload: `Chỉnh sửa ${payload.id}`,
          }),
          put({
            type: 'saveFormID',
            payload: payload.id,
          }),
          put({
            type: 'fetchForm',
            payload: {
              id: payload.id
            },
          }),
        ]);
      }
      if (callback) {
        callback();
      }
    },
    *fetchForm({ payload, callback }, { call, put }) {
      let response;
      let record;
      try {
        response = yield call(saleTargetServices.get, payload.id, payload);
        if (response && response.data && response.data.data && response.data.data.length > 0) {
          record = transfromDocument(response.data.data[0]);
        }
        yield [
          put({
            type: 'saveFormData',
            payload: record,
          }),
        ];
      } catch (error) {
        console.log('*fetch -> error', error);
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(record);
      }
    },
    *submit({ payload, callback }, { call, put, select }) {
      yield put({
        type: 'changeSubmitting',
        payload: true,
      });

      const params = { ...payload };
      const formType = yield select(state => state[`${RESOURCE}`].formType);

      let success = false;
      let response;
      let errorObj;
      try {
        if (formType === 'E') {
          const id = yield select(state => state[`${RESOURCE}`].formID);
          if (!params.id) {
            params.id = id;
          }
          response = yield call(saleTargetServices.update, id, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        } else {
          response = yield call(saleTargetServices.create, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        }
      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
      }

      yield put({
        type: 'changeSubmitting',
        payload: false,
      });

      if (success) {
        message.success('Thành công');
        yield put({
          type: 'changeFormVisible',
          payload: false,
        });
        // yield put({
        //   type: 'fetch',
        // });
        if (callback) {
          callback(response);
        }
      } else {
        message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    },
    *createMulti({ payload, callback }, { call, put, select }) {
      yield put({
        type: 'changeSubmitting',
        payload: true,
      });

      const params = { ...payload };
      let success = false;
      let response;
      let errorObj;
      try {
        response = yield call(saleTargetServices.createMulti, params);
        if (response && response.status == 200 && response.data) {
          success = true;
        }
      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
      }

      yield put({
        type: 'changeSubmitting',
        payload: false,
      });

      if (success) {
        message.success('Thành công');
        yield put({
          type: 'changeFormVisible',
          payload: false,
        });
        // yield put({
        //   type: 'fetch',
        // });
        if (callback) {
          callback(response);
        }
      } else {
        message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    },
    *report3Months({ payload, callback }, { call, put, select }) {
      const params = { ...payload };
      let success = false;
      let response;
      let errorObj;
      try {
        response = yield call(saleTargetServices.report3Months, params);
        if (response && response.status == 200 && response.data) {
          success = true;
          yield put({
            type: 'saveReport',
            payload: transformReport(get(response, "data.data", []) || [])
          });
        }
      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
      }
      if (success) {
        if (callback) {
          callback(response);
        }
      } else {
        // message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          ...state.data,
          list: action.payload.list,
          pagination: action.payload.pagination
        },
      };
    },
    changeFormVisible(state, { payload }) {
      return { ...state, formVisible: payload };
    },
    saveFormTitle(state, { payload }) {
      return { ...state, formTitle: payload };
    },
    saveFormType(state, { payload }) {
      return { ...state, formType: payload };
    },
    saveFormID(state, { payload }) {
      return { ...state, formID: payload };
    },
    saveFormData(state, { payload }) {
      return { ...state, formData: payload };
    },
    changeSubmitting(state, { payload }) {
      return { ...state, submitting: payload };
    },
    saveReport(state, action) {
      return {
        ...state,
        report: action.payload,
      };
    },
  },

  subscription: {

  }
};
