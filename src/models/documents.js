import dayjs from 'dayjs';
import { message } from 'antd';
import get from 'lodash/get';
import * as documentsServices from 'services/documents';

const RESOURCE = 'documents';
const transfromDocument = (record) => {

  if (record.createdAt) {
    const createdAt = record.createdAt;
    record.createdAt = dayjs(createdAt);
  }
  if (record.updatedAt) {
    const updatedAt = record.updatedAt;
    record.updatedAt = dayjs(updatedAt);
  }
  if (record.isActive) {
    record.isActive = `${record.isActive}`;
  }

  if (record.type === "pdf") {
    if (record.link && Array.isArray(record.link)) {
      record.link = record.link && record.link.length > 0
        ? record.link.filter(i => i && i !== '').map((i, idx) => ({
          uid: `${record.id}${idx + 1}`,
          name: `${record.name}-${idx}` || '',
          status: 'done',
          url: i,
        }))
        : [];
    } else if (record.link && typeof record.link === 'string') {
      record.link = [{
        uid: `${record.id}`,
        name: `${record.name}` || '',
        status: 'done',
        url: record.link,
      }];
    }
  }
  record.type = record.type === "pdf" ? true : false;

  return record;
};

export default {
  namespace: `${RESOURCE}`,

  state: {
    data: {
      list: [],
      pagination: {
        total: 0,
        totalPages: 0,
        page: 1,
        current: 1,
        pageSize: parseInt(process.env.PAGE_SIZE || 10, 10)
      },
    },
    submitting: false,
    formTitle: '',
    formID: '',
    formVisible: false,
    formData: {},
  },

  effects: {
    *fetch({ payload, callback }, { call, put }) {
      const pageSize = get(payload, ["pageSize"], parseInt(process.env.PAGE_SIZE || 10, 10));
      let payloadRet = {};
      let list = [];
      try {
        const { status, data = {} } = yield call(documentsServices.getList, payload);
        if (status == 200) {
          let list = [];
          if (data.data) {
            list = get(data, "data", []).map(item => {
              transfromDocument(item);
              return item;
            });
          }
          payloadRet = {
            list,
            pagination: {
              pageSize,
              total: get(data, "count", 0),
              totalPages: Math.floor((get(data, "count", 0) + pageSize - 1) / pageSize),
              page: get(payload, "page", 1),
              current: get(payload, "current", 1)
            }
          };
          yield put({
            type: 'save',
            payload: payloadRet
          });
        }
      } catch (error) {
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(list);
      }
    },
    *loadForm({ payload, callback }, { put }) {
      yield put({
        type: 'changeFormVisible',
        payload: true,
      });

      yield [
        put({
          type: 'saveFormType',
          payload: payload.type,
        }),
        put({
          type: 'saveFormID',
          payload: '',
        })
      ];
      if (payload.type === 'A') {
        yield [
          put({
            type: 'saveFormTitle',
            payload: 'Thêm mới',
          }),
          put({
            type: 'saveFormData',
            payload: {
              status: "true"
            },
          }),
        ];
      }
      else if (payload.type === 'E') {
        yield [
          put({
            type: 'saveFormTitle',
            payload: `Chỉnh sửa ${payload.id}`,
          }),
          put({
            type: 'saveFormID',
            payload: payload.id,
          }),
          put({
            type: 'fetchForm',
            payload: {
              classLessonId: payload.classLessonId
            },
          }),
        ];
      }
      if (callback) {
        callback();
      }
    },
    *fetchForm({ payload, callback }, { call, put }) {
      let response;
      let record;
      try {
        response = yield call(documentsServices.get, payload.classLessonId, payload);
        if (response && response.data && response.data.data && response.data.data.length > 0) {
          record = transfromDocument(response.data.data[0]);
        }
        yield [
          put({
            type: 'saveFormData',
            payload: record,
          }),
        ];
      } catch (error) {
        console.log('*fetch -> error', error);
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(record);
      }
    },


    *submit({ payload, callback }, { call, put, select }) {

      const params = { ...payload };
      // const formType = params.formType;

      let success = false;
      let response;
      let errorObj;
      try {
        if (params.id) {
          response = yield call(documentsServices.update, params.id, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        }
        else {
          response = yield call(documentsServices.create, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        }

      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
      }
      if (success) {
        // message.success('Thành công');
        if (callback) {
          callback(response);
        }
      } else {
        message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          ...state.data,
          list: action.payload.list,
          pagination: action.payload.pagination
        },
      };
    },
    changeFormVisible(state, { payload }) {
      return { ...state, formVisible: payload };
    },
    saveFormTitle(state, { payload }) {
      return { ...state, formTitle: payload };
    },
    saveFormType(state, { payload }) {
      return { ...state, formType: payload };
    },
    saveFormID(state, { payload }) {
      return { ...state, formID: payload };
    },
    saveFormData(state, { payload }) {
      return { ...state, formData: payload };
    },
    changeSubmitting(state, { payload }) {
      return { ...state, submitting: payload };
    },
  },

  subscription: {

  }
};
