
import { delay } from 'redux-saga/effects';

export default {
  namespace: 'global',

  state: {
    collapsed: true,
    notices: [],
    clock: {
      lastUpdate: null,
      light: false
    }
  },

  effects: {
    *fetchNotices(_, { call, put, select }) {
      /* const data = yield call(queryNotices);
      yield put({
        type: 'saveNotices',
        payload: data,
      });
      const unreadCount = yield select(
        state => state.global.notices.filter(item => !item.read).length
      );
      yield put({
        type: 'user/changeNotifyCount',
        payload: {
          totalCount: data.length,
          unreadCount,
        },
      }); */
    },
    *clearNotices({ payload }, { put, select }) {
      yield put({
        type: 'saveClearedNotices',
        payload,
      });
      const count = yield select(state => state.global.notices.length);
      const unreadCount = yield select(
        state => state.global.notices.filter(item => !item.read).length
      );
      yield put({
        type: 'user/changeNotifyCount',
        payload: {
          totalCount: count,
          unreadCount,
        },
      });
    },
    *changeNoticeReadState({ payload }, { put, select }) {
      const notices = yield select(state =>
        state.global.notices.map(item => {
          const notice = { ...item };
          if (notice.id === payload) {
            notice.read = true;
          }
          return notice;
        })
      );
      yield put({
        type: 'saveNotices',
        payload: notices,
      });
      yield put({
        type: 'user/changeNotifyCount',
        payload: {
          totalCount: notices.length,
          unreadCount: notices.filter(item => !item.read).length,
        },
      });
    },
    *runClockSaga({ payload }, { put, ...rest }) {
      while (true) {
        try {
          yield put({
            type: 'tickClock',
            payload: {
              light: !payload.isServer,
              ts: Date.now()
            }
          });
          yield delay(1000);
        } catch (error) {
          console.log(`🚀 ~ file: global.js ~ line 92 ~ *runClockSaga ~ error`, error);

        }
      }
    }
  },

  reducers: {
    changeLayoutCollapsed(state, { payload }) {
      return {
        ...state,
        collapsed: payload,
      };
    },
    saveNotices(state, { payload }) {
      return {
        ...state,
        notices: payload,
      };
    },
    saveClearedNotices(state, { payload }) {
      return {
        ...state,
        notices: state.notices.filter(item => item.type !== payload),
      };
    },
    tickClock(state, { payload }) {
      console.log(`🚀 ~ file: global.js ~ line 106 ~ tickClock ~ payload`, payload);
      return {
        ...state,
        clock: {
          lastUpdate: payload.ts,
          light: !!payload.light
        }
      };
    }
  },

  subscriptions: {
    // keyEvent({ dispatch }) {
    //   console.log('keyEvent -> dispatch', dispatch);
    // },
    setup({ history, ...rest }) {
      // console.log('setup -> rest', rest);
      console.log('setup -> history', history);
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      return history.listen(({ pathname, search }) => {
        if (typeof window.ga !== 'undefined') {
          window.ga('send', 'pageview', pathname + search);
        }
      });
    },
  },
};
