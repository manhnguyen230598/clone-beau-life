import memoizeOne from 'memoize-one';
import isEqual from 'lodash/isEqual';
import get from 'lodash/get';
import * as menuServices from 'services/menu';

// Conversion router to menu.
function formatter(data, parentAuthority, parentName) {
  return data
    .map(item => {
      /* if (!item.name || !item.path) {
        return null;
      } */

      let locale = 'menu';
      if (parentName) {
        locale = `${parentName}.${item.name}`;
      } else {
        locale = `menu.${item.name}`;
      }

      const result = {
        ...item,
        // name: formatMessage({ id: locale, defaultMessage: item.name }),
        name: item.name,
        locale,
        // authority: item.authority || parentAuthority,
      };
      if (item.children) {
        const children = formatter(item.children, item.authority, locale);
        // console.log('formatter -> children', children);
        // Reduce memory usage
        result.children = children;
      }
      // delete result.children;
      return result;
    })
    .filter(item => item);
}

const memoizeOneFormatter = memoizeOne(formatter, isEqual);

/**
 * get SubMenu or Item
 */
const getSubMenu = item => {
  // doc: add hideChildrenInMenu
  if (item.children && !item.hideChildrenInMenu && item.children.some(child => child.name)) {
    return {
      ...item,
      children: filterMenuData(item.children), // eslint-disable-line
    };
  }
  return item;
};

/**
 * filter menuData
 */
const filterMenuData = menuData => {
  if (!menuData) {
    return [];
  }
  return menuData
    .filter(item => item.name && !item.hideInMenu)
    .map(item => getSubMenu(item))
    .filter(item => item);
};
/**
 * 获取面包屑映射
 * @param {Object} menuData 菜单配置
 */
const getBreadcrumbNameMap = menuData => {
  const routerMap = {};

  const flattenMenuData = data => {
    data.forEach(menuItem => {
      if (menuItem.children) {
        flattenMenuData(menuItem.children);
      }
      // Reduce memory usage
      routerMap[menuItem.key] = menuItem;
    });
  };
  flattenMenuData(menuData);
  return routerMap;
};

const memoizeOneGetBreadcrumbNameMap = memoizeOne(getBreadcrumbNameMap, isEqual);

export default {
  namespace: 'menu',

  state: {
    menus: [],
    breadcrumbNameMap: {},
  },

  effects: {
    *getMenuData({ payload }, { put, call }) {
      try {
        const resMenu = yield call(menuServices.getMenu);
        let menuData = [];
        let routes = [];
        if (resMenu.status == 200) {
          routes = get(resMenu, "data.data", []) || [];
        }
        // const { routes, authority } = payload;
        const menuFormatered = memoizeOneFormatter(routes, ["admin"]);
        menuData = filterMenuData(menuFormatered);
        const breadcrumbNameMap = memoizeOneGetBreadcrumbNameMap(routes);
        yield put({
          type: 'save',
          payload: { menus: menuData, breadcrumbNameMap },
        });
      } catch (error) {
        console.log(`🚀 ~ file: menu.js ~ line 111 ~ *getMenuData ~ error`, error);
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
