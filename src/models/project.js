import dayjs from "dayjs";
import { message } from "antd";
import { get, pick, omit, forEach } from "lodash";
import * as projectServices from "services/project";
import * as documentProjectServices from "services/documentProject";
import { transfromDocument as transfromDocumentProject, transformCreateDocumentProject } from './documentProject';

const RESOURCE = "project";
const transfromDocument = record => {
  if (record.createdAt) {
    const createdAt = record.createdAt;
    record.createdAt = dayjs(createdAt);
  }
  if (record.updatedAt) {
    const updatedAt = record.updatedAt;
    record.updatedAt = dayjs(updatedAt);
  }
  if (record.images && Array.isArray(record.images)) {
    record.images = record.images && record.images.length > 0
      ? record.images.filter(i => i && i !== '').map((i, idx) => ({
        uid: `${record.id}${idx + 1}`,
        name: `${record.name}-${idx}` || '',
        status: 'done',
        url: i,
      }))
      : [];
  } else if (record.images && typeof record.images === 'string') {
    record.images = [{
      uid: `${record.id}`,
      name: `${record.name}` || '',
      status: 'done',
      url: record.images,
    }];
  }
  if (record.thumbnail && Array.isArray(record.thumbnail)) {
    record.thumbnail = record.thumbnail && record.thumbnail.length > 0
      ? record.thumbnail.filter(i => i && i !== '').map((i, idx) => ({
        uid: `${record.id}${idx + 1}`,
        name: `${record.name}-${idx}` || '',
        status: 'done',
        url: i,
      }))
      : [];
  } else if (record.thumbnail && typeof record.thumbnail === 'string') {
    record.thumbnail = [{
      uid: `${record.id}`,
      name: `${record.name}` || '',
      status: 'done',
      url: record.thumbnail,
    }];
  }
  if (record.logoInvestor && Array.isArray(record.logoInvestor)) {
    record.logoInvestor = record.logoInvestor && record.logoInvestor.length > 0
      ? record.logoInvestor.filter(i => i && i !== '').map((i, idx) => ({
        uid: `${record.id}${idx + 1}`,
        name: `${record.name}-${idx}` || '',
        status: 'done',
        url: i,
      }))
      : [];
  } else if (record.logoInvestor && typeof record.logoInvestor === 'string') {
    record.logoInvestor = [{
      uid: `${record.id}`,
      name: `${record.name}` || '',
      status: 'done',
      url: record.logoInvestor,
    }];
  }
  if (record.logoLinkWeb && Array.isArray(record.logoLinkWeb)) {
    record.logoLinkWeb = record.logoLinkWeb && record.logoLinkWeb.length > 0
      ? record.logoLinkWeb.filter(i => i && i !== '').map((i, idx) => ({
        uid: `${record.id}${idx + 1}`,
        name: `${record.name}-${idx}` || '',
        status: 'done',
        url: i,
      }))
      : [];
  } else if (record.logoLinkWeb && typeof record.logoLinkWeb === 'string') {
    record.logoLinkWeb = [{
      uid: `${record.id}`,
      name: `${record.name}` || '',
      status: 'done',
      url: record.logoLinkWeb,
    }];
  }
  if (record.templateCustomerFile && Array.isArray(record.templateCustomerFile)) {
    record.templateCustomerFile = record.templateCustomerFile && record.templateCustomerFile.length > 0
      ? record.templateCustomerFile.filter(i => i && i !== '').map((i, idx) => ({
        uid: `${record.id}${idx + 1}`,
        name: `${record.name}-${idx}` || '',
        status: 'done',
        url: i,
      }))
      : [];
  } else if (record.templateCustomerFile && typeof record.templateCustomerFile === 'string') {
    record.templateCustomerFile = [{
      uid: `${record.id}`,
      name: `Template mẫu` || '',
      status: 'done',
      url: record.templateCustomerFile,
    }];
  }
  return record;
};

export default {
  namespace: `${RESOURCE}`,

  state: {
    data: {
      list: [],
      pagination: {
        total: 0,
        totalPages: 0,
        page: 1,
        pageSize: parseInt(process.env.PAGE_SIZE || 10, 10)
      }
    },
    submitting: false,
    formTitle: "",
    formID: "",
    formVisible: false,
    formData: {}
  },

  effects: {
    *fetch({ payload, callback }, { call, put }) {
      const pageSize = get(
        payload,
        ["pageSize"],
        parseInt(process.env.PAGE_SIZE || 10, 10)
      );
      let payloadRet = {};
      let list = [];
      try {
        const { status, data = {} } = yield call(
          projectServices.getList,
          payload
        );
        if (status == 200) {
          let list = [];
          if (data.data) {
            list = get(data, "data", []).map(item => {
              transfromDocument(item);
              return item;
            });
          }
          payloadRet = {
            list,
            pagination: {
              pageSize,
              total: get(data, "count", 0),
              totalPages: Math.floor(
                (get(data, "count", 0) + pageSize - 1) / pageSize
              ),
              page: get(payload, "page", 1)
            }
          };
          yield put({
            type: "save",
            payload: payloadRet
          });
        }
      } catch (error) {
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(list);
      }
    },
    *loadForm({ payload, callback }, { put, all }) {
      yield put({
        type: "changeFormVisible",
        payload: true
      });

      yield all([
        put({
          type: "saveFormType",
          payload: payload.type
        }),
        put({
          type: "saveFormID",
          payload: ""
        })
      ]);
      if (payload.type === "A") {
        yield all([
          put({
            type: "saveFormTitle",
            payload: "Thêm mới"
          }),
          put({
            type: "saveFormData",
            payload: {
              isActive: "true",
              documents: [{
                "name": "",
                "type": true,
                "link": "",
                "linkPdf": [],
                "linkVideo": "",
                "thumbnail": [],
                "sequence": "",
                "isActive": true,
                "shortDescription": ""
              }]
            }
          })
        ]);
      } else if (payload.type === "E") {
        yield all([
          put({
            type: "saveFormTitle",
            payload: `Chỉnh sửa ${payload.id}`
          }),
          put({
            type: "saveFormID",
            payload: payload.id
          }),
          put({
            type: "fetchForm",
            payload: {
              id: payload.id
            }
          })
        ]);
      }
      if (callback) {
        callback();
      }
    },
    *fetchForm({ payload, callback }, { call, put, all }) {
      let response;
      let record;
      try {
        const responses = yield all([
          call(projectServices.get, payload.id),
          call(documentProjectServices.getList, { queryInput: { projectId: payload.id } })
        ]);
        response = responses[0] || {};
        if (response && response.status == 200 && response.data) {
          record = transfromDocument(response.data.data[0]);
          if (responses[1] && responses[1].status == 200 && responses[1].data) {
            record.documents = responses[1].data.data.map(item => transfromDocumentProject(item));
          }
        }
        yield [
          put({
            type: "saveFormData",
            payload: record
          })
        ];
      } catch (error) {
        console.log("*fetch -> error", error);
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(record);
      }
    },
    *submit({ payload, callback }, { call, put, select, all }) {
      yield put({
        type: "changeSubmitting",
        payload: true
      });

      const params = omit(payload, "documents");
      console.log(`🚀 ~ file: project.js ~ line 218 ~ *submit ~ params`, params);
      const { documents } = pick(payload, "documents");
      console.log(`🚀 ~ file: project.js ~ line 220 ~ *submit ~ documents`, documents);
      const formType = yield select(state => state[`${RESOURCE}`].formType);

      let success = false;
      let response;
      try {
        if (formType === "E") {
          const id = yield select(state => state[`${RESOURCE}`].formID);
          if (!params.id) {
            params.id = id;
          }
          response = yield call(projectServices.update, id, params);
          if (response && response.status == 200 && response.data) {
            success = true;
            if (documents) {
              let promises = [];
              if (Array.isArray(documents) && documents.length) {
                if(documents.length === 1 && !documents[0].name) return
                forEach(documents, (item) => {
                  item.projectId = Number(id);
                  if (!item.id) {
                    promises.push(call(documentProjectServices.create, transformCreateDocumentProject(item)));
                  } else {
                    promises.push(call(documentProjectServices.update, item.id, transformCreateDocumentProject(item)));
                  }
                });
              }
              if (promises.length) {
                const resDocuments = yield all(promises);
                console.log(`🚀 ~ file: project.js ~ line 252 ~ *submit ~ resDocuments`, resDocuments);
              }
            }
          }
        } else {
          response = yield call(projectServices.create, params);
          if (response && response.status == 200 && response.data) {
            success = true;
            if (documents) {
            
              let promises = [];
              if (Array.isArray(documents) && documents.length) {
                if(documents.length === 1 && !documents[0].name) return
                forEach(documents, (item) => {
                  item.projectId = Number(response.data.id);
                  promises.push(call(documentProjectServices.create, transformCreateDocumentProject(item)));
                });
              }
              if (promises.length) {
                const resDocuments = yield all(promises);
                console.log(`🚀 ~ file: project.js ~ line 252 ~ *submit ~ resDocuments`, resDocuments);
              }
            }
          }
        }

      } catch (error) {
        success = false;
        message.error(error.message);
      }

      yield put({
        type: "changeSubmitting",
        payload: false
      });

      if (success) {
        message.success("Thành công");
        yield put({
          type: "changeFormVisible",
          payload: false
        });
        // yield put({
        //   type: 'fetch',
        // });
      }
      if (callback) {
        callback(response);
      }
    },
    *addStackingPlan({ payload, callback }, { call, put, select, all }) {
      let success = false;
      let response;
      try {
        response = yield call(projectServices.addStackingPlan, payload);
        if (response && response.status == 200 && response.data) {
          success = true;
        }
      } catch (error) {
        success = false;
        message.error(error.message);
      }

      yield put({
        type: "changeSubmitting",
        payload: false
      });

      if (success) {
        message.success(get(response, "data.message", "Thêm bảng hàng thành công") || "Thêm bảng hàng thành công");
      }
      if (callback) {
        callback(response);
      }
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          ...state.data,
          list: action.payload.list,
          pagination: action.payload.pagination
        }
      };
    },
    changeFormVisible(state, { payload }) {
      return { ...state, formVisible: payload };
    },
    saveFormTitle(state, { payload }) {
      return { ...state, formTitle: payload };
    },
    saveFormType(state, { payload }) {
      return { ...state, formType: payload };
    },
    saveFormID(state, { payload }) {
      return { ...state, formID: payload };
    },
    saveFormData(state, { payload }) {
      return { ...state, formData: payload };
    },
    changeSubmitting(state, { payload }) {
      return { ...state, submitting: payload };
    }
  },

  subscription: {}
};
