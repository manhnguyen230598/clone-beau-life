
import {
  // LAYOUT_TYPE,
  LAYOUT_TYPE_FULL,
  // NAV_STYLE,
  // NAV_STYLE_FIXED,
  NAV_STYLE_INSIDE_HEADER_HORIZONTAL,
  // THEME_COLOR_SELECTION,
  THEME_COLOR_SELECTION_PRESET,
  // THEME_TYPE,
  THEME_TYPE_SEMI_DARK
} from "../constants/ThemeSetting";

const initialSettings = {
  navCollapsed: true,
  navStyle: NAV_STYLE_INSIDE_HEADER_HORIZONTAL,
  layoutType: LAYOUT_TYPE_FULL,
  themeType: THEME_TYPE_SEMI_DARK,
  colorSelection: THEME_COLOR_SELECTION_PRESET,

  pathname: '',
  width: window.innerWidth,
  isDirectionRTL: false,
  locale: {
    languageId: 'vietnam',
    locale: 'vi',
    name: 'VietNam',
    icon: 'vn'
  },
  query: {}
};

export default {
  namespace: 'settings',
  state: initialSettings,

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    toogleCollapsedNav(state, { payload }) {
      console.log(`🚀 ~ file: settings.js ~ line 45 ~ toogleCollapsedNav ~ payload`, payload);
      return {
        ...state,
        navCollapsed: payload
      };
    },
    windowWidth(state, { payload }) {
      return {
        ...state,
        width: payload
      };
    },
    themeType(state, { payload }) {
      return {
        ...state,
        themeType: payload
      };
    },
    themeColorSelection(state, { payload }) {
      return {
        ...state,
        colorSelection: payload
      };
    },
    navStyle(state, { payload }) {
      return {
        ...state,
        navStyle: payload
      };
    },
    layoutType(state, { payload }) {
      return {
        ...state,
        layoutType: payload
      };
    },
    switchLanguage(state, { payload }) {
      return {
        ...state,
        locale: payload,
      };
    },
    queryAction(state, { payload }) {
      return {
        ...state,
        query: payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch }) {
      dispatch({ type: 'query' });
    },
    setupHistory({ dispatch, history }) {
      history.listen(location => {
        console.log(`🚀 ~ history ~ location`, location);
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
            query: location.query,
            navCollapsed: false
          },
        });
      });
    },
  },

  effects: {
    *query({ payload }, { call, put, select }) {
      console.log(`🚀 ~ file: setting.js ~ line 54 ~ *query ~ payload`, payload);
      yield put({
        type: 'queryAction',
        payload
      });
    }
  }
};
