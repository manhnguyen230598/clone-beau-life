

export default {
  namespace: 'common',

  state: {
    error: "",
    loading: false,
    message: ''
  },

  effects: {

  },

  reducers: {
    fetchStart(state, { payload }) {
      return { ...state, error: '', message: '', loading: true };
    },
    fetchSuccess(state, { payload }) {
      return { ...state, error: '', message: '', loading: false };
    },
    fetchError(state, { payload }) {
      return { ...state, loading: false, error: payload, message: '' };
    },
    showMessage(state, { payload }) {
      return { ...state, error: '', message: payload, loading: false };
    },
    hideMessage(state, { payload }) {
      return { ...state, loading: false, error: '', message: '' };
    },
  },

  subscriptions: {

  },
};
