import local from 'util/local';
import {
  signInUserWithUserPasswordRequest,
  signOutRequest
} from '../services/auth';
// import local from '../utils/local';

export default {
  namespace: 'auth',

  state: {
    loader: false,
    alertMessage: '',
    showMessage: false,
    initURL: '',
    authUser: local.get('user_id'),
    token: local.get('token')
  },

  effects: {
    *userSignIn({ payload }, { call, put }) {
      const { username, password } = payload;
      try {
        const res = yield call(signInUserWithUserPasswordRequest, username, password);
        console.log(`🚀 ~ file: auth.js ~ line 26 ~ *userSignIn ~ res`, res);
        if (res instanceof Error) {
          // message.error(res.message || 'Hệ thống bận, vui lòng quay lại sau!', 30);
          yield put({
            type: 'showAuthMessage',
            payload: res.message || 'Hệ thống bận, vui lòng quay lại sau!',
          });
        } else {
          const signInUser = res.data;
          console.log(`🚀 ~ file: auth.js ~ line 28 ~ *userSignIn ~ signInUser`, signInUser);
          if (signInUser && signInUser.code == 0) {
            yield put({
              type: "menu/getMenuData",
              payload: {}
            });
            localStorage.setItem('user_id', JSON.stringify(signInUser.data.userInfo));
            localStorage.setItem('token', signInUser.data.token);
            yield put({
              type: 'userSignInSuccess',
              payload: signInUser.data
            });
          } else {
            // message.error((signInUser || {}).message || 'Hệ thống bận, vui lòng quay lại sau!', 30);
            yield put({
              type: 'showAuthMessage',
              payload: (signInUser || {}).message || 'Hệ thống bận, vui lòng quay lại sau!',
            });
          }
        }
      } catch (error) {
        console.log(`🚀 ~ file: auth.js ~ line 45 ~ *userSignIn ~ error`, error);
        // message.error(error.message || 'Hệ thống bận, vui lòng quay lại sau!', 30);
        yield put({
          type: 'showAuthMessage',
          payload: error.message || 'Hệ thống bận, vui lòng quay lại sau!'
        });
      }
    },
    *userSignOut({ payload }, { call, put }) {
      try {
        const signOutUser = yield call(signOutRequest);
        if (signOutUser === undefined) {
          localStorage.removeItem('user_id');
          localStorage.removeItem('token');
          yield put({
            type: 'userSignOutSuccess',
            payload: signOutUser
          });
        } else {
          yield put({
            type: 'showAuthMessage',
            payload: signOutUser.message
          });
        }
      } catch (error) {
        yield put({
          type: 'showAuthMessage',
          payload: error
        });
      }
    }
  },

  reducers: {
    setInitUrl(state, action) {
      return {
        ...state,
        initURL: action.payload
      };
    },
    userSignOutSuccess(state, action) {
      return {
        ...state,
        authUser: null,
        token: null,
        initURL: '/',
        loader: false
      };
    },
    showMessage(state, action) {
      return {
        ...state,
        alertMessage: action.payload,
        showMessage: true,
        loader: false
      };
    },
    hideMessage(state, action) {
      return {
        ...state,
        alertMessage: '',
        showMessage: false,
        loader: false
      };
    },
    hideLoader(state, action) {
      return {
        ...state,
        alertMessage: action.payload,
        showMessage: true,
        loader: false
      };
    },
    showAuthLoader(state, action) {
      return {
        ...state,
        loader: true
      };
    },
    hideAuthLoader(state, action) {
      return {
        ...state,
        loader: false
      };
    },
    showAuthMessage(state, action) {
      return {
        ...state,
        alertMessage: action.payload,
        showMessage: true,
        loader: false
      };
    },
    userSignInSuccess(state, action) {
      return {
        ...state,
        loader: false,
        authUser: action.payload.userInfo,
        token: action.payload.token
      };
    },
  },
};
