import dayjs from 'dayjs';
import { message } from 'antd';
import get from 'lodash/get';
import * as classroomServices from 'services/classroom';
import * as classlessonServices from 'services/classlesson';
import * as examinationServices from 'services/examination';
import { tranformClassRoom, tranformClassLesson, tranformClassExam, tranformClassUser } from 'util/helpers';

const RESOURCE = 'classroom';
const transfromDocument = (record) => {

  if (record.createdAt) {
    const createdAt = record.createdAt;
    record.createdAt = dayjs(createdAt);
  }
  if (record.updatedAt) {
    const updatedAt = record.updatedAt;
    record.updatedAt = dayjs(updatedAt);
  }
  if (record.startDate) {
    const startDate = record.startDate;
    record.startDate = dayjs(startDate);
  }
  if (record.endDate) {
    const endDate = record.endDate;
    record.endDate = dayjs(endDate);
  }
  if (record.isActive) {
    record.isActive = `${record.isActive}`;
  }
  if (record.thumbnail && Array.isArray(record.thumbnail)) {
    record.thumbnail = record.thumbnail && record.thumbnail.length > 0
      ? record.thumbnail.filter(i => i && i !== '').map((i, idx) => ({
        uid: `${record.id}${idx + 1}`,
        name: `${record.name}-${idx}` || '',
        status: 'done',
        url: i,
      }))
      : [];
  } else if (record.thumbnail && typeof record.thumbnail === 'string') {
    record.thumbnail = [{
      uid: `${record.id}`,
      name: `${record.name}` || '',
      status: 'done',
      url: record.thumbnail,
    }];
  }


  return record;
};

export default {
  namespace: `${RESOURCE}`,

  state: {
    data: {
      list: [],
      pagination: {
        total: 0,
        totalPages: 0,
        page: 1,
        pageSize: parseInt(process.env.PAGE_SIZE || 10, 10)
      },
    },
    submitting: false,
    formTitle: '',
    formID: '',
    formVisible: false,
    formData: {},
    current: 'classroom',
    step: {
      classroom: {
        isActive: 'true',
        type: 'free',
        isViewAll: true,
        useFor: 'project'
      },
      classlesson: {

      },
      examination: {
        isActive: 'true'
      },
      userclass: {
        type: true
      }
    },
  },

  effects: {
    *fetch({ payload, callback }, { call, put }) {
      console.log(`🚀 ~ file: classroom.js ~ line 43 ~ *fetch ~ payload`, payload);
      const pageSize = get(payload, ["pageSize"], parseInt(process.env.PAGE_SIZE || 10, 10));
      let payloadRet = {};
      let list = [];
      try {
        const { status, data = {} } = yield call(classroomServices.getList, payload);
        if (status == 200) {
          let list = [];
          if (data.data) {
            list = get(data, "data", []).map(item => {
              transfromDocument(item);
              return item;
            });
          }
          payloadRet = {
            list,
            pagination: {
              defaultCurrent: get(payload, "current", 1),
              defaultPageSize: pageSize,
              pageSize,
              total: get(data, "count", 0),
              totalPages: Math.floor((get(data, "count", 0) + pageSize - 1) / pageSize),
              page: get(payload, "page", 1),
              current: get(payload, "current", 1),
              skip: get(payload, "skip", 0),
              limit: get(payload, "limit", pageSize),
            }
          };
          yield put({
            type: 'save',
            payload: payloadRet
          });
        }
      } catch (error) {
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(list);
      }
    },
    *loadForm({ payload, callback }, { put }) {
      console.log(`🚀 ~ file: classroom.js ~ line 80 ~ *loadForm ~ payload`, payload);
      yield put({
        type: 'changeFormVisible',
        payload: true,
      });

      yield [
        put({
          type: 'saveFormType',
          payload: payload.type,
        }),
        put({
          type: 'saveFormID',
          payload: '',
        })
      ];
      if (payload.type === 'A') {
        yield [
          put({
            type: 'saveFormTitle',
            payload: 'Thêm mới',
          }),
          put({
            type: 'saveFormData',
            payload: {
              status: "true"
            },
          }),
        ];
      }
      else if (payload.type === 'E') {
        yield [
          put({
            type: 'saveFormTitle',
            payload: `Chỉnh sửa ${payload.id}`,
          }),
          put({
            type: 'saveFormID',
            payload: payload.id,
          }),
          put({
            type: 'fetchForm',
            payload: {
              id: payload.id
            },
          }),
        ];
      }
      if (callback) {
        callback();
      }
    },
    *fetchForm({ payload, callback }, { call, put }) {
      let response;
      let record;
      try {
        response = yield call(classroomServices.get, payload.id, payload);
        if (response && response.data && response.data.data && response.data.data.length > 0) {
          record = transfromDocument(response.data.data[0]);
          console.log(`🚀 ~ file: classroom.js ~ line 139 ~ *fetchForm ~ record`, record);
        }
        yield [
          put({
            type: 'saveFormData',
            payload: record,
          }),
        ];
      } catch (error) {
        console.log('*fetch -> error', error);
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(record);
      }
    },
    *submit({ payload, callback }, { call, put, select }) {
      yield put({
        type: 'changeSubmitting',
        payload: true,
      });

      const params = { ...payload };
      const formType = yield select(state => state[`${RESOURCE}`].formType);

      let success = false;
      let response;
      let errorObj;
      try {
        if (formType === 'E') {
          const id = yield select(state => state[`${RESOURCE}`].formID);
          if (!params.id) {
            params.id = id;
          }
          response = yield call(classroomServices.update, id, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        } else {
          response = yield call(classroomServices.create, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        }
      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
      }

      yield put({
        type: 'changeSubmitting',
        payload: false,
      });

      if (success) {
        message.success('Thành công');
        yield put({
          type: 'changeFormVisible',
          payload: false,
        });
        // yield put({
        //   type: 'fetch',
        // });
        if (callback) {
          callback(response);
        }
      } else {
        message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    },
    *submitStepForm({ payload, callback }, { call, put, select }) {
      let success = true;
      let response;
      let errorObj;
      let classId;
      try {
        yield put({
          type: 'saveStepFormData',
          payload,
        });
        let { classroom, classlesson, examination, userclass } = yield select(state => state[`${RESOURCE}`].step);
        const resClassRoom = yield call(classroomServices.create, tranformClassRoom(classroom));
        if (resClassRoom && resClassRoom.status == 200) {
          classId = get(resClassRoom, "data.id", null);
          try {
            // eslint-disable-next-line no-unused-vars
            const resLesson = yield call(classlessonServices.createMulti, { data: tranformClassLesson(classlesson, classId) });
          } catch (errLesson) {
            console.log(`🚀 ~ file: classroom.js ~ line 273 ~ *submitStepForm ~ errLesson`, errLesson);
          }
          try {
            // eslint-disable-next-line no-unused-vars
            if (examination.name) {
              const resExam = yield call(examinationServices.createMulti, tranformClassExam(examination, classId));
              console.log(`🚀 ~ file: classroom.js ~ line 294 ~ *submitStepForm ~ resExam`, resExam);
            }
          } catch (errorExam) {
            console.log(`🚀 ~ file: classroom.js ~ line 278 ~ *submitStepForm ~ errorExam`, errorExam);
          }
          try {
            // eslint-disable-next-line no-unused-vars
            const resUserclass = yield call(classroomServices.appendBroker, tranformClassUser(userclass, classId));
          } catch (errorUserClass) {
            console.log(`🚀 ~ file: classroom.js ~ line 284 ~ *submitStepForm ~ errorUserClass`, errorUserClass);
          }

          success = true;
          response = get(resClassRoom, "data", null);
        } else {
          success = false;
          errorObj = {
            message: get(resClassRoom, "data.message", "Đã có lỗi xảy ra")
          };
        }
      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
      }

      if (success) {
        // message.success('Thành công');
        yield [
          put({
            type: 'saveStepFormData',
            payload: {
              classId
            },
          }),
          put({
            type: 'saveCurrentStep',
            payload: 'result',
          })
        ];
        if (callback) {
          callback(response);
        }
      } else {
        message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          ...state.data,
          list: action.payload.list,
          pagination: action.payload.pagination
        },
      };
    },
    changeFormVisible(state, { payload }) {
      return { ...state, formVisible: payload };
    },
    saveFormTitle(state, { payload }) {
      return { ...state, formTitle: payload };
    },
    saveFormType(state, { payload }) {
      return { ...state, formType: payload };
    },
    saveFormID(state, { payload }) {
      return { ...state, formID: payload };
    },
    saveFormData(state, { payload }) {
      return { ...state, formData: payload };
    },
    changeSubmitting(state, { payload }) {
      return { ...state, submitting: payload };
    },
    saveCurrentStep(state, { payload }) {
      return {
        ...state,
        current: payload,
      };
    },
    saveStepFormData(state, { payload }) {
      return {
        ...state,
        step: {
          ...state.step,
          ...payload,
        },
      };
    },
  },

  subscription: {

  }
};
