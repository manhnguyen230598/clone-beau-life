import dayjs from "dayjs";
import { message } from "antd";
import get from "lodash/get";
import difference from "lodash/difference";
import * as bookApartmentServices from "services/bookapartment";

const RESOURCE = "bookapartment";

// const ListHideApartment = (apartments, rows, columns) => {
//   let listMaxApartment = [];
//   let listCurrentApartment = [];
//   let result = [];
//   rows.map(row => {
//     columns.map(column => {
//       listMaxApartment.push(`${row}-${column}`);
//     });
//   });
//   apartments.map(apartment => {
//     listCurrentApartment.push(
//       `${apartment.floorCode}-${apartment.apartmentCode}`
//     );
//   });
//   let diff = difference(listMaxApartment, listCurrentApartment);
//   console.log(
//     "🚀 ~ file: bookapartment.js ~ line 23 ~ ListHideApartment ~ diff",
//     diff
//   );
//   diff.map(item => {
//     const split = item.split("-");
//     result.push({
//       floorCode: split[0],
//       apartmentCode: split[1],
//       statusSale: "HIDE",
//       shortCode: "hide"
//     });
//   });

//   console.log("$$my result", result);
//   return result;
// };
const transfromDocument = record => {
  if (record.createdAt) {
    const createdAt = record.createdAt;
    record.createdAt = dayjs(createdAt);
  }
  if (record.updatedAt) {
    const updatedAt = record.updatedAt;
    record.updatedAt = dayjs(updatedAt);
  }
  if (record.isActive) {
    record.isActive = `${record.isActive}`;
  }
  if (record.isView === false) {
    record.statusSale = "HIDE";
  }

  return record;
};

export default {
  namespace: `${RESOURCE}`,

  state: {
    data: {
      list: [],
      pagination: {
        total: 0,
        totalPages: 0,
        page: 1,
        current: 1,
        pageSize: parseInt(process.env.PAGE_SIZE || 10, 10)
      }
    },
    submitting: false,
    formTitle: "",
    formID: "",
    formVisible: false,
    formData: {}
  },

  effects: {
    *fetch({ payload, callback }, { call, put }) {
      const pageSize = get(
        payload,
        ["pageSize"],
        parseInt(process.env.PAGE_SIZE || 10, 10)
      );
      let payloadRet = {};
      let list = [];
      let row = [];
      let column = [];
      let projectName;
      let rowInfo = {},
        building = [], colorStatusSale = [],countStatusSale = {};

      try {
        const { status, data = {} } = yield call(
          bookApartmentServices.getList,
          payload
        );

        if (status == 200) {
          let list = [];
          if (data.data) {
            list = get(data, "data", []).map(item => {
              transfromDocument(item);
              return item;
            });
            row = get(data, "row", []);
            column = get(data, "collumn", []);
            projectName = get(data, "projectName", "");
            rowInfo = get(data, "rowInfo", {});
            building = get(data, "building", []);
            colorStatusSale = get(data, "colorStatusSale", []);
            countStatusSale = get(data, "countStatusSale", []);
            // list = [...list, ...ListHideApartment(list, row, column)];
          }
          console.log("$$ref list",list)
          payloadRet = {
            projectName: projectName,
            list,
            row,
            column,
            rowInfo,
            building,
            colorStatusSale,
            countStatusSale,
            pagination: {
              defaultCurrent: get(payload, "current", 1),
              defaultPageSize: pageSize,
              pageSize,
              page: get(payload, "page", 1),
              current: get(payload, "current", 1),
              skip: get(payload, "skip", 0),
              limit: get(payload, "limit", pageSize),
              total: get(data, "count", 0),
              totalPages: Math.floor(
                (get(data, "count", 0) + pageSize - 1) / pageSize
              )
            }
          };
          console.log(
            "🚀 ~ file: bookapartment.js ~ line 65 ~ *fetch ~ payloadRet",
            payloadRet
          );
          yield put({
            type: "save",
            payload: payloadRet
          });
        }
      } catch (error) {
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(list);
      }
    },
    *loadForm({ payload, callback }, { put }) {
      yield put({
        type: "changeFormVisible",
        payload: true
      });

      yield [
        put({
          type: "saveFormType",
          payload: payload.type
        }),
        put({
          type: "saveFormID",
          payload: ""
        })
      ];
      if (payload.type === "A") {
        yield [
          put({
            type: "saveFormTitle",
            payload: "Thêm mới"
          }),
          put({
            type: "saveFormData",
            payload: {
              status: "true"
            }
          })
        ];
      } else if (payload.type === "E") {
        yield [
          put({
            type: "saveFormTitle",
            payload: `Chỉnh sửa ${payload.id}`
          }),
          put({
            type: "saveFormID",
            payload: payload.id
          }),
          put({
            type: "fetchForm",
            payload: {
              id: payload.id
            }
          })
        ];
      }
      if (callback) {
        callback();
      }
    },
    *book({ payload, callback }, { call, put }) {
      let response;
      try {
        const params = { ...payload };
        response = yield call(bookApartmentServices.book, params);
        if (response && response.status == 200 && response.data) {
          // success = true;
        }
      } catch (error) {
        console.log(error);
        return error;
      }

      if (callback) {
        callback(response);
      }
    },
    *fetchForm({ payload, callback }, { call, put }) {
      let response;
      let record;
      try {
        response = yield call(bookApartmentServices.get, payload);
        console.log(
          "🚀 ~ file: bookapartment.js ~ line 168 ~ *fetchForm ~ response",
          response
        );

        if (response && response.data && response.data.data) {
          record = response.data;
          console.log(
            "🚀 ~ file: bookapartment.js ~ line 172 ~ *fetchForm ~ record",
            record
          );
        }
        yield [
          put({
            type: "saveFormData",
            payload: record
          })
        ];
      } catch (error) {
        console.log("*fetch -> error", error);
        message.error(error.message);
        // throw error;
      }
      if (callback) {
        callback(record);
      }
    },
    *submit({ payload, callback }, { call, put, select }) {
      yield put({
        type: "changeSubmitting",
        payload: true
      });

      const params = { ...payload };
      const formType = yield select(state => state[`${RESOURCE}`].formType);

      let success = false;
      let response;
      let errorObj;
      try {
        if (formType === "E") {
          const id = yield select(state => state[`${RESOURCE}`].formID);
          if (!params.id) {
            params.id = id;
          }
          response = yield call(bookApartmentServices.update, id, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        } else {
          response = yield call(bookApartmentServices.create, params);
          if (response && response.status == 200 && response.data) {
            success = true;
          }
        }
      } catch (error) {
        success = false;
        errorObj = {
          message: error.message
        };
      }

      yield put({
        type: "changeSubmitting",
        payload: false
      });

      if (success) {
        message.success("Thành công");
        yield put({
          type: "changeFormVisible",
          payload: false
        });
        // yield put({
        //   type: 'fetch',
        // });
        if (callback) {
          callback(response);
        }
      } else {
        message.error(errorObj.message, 10);
        if (callback) {
          callback({ error: errorObj });
        }
      }
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: {
          ...state.data,
          row: action.payload.row,
          column: action.payload.column,
          list: action.payload.list,
          pagination: action.payload.pagination,
          projectName: action.payload.projectName,
          rowInfo: action.payload.rowInfo,
          building: action.payload.building,
          colorStatusSale : action.payload.colorStatusSale,
          countStatusSale :  action.payload.countStatusSale,
        }
      };
    },
    changeFormVisible(state, { payload }) {
      return { ...state, formVisible: payload };
    },
    saveFormTitle(state, { payload }) {
      return { ...state, formTitle: payload };
    },
    saveFormType(state, { payload }) {
      return { ...state, formType: payload };
    },
    saveFormID(state, { payload }) {
      return { ...state, formID: payload };
    },
    saveFormData(state, { payload }) {
      return { ...state, formData: payload };
    },
    changeSubmitting(state, { payload }) {
      return { ...state, submitting: payload };
    }
  },

  subscription: {}
};
