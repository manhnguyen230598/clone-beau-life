/* eslint-disable array-callback-return */
import React, { useCallback, useEffect, useState } from 'react';
import { ComposedChart, Line, Bar, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { BarChart } from 'recharts';
import { PieChart, Pie, Sector, Cell } from 'recharts';
import { Row, Card, Form, Button } from 'antd';
import { Col } from "antd";
import Widget from 'src/components/Widget';
import * as projectServices from 'src/services/project';
import ProjectSelect from "src/components/Select/Project/ListProject";
const MediaCustomer = (props) => {
  const [form] = Form.useForm();
  const [pieMedia, setPieMedia] = useState([]);
  const [activeIndex, setActiveIndex] = useState(0);
  const [projectId, setProjectId] = useState(1);
  form.setFieldsValue({projectId : projectId})
  const onPieEnter = useCallback(
    (_, index) => {
      setActiveIndex(index);
    },
    [setActiveIndex]
  );
  const onSubmitImport = value => {
    if(typeof value.projectId === "object"){
      setProjectId(value.projectId.id)
    }else{
      setProjectId(value.projectId)
    }
    
  };
  const getDataMeida = async () => {
    let api = await projectServices.reportMedia(projectId)
    setPieMedia(api.data.data)
  }
  useEffect(() => {
    getDataMeida();
  },[])
  useEffect(() => {
    getDataMeida();
  },[projectId])

  //---------------------------
  const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];
  const RADIAN = Math.PI / 180;

  const renderCustomizedLabel1 = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };
  const renderActiveShape = (props) => {

    const {
      cx,
      cy,
      midAngle,
      innerRadius,
      outerRadius,
      startAngle,
      endAngle,
      fill,
      percent,
      value
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? "start" : "end";

    return (
      <>
       
        <g>
          {/* <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
          {payload.name}
        </text> */}
          <Sector
            cx={cx}
            cy={cy}
            innerRadius={innerRadius}
            outerRadius={outerRadius}
            startAngle={startAngle}
            endAngle={endAngle}
            fill={fill}
          />
          <Sector
            cx={cx}
            cy={cy}
            startAngle={startAngle}
            endAngle={endAngle}
            innerRadius={outerRadius + 6}
            outerRadius={outerRadius + 10}
            fill={fill}
          />
          <path
            d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
            stroke={fill}
            fill="none"
          />

          <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
          <text
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            textAnchor={textAnchor}
            fill="#333"
          >{`SL ${value}`}</text>
          <text
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            dy={18}
            textAnchor={textAnchor}
            fill="#999"
          >
            {`(Rate ${(percent * 100).toFixed(2)}%)`}
          </text>

        </g>
      </>
    );
  };
  
  return (
    <> 
     <Card>
        <Row>
          <Col lg={6} offset={1}>
            <Form form={form} onFinish={onSubmitImport}>
              <Form.Item name="projectId" label={"Dự án"}>
                <ProjectSelect mode="radio" />
              </Form.Item>
            </Form>
          </Col>
          <Col lg={5} offset={2}>
            {" "}
            <Button onClick={() => form.submit()}>Báo cáo</Button>
          </Col>
        </Row>
      </Card>    
    <Widget styleName="gx-card-full is-build-chartclass" title="Mục đích mua căn">
    <Row>
      <Col xl={12} lg={12} md={12} sm={12} xs={24}>
          <ResponsiveContainer width="100%" height={300}>
            <PieChart width={600} height={400}>
              <Pie
                activeIndex={activeIndex}
                activeShape={renderActiveShape}
                data={pieMedia ? pieMedia.pieNeed : null}
                cx={"50%"}
                cy={"50%"}
                labelLine={false}
                label={renderCustomizedLabel1}
                outerRadius={80}
                fill="#8884d8"
                dataKey="value"
                onMouseEnter={onPieEnter}
              >
                {pieMedia ? (pieMedia.pieNeed || []).map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                )) : null}
              </Pie>
              <Legend/>
            </PieChart>
          </ResponsiveContainer>
        </Col>
        <Col xl={4} lg={4} md={4} sm={4} xs={8}>
          {pieMedia ? (pieMedia.pieNeed || []).map((item, index) => {
            return (
              <>
                <div className="nv-info-classroom">
                  <span key={index}>{item.name}: </span>
                  <span key={index}>{item.value} Lượt</span>
                </div>
              </>
            );
          }) : null}
        </Col>
        
      </Row>
  </Widget>
  <Widget styleName="gx-card-full is-build-chartclass" title="Khách hàng lo lắng">
    <Row>
        
    <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <ResponsiveContainer width="100%" height={300}>
              <PieChart width={"100%"} height={"100%"}>
                <Pie
                  activeIndex={activeIndex}
                  activeShape={renderActiveShape}
                  data={pieMedia ? pieMedia.pieGroupType : null}
                  cx={240}
                  cy={150}
                  labelLine={false}
                  label={renderCustomizedLabel1}
                  outerRadius={80}
                  fill="#8884d8"
                  dataKey="value"
                  onMouseEnter={onPieEnter}
                >
                  {pieMedia ? (pieMedia.pieGroupType || []).map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  )) : null}
                </Pie>
                <Legend/>
              </PieChart>
            </ResponsiveContainer>
          </Col>
          <Col xl={4} lg={4} md={4} sm={4} xs={8}>
            {pieMedia ? (pieMedia.pieGroupType || []).map((item, index) => {
              return (
                <>
                  <div className="nv-info-classroom">
                    <span key={index}>{item.name}: </span>
                    <span key={index}>{item.value} Lượt</span>
                  </div>
                </>
              );
            }) : null}
          </Col>
      </Row>
  </Widget>
  {/* <Widget styleName="gx-card-full is-build-chartclass">
      <Row>
          
        </Row>
    </Widget> */}
    <Widget styleName="gx-card-full is-build-chartclass" title="Sản phẩm quan tâm">
      <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <ResponsiveContainer width="100%" height={300}>
              <PieChart width={600} height={400}>
                <Pie
                  activeIndex={activeIndex}
                  activeShape={renderActiveShape}
                  data={pieMedia ? pieMedia.pieProduct : null}
                  cx={"50%"}
                  cy={"50%"}
                  labelLine={false}
                  label={renderCustomizedLabel1}
                  outerRadius={80}
                  fill="#8884d8"
                  dataKey="value"
                  onMouseEnter={onPieEnter}
                >
                  {pieMedia ? (pieMedia.pieProduct || []).map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  )) : null}
                </Pie>
                <Legend/>
              </PieChart>
            </ResponsiveContainer>
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            {pieMedia ? (pieMedia.pieProduct || []).map((item, index) => {
              return (
                <>
                  <div className="nv-info-classroom">
                    <span key={index}>{item.name}: </span>
                    <span key={index}>{item.value} Lượt</span>
                  </div>
                </>
              );
            }) : null}
          </Col>
        </Row>
    </Widget>
  </>
  );
};

export default MediaCustomer;
