import React from "react";
import { Card, Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { router } from "dva";
import asyncComponent from "util/asyncComponent";
const { Redirect, Switch, Route } = router;

const TabPane = Tabs.TabPane;
const ProjectDetailRoute = ({
  component: Component,
  projectId,
  templateCustomerFile,
  projectType,
  projectName,
  ...rest
}) => <Route {...rest} render={props => <Component {...props} />} />;
const Booking = ({ match, location, history, ...rest }) => {
  const getTabKey = () => {
    const url = match.url === "/" ? "" : match.url;
    const tabKey = location.pathname.replace(`${url}`, "");
    if (tabKey && tabKey !== "") {
      const tkey = tabKey.replace(/\//g, "");
      return tkey;
    }
    return "customProject";
  };
  function callback(key) {
    switch (key) {
        case 'customProject':
            history.push({ pathname: `${match.url}/customProject` });
            break;
          case 'chartProject':
            history.push({ pathname: `${match.url}/chartProject` });
            break;
          case 'mediaCustomer':
            history.push({ pathname: `${match.url}/mediaCustomer` });
            break;
          case 'VPBHCustomer':
            history.push({ pathname: `${match.url}/VPBHCustomer` });
            break;

      default:
        break;
    }
  }
  const tabList = [
    {
        key: 'customProject',
        tab: <IntlMessages id="customProject.customProject" />,
      },
      {
        key: 'chartProject',
        tab: <IntlMessages id="customProject.chartProject" />,
      },
      {
        key: 'mediaCustomer',
        tab: <IntlMessages id="customProject.mediaCustomer" />,
      },
      {
        key: 'VPBHCustomer',
        tab: <IntlMessages id="customProject.VPBHCustomer" />,
      },
  ];
  return (
    <Card>
      <Tabs
        // defaultActiveKey="1"
        onChange={callback}
        activeKey={getTabKey()}
      >
        {tabList.map((item, index) => (
          <TabPane {...item} tab={item.tab} key={item.key || index} />
        ))}
      </Tabs>
      <Switch>
        <Redirect
          exact
          from={`${match.url}/`}
          to={`${match.url}/${getTabKey()}`}
        />
        <ProjectDetailRoute path={`${match.url}/customProject`} component={asyncComponent(() => import("./customProject/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/chartProject`} component={asyncComponent(() => import("./chartProject/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/mediaCustomer`} component={asyncComponent(() => import("./mediaCustomer/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/VPBHCustomer`} component={asyncComponent(() => import("./VPBHCustomer/list"))} exact={true} />
        
      </Switch>
    </Card>
  );
};

export default Booking;
