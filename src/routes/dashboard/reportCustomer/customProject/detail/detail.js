// import React, { useEffect, useState } from "react";
// import { Card, Tabs } from "antd";
// import { router } from 'dva';
// import get from 'lodash/get';
// import asyncComponent from "util/asyncComponent";
// import IntlMessages from "util/IntlMessages";
// import * as projectServices from 'services/project';

// const TabPane = Tabs.TabPane;
// const { Redirect, Switch, Route } = router;
// const ProjectDetailRoute = ({ component: Component, projectId, ...rest }) =>
//   <Route
//     {...rest}
//     render={props => <Component {...props} projectId={Number(projectId)} />}
//   />;

// const ProjectInSide = ({ match, location, history, ...rest }) => {
//   const [projectInfo, setProjectInfo] = useState(null);
//   useEffect(() => {
//     projectServices.get(Number(match.params.id), {}, false).then(res => {
//       setProjectInfo(get(res, "data.data[0]", {}));
//     });
//   }, [match.params.id]);

//   if (!projectInfo){
//     return null;
//   }

//   function callback(key) {
//     switch (key) {
//       case 'customProject':
//         history.push({ pathname: `${match.url}/customProject` });
//         break;
//       case 'chartProject':
//         history.push({ pathname: `${match.url}/chartProject` });
//         break;
//       default:
//         break;
//     }
//   }

//   const tabList = [
//     {
//       key: 'customProject',
//       tab: <IntlMessages id="customProject.customProject" />,
//     },
//     {
//       key: 'chartProject',
//       tab: <IntlMessages id="customProject.chartProject" />,
//     },
//   ];

//   const getTabKey = () => {
//     const url = match.url === '/' ? '' : match.url;
//     const tabKey = location.pathname.replace(`${url}`, '');
//     if (tabKey && tabKey !== '') {
//       return tabKey.replace(/\//g, "");
//     }
//     return 'customProject';
//   };

//   return (
//     <Card className="gx-card" title={`Mã dự án ${match.params.id} - ${projectInfo.name || ""} `}>
//       <Tabs
//         // defaultActiveKey="1"
//         onChange={callback}
//         activeKey={getTabKey()}
//       >
//         {tabList.map((item, index) => (
//           <TabPane {...item} tab={item.tab} key={item.key || index} />
//         ))}
//       </Tabs>
//       <Switch>
//         <Redirect exact from={`${match.url}/`} to={`${match.url}/${getTabKey()}`} />

//         {/* <ProjectDetailRoute path={`${match.url}/customProject`} projectId={match.params.id} component={asyncComponent(() => import("../../customProject/list"))} exact={true} /> */}
//         <ProjectDetailRoute path={`${match.url}/chartProject`} projectId={match.params.id} component={asyncComponent(() => import("../../chartProject/list"))} exact={true} />
//       </Switch>
//     </Card>
//   );
// };

// export default ProjectInSide;
