import React, { useEffect, useState } from "react";
import { Col, Row } from "antd";
import { connect } from "dva";
import {
  ResponsiveContainer,
  Tooltip,
  PieChart,
  Pie,
  Cell,
  Area,
  AreaChart,
  Legend
} from "recharts";
import { increamentData } from "../dataMetric";
// import Portfolio from "components/dashboard/Crypto/Portfolio";

// import { Option } from "antd/lib/mentions";
/* import SendMoney from "components/dashboard/Crypto/SendMoney";
import RewardCard from "components/dashboard/Crypto/RewardCard";
import CurrencyCalculator from "components/dashboard/Crypto/CurrencyCalculator";
import CryptoNews from "components/dashboard/Crypto/CryptoNews";
import DownloadMobileApps from "components/dashboard/Crypto/DownloadMobileApps";
import OrderHistory from "components/dashboard/Crypto/OrderHistory"; */
import AgencySelect from "src/components/Select/Project/ListProject";
import ChartCard from "src/components/Metrics/ChartCard";
import Widget from "src/components/Widget";
import BalanceHistory from "src/components/dashboard/Crypto/BalanceHistory";
import BussinessMan from "src/components/dashboard/Crypto/BusinessStaff";
import Auxiliary from "src/util/Auxiliary";
import * as reportclass from "src/services/reportclass";
import {
  PicCenterOutlined,
  PicLeftOutlined,
  PicRightOutlined
} from "@ant-design/icons";
import { wrap } from "module";

const RESOURCE = "reportclass";
const Crypto = props => {
  const {
    dispatch
    // [RESOURCE]: { data },
  } = props;

  const [checkView, setCheckView] = useState("view1");
  const [dataCount, setDataCount] = useState({});
  const [dataProject, setDataProject] = useState({});
  const [dataUser, setDataUser] = useState([]);
  const [dataProjectStaff, setDataProjectStaff] = useState([]);

  const CustomTooltip = ({ active, payload, label }) => {
    console.log(payload, 12345);
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p className="label">{`${payload[0].payload.agencyName} : ${payload[0].value} người`}</p>
          {/* <p className="intro">{getIntroOfPage(label)}</p> */}
          {/* <p className="desc">Anything you want can be displayed here.</p> */}
        </div>
      );
    }

    return null;
  };

  const CustomTooltip1 = ({ active, payload, label }) => {
    console.log(payload, 12345);
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p className="label">{`${payload[0].payload.name} : ${payload[0].value} người`}</p>
          {/* <p className="intro">{getIntroOfPage(label)}</p> */}
          {/* <p className="desc">Anything you want can be displayed here.</p> */}
        </div>
      );
    }

    return null;
  };

  const getDataProject = () => {
    dispatch({
      type: `${RESOURCE}/projectHome`,
      payload: {
        id: 1
      },
      callback: data => {
        setDataProject(data);
      }
    });
  };

  const getDataCount = async () => {
    let apiCount = await reportclass.countHome();
    setDataCount(apiCount.data);
  };

  const getDataUser = async () => {
    let api = await reportclass.reportUser();
    setDataUser(api.data);
  };
  const getDataProjectStaff = async (projectId = 1) => {
    let api = await reportclass.dataProjectStaff({ projectId });
    setDataProjectStaff(api.data);
  };

  useEffect(() => {
    getDataCount();
    getDataProject();
    getDataUser();
  }, []);

  console.log(dataCount.dataProject, 123456);

  function handleChangeProject(value) {
    dispatch({
      type: `${RESOURCE}/projectHome`,
      payload: {
        id: value.id
      },
      callback: data => {
        setDataProject(data);
      }
    });
  }
  // const renderColorfulLegendText = (value, entry) => {
  //   const { color } = entry;
  
  //   return <span style={{ color }}>{value}</span>;
  // };

  // const renderLegend = (props) => {
  //   const { payload } = props;
    
  //   return (
      
  //       <Swiper
  //           spaceBetween={10}
  //           slidesPerView={1}
  //           breakpoints={{
  //             425: {
  //               slidesPerView: 3,
  //             },
  //           }}
  //         >
  //       {
  //         payload.map((entry, index) => (
          
  //           <SwiperSlide key={`item-${index}`}>
  //           <div style={{display:"flex" , alignItems:"center"}}>
  //           <div style={{background:`${entry.color}` , width:"14px" , height:"10px" , marginRight:"5px"}}></div>
  //           <span>{entry.value}</span>
  //           </div>
            
  //           </SwiperSlide>
            
  //         ))
  //       }
  //       </Swiper>
     
  //   );
  // }

  // const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

  // const RADIAN = Math.PI / 180;
  // const renderCustomizedLabel = ({
  //   cx,
  //   cy,
  //   midAngle,
  //   innerRadius,
  //   outerRadius,
  //   percent,
  //   index
  // }) => {
  //   const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  //   const x = cx + radius * Math.cos(-midAngle * RADIAN);
  //   const y = cy + radius * Math.sin(-midAngle * RADIAN);

  //   return (
  //     <text
  //       x={x}
  //       y={y}
  //       fill="white"
  //       textAnchor={x > cx ? "start" : "end"}
  //       dominantBaseline="central"
  //     >
  //       {`${(percent * 100).toFixed(0)}%`}
  //     </text>
  //   );
  // };
  // const style = {
  //   left: 60,
  //   bottom: 25,
  //   lineHeight: 2,
  //   // paddingTop: 5,
  //   // paddingLeft: 5,
  //   width: "200%",
  // };

  // const renderLegend = (props) => {
  //   const { payload } = props;
  //   console.log(payload, 112233)
  //   return (
  //     <ul>
  //       {
  //         payload.map((entry, index) => (
  //           <li key={`item-${index}`}>{entry.payload.agencyName}</li>
  //         ))
  //       }
  //     </ul>
  //   );
  // }

  return (
    <>
      <div className="ant-row-flex gx-px-4 gx-pt-4">
        <div className="gx-ml-auto">
          <div className="nv-group-view">
            <div className="nv-group-view__icon">
              <PicCenterOutlined
                style={{ color: `${checkView === "view1" ? "blue" : ""}` }}
                onClick={() => setCheckView("view1")}
              />
            </div>
            <div className="nv-group-view__icon">
              <PicLeftOutlined
                style={{ color: `${checkView === "view2" ? "blue" : ""}` }}
                onClick={() => setCheckView("view2")}
              />
            </div>
            <div className="nv-group-view__icon">
              <PicRightOutlined
                style={{ color: `${checkView === "view3" ? "blue" : ""}` }}
                onClick={() => setCheckView("view3")}
              />
            </div>
          </div>
        </div>
      </div>

      <Auxiliary>
        <Row gutter={[24]} className={`nv-chart-homepage ${checkView}`}>
          <Col
            xl={8}
            lg={12}
            md={12}
            sm={12}
            xs={24}
            className="c-max-width is-info-project"
          >
            <div className="nv-list-project">
              {dataCount
                ? (dataCount.dataProject || []).map((item, index) => {
                    return (
                      <>
                        <div className="nv-the-project">
                          <span key={index}>{item.id}.</span>
                          <span key={index}>{item.name}</span>
                        </div>
                      </>
                    );
                  })
                : null}
            </div>
            <div className="nv-hover-info">
              <ChartCard
                prize={dataCount.countProject}
                icon="bitcoin"
                children={
                  <ResponsiveContainer width="100%" height={75}>
                    <AreaChart
                      data={increamentData}
                      margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                    >
                      <defs>
                        <linearGradient id="color3" x1="0" y1="0" x2="1" y2="0">
                          <stop
                            offset="5%"
                            stopColor="#163469"
                            stopOpacity={0.9}
                          />
                          <stop
                            offset="95%"
                            stopColor="#FE9E15"
                            stopOpacity={0.9}
                          />
                        </linearGradient>
                      </defs>
                      <Area
                        dataKey="price"
                        strokeWidth={0}
                        stackId="2"
                        stroke="#4D95F3"
                        fill="url(#color3)"
                        fillOpacity={1}
                      />
                    </AreaChart>
                  </ResponsiveContainer>
                }
                styleName="up"
                desc="Dự án"
              />
            </div>
          </Col>
          <Col xl={8} lg={12} md={12} sm={12} xs={24} className="c-max-width">
            <ChartCard
              prize={dataCount.countClass}
              icon="etherium"
              children={
                <ResponsiveContainer width="100%" height={75}>
                  <AreaChart
                    data={increamentData}
                    margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                  >
                    <defs>
                      <linearGradient id="color4" x1="0" y1="0" x2="1" y2="0">
                        <stop
                          offset="5%"
                          stopColor="#4ECDE4"
                          stopOpacity={0.9}
                        />
                        <stop
                          offset="95%"
                          stopColor="#06BB8A"
                          stopOpacity={0.9}
                        />
                      </linearGradient>
                    </defs>
                    <Area
                      dataKey="price"
                      type="monotone"
                      strokeWidth={0}
                      stackId="2"
                      stroke="#4D95F3"
                      fill="url(#color4)"
                      fillOpacity={1}
                    />
                  </AreaChart>
                </ResponsiveContainer>
              }
              styleName="up"
              desc="Lớp học"
            />
          </Col>
          <Col xl={8} lg={12} md={12} sm={12} xs={24} className="c-max-width">
            <ChartCard
              prize={dataCount.countUser}
              icon="ripple"
              children={
                <ResponsiveContainer width="100%" height={75}>
                  <AreaChart
                    data={increamentData}
                    margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
                  >
                    <defs>
                      <linearGradient id="color5" x1="0" y1="0" x2="0" y2="1">
                        <stop
                          offset="5%"
                          stopColor="#e81a24"
                          stopOpacity={0.8}
                        />
                        <stop
                          offset="95%"
                          stopColor="#FEEADA"
                          stopOpacity={0.8}
                        />
                      </linearGradient>
                    </defs>
                    <Area
                      dataKey="price"
                      strokeWidth={0}
                      stackId="2"
                      stroke="#FEEADA"
                      fill="url(#color5)"
                      fillOpacity={1}
                    />
                  </AreaChart>
                </ResponsiveContainer>
              }
              styleName="down"
              desc="Người dùng"
            />
          </Col>
        </Row>
      </Auxiliary>

      <Auxiliary>
        <Row gutter={[24]} className={`nv-chart-homepage ${checkView}`}></Row>

        <Row gutter={[24]}></Row>
      </Auxiliary>
      <Auxiliary>
        <Row gutter={[24]} className={`nv-chart-homepage ${checkView}`}>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <BalanceHistory />
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <Widget styleName="gx-card-full" title="Người dùng hệ thống">
              <div className="nv-chart-home">
                {/* <Row> */}
                  {/* <Col xl={10} lg={10} md={10} sm={10} xs={24}>
                    <ResponsiveContainer width="100%" height={270}>
                      <PieChart width={400} height={400}>
                        <Pie
                          data={dataUser ? dataUser.data : []}
                          cx={120}
                          cy={100}
                          labelLine={false}
                          label={renderCustomizedLabel}
                          outerRadius={80}
                          fill="#8884d8"
                          dataKey="value"
                        >
                          {dataUser
                            ? (dataUser.data || []).map((entry, index) => (
                                <Cell
                                  key={`cell-${index}`}
                                  fill={COLORS[index % COLORS.length]}
                                />
                              ))
                            : null}
                        </Pie>
                        <Tooltip content={<CustomTooltip1 />} />
                        
                        <Legend content={renderLegend} formatter={renderColorfulLegendText} wrapperStyle={style}/>
                      </PieChart>
                    </ResponsiveContainer>
                  </Col> */}
                  {/* <Col xl={14} lg={14} md={14} sm={14} xs={24}> */}
                    <div className="nv-build-infochart" style={{margin:"0", height:"150px" , overflowY:"scroll"}}>
                      <div className="nv-info-classroom">
                        <span> Tổng số người dùng </span>
                        <span> {dataUser ? dataUser.total : "0"} Người</span>
                      </div>
                      {dataUser
                        ? (dataUser.data || []).map((item, index) => {
                            return (
                              <>
                                <div className="nv-info-classroom nv-width-50">
                                  <span key={index}>{item.name}: </span>
                                  <span key={index}>{item.value} Người</span>
                                </div>
                              </>
                            );
                          })
                        : null}
                    </div>
                  {/* </Col> */}
                {/* </Row> */}
              </div>
            </Widget>
          </Col>
        </Row>

        <Row gutter={[24]}>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <BussinessMan />
          </Col>
        </Row>
      </Auxiliary>
    </>
  );
};
export default connect(({ reportclass, loading }) => {
  const { data: homeProject } = reportclass;
  return {
    homeProject,
    loading: loading.models.projectHome
  };
})(Crypto);
