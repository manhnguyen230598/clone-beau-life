import React, { useState } from "react";
import {
  Card,
  Modal,
  Tabs,
  Row,
  Col,
  Form,
  Select,
  Descriptions,
  DatePicker
} from "antd";
import AgencySelect from "../../../components/Select/Agency/ListAgency";
import CustomerSelect from "../../../components/Select/Customer/ListCustomer";
import DistrictsSelect from "../../../components/Select/Districts";
import WardSelect from "../../../components/Select/Wards";
import ProvincesSelect from "../../../components/Select/Provinces";
import Upload from "../../../components/Upload";
import { FormInputRender } from "packages/pro-table/form";
import { connect } from "dva";
import { useIntl } from "packages/pro-table/component/intlContext";
import _ from "lodash";
import { toDot } from "../../../util/utils";
import moment from "moment";

const { TabPane } = Tabs;
const { Option } = Select;

const fieldLabelsBookApartment = {
  agencyId: "Đơn vị",
  customerId: "Khách hàng",
  moneyCollect: "Tiền thu",
  images: "Giấy tờ liên quan",
  salePolicyId: "Chính sách bán"
};

const fieldLabelsCustomer = {
  name: "Tên khách hàng",

  phone: "Số điện thoại",
  gender: "Giới tính",
  birthYear: "Ngày sinh",
  ageRange: "Khoảng tuổi",

  indentityNumber: "Số CCCD",
  indentityIssueDate: "Ngày cấp CCCD",
  indentityProvince: "Nơi cấp CCCD",
  email: "Email",
  address: "Địa chỉ",

  wardId: "Phường/Xã",
  districtId: "Quận/Huyện",
  provinceId: "Tỉnh/Thành phố"
};
const BookApartmentForm = ({
  formData,
  type,
  isShowModal,
  setIsShowModal,
  dispatch,
  projectId,
  fullCode
}) => {
  const intl = useIntl();
  const [error, setError] = useState([]);
  const [formBook] = Form.useForm();
  const [formAddCustomer] = Form.useForm();
  const [activeKey, setActiveKey] = useState("1");
  const layout = {
    labelCol: { span: 12 },
    wrapperCol: { span: 16 }
  };

  const onFinish = values => {
    setError([]);
    const data = _.cloneDeep(values);
    console.log(
      "🚀 ~ file: bookapartmentForm.js ~ line 51 ~ onFinish ~ data",
      data
    );
    switch (activeKey) {
      case "1":
        if (data.agencyId) {
          data.agencyId = data.agencyId.id;
        }
        if (data.customerIds) {
          data.customerIds = data.customerIds.map(e=>e.id);
        }
        if (data.indentityIssueDate) {
          data.indentityIssueDate = moment(data.indentityIssueDate).format(
            "DD/MM/YYYY"
          );
        }
        if (data.images && Array.isArray(data.images)) {
          data.images = data.images.map(e =>
            e.url ? e.url : e.response.url ? e.response.url : ""
          );
        }
        if (projectId) {
          data.projectId = projectId;
        }
        if (fullCode) {
          data.fullCode = fullCode;
        }

        dispatch({
          type: `bookapartment/book`,
          payload: data,
          callback: res => {
            if (res && res.data.message) {
              Modal.success({
                title: "Thông báo",
                content: <>{res.data.message}</>
              });
            }
            setIsShowModal(false)
            dispatch({
              type: `bookapartment/fetch`,
              payload: {
                projectId: projectId
              }
            });
          }
        });
        break;
      case "2":
        if (data.birthYear) {
          data.birthYear = moment(data.birthYear).valueOf()/10e2;
        }
        if (data.wardId) {
          data.wardId = data.wardId.key;
        }
        if (data.districtId) {
          data.districtId = data.districtId.key;
        }
        if (data.provinceId) {
          data.provinceId = data.provinceId.key;
        }
        dispatch({
          type: `customer/submit`,
          payload: data,
          callback: res => {
            if (res && res.data.message) {
              alert(res.data.message);
            }
            if (res && !res.data.message) {
              alert("Thêm thành công");
            }
          }
        });
        break;
      default:
        break;
    }
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };

  // const getErrorInfo = (errors) => {
  //     const errorCount = errors.filter((item) => item.errors.length > 0).length;
  //     if (!errors || errorCount === 0) {
  //         return null;
  //     }
  //     const scrollToField = (fieldKey) => {
  //         const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
  //         if (labelNode) {
  //             labelNode.scrollIntoView(true);
  //         }
  //     };
  //     const errorList = errors.map((err) => {
  //         if (!err || err.errors.length === 0) {
  //             return null;
  //         }
  //         const key = err.name[0];
  //         switch (activeKey) {
  //             case "1":
  //                 return (
  //                     <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
  //                         <CloseCircleOutlined className="nv-error-icon" />
  //                         <div className="nv-error-message">{err.errors[0]}</div>
  //                         <div className="nv-error-field">{fieldLabelsBookApartment[key]}</div>
  //                     </li>
  //                 );
  //             case "2":
  //                 return (
  //                     <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
  //                         <CloseCircleOutlined className="nv-error-icon" />
  //                         <div className="nv-error-message">{err.errors[0]}</div>
  //                         <div className="nv-error-field">{fieldLabelsCustomer[key]}</div>
  //                     </li>
  //                 );
  //             default:
  //                 break
  //         }
  //     });
  //     return (
  //         <span className="nv-error-icon">
  //             <Popover
  //                 title="Thông tin lỗi"
  //                 content={errorList}
  //                 overlayClassName="nv-error-popover"
  //                 trigger="hover"
  //                 getPopupContainer={(trigger) => {
  //                     if (trigger && trigger.parentNode) {
  //                         return trigger.parentNode;
  //                     }
  //                     return trigger;
  //                 }}
  //             >
  //                 <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
  //             </Popover>
  //         </span>
  //     );
  // };

  return (
    <>
      <Modal
        visible={isShowModal}
        onCancel={() => setIsShowModal(false)}
        width={"80%"}
        closable={true}
        okText={activeKey === "1" ? "Đặt căn hộ" : "Thêm khách hàng"}
        onOk={() =>
          activeKey === "1" ? formBook.submit() : formAddCustomer.submit()
        }
        closable={false}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        zIndex={1031}
        centered={true}
        okButtonProps={{ disabled: !formData?.isBooking }}
      >
        <Tabs
          defaultValue="1"
          type="card"
          onChange={value => {
            setActiveKey(value);
          }}
        >
          <TabPane tab="Đặt căn hộ" key="1">
            <Card>
              {type === "house" ? (
                <>
                  <Descriptions
                    title={`Căn hộ ${formData?.data?.shortCode}`}
                    column={{ xxl: 2, xl: 2, lg: 2, md: 3, sm: 2, xs: 1 }}
                  >
                    <Descriptions.Item label="Số phòng ngủ">
                      {formData?.data?.numberRooms}
                    </Descriptions.Item>
                    <Descriptions.Item label="Diện tích tim tường" span={2}>
                      {formData?.data?.areaHeartWall} (m2)
                    </Descriptions.Item>
                    <Descriptions.Item label="View">
                      {formData?.data?.view}
                    </Descriptions.Item>
                    <Descriptions.Item label="Diện tích thông thủy" span={2}>
                      {formData?.data?.area} (m2)
                    </Descriptions.Item>

                    {/* <Descriptions.Item label="Trạng thái">
                      {toDot(formData?.data?.statusSale)}
                    </Descriptions.Item> */}
                  </Descriptions>
                  <div
                    style={{
                      display: "grid",
                      gridTemplateColumns: "1fr 1fr"
                    }}
                  >
                    <Descriptions>
                      <Descriptions.Item
                        label="Đơn giá bán/m2 thông thủy (chưa VAT & KPBT)"
                        labelStyle={{ fontWeight: 600 }}
                        span={3}
                      >
                        {toDot(formData?.data?.pricePerNotVAT)} đ
                      </Descriptions.Item>

                      <Descriptions.Item
                        label="Tổng giá bán (chưa VAT & KPBT)"
                        labelStyle={{ fontWeight: 600 }}
                        span={3}
                      >
                        {toDot(formData?.data?.totalPriceNotVAT)} đ
                      </Descriptions.Item>
                      <Descriptions.Item
                        label="Tổng giá bán (gồm VAT & KBPT)"
                        labelStyle={{ fontWeight: 600 }}
                        span={3}
                      >
                        {toDot(formData?.data?.totalPrice)} đ
                      </Descriptions.Item>
                    </Descriptions>

                    <Descriptions>
                      <Descriptions label="Chính sách" span={3}>
                        <ul>
                          {formData?.salePolicies?.length > 0 &&
                            formData?.salePolicies?.map((item, index) => (
                              <li key={index}>
                                {" "}
                                {item.name}{" "}
                                <a onClick={() => window.open(item.file)}>
                                  {" "}
                                  (Tải tài liệu){" "}
                                </a>
                              </li>
                            ))}
                        </ul>
                      </Descriptions>
                    </Descriptions>
                  </div>
                </>
              ) : (
                <>
                  <Descriptions title={`Căn hộ ${formData?.data?.shortCode}`}>
                    <Descriptions.Item label="Hướng cửa">
                      {formData?.data?.direction}
                    </Descriptions.Item>
                    <Descriptions.Item label="Diện tích đất">
                      {formData?.data?.areaLand} (m2)
                    </Descriptions.Item>
                    <Descriptions.Item label="Tổng diện tích xây dựng">
                      {formData?.data?.areaBuilding} (m2)
                    </Descriptions.Item>
                    <Descriptions.Item label="Tầng cao">
                      {formData?.data?.numberOfFloor || "-"}{" "}
                    </Descriptions.Item>
                    <Descriptions.Item label="Loại hình sản phẩm/phân khu">
                      {formData?.data?.numberRooms || "-"}{" "}
                    </Descriptions.Item>
                    <Descriptions.Item label="Giá bán (có VAT)">
                      {toDot(formData?.data?.totalPrice)} đ
                    </Descriptions.Item>
                    <Descriptions.Item label="Trạng thái">
                      {toDot(formData?.data?.statusSale)}
                    </Descriptions.Item>
                    <Descriptions label="Chính sách">
                      <ul>
                        {formData?.salePolicies?.length > 0 &&
                          formData?.salePolicies?.map(item => (
                            <li>
                              {item.name}
                              <a onClick={() => window.open(item.file)}>
                                (Tải tài liệu)
                              </a>
                            </li>
                          ))}
                      </ul>
                    </Descriptions>
                  </Descriptions>
                </>
              )}
            </Card>
            {formData?.isBooking ? (
              <Card title={"Thông tin đặt căn"}>
                <Form
                  {...layout}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  form={formBook}
                  layout={"vertical"}
                >
                  <Row>
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <Form.Item
                        label={fieldLabelsBookApartment["agencyId"]}
                        name="agencyId"
                        rules={[
                          { required: true, message: "Loại không được trống" }
                        ]}
                      >
                        <AgencySelect mode={"checkbox"} />
                      </Form.Item>
                    </Col>
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <Form.Item
                        label={fieldLabelsBookApartment["customerId"]}
                        name="customerIds"
                        rules={[
                          { required: true, message: "Loại không được trống" }
                        ]}
                      >
                        <CustomerSelect modeChoose={"checkbox"} />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <Form.Item
                        label={fieldLabelsBookApartment["moneyCollect"]}
                        name="moneyCollect"
                        rules={[
                          {
                            required: true,
                            message: "Tiền phải là số",
                            pattern: /^\d+$/
                          }
                        ]}
                      >
                        <FormInputRender
                          item={{
                            title: "Tên căn hộ",
                            dataIndex: "name",
                            width: 200,
                            valueType : "money"
                          }}
                          intl={intl}
                        />
                      </Form.Item>
                    </Col>
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <Form.Item
                        label={fieldLabelsBookApartment["images"]}
                        name="images"
                      >
                        <Upload multiple={true} />
                      </Form.Item>
                    </Col>
                  </Row>

                  {/* <Row>
                                    <Col xl={24} lg={24} md={24} sm={24} xs={24}>

                                        <Form.Item label={fieldLabelsBookApartment["salePolicyId"]} name="salePolicy">
                                            <Radio.Group name="radiogroup" defaultValue={formData?.salePolicy}>
                                                    <Radio value={"Vay"}>{"Vay"}</Radio>
                                                    <Radio value={"Không vay"}>{"Không vay"}</Radio>
                                                    <Radio value={"Thanh toán sớm"}>{"Thanh toán sớm"}</Radio>
                                            </Radio.Group>
                                        </Form.Item>

                                    </Col>
                                </Row> */}
                </Form>
              </Card>
            ) : (
              ""
            )}
          </TabPane>
          {formData?.isBooking ? (
            <TabPane tab="Thêm khách hàng" key="2">
              <Form
                {...layout}
                onFinish={onFinish}
                layout="vertical"
                onFinishFailed={onFinishFailed}
                form={formAddCustomer}
              >
                <Row>
                  <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                    <Form.Item
                      label={fieldLabelsCustomer["name"]}
                      name="name"
                      rules={[{ required: true, message: "Bắt buộc điền tên" }]}
                    >
                      <FormInputRender
                        item={{ title: "Tên căn hộ", dataIndex: "name" }}
                        intl={intl}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["phone"]}
                      name="phone"
                      rules={[
                        {
                          required: true,
                          message: "Bắt buộc điền số điện thoại"
                        }
                      ]}
                    >
                      <FormInputRender
                        item={{
                          title: "Tên căn hộ",
                          dataIndex: "name",
                          width: 200
                        }}
                        intl={intl}
                      />
                    </Form.Item>
                  </Col>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["gender"]}
                      name="gender"
                      required={true}
                    >
                      <Select>
                        <Option value="Nam">Nam</Option>
                        <Option value="Nữ">Nữ</Option>
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["birthYear"]}
                      name="birthYear"
                    >
                      <DatePicker
                        item={{
                          title: "Tên căn hộ",
                          dataIndex: "name",
                          width: 200
                        }}
                        intl={intl}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["ageRange"]}
                      name="ageRange"
                      rules={[
                        { required: true, message: "Bắt buộc điền khoảng tuổi" }
                      ]}
                    >
                      <Select>
                        <Option value="Dưới 25">Dưới 25</Option>
                        <Option value="Từ 25-35">Từ 25-35</Option>
                        <Option value="Từ 35-45">Từ 35-45</Option>
                        <Option value="Từ 45-55">Từ 45-55</Option>
                        <Option value="Trên 55">Trên 55</Option>
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["email"]}
                      name="email"
                    >
                      <FormInputRender
                        item={{
                          title: "Tên căn hộ",
                          dataIndex: "name",
                          width: 200
                        }}
                        intl={intl}
                      />
                    </Form.Item>
                  </Col>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["address"]}
                      name="address"
                    >
                      <FormInputRender
                        item={{
                          title: "Tên căn hộ",
                          dataIndex: "name",
                          width: 200
                        }}
                        intl={intl}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["indentityNumber"]}
                      name="indentityNumber"
                      rules={[
                        {
                          required: false,
                          message: "Số CCCD phải là số",
                          // pattern: /^\d+$/
                        }
                      ]}
                    >
                      <FormInputRender
                        item={{
                          title: "Tên căn hộ",
                          dataIndex: "name",
                          width: 200
                        }}
                        intl={intl}
                      />
                    </Form.Item>
                  </Col>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["indentityIssueDate"]}
                      name="indentityIssueDate"
                    >
                      <DatePicker
                        item={{
                          title: "Tên căn hộ",
                          dataIndex: "name",
                          width: 200
                        }}
                        intl={intl}
                      />
                    </Form.Item>
                  </Col>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["indentityProvince"]}
                      name="indentityProvince"
                    >
                      <FormInputRender
                        item={{
                          title: "Tên căn hộ",
                          dataIndex: "name",
                          width: 200
                        }}
                        intl={intl}
                      />
                    </Form.Item>
                  </Col>
                </Row>

                <Row>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["provinceId"]}
                      name="provinceId"
                      rules={[
                        {
                          required: true,
                          message: "Tỉnh/thành phố không được trống"
                        }
                      ]}
                    >
                      <ProvincesSelect mode="simple" />
                    </Form.Item>
                  </Col>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["districtId"]}
                      shouldUpdate={(prevValues, curValues) =>
                        prevValues.provinceId !== curValues.provinceId
                      }
                      required={true}
                    >
                      {() => {
                        return (
                          <Form.Item
                            name="districtId"
                            rules={[
                              {
                                required: true,
                                message: "Quận/huyện không được trống"
                              }
                            ]}
                            noStyle
                          >
                            <DistrictsSelect
                              mode="simple"
                              province={
                                (
                                  formAddCustomer.getFieldValue("provinceId") ||
                                  {}
                                ).key || ""
                              }
                            />
                          </Form.Item>
                        );
                      }}
                    </Form.Item>
                  </Col>
                  <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Form.Item
                      label={fieldLabelsCustomer["wardId"]}
                      shouldUpdate={(prevValues, curValues) =>
                        prevValues.districtId !== curValues.districtId
                      }
                      required={true}
                    >
                      {() => {
                        return (
                          <Form.Item
                            name="wardId"
                            rules={[
                              {
                                required: true,
                                message: "Phường không được trống"
                              }
                            ]}
                            noStyle
                          >
                            <WardSelect
                              mode="simple"
                              district={
                                (
                                  formAddCustomer.getFieldValue("districtId") ||
                                  {}
                                ).key || ""
                              }
                            />
                          </Form.Item>
                        );
                      }}
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </TabPane>
          ) : (
            ""
          )}
        </Tabs>
      </Modal>
    </>
  );
};

export default connect(({ bookapartment, loading, router }) => ({
  submitting: loading.effects["bookapartment/dispatch"],
  bookapartment,
  router
}))(BookApartmentForm);
