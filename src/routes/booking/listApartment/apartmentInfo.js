import React, { useState, useEffect } from 'react';
import { ShopOutlined,HomeOutlined,ShoppingCartOutlined,TableOutlined ,CompassOutlined} from '@ant-design/icons';
import { Button, Card } from 'antd';
import BookApartmentForm from './bookapartmentForm';
import { connect } from 'dva';
import { formatMoney } from '../../../util/utils';
const ApartmentInfo = ({ bookapartment: { formData }, apartment, type, projectId, dispatch }) => {
  const [isShowModal, setIsShowModal] = useState(false);

  useEffect(() => {
    dispatch({
      type: `bookapartment/fetchForm`,
      payload: {
        projectId: projectId,
        fullCode: apartment.fullCode
      }
    });
  }, [apartment]);

  return (
    <>
      <Card
        style={{ width: 220 , marginBottom:"0" }}
        cover={<>
          <div className="nv-booking-hover">
            <div className="nv-booking-hover__title">
              <div className="nv-booking-hover__title__left">
                <div className="nv-booking-hover__title__left__icon">
                <ShoppingCartOutlined />
                </div>
                <div className="nv-booking-hover__title__left__text">
                  <span>Căn hộ</span>
                  <span>{apartment.shortCode}</span>
                </div>  
              </div>
              <div className="nv-booking-hover__title_right">
                <span>Giá niêm yết</span>
                <span>{formatMoney(apartment.totalPrice)}</span>
              </div>
            </div>
            
            
          </div>
        </>
        }
        actions={[
          <Button onClick={() => {
            setIsShowModal(true);
            dispatch({
              type: `bookapartment/fetchForm`,
              payload: {
                projectId: projectId,
                fullCode: apartment.fullCode
              }
            });
          }}
            icon={<ShopOutlined key="book" />}>
            {apartment.isBooking === true ? "Book căn" : "Xem thông tin"}
          </Button>,
        ]}
      >
        {type === "house" ?
          (<>
            <div style={{display:"flex" , alignItems:"center", marginBottom:"5px" , paddingBottom:"5px", borderBottom:"1px dashed"}}>
              <div style={{marginRight:"5px"}}><CompassOutlined /></div>
              <span>{apartment.direction}</span>
            </div>
            <div style={{display:"flex" , alignItems:"center", marginBottom:"5px" , paddingBottom:"5px", borderBottom:"1px dashed"}}>
            <div style={{marginRight:"5px"}}><HomeOutlined /></div>
              <span>{apartment.numberRooms}</span>
            </div>
            <div style={{display:"flex" , alignItems:"center", marginBottom:"5px" , paddingBottom:"5px", borderBottom:"1px dashed"}}>
              <div style={{marginRight:"5px"}}><TableOutlined /></div>
              <span>{apartment.area} (m2)</span>
            </div>
       
          </>)
          :
          (<>
             <div style={{display:"flex" , alignItems:"center", marginBottom:"5px" , paddingBottom:"5px", borderBottom:"1px dashed"}}>
             <div style={{marginRight:"5px"}}><CompassOutlined /></div>
              <span>{apartment.direction}</span>
            </div>
            <div style={{display:"flex" , alignItems:"center", marginBottom:"5px" , paddingBottom:"5px", borderBottom:"1px dashed"}}>
              <div style={{marginRight:"5px"}}><HomeOutlined /></div>
              <span>{apartment.numberOfFloor ? apartment.numberOfFloor + "tầng" : ""} </span>
            </div>
            <div style={{display:"flex" , alignItems:"center", marginBottom:"5px" , paddingBottom:"5px", borderBottom:"1px dashed"}}>
              <div style={{marginRight:"5px"}}><TableOutlined/></div>
              <span>{apartment.areaLand} (m2)</span>
            </div>
          </>)}
      </Card>

      <BookApartmentForm formData={formData} type={type} fullCode={apartment.fullCode} projectId={projectId} isShowModal={isShowModal} setIsShowModal={setIsShowModal} />
    </>
  );
};

export default connect(({ bookapartment, loading, router }) => ({
  submitting: loading.effects['bookapartment/dispatch'],
  bookapartment,
  router
}))(ApartmentInfo);

