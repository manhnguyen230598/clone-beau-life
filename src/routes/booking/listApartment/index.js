import React, { useState, useEffect } from 'react';
import { Form,Typography, Badge, Card, Col, Row, Divider } from 'antd';
// import { SearchOutlined, CloseCircleOutlined, DownOutlined, UpOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import _ from 'lodash';
import { getTableChange } from 'util/helpers';
// import ApartmentForm from './crud'
import ApartmentList from './apartmentList';
import { useLocation } from "react-router-dom";
import Loading from 'src/components/Loading';

const { Title } = Typography;
const fieldLabels = { "name": "Tên căn hộ", "state": "Trạng thái" };
const tranformApartmentParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "bookapartment";
const color = [
  { color: "yellow", text: "BÁN 4_Đã ký HĐMB" },
  { color: 'green', text: "BÁN 1_Đủ cọc và thủ tục" },
  { color: '#B003F2', text: "BÁN 2_Đủ cọc" },
  { color: '#f7fcfc', text: "CHƯA BÁN 1_Chưa cọc" },
  { color: '#FCA947', text: "CHƯA BÁN 2_Chưa đủ cọc" },
  { color: '#c0c0c0', text: "CHƯA BÁN 3_Hủy HDV/HĐMB" },
  { color: "red", text: "CHƯA BÁN 4_Lock" },
];

function useQuery() {
  return new URLSearchParams(useLocation().search);
}
const BookApartmentList = (props) => {
  let query = useQuery()
  const [form] = Form.useForm();
  const [colorEnum, setColorEnum] = useState({});
  // const searchInput = useRef();
  // const [error, setError] = useState([]);
  // const [dataModal, setDataModal] = useState({});
  // const [selectedRows, setSelectedRows] = useState([]);
  // const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  // const [skip,] = useState(0);
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ?
    Number(process.env.REACT_APP_PAGESIZE) : 10);
  // const [current, setCurrent] = useState(1);
  // const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  // const [formValues, setFormValues] = useState((origin) => {
  //   return tableChange?.formValues || {};
  // });
  const [colorStatus, setColorStatus] = useState([]);
  const [countStatus, setCountStatus] = useState({});
  const [pagination, setPagination] = useState({
    pageSize: limit
  });

  const {
    dispatch,
    [RESOURCE]: { data },
  } = props;
  useEffect(() => {

    let initParams = {};
    let pagi = Object.assign(pagination, (tableChange?.pagination) || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformApartmentParams(tableChange?.filtersArg, tableChange?.formValues, tableChange?.sort);

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        sort: initParams.sort ? (initParams.sort.length <= 0 ? [{ "createdAt": "desc" }] : initParams.sort) : [{ "createdAt": "desc" }],
        projectId: query.get("projectId")
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);
  useEffect(() => {
    setColorStatus(data.colorStatusSale);
    setCountStatus(data.countStatusSale);
  }, [data]);
  useEffect(() => {
    let temp = {};
    if (Array.isArray(colorStatus)) {
      colorStatus.forEach(e => {
        temp[e.status] = e.color;
      });
      temp["HIDE"] = "#e0e0e0"
      setColorEnum(temp);
    }
  }, [colorStatus]);
  // const handleTableChange = (pagi, filtersArg, sorter) => {
  //   const filters = Object.keys(filtersArg).reduce((obj, key) => {
  //     const newObj = { ...obj };
  //     if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
  //     return newObj;
  //   }, {});

  //   const initParams = tranformApartmentParams(filters, formValues, sorter);

  //   if (sessionStorage.getItem('isReset') === 'true') {
  //     sessionStorage.setItem('isReset', false);
  //   }
  //   const params = {
  //     queryInput: {
  //       ...initParams.filters
  //     },
  //     page: Number(pagi.current),
  //     current: Number(pagi.current),
  //     skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
  //     limit: Number(pagi.pageSize),
  //     pageSize: Number(pagi.pageSize)
  //   };
  //   if (sorter.field) {
  //     params.sort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
  //     // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
  //   } else {
  //     params.sort = union(initParams.sort || [], [{ "createdAt": "desc" }]);
  //   }

  //   setCurrent(pagination.current);
  //   dispatch({
  //     type: `${RESOURCE}/fetch`,
  //     payload: params
  //   });
  // };

  // const handleTableSearch = () => {
  //   form.current.validateFields().then(fieldsValue => {
  //     console.log('handleSearch -> fieldsValue', fieldsValue);
  //     const values = {};
  //     Object.keys(fieldsValue).forEach(key => {
  //       if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
  //         values[key] = fieldsValue[key];
  //       }
  //     });

  //     if (sessionStorage.getItem('isReset') === 'true') {
  //       sessionStorage.setItem('isReset', false);
  //       values = {};
  //     }
  //     const initParams = tranformApartmentParams(tableChange?.filters, values, tableChange?.sort);

  //     setTableChange(RESOURCE, {
  //       ...tableChange,
  //       pagination: {
  //         ...tableChange.pagination,
  //         page: 1,
  //         current: 1,
  //         skip: 0,
  //         limit,
  //         pageSize: limit
  //       },
  //       formValues: values
  //     });
  //     setFormValues(fieldsValue);
  //     dispatch({
  //       type: `${RESOURCE}/fetch`,
  //       payload: {
  //         queryInput: {
  //           ...initParams.filters
  //         },
  //         page: 1,
  //         current: 1,
  //         skip: 0,
  //         limit,
  //         pageSize: limit,
  //         sort: union(initParams.sort || [], [{ "createdAt": "desc" }])
  //       }
  //     });
  //   }).catch(err => {
  //     if (err) return;
  //   });
  // };

  // const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
  //   setListSearch({
  //     ...listSearch,
  //     [`search_${dataIndex}`]: selectedKeys[0],
  //   });
  //   confirm();
  // };
  // const getErrorInfo = (errors) => {
  //   const errorCount = errors.filter((item) => item.errors.length > 0).length;
  //   if (!errors || errorCount === 0) {
  //     return null;
  //   }
  //   const scrollToField = (fieldKey) => {
  //     const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
  //     if (labelNode) {
  //       labelNode.scrollIntoView(true);
  //     }
  //   };
  //   const errorList = errors.map((err) => {
  //     if (!err || err.errors.length === 0) {
  //       return null;
  //     }
  //     const key = err.name[0];
  //     return (
  //       <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
  //         <CloseCircleOutlined className="nv-error-icon" />
  //         <div className="nv-error-message">{err.errors[0]}</div>
  //         <div className="nv-error-field">{fieldLabels[key]}</div>
  //       </li>
  //     );
  //   });
  //   return (
  //     <span className="nv-error-icon">
  //       <Popover
  //         title="Thông tin lỗi"
  //         content={errorList}
  //         overlayClassName="nv-error-popover"
  //         trigger="click"
  //         getPopupContainer={(trigger) => {
  //           if (trigger && trigger.parentNode) {
  //             return trigger.parentNode;
  //           }
  //           return trigger;
  //         }}
  //       >
  //         <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
  //       </Popover>
  //     </span>
  //   );
  // };

  // const getColumnSearchProps = dataIndex => ({
  //   filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
  //     <div style={{ padding: 8 }}>
  //       <Input
  //         ref={searchInput}
  //         placeholder={`Tìm kiếm ${dataIndex}`}
  //         value={selectedKeys[0]}
  //         onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
  //         onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
  //         style={{ width: 188, marginBottom: 8, display: 'block' }}
  //       />
  //       <Space>
  //         <Button
  //           type="primary"
  //           onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
  //           icon={<SearchOutlined />}
  //           size="small"
  //           style={{ width: 90 }}
  //         >
  //           Tìm
  //         </Button>
  //         <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
  //           Reset
  //         </Button>
  //       </Space>
  //     </div>
  //   ),
  //   filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
  //   onFilterDropdownVisibleChange: visible => {
  //     if (visible) {
  //       setTimeout(() => searchInput.current.select());
  //     }
  //   },
  //   onFilter: (value, record) => record
  // });

  // const handleReset = (clearFilters, confirm, dataIndex) => {
  //   clearFilters();
  //   setListSearch({
  //     ...listSearch,
  //     [`search_${dataIndex}`]: '',
  //   });
  //   confirm();
  // };

  
  // const onFinish = (values) => {
  //   setError([]);
  //   const data = _.cloneDeep(values);
  //   dispatch({
  //     type: `${RESOURCE}/submit`,
  //     payload: data,
  //     callback: (res) => {
  //       if (res && !res.error) {

  //       }
  //     }
  //   });
  // };

  // const onFinishFailed = (errorInfo) => {
  //   console.log('Failed:', errorInfo);
  //   setError(errorInfo.errorFields);
  // };
  if(!data.colorStatusSale) return <Loading />
  return (
    <>
      {/* <SearchForm handleAddClick={handleAddClick} /> */}
      <Title level={4}>{`Dự án ${data.projectName}`}</Title>
      <Card style={{ backgroundColor: "#e1f5fe", border : "solid 1px" }}>
        <Row>
        <Col xl={6} lg={6} md={6} sm={6} xs={12}>
            {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
             */}
             <span style={{width : "40px", height: "15px", backgroundColor:"transparent", marginRight : "8px", display : "inline-block", border : "solid 1px"}}></span>
             <span style={{fontWeight : 600}}>TỔNG SỐ CĂN ({data?.list?.length || 0} căn)</span>
          </Col>
          {colorStatus?.map((item, index) =>
          (<Col key={index} xl={6} lg={6} md={6} sm={6} xs={12}>
            {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
             */}
             <span style={{width : "40px", height: "15px", backgroundColor:`${item.color}`, marginRight : "8px", display : "inline-block"}}></span>
             <span style={{fontWeight : 600}}>{item.status} ({countStatus[(item.status)] || 0} căn)</span>
          </Col>)
          )}
          
        </Row>
      </Card>
      <Divider orientation="left">Danh sách các căn hộ</Divider>
      <ApartmentList type={query.get("type")} colorEnum={colorEnum} data={data} projectId={query.get("projectId")} />
      {/* <ApartmentForm
        formType={"add"}
        modal={{ "dataModal": dataModal, "setDataModal": setDataModal }} /> */}
    </>
  );
};

// const SearchForm = ({ handleAddClick }) => {
//   const [isExpand, setIsExpand] = useState(false)
//   return (
//     <>
//       <Card >
//         <Form >
//           <Row>
//             <Col xl={{ span: 7 }} lg={{ span: 7 }} md={{ span: 7 }} sm={{ span: 7 }} xs={{ span: 7 }}>
//               <Form.Item
//                 label={"Tên căn hộ"}
//                 name="name"
//               >
//                 <Input />
//               </Form.Item>
//             </Col>
//             <Col style={{ marginLeft: "5%" }} xl={{ span: 7 }} lg={{ span: 7 }} md={{ span: 7 }} sm={{ span: 7 }} xs={{ span: 7 }}>
//               <Form.Item
//                 label={"Diện tích"}
//                 name="name"
//               >
//                 <Input />
//               </Form.Item>
//             </Col>
//             <Col style={{ marginLeft: "3%" }} xl={{ span: 2 }} lg={{ span: 2 }} md={{ span: 2 }} sm={{ span: 2 }} xs={{ span: 2 }}>
//               <Button style={{ marginLeft: "8%", fontSize: "1vw", width: "100%" }} type="primary" >Tìm kiếm</Button>
//             </Col>
//             <Col style={{ marginLeft: "2%" }} xl={{ span: 2 }} lg={{ span: 2 }} md={{ span: 2 }} sm={{ span: 2 }} xs={{ span: 2 }}>
//               <Button style={{ marginLeft: "2%", fontSize: "1vw" }} onClick={handleAddClick} type="primary" >Thêm mới</Button>
//             </Col>
//             <Col style={{ marginLeft: "2%" }} xl={{ span: 3 }} lg={{ span: 3 }} md={{ span: 3 }} sm={{ span: 3 }} xs={{ span: 3 }}>
//               {isExpand === false ?
//                 <Button style={{ marginLeft: "5%", fontSize: "1vw", width: "100%" }} icon={<DownOutlined />} onClick={() => setIsExpand(true)}>Mở rộng</Button> :
//                 <Button style={{ marginLeft: "5%", fontSize: "1vw", width: "100%" }} icon={<UpOutlined />} onClick={() => setIsExpand(false)}>Thu gọn</Button>}
//             </Col>
//           </Row>

//           {isExpand ? <Row >
//             <Col xl={7} lg={7} md={7} sm={7} xs={7}>
//               <Form.Item
//                 label={"Hướng phòng"}
//                 name="name"
//               >
//                 <Input />
//               </Form.Item>
//             </Col>
//             <Col style={{ marginLeft: "5%" }} xl={7} lg={7} md={7} sm={7} xs={7}>
//               <Form.Item
//                 label={"Số phòng ngủ"}
//                 name="name"
//               >
//                 <Input />
//               </Form.Item>
//             </Col>
//           </Row>
//             : ""}
//         </Form>
//       </Card>

//     </>

//   );
// };



export default connect(({ bookapartment, loading }) => ({
  bookapartment,
  loading: loading.models.bookapartment
}))(BookApartmentList);
