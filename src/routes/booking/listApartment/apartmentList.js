import React, { useState, useEffect } from "react";
import { Select, Row, Col, Popover, Radio, Switch } from "antd";
import * as enums from "../../../util/enums";
import { LockOutlined } from "@ant-design/icons";
import ApartmentInfo from "./apartmentInfo";
import { formatMoney } from "src/util/utils";
import { v4 as uuid } from "uuid";
const { Option } = Select;
const ApartmentList = ({ data, type, projectId, colorEnum }) => {
  const [dataRow, setDataRow] = useState([]);
  const [dataCollumn, setDataCollumn] = useState([]);
  const [listApt, setListApt] = useState(data.list);
  const [buildingCode, setbuildingCode] = useState();
  const [list, setList] = useState([]);
  const [height, setHeight] = useState(40);
  const [width, setWidth] = useState(100);
  const [aptRow, setAptRow] = useState({});
  const [viewPrice, setViewPrice] = useState(true);
  useEffect(() => {
    setDataRow(data.row);
    setDataCollumn(data.column);
    setList(data.list);
    if (type === "house") {
      let build =
        data && (data.building || []).length ? data?.building?.at(0) : "";
      setbuildingCode(build || "");
      let newArr = data?.list?.filter(item => item.buildingCode === build);
      setListApt(newArr);
    } else {
      setListApt(data.list);
    }
  }, [data]);
  useEffect(() => {
    let temp = {};
    if (listApt.length > 0) {
      if (type === "house") {
        listApt.map(e => {
          if (!temp[e.floorCode]) temp[e.floorCode] = [];
          temp[e.floorCode].push(e);
        });
      } else {
        listApt.map(e => {
          if (!temp[e.row]) temp[e.row] = [];
          temp[e.row].push(e);
        });
      }
    }
    setAptRow(temp);
  }, [listApt]);
  useEffect(() => {
    if (type === "house") {
      let row = new Set();
      let collumn = new Set();
      listApt.map(e => {
        row.add(e.floorCode);
        collumn.add(e.apartmentCode);
      });
      row = [...row].sort((a, b) => {
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }

        // names must be equal
        return 0;
      });
      collumn = [...collumn].sort((a, b) => {
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }

        // names must be equal
        return 0;
      });
      setDataRow([...row]);
      setDataCollumn([...collumn]);
    }
  }, [listApt]);
  function handleChange(value) {
    if (value === "all") {
      setDataRow(data?.row);
      return;
    }
    setDataRow(data?.row?.filter(item => item === value));
  }
  function handleChangeBuilding(value) {
    setbuildingCode(value.target.value);
    let newArr = list.filter(item => item.buildingCode === value.target.value);
    setListApt(newArr);
  }
  function handleChangeSubdivision(value) {
    if (value === "all") {
      setListApt(data?.list);
      return;
    }
    setListApt(list?.filter(item => item.subdivision === value));
  }
  function handleChangeWidth(value) {
    setWidth(value);
  }
  const getMaxColumn = () => {
    let MAX = 1;
    for (let i = 0; i < data?.row?.length; i++) {
      let newMax = data?.list?.filter(item => item.row === data?.row[i]).length;
      if (newMax > MAX) {
        MAX = newMax;
      }
    }
    return MAX;
  };

  return (
    <>
      {type === "house" ? (
        <>
          <div>
            <Row>
              <Col
                lg={{ span: 5, offset: 1 }}
                style={{ display: "flex", alignItems: "center" }}
              >
                <label style={{ marginRight: 5, fontWeight: 600 }}>
                  Chọn tòa:
                </label>
                {/* <Select
                  defaultValue = {buildingCode}
                  style={{ width: 200, marginLeft: 10 }}
                  onChange={handleChangeBuilding}
                >
                  {data?.building?.map((item, index) => (
                    <Option key={index} value={item}>
                      Tòa {item}
                    </Option>
                  ))}
                  </Select> */}
                <Radio.Group
                  onChange={handleChangeBuilding}
                  value={buildingCode}
                >
                  {data?.building?.map((item, index) => (
                    <Radio key={index} value={item}>
                      Tòa {item}
                    </Radio>
                  ))}
                  {/* <Radio  value={"S2"}>
                      Tòa 2
                    </Radio> */}
                </Radio.Group>
              </Col>
              <Col lg={{ span: 6, offset: 1 }}>
                <label style={{ fontWeight: 600 }}>Chọn tầng:</label>
                <Select
                  defaultValue="all"
                  style={{ width: 200, marginLeft: 10 }}
                  onChange={handleChange}
                >
                  <Option key={"all"} value="all">
                    Tất cả
                  </Option>
                  {data?.row?.map((item, index) => (
                    <Option key={index} value={item}>
                      Tầng {item}
                    </Option>
                  ))}
                </Select>
              </Col>
              <Col lg={{ span: 4, offset: 3 }}>
                <label style={{ fontWeight: 600 }}>Tỉ lệ:</label>
                <Select
                  defaultValue={"100"}
                  style={{ width: 100, marginLeft: 10 }}
                  onChange={handleChangeWidth}
                >
                  <Option value="100">100%</Option>
                  <Option value="50">50%</Option>
                </Select>
              </Col>
              <Col lg={{ span: 3 }} style={{display : "flex", alignItems : "center"}}>
                <label style={{ fontWeight: 600 }}>Hiển thị: </label>
                <Switch
                  checkedChildren={"Giá tiền"}
                  unCheckedChildren={"Mã căn"}
                  defaultChecked={viewPrice}
                  onChange={()=>{setViewPrice(!viewPrice)}}
                />
              </Col>
            </Row>
          </div>

          <div style={{ alignItems: "center", margin: "30px 0px" }}>
            <Row>
              <Col lg={1}>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px"
                  }}
                >
                  <i className="icon icon-company" />
                </p>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px",
                    overflow: "hidden",
                    whiteSpace: "nowrap"
                  }}
                >
                  <i className="icon icon-home" />
                </p>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px",
                    overflow: "hidden",
                    whiteSpace: "nowrap"
                  }}
                >
                  <i className=" icon icon-map-directions" />
                </p>
                <div style={{ height: "100%" }}>
                  {dataRow?.map((item, index) => (
                    <p
                      key={index}
                      style={{
                        height: `${height}px`,
                        margin: 0,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      {item}
                    </p>
                  ))}
                </div>
              </Col>
              <Col lg={23}>
                <div style={{ overflowX: "scroll" }}>
                  <div style={{ width: dataCollumn?.length * width }}>
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px"
                        }}
                      >
                        {item}
                      </p>
                    ))}
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px",
                          // overflow: "hidden",
                          whiteSpace: "nowrap"
                        }}
                      >
                        {data?.rowInfo[item]?.numberRooms || "-"}
                      </p>
                    ))}
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px",
                          overflow: "hidden",
                          whiteSpace: "nowrap"
                        }}
                      >
                        {data?.rowInfo[item]?.direction || "-"}
                      </p>
                    ))}

                    {dataRow?.map(row =>
                      dataCollumn?.map(column => {
                        let item = aptRow[row]?.find(ele => {
                          return (
                            ele.floorCode == row && ele.apartmentCode == column
                          );
                        });
                        if (item) {
                          return (
                            <Popover
                              key={"apm" + item.id}
                              style={{ cursor: "pointer", zIndex: 10 }}
                              placement="topLeft"
                              content={
                                item.statusSale !== "HIDE" ? (
                                  <ApartmentInfo
                                    type={type}
                                    projectId={projectId}
                                    apartment={item}
                                    index={"apm" + item.id}
                                  />
                                ) : (
                                  ""
                                )
                              }
                              trigger="hover"
                            >
                              <div
                                key={"apmdiv" + item.id}
                                style={{
                                  width: `${width}px`,
                                  backgroundColor: colorEnum[item.statusSale],
                                  height: `${height}px`,
                                  border: "solid 1px",
                                  display: "inline-flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                  overflow: "hidden"
                                }}
                              >
                                {item.statusSale === "HIDE" ? (
                                  <p style={{ margin: 0 }}>
                                    <LockOutlined />
                                  </p>
                                ) : (
                                  <p
                                    style={{
                                      fontWeight: "bolder",
                                      color: "black",
                                      margin: 0,
                                      whiteSpace : "nowrap"
                                    }}
                                  >
                                    {width > 50 ? ( viewPrice ?
                                      formatMoney(item.totalPrice) : item.shortCode
                                    ) : (
                                      <i className="icon icon-home" />
                                    )}
                                  </p>
                                )}
                              </div>
                            </Popover>
                          );
                        } else {
                          return (
                            <div
                              key={uuid()}
                              style={{
                                width: `${width}px`,
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                backgroundImage:
                                  "repeating-linear-gradient( 45deg, black, black 2px, transparent 2px, transparent 5px)"
                              }}
                            >
                              <span style={{}}>Trống</span>
                            </div>
                          );
                        }
                      })
                    )}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </>
      ) : (
        <>
          <div>
            <Row>
              <Col lg={{ span: 5, offset: 1 }}>
                <label>Chọn dãy:</label>
                <Select
                  defaultValue="all"
                  style={{ width: 200, marginLeft: 10 }}
                  onChange={handleChange}
                >
                  <Option key={"all"} value="all">
                    Tất cả
                  </Option>
                  {data?.row?.map((item, index) => (
                    <Option key={index} value={item}>
                      Dãy {item}
                    </Option>
                  ))}
                </Select>
              </Col>
              <Col lg={{ span: 4, offset: 11 }}>
                <label style={{ fontWeight: 600 }}>Tỉ lệ:</label>
                <Select
                  defaultValue={"100"}
                  style={{ width: 100, marginLeft: 10 }}
                  onChange={handleChangeWidth}
                >
                  <Option value="100">100%</Option>
                  <Option value="50">50%</Option>
                </Select>
              </Col>
              <Col lg={{ span: 3 }} style={{display : "flex", alignItems : "center"}}>
                <label style={{ fontWeight: 600 }}>Hiển thị: </label>
                <Switch
                  checkedChildren={"Giá tiền"}
                  unCheckedChildren={"Mã căn"}
                  defaultChecked={viewPrice}
                  onChange={()=>{setViewPrice(!viewPrice)}}
                />
              </Col>
            </Row>
          </div>

          <div style={{ alignItems: "center", marginTop: 30 }}>
            <Row>
              <Col lg={1}>
                <div style={{ height: "100%" }}>
                  <p
                    style={{
                      textAlign: "center",
                      border: "solid 1px",
                      overflow: "hidden",
                      whiteSpace: "nowrap"
                    }}
                  >
                    <i className="icon icon-home" />
                  </p>
                  {dataRow?.map((item, index) => (
                    <span
                      key={index}
                      style={{
                        height: `${height}px`,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      {item}
                    </span>
                  ))}
                </div>
              </Col>
              <Col lg={23}>
                {/* <div>
                    {dataCollumn?.map(item =>
                      <p style={{ display: "inline-block", width: customWidth(), textAlign: "center" }}>{item}</p>
                    )}
                  </div> */}
                <div style={{ overflowX: "scroll" }}>
                  <div
                    style={{ width: (dataCollumn?.length || 0) * width + 10 }}
                  >
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px"
                        }}
                      >
                        {item}
                      </p>
                    ))}
                  </div>
                  <div
                    style={{ width: (dataCollumn?.length || 0) * width + 10 }}
                  >
                    {dataRow?.map(row =>
                      dataCollumn?.map(column => {
                        let item = aptRow[row]?.find(ele => {
                          return ele.row == row && ele.apartmentCode == column;
                        });
                        if (item) {
                          return (
                            <Popover
                              key={"apm" + item.id}
                              style={{ cursor: "pointer", zIndex: 10 }}
                              placement="topLeft"
                              content={
                                item.statusSale !== "HIDE" ? (
                                  <ApartmentInfo
                                    type={type}
                                    projectId={projectId}
                                    apartment={item}
                                    index={"apm" + item.id}
                                  />
                                ) : (
                                  ""
                                )
                              }
                              trigger="hover"
                            >
                              <div
                                key={"apmdiv" + item.id}
                                style={{
                                  width: `${width}px`,
                                  backgroundColor: colorEnum[item.statusSale],
                                  height: `${height}px`,
                                  border: "solid 1px",
                                  display: "inline-flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                  overflow: "hidden"
                                }}
                              >
                                {item.statusSale === "HIDE" ? (
                                  <p style={{ margin: 0 }}>
                                    <LockOutlined />
                                  </p>
                                ) : (
                                  <p
                                    style={{
                                      fontWeight: "bolder",
                                      color: "black",
                                      margin: 0,
                                      whiteSpace : "nowrap"
                                    }}
                                  >
                                    {width > 50 ? ( viewPrice ? 
                                      formatMoney(item.totalPrice) : item.shortCode
                                    ) : (
                                      <i className="icon icon-home" />
                                    )}
                                  </p>
                                )}
                              </div>
                            </Popover>
                          );
                        } else {
                          return (
                            <div
                              key={"aptlow" + uuid()}
                              style={{
                                width: `${width}px`,
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                backgroundImage:
                                  "repeating-linear-gradient( 45deg, black, black 2px, transparent 2px, transparent 5px)"
                              }}
                            >
                              <span style={{}}>Trống</span>
                            </div>
                          );
                        }
                      })
                    )}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </>
      )}
    </>
  );
};

export default ApartmentList;
