import React from "react";
import { router } from 'dva';
import asyncComponent from "../../util/asyncComponent";
/* PLOP_INJECT_IMPORT */
const { Route, Switch } = router;
const Booking = ({ match }) => {
  return (
    <Switch>
      <Route path={`${match.url}/apartment-site`} component={asyncComponent(() => import("./listApartment"))} />
    </Switch>
  );
};

export default Booking;
