import React, { useEffect, useState } from "react";
import { router } from "dva";
import { Card, Row, Col, Divider } from "antd";
import { Redirect } from "react-router";
import * as projectServices from "../../../services/project";
/* PLOP_INJECT_IMPORT */

const { Meta } = Card;
const { Route, Switch, Link } = router;
const ListProject = ({ match }) => {
  const [listProject, setListProject] = useState([]);

  useEffect(() => {
    (async () => {
      const response = await projectServices.getListForBooking();
      setListProject(response.data.data);
    })();
  }, []);
  return (
    <>
      <div>
          <span style={{fontSize:"1.2em"}}>DANH SÁCH DỰ ÁN</span>
          <Divider />
      </div>
      <Row gutter={[32, 24]}>
        {listProject?.map((item, index) => (
          <Col
            style={{ marginTop: 30 }}
            key={index}
            xl={6}
            lg={6}
            md={8}
            sm={8}
            xs={12}
          >
            <Link to={`/apartment-site?projectId=${item.id}&type=${item.type}`}>
              {/* <Card
                                key={index}
                                hoverable
                                style={{ width: "100%", height: "100%" }}
                                // cover={<img alt="example" src={item.thumbnail} />}
                                bodyStyle ={{padding : "3px"}}
                            > */}
              {/* <img src={item.thumbnail}></img> */}
              {/* <Meta title={`Mã ${item.id} : ${item.name}`} description={item.type === "house" ? "Nhà cao tầng" : "Resort"} /> */}
              {/* </Card> */}
              <div
                style={{
                  padding: "4px"
                }}
                className="card-project"
              >
                <div
                  style={{
                    width: "100%",
                    height: "300px"
                  }}
                >
                  <img
                    src={item.thumbnail}
                    style={{ objectFit: "cover", height: "100%" , borderRadius:"15px"}}
                    className="imgproject"
                  />
                  <span
                    style={{
                      position: "absolute",
                      // top: "50%",
                      // left: "50%",
                      // transform: "translate(-50%, -50%)",
                      fontSize:"1em",
                      bottom:"8%",
                      left:"15%",
                      right:"27%"
                    }}
                  >
                    {item.name}
                  </span>
                </div>
              </div>
            </Link>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default ListProject;
