import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import request from "./../../util/request";
const Home: React.FC<any> = () => {
  const [listBanner, setListBanner] = useState([]);
  useEffect(() => {
    request("admin/banner/get-list-banner").then((rs: any) => {
      if (rs && rs.data?.data) {
        setListBanner(rs.data?.data);
      }
    });
  }, []);

  return (
    <React.Fragment>
      <div>
        {/* <h1>Chào mừng bạn đến WEHOME!</h1>
        <img src="/static/images/bg.jpg" /> */}
        <Carousel autoplay>
          {listBanner.map((e: any) => (
            <div>
              <img
                src={e?.image}
                style={{
                  maxHeight: "600px",
                  objectFit: "cover"
                }}
              />
              ;
            </div>
          ))}
        </Carousel>
      </div>
    </React.Fragment>
  );
};

export default Home;
