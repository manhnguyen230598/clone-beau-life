import React from "react";
import { Card, Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { router } from "dva";
import asyncComponent from "../../util/asyncComponent";
const { Redirect, Switch, Route } = router;

const TabPane = Tabs.TabPane;
const ProjectDetailRoute = ({
  component: Component,
  projectId,
  templateCustomerFile,
  projectType,
  projectName,
  ...rest
}) => <Route {...rest} render={props => <Component {...props} />} />;
const Booking = ({ match, location, history, ...rest }) => {
  const getTabKey = () => {
    const url = match.url === "/" ? "" : match.url;
    const tabKey = location.pathname.replace(`${url}`, "");
    if (tabKey && tabKey !== "") {
      const tkey = tabKey.replace(/\//g, "");
      return tkey;
    }
    return "projectMediaCustomer";
  };
  function callback(key) {
    switch (key) {
      case "projectMediaCustomer":
        history.push({ pathname: `${match.url}/projectMediaCustomer` });
        break;
      case "projectVisitingCustomer":
        history.push({ pathname: `${match.url}/projectVisitingCustomer` });
        break;

      default:
        break;
    }
  }
  const tabList = [
    {
      key: "projectMediaCustomer",
      tab: <IntlMessages id="projectMediaCustomer.title" />
    },
    {
      key: "projectVisitingCustomer",
      tab: <IntlMessages id="projectVisitingCustomer.title" />
    }
  ];
  return (
    <Card>
      <Tabs
        // defaultActiveKey="1"
        onChange={callback}
        activeKey={getTabKey()}
      >
        {tabList.map((item, index) => (
          <TabPane {...item} tab={item.tab} key={item.key || index} />
        ))}
      </Tabs>
      <Switch>
        <Redirect
          exact
          from={`${match.url}/`}
          to={`${match.url}/${getTabKey()}`}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectmediacustomer`}
          component={asyncComponent(() =>
            import("./projectMediaCustomer/list")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectmediacustomer/:id`}
          component={asyncComponent(() =>
            import("./projectMediaCustomer/crud")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectvisitingcustomer`}
          component={asyncComponent(() =>
            import("./projectVisitingCustomer/list")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectvisitingcustomer/:id`}
          component={asyncComponent(() =>
            import("./projectVisitingCustomer/crud")
          )}
          exact={true}
        />
      </Switch>
    </Card>
  );
};

export default Booking;
