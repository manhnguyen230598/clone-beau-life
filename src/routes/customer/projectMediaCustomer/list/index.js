import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  // Input,
  Button,
  // Space,
  Tooltip,
  // Avatar,
  Upload,
  Card,
  Modal,
  message
} from "antd";
import {
  // SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  UploadOutlined,
  FileExcelOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import {  union, cloneDeep } from "lodash";
import ProjectMediaCustomerDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import ProjectSelect from "../../../../components/Select/Project/ListProject";
// const { Link } = router;
const UpLoadFile = Upload;
const tranformProjectMediaCustomerParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "projectMediaCustomer";
const ProjectMediaCustomerList = props => {
  const [form] = Form.useForm();
  // const searchInput = useRef();
  const actionRef = useRef();
  const [isShowModal, setIsShowModal] = useState(false);
  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  // const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState({
    id: { fixed: "left" },
    saleName: { fixed: "left" },
    projectId: { fixed: "left" },
    name: { fixed: "left" },
    phone: { fixed: "left" },
    address: { fixed: "left" },
    email: { fixed: "left" },
    facebook: { fixed: "left" },
    comments: { fixed: "left" }
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;
  const fetchData = () => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformProjectMediaCustomerParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ id: "desc" }]
            : initParams.sort
          : [{ id: "desc" }],
        queryInput: {
          ...initParams.filters
          // ["projectId"]: props.projectId,
        },
        populate: "projectId"
      }
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformProjectMediaCustomerParams(
      filters,
      formValues,
      sorter
    );

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ id: "desc" }]);
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", 'false');
          values = {};
        }
        if (typeof values.projectId == "object") {
          values.projectId = values.projectId?.id;
        }
        const initParams = tranformProjectMediaCustomerParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(fieldsValue);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ id: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  // const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
  //   setListSearch({
  //     ...listSearch,
  //     [`search_${dataIndex}`]: selectedKeys[0]
  //   });
  //   confirm();
  // };

  // const getColumnSearchProps = dataIndex => ({
  //   filterDropdown: ({
  //     setSelectedKeys,
  //     selectedKeys,
  //     confirm,
  //     clearFilters
  //   }) => (
  //     <div style={{ padding: 8 }}>
  //       <Input
  //         ref={searchInput}
  //         placeholder={`Tìm kiếm ${dataIndex}`}
  //         value={selectedKeys[0]}
  //         onChange={e =>
  //           setSelectedKeys(e.target.value ? [e.target.value] : [])
  //         }
  //         onPressEnter={() =>
  //           handleSearchFilter(selectedKeys, confirm, dataIndex)
  //         }
  //         style={{ width: 188, marginBottom: 8, display: "block" }}
  //       />
  //       <Space>
  //         <Button
  //           type="primary"
  //           onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
  //           icon={<SearchOutlined />}
  //           size="small"
  //           style={{ width: 90 }}
  //         >
  //           Tìm
  //         </Button>
  //         <Button
  //           onClick={() => handleReset(clearFilters, confirm, dataIndex)}
  //           size="small"
  //           style={{ width: 90 }}
  //         >
  //           Reset
  //         </Button>
  //       </Space>
  //     </div>
  //   ),
  //   filterIcon: filtered => (
  //     <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
  //   ),
  //   onFilterDropdownVisibleChange: visible => {
  //     if (visible) {
  //       setTimeout(() => searchInput.current.select());
  //     }
  //   },
  //   onFilter: (value, record) => record
  // });

  // const handleReset = (clearFilters, confirm, dataIndex) => {
  //   clearFilters();
  //   setListSearch({
  //     ...listSearch,
  //     [`search_${dataIndex}`]: ""
  //   });
  //   confirm();
  // };

  const handleAddClick = () => {
    history.push({
      pathname: `${props.match.url}/add`,
      state: { projectId: props.projectId }
    });
  };

  const handleEditClick = item => {
    history.push({
      pathname: `${props.match.url}/${item.id}`,
      state: { projectId: props.projectId }
    });
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Dự án",
      dataIndex: "projectId",
      width: 150,
      fixed: "left",
      hideInSearch: false,
      // hideInTable : true,
      render: val => {
        return (
          <span>
            {val ? val.name : ""}
          </span>
        );
      },
      renderFormItem: (_, { type, defaultRender, ...rest }, form) => {
        if (type === "form") {
          return null;
        }
        return <ProjectSelect mode="radio" />;
        // return defaultRender(_);
      }
    },
    {
      title: "Tên khách",
      dataIndex: "name",
      width: 120,
      hideInSearch: false,
      render: (val, record) => {
        return (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start"
            }}
          >
            <div style={{ textTransform: "uppercase" }}>
              {/* <Link
                to={{
                  pathname: `/manage-project/media-customer/${record.id}`,
                  state: record.name
                }}
              > */}
                <span style={{ marginLeft: "5px" }}>{val}</span>
              {/* </Link> */}
            </div>
            <a
              style={{ fontSize: "12px", marginLeft: "5px" }}
              onClick={() => {
                setDrawerDetail({
                  visible: true,
                  record
                });
              }}
            >
              <Tooltip title="Chi tiết">
                <EyeOutlined />
              </Tooltip>
            </a>
          </div>
        );
      }
    },
    {
      title: "Tên người bán",
      dataIndex: "saleName",
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      width: 60,
      hideInSearch: false
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      width: 80,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Email",
      dataIndex: "email",
      width: 60,
      fixed: "left",
      hideInSearch: true
    },
    {
      title: "Facebook",
      dataIndex: "facebook",
      width: 60,
      fixed: "left",
      hideInSearch: true
    },
    {
      title: "Bình luận",
      dataIndex: "comments",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Thao tác",
      width: 100,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      )
    }
  ];
  const [fileList, setFileList] = useState([]);
  const onSubmitImport = value => {
    const data = cloneDeep(value);
    // if (props.projectId) {
    //   data.projectId = props.projectId;
    // }
    if(data.projectId && typeof data.projectId === "object"){
      data.projectId = data.projectId.id;
    }
    if (fileList.length > 0) {
      data.fileExcel = fileList;
    }
    if(!data.projectId){
      delete data.projectId;
    }
    dispatch({
      type: `${RESOURCE}/importExcel`,
      payload: data,
      callback: response => {
        if (response.status === 200) {
          fetchData();
        }
      }
    });
  };

  const uploadProps = {
    maxCount: 1,
    onRemove: file => {
      setFileList(fileList => {
        const index = fileList.indexOf(file);
        const newFileList = fileList.slice();
        newFileList.splice(index, 1);
        return newFileList;
      });
    },
    beforeUpload: file => {
      if (fileList.length > 0) {
        message.warn("Tối đa một file");
        return;
      }
      setFileList(fileList => [...fileList, file]);
      return false;
    },
    fileList
  };
  return (
    <>
      <Form form={form} onFinish={onSubmitImport}>
        <Modal
          visible={isShowModal}
          onOk={() => form.submit()}
          onCancel={() => setIsShowModal(false)}
        >
          <Card
            title="Import file Excel danh sách khách hàng từ mạng xã hội"
            // className={styles.cardCheckCodeResult}
          >
            <div style={{ textAlign: "center" }}>
              <div>
                <p>
                  Tải file mẫu{" "}
                  <a href={"./static/templates/import-customer.xlsx"} download>
                    tại đây
                  </a>
                </p>
              </div>
              <Form.Item name="fileExcel">
                <UpLoadFile {...uploadProps}>
                  <Button icon={<UploadOutlined />}>Upload</Button>
                </UpLoadFile>
              </Form.Item>
              <Form.Item name="projectId" title={"Dự án"}>
                <ProjectSelect mode="radio" />
              </Form.Item>
              <p style={{ color: "red" }}>
                * Dữ liệu sẽ được lấy từ sheet "List MKT"
              </p>
            </div>
          </Card>
        </Modal>
      </Form>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Khách hàng từ Mạng xã hội"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
          >
            {`Thêm mới`}
          </Button>,
          <Button
            icon={<FileExcelOutlined />}
            type="default"
            onClick={() => setIsShowModal(true)}
          >
            {`Import excel`}
          </Button>
        ]}
      />
      <ProjectMediaCustomerDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ projectMediaCustomer, loading }) => ({
  projectMediaCustomer,
  loading: loading.models.projectMediaCustomer
}))(ProjectMediaCustomerList);
