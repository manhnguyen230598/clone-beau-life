
import React from 'react';
import { Image, Drawer, Row, Col } from 'antd';
import TableStatus from 'packages/pro-table/component/status';
import get from 'lodash/get';
import * as enums from 'util/enums';
import { formatDate } from 'util/helpers';

const DescriptionItem = ({ title, content }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const ImagesItem = ({ title, images }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <div>
      {images.map(item => (
        <Image width={80} src={item.url} />
      ))}
    </div>
  </div>
);

const DocumentProjectDetail = ({ record, drawerVisible, onChange }) => {
  const triggerChange = (v) => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };
/*   const genderStatus = get(enums.genderEnum[get(record, "gender", "-")], "status", null);
  const Status = TableStatus[genderStatus || 'Init'];
  const genderText = get(enums.genderEnum[get(record, "gender", "-")], "text", "-"); */

  return (
    <Drawer
      width={640}
      style={{ width: "640px !important" }}
      className="nv-drawer-detail"
      title={<span style={{ color: "#f09b1b" }}>{`Chi tiết ${get(record, "name", "-").toUpperCase()}`}</span>}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="ID" content={get(record, "id" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tên tài liệu" content={get(record, "name" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Loại tài liệu" content={get(record, "type" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Link tài liệu" content={get(record, "link" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        {/* <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Hình ảnh" content={get(record, "thumbnail" , "-" )[0]?.url} />
            <div>Ảnh thumbnail:</div>
           <Image src={get(record, "thumbnail" , "-" )[0]?.url}></Image>
        </Col> */}
        {/* <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Dự án" content={get(record, "projectId" , "-" )?.name} />
        </Col> */}
      </Row>
    </Drawer>
  );
};

export default DocumentProjectDetail;
