import React, { useEffect, useState } from "react";
import {
  Card,
  Button,
  Form,
  Col,
  Row,
  Popover,
  message,
  Popconfirm
} from "antd";
import _ from "lodash";
import { CloseCircleOutlined } from "@ant-design/icons";
import { connect } from "dva";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import FooterToolbar from "packages/FooterToolbar";
import Upload from "components/Upload";
import DocumentProjectCreate from "./components/DocumentProjectCreate";
import UploadFile from "components/UploadFile";
import UserSelect from "components/Select/User/ListUser";
import IntlMessages from "src/util/IntlMessages";
/* import * as enums from "util/enums";
import { camelCaseToDash } from "util/helpers"; */
import ProjectAreaSelect from "../../../../components/Select/ProjectArea/ListProjectArea";
import ProvincesSelect from "../../../../components/Select/Provinces";
import DistrictsSelect from "../../../../components/Select/Districts";
import WardSelect from "../../../../components/Select/Wards";
import * as projectServices from "src/services/project";
const RESOURCE = "project";
const fieldLabels = {
  type: "Phân loại",
  thumbnail: "Ảnh",
  description: "Mô tả",
  projectAreaIds: "Khu vực",
  images: "Thư viện ảnh",
  isHot: "Dự án hot",
  isActive: "Trạng thái",
  adminId: "Giám đốc dự án",
  isViewAll: "Xem tất cả",
  sequence: "Thứ tự",
  refLinkWeb: "Website",
  name: "Tên dự án",
  documents: "Tài liệu dự án",
  // adminId : "Giám đốc dự án",
  kskdUserId: "Kiểm soát kinh doanh"
};
const ProjectForm = ({
  project: { formTitle, formData },
  dispatch,
  submitting,
  match: { params },
  history,
  ...rest
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState("100%");
  // const [isHot, setIsHot] = useState("100%");
  const [uid, setUid] = useState(Date.now());
  const [userInfo, setUserInfo] = useState({});

  const getErrorInfo = errors => {
    const errorCount = errors.filter(item => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = fieldKey => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map(err => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li
          key={key}
          className="nv-error-list-item"
          onClick={() => scrollToField(key)}
        >
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={trigger => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = values => {
    setError([]);
    const data = _.cloneDeep(values);
    if (data.adminId && typeof data.adminId == "object") {
      data.adminId = data.adminId.id;
    }
    if (data.kskdUserId && typeof data.kskdUserId == "object") {
      data.kskdUserId = data.kskdUserId.id;
    }
    if (data.adminId2 && typeof data.adminId2 == "object") {
      data.adminId2 = data.adminId2.id;
    }
    if (data.adminId3 && typeof data.adminId3 == "object") {
      data.adminId3 = data.adminId3.id;
    }
    if (data.adminId4 && typeof data.adminId4 == "object") {
      data.adminId4 = data.adminId4.id;
    }
    if (data.adminId5 && typeof data.adminId5 == "object") {
      data.adminId5 = data.adminId5.id;
    }
    if (data.kskdUserId2 && typeof data.kskdUserId2 == "object") {
      data.kskdUserId2 = data.kskdUserId2.id;
    }
    if (data.images && Array.isArray(data.images)) {
      data.images = data.images.map(e =>
        e.url ? e.url : e.response.url ? e.response.url : ""
      );
    }
    if (data.thumbnail) {
      data.thumbnail =
        data.thumbnail && data.thumbnail.length > 0
          ? data.thumbnail
              .map(i => i.url || (i.response ? i.response.url : ""))
              .toString()
          : "";
    }
    if (data.provinceId) {
      data.provinceId = data.provinceId.value;
    }
    if (data.districtId) {
      data.districtId = data.districtId.value;
    }
    if (data.wardId) {
      data.wardId = data.wardId.value;
    }

    if (data.logoLinkWeb) {
      data.logoLinkWeb =
        data.logoLinkWeb && data.logoLinkWeb.length > 0
          ? data.logoLinkWeb
              .map(i => i.url || (i.response ? i.response.url : ""))
              .toString()
          : "";
    }
    if (data.logoInvestor) {
      data.logoInvestor =
        data.logoInvestor && data.logoInvestor.length > 0
          ? data.logoInvestor
              .map(i => i.url || (i.response ? i.response.url : ""))
              .toString()
          : "";
    }
    if (data.projectAreaIds && Array.isArray(data.projectAreaIds)) {
      let temp = [];
      data.projectAreaIds.map(e => {
        if (typeof e == "object") {
          temp.push(e.id);
        } else {
          temp.push(e);
        }
      });
      data.projectAreaIds = temp;
    }
    if (
      data.templateCustomerFile &&
      typeof data.templateCustomerFile === "object"
    ) {
      data.templateCustomerFile = data.templateCustomerFile[0].url;
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: res => {
        if (res.error) {
          message.error(res.error.message);
        } else {
          if (params.id === "add") {
            history.goBack();
          } else {
            dispatch({
              type: `${RESOURCE}/loadForm`,
              payload: {
                type: params.id !== "add" ? "E" : "A",
                id: params.id !== "add" ? params.id : null
              }
            });
          }

          // history.goBack();
          // dispatch(routerRedux.push({ pathname: `/base/${camelCaseToDash(RESOURCE)}` }));
        }
      }
    });
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };
  const handleClone = async () => {
    let rs = await projectServices.cloneProject({ id: params.id });
    message.success("Nhân bản dự án thành công.");
  };

  useEffect(() => {
    let userInfo = localStorage.getItem("user_id");
    if (userInfo) {
      userInfo = JSON.parse(userInfo);
      console.log(userInfo);
      setUserInfo(userInfo);
    }
    setUserInfo(userInfo);
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
    setUid(Date.now());
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll(".ant-layout-sider")[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    }
    window.addEventListener("resize", resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener("resize", resizeFooterToolbar);
    };
  });

  if (
    params.id === "add" ||
    (formData && formData.id && formData.id !== "add")
  ) {
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
        // preserve={false}
      >
        <Card title="Thông tin cơ bản" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col xxl={14} xl={14} lg={14} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["name"]}
                name="name"
                rules={[{ required: true, message: "Tên không được trống" }]}
              >
                <FormInputRender
                  item={{ title: "Tên", dataIndex: "name" }}
                  placeHolder={`Nhập tên dự án`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={5} xl={5} lg={5} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["type"]}
                name="typeProject"
                rules={[{ required: true, message: "Loại không được trống" }]}
              >
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      resort: { text: "BĐS nghỉ dưỡng" },
                      house: { text: "BĐS nhà ở" }
                    },
                    hideInTable: false,
                    hideInSearch: false
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={5} xl={5} lg={5} md={24} sm={24} xs={24}>
              <Form.Item
                label={"Loại căn"}
                name="type"
                rules={[{ required: false, message: "Loại không được trống" }]}
              >
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      resort: { text: "Dự án thấp tầng" },
                      house: { text: "Dự án cao tầng" }
                    },
                    hideInTable: false,
                    hideInSearch: false
                  }}
                  disabled
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={16} xl={16} lg={16} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["description"]}
                name="description"
                rules={[{ required: true, message: "Mô tả không được trống" }]}
              >
                <FormInputRender
                  type="form"
                  item={{
                    title: "Mô tả dự án",
                    dataIndex: "defaultsTo",
                    valueType: "textarea"
                  }}
                  placeHolder={"Nhập mô tả dự án"}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={8} xl={8} lg={8} md={12} sm={24} xs={24}>
              <Form.Item
                label={"Số lượng căn"}
                name="amountApartment"
                rules={[{ required: true, message: "Mô tả không được trống" }]}
              >
                <FormInputRender
                  type="form"
                  item={{
                    title: "Mô tả dự án",
                    dataIndex: "defaultsTo",
                    valueType: "digit"
                  }}
                  placeHolder={"Nhập mô tả dự án"}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={3} xl={3} lg={3} md={12} sm={12} xs={12}>
              <Form.Item
                label={fieldLabels["isHot"]}
                name="isHot"
                rules={[{ required: false }]}
                valuePropName="checked"
              >
                <FormInputRender
                  item={{
                    title: "Hot",
                    dataIndex: "isHot",
                    valueType: "switch"
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>

            <Col xxl={3} xl={3} lg={3} md={12} sm={12} xs={12}>
              <Form.Item
                label={fieldLabels["isActive"]}
                name="isActive"
                rules={[{ required: false, message: "Loại không được trống" }]}
                valuePropName="checked"
              >
                <FormInputRender
                  item={{
                    title: "Kích hoạt",
                    dataIndex: "isActive",
                    valueType: "switch"
                  }}
                  placeHolder={`Nhập loại sản phẩm`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={3} xl={3} lg={3} md={12} sm={12} xs={12}>
              <Form.Item
                label={"Hiển thị trên app"}
                name="isViewAll"
                rules={[{ required: false }]}
                valuePropName="checked"
              >
                <FormInputRender
                  item={{
                    title: "isViewAll",
                    dataIndex: "isViewAll",
                    valueType: "switch"
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={10} xl={10} lg={10} md={12} sm={12} xs={12}>
              <Form.Item
                label={"Địa chỉ web liên kết"}
                name="refLinkWeb"
                rules={[
                  { required: false, message: "Địa chỉ không được trống" }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Kích hoạt"
                  }}
                  placeHolder={`Nhập địa chỉ`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={3} xl={3} lg={3} md={12} sm={12} xs={12}>
              <Form.Item
                label={"Logo web liên kết"}
                name="logoLinkWeb"
                rules={[
                  { required: false, message: "Địa chỉ không được trống" }
                ]}
              >
                <Upload multipe />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={12}>
              <Form.Item
                label={"Diện tích xây dựng"}
                name="areaLand"
                rules={[
                  {
                    required: true,
                    message: "Danh sách khu vực không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Kích hoạt"
                  }}
                  placeHolder={`Nhập thứ tự hiển thị`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={12}>
              <Form.Item
                label={"Thứ tự hiển thị"}
                name="sequence"
                rules={[
                  {
                    required: false,
                    message: "Danh sách khu vực không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Kích hoạt"
                  }}
                  placeHolder={`Nhập thứ tự hiển thị`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
              <Form.Item
                label={"Link facebook"}
                name="refLinkFacebook"
                rules={[
                  {
                    required: false,
                    message: "Danh sách khu vực không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Kích hoạt"
                  }}
                  placeHolder={`Nhập thứ tự hiển thị`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={<IntlMessages id="user.province" />}
                name="provinceId"
                rules={[
                  {
                    required: true,
                    // required: requiredMode,
                    message: "Tỉnh/thành phố không được trống"
                  }
                ]}
              >
                <ProvincesSelect
                  mode="simple"
                  // onSelect={(value) => {
                  //   console.log('value', value);
                  // }}
                />
              </Form.Item>
            </Col>
            <Col
              xl={{ span: 6, offset: 2 }}
              lg={{ span: 8 }}
              md={{ span: 12 }}
              sm={24}
            >
              <Form.Item
                label={<IntlMessages id="user.district" />}
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.provinceId !== curValues.provinceId
                }
              >
                {() => {
                  return (
                    <Form.Item
                      name="districtId"
                      rules={[
                        {
                          required: false,
                          message: "Quận/huyện không được trống"
                        }
                      ]}
                      noStyle
                    >
                      <DistrictsSelect
                        mode="simple"
                        province={
                          (form.getFieldValue("provinceId") || {}).key ||
                          formData.provinceId ||
                          ""
                        }
                      />
                    </Form.Item>
                  );
                }}
              </Form.Item>
            </Col>
            <Col
              xl={{ span: 8, offset: 2 }}
              lg={{ span: 10 }}
              md={{ span: 24 }}
              sm={24}
            >
              <Form.Item
                label={<IntlMessages id="user.ward" />}
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.districtId !== curValues.districtId
                }
              >
                {() => {
                  return (
                    <Form.Item
                      name="wardId"
                      rules={[
                        {
                          required: false,
                          message: "Phường không được trống"
                        }
                      ]}
                      noStyle
                    >
                      <WardSelect
                        mode="simple"
                        district={
                          (form.getFieldValue("districtId") || {}).key ||
                          formData.districtId ||
                          ""
                        }
                      />
                    </Form.Item>
                  );
                }}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={16} xl={16} lg={16} md={24} sm={24} xs={24}>
              <Form.Item
                label={"Địa chỉ dự án"}
                name="address"
                rules={[
                  { required: false, message: "Địa chỉ không được trống" }
                ]}
              >
                <FormInputRender
                  type="form"
                  item={{
                    title: "Mô tả sản phẩm",
                    dataIndex: "address",
                    valueType: "textarea"
                  }}
                  placeHolder={"Nhập địa chỉ dự án"}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
              <Form.Item
                label={"Giá thấp nhất"}
                name="minPriceText"
                rules={[
                  { required: false, message: "Địa chỉ không được trống" }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Mô tả sản phẩm",
                    dataIndex: "address"
                  }}
                  placeHolder={"Nhập giá căn hộ thấp nhất"}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            {/* <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
              <Form.Item
                label={"Template import dữ liệu khách hàng"}
                name="templateCustomerFile"
                //rules={[{ required: true, message: 'Danh mục không được để trống' }]}
              >
                <UploadFile />
              </Form.Item>
            </Col> */}
            {userInfo.roleId !== 19 ? (
              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Form.Item
                  label={"Giám đốc dự án"}
                  name="adminId"
                  //rules={[{ required: true, message: 'Danh mục không được để trống' }]}
                >
                  <UserSelect paramsList={{ roleId: 15 }} />
                </Form.Item>
              </Col>
            ) : (
              ""
            )}
            {userInfo.roleId !== 19 ? (
              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Form.Item
                  label={"Kiểm soát kinh doanh"}
                  name="kskdUserId"
                  //rules={[{ required: true, message: 'Danh mục không được để trống' }]}
                >
                  <UserSelect paramsList={{ roleId: 14 }} />
                </Form.Item>
              </Col>
            ) : (
              ""
            )}
            <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
              <Form.Item
                label={"Tình trạng dự án"}
                name="status"
                rules={[{ required: false, message: "Loại không được trống" }]}
              >
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      open: { text: "Đang mở bán" },
                      close: { text: "Chưa mở bán" },
                      done: { text: "Đã hoàn thành" }
                    },
                    hideInTable: false,
                    hideInSearch: false
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          {userInfo.roleId !== 19 ? (
            <Row gutter={24}>
              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Form.Item
                  label={"Giám đốc dự án 2"}
                  name="adminId2"
                  //rules={[{ required: true, message: 'Danh mục không được để trống' }]}
                >
                  <UserSelect paramsList={{ roleId: 15 }} />
                </Form.Item>
              </Col>
              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Form.Item
                  label={"Giám đốc dự án 3"}
                  name="adminId3"
                  //rules={[{ required: true, message: 'Danh mục không được để trống' }]}
                >
                  <UserSelect paramsList={{ roleId: 15 }} />
                </Form.Item>
              </Col>
              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Form.Item
                  label={"Giám đốc dự án 4"}
                  name="adminId4"
                  //rules={[{ required: true, message: 'Danh mục không được để trống' }]}
                >
                  <UserSelect paramsList={{ roleId: 15 }} />
                </Form.Item>
              </Col>
              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Form.Item
                  label={"Giám đốc dự án 5"}
                  name="adminId5"
                  //rules={[{ required: true, message: 'Danh mục không được để trống' }]}
                >
                  <UserSelect paramsList={{ roleId: 15 }} />
                </Form.Item>
              </Col>
              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Form.Item
                  label={"Kiểm soát kinh doanh 2"}
                  name="kskdUserId2"
                  //rules={[{ required: true, message: 'Danh mục không được để trống' }]}
                >
                  <UserSelect paramsList={{ roleId: 14 }} />
                </Form.Item>
              </Col>
            </Row>
          ) : (
            ""
          )}
        </Card>
        <Card title="Thông tin chủ đầu tư" className="gx-card" bordered={false}>
          <Row>
            <Col xxl={6} xl={6} lg={6} md={24} sm={24} xs={24}>
              <Form.Item
                label={"Tên chủ đầu tư"}
                name="investor"
                rules={[
                  { required: false, message: "Chủ đầu tư không được trống" }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Mô tả sản phẩm",
                    dataIndex: "short"
                  }}
                  placeHolder={"Nhập chủ đầu tư dự án"}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={16} xl={16} lg={16} md={24} sm={24} xs={24} offset={1}>
              <Form.Item
                label={"Mô tả chủ đầu tư"}
                name="investorDescription"
                rules={[
                  { required: false, message: "Chủ đầu tư không được trống" }
                ]}
              >
                <FormInputRender
                  type="form"
                  item={{
                    title: "Mô tả sản phẩm",
                    dataIndex: "investorDescription",
                    valueType: "textarea"
                  }}
                  placeHolder={"Nhập chủ đầu tư dự án"}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
              <Form.Item label={"Logo"} name="logoInvestor">
                <Upload multipe />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={[16, 16]}>
            <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={12}>
              <Form.Item
                label={"Công ty phát triển"}
                name="developmentCompany"
                rules={[
                  {
                    required: false,
                    message: "Danh sách khu vực không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Kích hoạt"
                  }}
                  placeHolder={`Nhập thứ tự hiển thị`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={12}>
              <Form.Item
                label={"Công ty quản lý"}
                name="managementCompany"
                rules={[
                  {
                    required: false,
                    message: "Danh sách khu vực không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Kích hoạt"
                  }}
                  placeHolder={`Nhập thứ tự hiển thị`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={12}>
              <Form.Item
                label={"Quy mô dự án"}
                name="projectScale"
                rules={[
                  {
                    required: false,
                    message: "Danh sách khu vực không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Kích hoạt"
                  }}
                  placeHolder={`Nhập thứ tự hiển thị`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Ảnh" className="gx-card" bordered={false}>
          <Row>
            <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["thumbnail"]}
                name="thumbnail"
                required={true}
              >
                <Upload multipe />
              </Form.Item>
            </Col>
            <Col xxl={16} xl={16} lg={16} md={24} sm={24} xs={24}>
              <Form.Item label={fieldLabels["images"]} name="images">
                <Upload multiple={true} />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Tài liệu dự án" className="gx-card" bordered={false}>
          <Form.Item name="documents">
            <DocumentProjectCreate
              name={`documents-${formData.id || "add"}-${uid}`}
            />
          </Form.Item>
        </Card>

        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={submitting}
          >
            {params.id === "add" ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          {params.id !== "add" ? (
            <Popconfirm
              title="Bạn có chắc muốn nhân bản dự án này?"
              onConfirm={() => {
                handleClone();
              }}
            >
              <Button>Nhân bản</Button>
            </Popconfirm>
          ) : null}
          <Button
            type="default"
            style={{ color: "#fa5656" }}
            onClick={() => {
              history.goBack();
            }}
          >
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ project, loading, router }) => ({
  submitting: loading.effects["project/submit"],
  project,
  router
}))(ProjectForm);
