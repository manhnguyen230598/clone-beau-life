import React, { useState, useEffect } from "react";
import { Collapse, Form, Input, Switch, Row, Col, Button } from "antd";
import {
  CaretRightOutlined,
  DragOutlined,
  StopOutlined,
  CloseOutlined,
  PlusOutlined,
  CheckCircleOutlined,
  DownCircleOutlined
} from "@ant-design/icons";
import _ from "lodash";
import Upload from "components/Upload";
import UploadFile from "components/UploadFile";

const { Panel } = Collapse;
const schema = {
  jsonSchema: {
    title: "promotions",
    description: "",
    type: "object",
    required: [],
    properties: {
      name: {
        type: "string",
        title: "Tên tài liệu",
        default: "",
        maxLength: 100000
      },
      type: {
        type: "string",
        title: "Loại tài liệu",
        default: "",
        maxLength: 100000
      },
      link: {
        type: "string",
        title: "Link",
        default: "",
        maxLength: 100000
      },
      thumbnail: {
        type: "array",
        title: "Ảnh đại diện",
        default: [],
        maxFileSize: 10,
        maxFileNum: 10,
        items: {
          type: "string",
          format: "data-url"
        },
        uniqueItems: true
      },
      sequence: {
        type: "string",
        title: "Thứ tự",
        default: "",
        maxLength: 100000
      },
      shortDescription: {
        type: "string",
        title: "Mô tả ngắn",
        default: "",
        maxLength: 100000
      }
    }
  },
  uiSchema: {
    name: {
      "ui:placeholder": "",
      "ui:help": "",
      "ui:disabled": false
    },
    type: {
      "ui:placeholder": "",
      "ui:help": "",
      "ui:disabled": false
    },
    link: {
      "ui:widget": "RcImage",
      "ui:options": {
        exampleImageUrl: "",
        label: "Pictures upload",
        listType: "picture",
        uploadType: "picture",
        vertical: true,
        accept: "image/*"
      },
      "ui:help": "",
      "ui:disabled": false
    },
    thumbnail: {
      "ui:widget": "RcImage",
      "ui:options": {
        exampleImageUrl: "",
        label: "Pictures upload",
        listType: "picture",
        uploadType: "picture",
        vertical: true,
        accept: "image/*"
      },
      "ui:help": "",
      "ui:disabled": false
    },
    sequence: {
      "ui:widget": "textarea",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
    shortDescription: {
      "ui:widget": "collapse",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    }
  },
  formData: {
    name: "",
    type: true,
    link: "",
    linkPdf: [],
    linkVideo: "",
    thumbnail: [],
    sequence: 0,
    isActive: true,
    shortDescription: ""
  },
  bizData: {},
  sequence: [
    "name",
    "type",
    "link",
    "linkPdf",
    "linkVideo",
    "thumbnail",
    "sequence",
    "isActive",
    "shortDescription"
  ]
};

function callback(key) {
  console.log("Collapse -> callback -> key: ", key);
}

const genExtra = ({
  field,
  index,
  addGoden,
  removeGoden,
  godenChange,
  godenOff
}) => (
  <>
    <PlusOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        addGoden();
      }}
    />
    <DragOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
      }}
    />
    {field.status ? (
      <StopOutlined
        className="nv-icon-action"
        style={{ marginRight: "5px" }}
        onClick={event => {
          // If you don't want click extra trigger collapse, you can prevent this:
          event.stopPropagation();
          godenOff(index);
        }}
      />
    ) : (
      <CheckCircleOutlined
        className="nv-icon-action"
        style={{ marginRight: "5px" }}
        onClick={event => {
          // If you don't want click extra trigger collapse, you can prevent this:
          event.stopPropagation();
          godenOff(index);
        }}
      />
    )}
    <CloseOutlined
      className="nv-icon-action"
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        removeGoden(field, index);
      }}
    />
  </>
);

const CollapseItem = ({ value, onChange, name = "collapse" }) => {
  const [godens, setGodens] = useState(value || []);
  const uid = "";
  console.log(
    `🚀 ~ file: DocumentProjectCreate.js ~ line 189 ~ CollapseItem ~ godens`,
    godens
  );

  useEffect(() => {
    if (Array.isArray(value)) {
      setGodens(value);
    }
  }, [value]);

  const godenChange = (e, index) => {
    const curName = e.target.name;
    console.log(
      `🚀 ~ file: DocumentProjectCreate.js ~ line 195 ~ godenChange ~ curName`,
      curName
    );
    const curVal = e.target.value;
    console.log(
      `🚀 ~ file: DocumentProjectCreate.js ~ line 197 ~ godenChange ~ curVal`,
      curVal
    );
    setGodens(origin => {
      let arr = _.cloneDeep(origin || []);
      schema.sequence.forEach(f => {
        if (curName === f) {
          arr[index][f] = curVal;
        }
      });
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const godenOff = index => {
    setGodens(origin => {
      let arr = _.cloneDeep(origin || []);

      arr[index]["status"] = !arr[index]["status"];
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const addGoden = e => {
    // e.preventDefault();
    let arr = _.cloneDeep(godens || []);
    arr.push({
      // isNew: true,
      ...schema.formData
    });
    setGodens(arr);
  };

  const removeGoden = (id, index) => {
    setGodens(origin => {
      let arr = _.cloneDeep(godens || []);
      arr.splice(index, 1);
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  return (
    <>
      {godens && godens.length == 0 && (
        <Button
          type="dashed"
          icon={<PlusOutlined />}
          onClick={() => {
            addGoden();
          }}
          style={{ marginBottom: "5px" }}
        >
          Thêm tài liệu
        </Button>
      )}
      <Collapse
        onChange={callback}
        expandIconPosition={"left"}
        bordered={false}
        defaultActiveKey={["0"]}
        expandIcon={({ isActive }) => (
          <CaretRightOutlined rotate={isActive ? 90 : 0} />
        )}
        className="nv-custom-collapse"
      >
        {godens.map((field, index) => {
          return (
            <Panel
              header={field.name || index + 1}
              key={index}
              extra={genExtra({
                field,
                index,
                addGoden,
                removeGoden,
                godenChange,
                godenOff
              })}
              className="nv-custom-panel"
            >
              <Row gutter={[16]}>
                <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
                  <Form.Item
                    label={schema.jsonSchema.properties["name"]["title"]}
                    name={[`${name}-${index}-${uid}`, "name"]}
                    rules={[{ required: false, message: `Không được trống` }]}
                    initialValue={field["name"]}
                  >
                    <Input
                      name={"name"}
                      onChange={e => godenChange(e, index)}
                    />
                  </Form.Item>
                </Col>
                <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
                  <Form.Item
                    label={schema.jsonSchema.properties["sequence"]["title"]}
                    name={[`${name}-${index}-${uid}`, "sequence"]}
                    rules={[{ required: false, message: `Không được trống` }]}
                    initialValue={field["sequence"]}
                  >
                    <Input
                      name={"sequence"}
                      onChange={e => godenChange(e, index)}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={[16]}>
                <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
                  <Form.Item
                    label={schema.jsonSchema.properties["type"]["title"]}
                    name={[`${name}-${index}-${uid}`, "type"]}
                    rules={[{ required: true, message: `Không được trống` }]}
                    initialValue={field["type"]}
                    valuePropName="checked"
                  >
                    <Switch
                      checkedChildren="pdf"
                      unCheckedChildren="video"
                      onChange={val => {
                        godenChange(
                          {
                            target: {
                              name: "type",
                              value: val
                            }
                          },
                          index
                        );
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
                  <Form.Item
                    label={schema.jsonSchema.properties["link"]["title"]}
                    // label={[`${name}-${index}-${uid}`, 'linkPdf']}
                    rules={[{ required: true, message: `Không được trống` }]}
                    help={
                      <>
                        <small>
                          (*) Tài liệu chỉ nhận file pdf hoặc link video
                        </small>
                        <div>
                          <a
                            className="gx-ml-3"
                            href={`/static/huong_dan_lay_link_you_tube.docx`}
                            download
                          >
                            <DownCircleOutlined />
                            &nbsp;Hướng dẫn lấy link youtube
                          </a>
                        </div>
                      </>
                    }
                    shouldUpdate={(prevValues, curValues) =>
                      prevValues.type !== curValues.type
                    }
                  >
                    {({ getFieldValue }) => {
                      const type = getFieldValue([
                        `${name}-${index}-${uid}`,
                        "type"
                      ]);
                      if (type) {
                        return (
                          <Form.Item
                            name={[`${name}-${index}-${uid}`, "linkPdf"]}
                            initialValue={field["linkPdf"]}
                          >
                            <UploadFile
                              autoUpload={true}
                              accept=".pdf"
                              onChange={val => {
                                godenChange(
                                  {
                                    target: {
                                      name: "linkPdf",
                                      value: val
                                    }
                                  },
                                  index
                                );
                              }}
                            />
                          </Form.Item>
                        );
                      }
                      return (
                        <Form.Item
                          name={[`${name}-${index}-${uid}`, "linkVideo"]}
                          initialValue={field["linkVideo"]}
                        >
                          <Input
                            name={`linkVideo`}
                            onChange={e => godenChange(e, index)}
                          />
                        </Form.Item>
                      );
                    }}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={[16]}>
                <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
                  <Form.Item
                    label={
                      schema.jsonSchema.properties["shortDescription"]["title"]
                    }
                    name={[`${name}-${index}-${uid}`, "shortDescription"]}
                    rules={[{ required: false, message: `Không được trống` }]}
                    initialValue={field["shortDescription"]}
                  >
                    <Input.TextArea
                      name={"shortDescription"}
                      onChange={e => godenChange(e, index)}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Panel>
          );
        })}
      </Collapse>
    </>
  );
};

export default CollapseItem;
