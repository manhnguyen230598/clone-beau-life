import React, { useState, useEffect, useRef } from "react";
import { Form, Input, Button, Space, Tooltip } from "antd";
import {
  SearchOutlined,
  EditOutlined,
  EyeOutlined,
  PlusOutlined
} from "@ant-design/icons";
import { connect, router } from "dva";
import { union } from "lodash";
import ProTable from "packages/pro-table/Table";
import ProjectDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import StackingPlan from "../components/StackingPlan";

const tranformClassParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};
const { Link } = router;
const RESOURCE = "project";
const ProjectList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();
  const tableChange = getTableChange(RESOURCE);
  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  const [formValues, setFormValues] = useState({});
  // const [skip] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [userInfo, setUserInfo] = useState({});
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState({
    // id: { show: false }
  });
  const [isAddStackingPlan, setIsAddStackingPlan] = useState({
    isShow: false,
    projectId: null,
    projectName: ""
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;

  useEffect(() => {
    let userInfo = localStorage.getItem("user_id");
    if (userInfo) {
      userInfo = JSON.parse(userInfo);
      console.log(userInfo);
      setUserInfo(userInfo);
    }
    setUserInfo(userInfo);
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformClassParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: union(initParams.sort || [], [{ id: "desc" }]),
        queryInput: {
          ...initParams.filters
        }
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    console.log(`🚀 ~ file: index.js ~ line 110 ~ useEffect ~ pagi`, pagi);
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    console.log(
      `🚀 ~ file: index.js ~ line 100 ~ handleTableChange ~ pagi`,
      pagi
    );
    console.log(
      `🚀 ~ file: index.js ~ line 99 ~ handleTableChange ~ sorter`,
      sorter
    );
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformClassParams(filters, formValues, sorter);
    console.log(
      `🚀 ~ file: index.js ~ line 106 ~ handleTableChange ~ initParams`,
      initParams
    );

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      console.log("initParams.sort", initParams.sort);
      console.log(
        "(initParams.sort || []).filter(i => !i.createdAt)",
        union(initParams.sort || [], [{ createdAt: "desc" }])
      );
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagi.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        ...params
        // populate: "agencyId"
      }
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", 'false');
          values = {};
        }
        const initParams = tranformClassParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );
        props.history.push({
          search: "?" + new URLSearchParams(values).toString()
        });
        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters
            },
            page: 1,
            current: 1,
            sort: union(initParams.sort || [], [{ id: "desc" }]),
            skip: 0,
            limit,
            pageSize: limit
            // populate: "projectId,agencyId"
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  };

  const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  };

  const handleStackPlanClick = item => {
    setIsAddStackingPlan({
      isShow: true,
      projectId: item.id,
      projectName: item.name,
      type: item.type
    });
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 60,
      fixed: "left",
      hideInSearch: true
    },
    {
      title: "Tên",
      dataIndex: "name",
      width: 230,
      render: (val, record) => {
        if(userInfo.roleId === 19) return <span>{val}</span>
        return (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start"
            }}
          >
            <div style={{ textTransform: "uppercase" }}>
              <Link
                to={{
                  pathname: `/manage-project/project-detail/${record.id}`,
                  state: record.name
                }}
              >
                <span style={{ marginLeft: "5px" }}>{val}</span>
              </Link>
            </div>
            <a
              style={{ fontSize: "12px", marginLeft: "5px" }}
              onClick={() => {
                setDrawerDetail({
                  visible: true,
                  record
                });
              }}
            >
              <Tooltip title="Chi tiết">
                <EyeOutlined />
              </Tooltip>
            </a>
          </div>
        );
      },
      ...getColumnSearchProps("name")
    },
    {
      title: "Ảnh đại diện",
      dataIndex: "thumbnail",
      width: 100,
      hideInSearch: true,
      render: val => {
        return (
          <img
            style={{
              width: "100%",
              borderRadius: 10
            }}
            src={Array.isArray(val) ? val[0].url : ""}
          />
        );
      }
    },
    {
      title: "Thư viện ảnh",
      dataIndex: "images",
      width: 80,
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: "Mô tả",
      dataIndex: "description",
      width: 200,
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: "Trạng thái",
      dataIndex: "isActive",
      width: 130,
      filters: true,
      valueEnum: {
        true: {
          text: "Hoạt động",
          status: "Processing",
          color: "#ec3b3b",
          isText: "true"
        },
        false: {
          text: "Dừng",
          status: "Default",
          color: "#ec3b3b",
          isText: "true"
        }
      }
    },
    {
      title: "Loại dự án",
      dataIndex: "typeProject",
      width: 180,
      filters: true,
      hideInSearch: false,
      hideInTable: false,
      valueEnum: {
        resort: {
          text: "BĐS nghỉ dưỡng",
          color: "#ec3b3b",
          isText: "true"
        },
        house: { text: "BĐS nhà ở", color: "#ec3b3b", isText: "true" }
      }
    },
    {
      title: "Tình trạng",
      dataIndex: "status",
      width: 180,
      filters: true,
      hideInSearch: false,
      hideInTable: false,
      valueEnum: {
        open: {
          text: "Đang mở bán",
          color: "#ec3b3b",
          isText: "true"
        },
        close: { text: "Chưa mở bán", color: "#ec3b3b", isText: "true" },
        done: { text: "Đã hoàn thành", color: "#ec3b3b", isText: "true" }
      }
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdAt",
      valueType: "dateTime",
      width: 150
    },

    {
      title: "Website",
      dataIndex: "refLinkWeb",
      width: 150,
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: "Khu vực quản lý",
      dataIndex: "projectAreaIds",
      width: 150,
      hideInTable: true,
      hideInSearch: true
    },
    {
      title: "Thứ tự",
      dataIndex: "sequence",
      width: 50,
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Thao tác",
      width: 100,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
          {/* <Divider type="vertical" />
          <Tooltip title={`Thông tin bảng hàng`}>
            <DotChartOutlined onClick={() => handleStackPlanClick(record)} />
          </Tooltip> */}
        </>
      )
    }
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Các dự án"
        actionRef={actionRef}
        formRef={form}
        form={{
          initialValues: formValues
        }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
            disabled = {userInfo?.roleId === 19}
          >
            {`Thêm mới`}
          </Button>
        ]}
      />
      <ProjectDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
      <StackingPlan
        isAddStackingPlan={isAddStackingPlan.isShow}
        type={isAddStackingPlan.type}
        projectId={isAddStackingPlan.projectId}
        projectName={isAddStackingPlan.projectName}
        setIsAddStackingPlan={setIsAddStackingPlan}
      />
    </>
  );
};

export default connect(({ project, loading }) => ({
  project,
  loading: loading.models.project
}))(ProjectList);
