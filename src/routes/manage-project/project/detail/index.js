import React from "react";
import { Image, Drawer, Row, Col } from "antd";
import TableStatus from "packages/pro-table/component/status";
import get from "lodash/get";
import * as enums from "util/enums";
import { formatDate } from "util/helpers";

const DescriptionItem = ({ title, content }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const ImagesItem = ({ title, images }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <div>
      {images.map(item => (
        <Image width={80} src={item.url} />
      ))}
    </div>
  </div>
);

const ProjectDetail = ({ record, drawerVisible, onChange }) => {
  console.log(record);
  const triggerChange = v => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };
  /*   const genderStatus = get(enums.genderEnum[get(record, "gender", "-")], "status", null);
  const Status = TableStatus[genderStatus || 'Init'];
  const genderText = get(enums.genderEnum[get(record, "gender", "-")], "text", "-"); */

  return (
    <Drawer
      width={640}
      title={
        <span style={{ color: "#f09b1b" }}>{`Chi tiết ${get(
          record,
          "name",
          "-"
        ).toUpperCase()}`}</span>
      }
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Row gutter={16}>
        <Col xxl={6} xl={6} lg={6} md={12} sm={24} xs={24}>
          <DescriptionItem title="ID" content={get(record, "id", "-")} />
        </Col>
        <Col xxl={18} xl={18} lg={18} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tên" content={get(record, "name", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={24} xl={24} lg={24} md={12} sm={24} xs={24}>
          <ImagesItem
            title="Ảnh đại diện"
            images={get(record, "thumbnail", [])}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={24} xl={24} lg={24} md={12} sm={24} xs={24}>
          <ImagesItem title="Thư viện ảnh" images={get(record, "images", [])} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem
            title="Mô tả"
            content={get(record, "description", "-")}
          />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem
            title="Chủ đầu tư"
            content={get(record, "investor", "-")}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem
            title="Trạng thái"
            content={get(record, "isActive", "-")}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          {/* <DescriptionItem title="Ngày tạo" content={get(record, "createdAt" , "-" )} /> */}
        </Col>
      </Row>
    </Drawer>
  );
};

export default ProjectDetail;
