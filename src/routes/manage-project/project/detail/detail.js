import React, { useEffect, useState } from "react";
import { Card, Tabs } from "antd";
import { router } from "dva";
import get from "lodash/get";
import asyncComponent from "util/asyncComponent";
import IntlMessages from "util/IntlMessages";
import * as projectServices from "services/project";

const TabPane = Tabs.TabPane;
const { Redirect, Switch, Route } = router;
const ProjectDetailRoute = ({
  component: Component,
  projectId,
  templateCustomerFile,
  projectType,
  projectName,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => (
      <Component
        {...props}
        projectType={projectType}
        templateCustomerFile={templateCustomerFile}
        projectId={Number(projectId)}
        projectName={projectName}
      />
    )}
  />
);

const ProjectInSide = ({ match, location, history, ...rest }) => {
  const [projectInfo, setProjectInfo] = useState(null);
  const [user, setUser] = useState();
  useEffect(() => {
    projectServices.get(Number(match.params.id), {}, false).then(res => {
      setProjectInfo(get(res, "data.data[0]", {}));
    });
    const currentUser = JSON.parse(localStorage.getItem("user_id"));
    setUser(currentUser);
  }, [match.params.id]);

  if (!projectInfo) {
    return null;
  }

  function callback(key) {
    switch (key) {
      case "documentProject":
        history.push({ pathname: `${match.url}/documentProject` });
        break;
      case "projectMediaCustomer":
        history.push({ pathname: `${match.url}/projectMediaCustomer` });
        break;
      case "projectVisitingCustomer":
        history.push({ pathname: `${match.url}/projectVisitingCustomer` });
        break;
      case "apartment":
        history.push({ pathname: `${match.url}/apartment` });
        break;
      case "apartmentTemp":
        history.push({ pathname: `${match.url}/apartmentTemp` });
        break;
      case "overviewProject":
        history.push({ pathname: `${match.url}/overviewProject` });
        break;
      case "jointReport":
      case "jointReport/chartProject":
      case "jointReport/customProject":
      case "jointReport/mediaCustomer":
      case "jointReport/VPBHCustomer":
      case "jointReport/reportFund":
      case "jointReport/customerBought":
        if (user?.roleId === 21) {
          history.push({
            pathname: `${match.url}/jointReport/resultSaleAgency`
          });
        } else {
          history.push({ pathname: `${match.url}/jointReport/customProject` });
        }

        break;
      case "saletarget":
        history.push({ pathname: `${match.url}/saletarget` });
        break;
      case "salePolicy":
        history.push({ pathname: `${match.url}/salePolicy` });
        break;
      case "procedureProject":
        history.push({ pathname: `${match.url}/procedureProject` });
        break;
      case "paymentTransaction":
        history.push({ pathname: `${match.url}/paymentTransaction` });
        break;
      case "businessStaff":
        history.push({ pathname: `${match.url}/businessStaff` });
        break;
      case "targetsDealer":
        history.push({ pathname: `${match.url}/targetsDealer` });
        break;
      case "stackingPlanAgency":
        history.push({ pathname: `${match.url}/stackingPlanAgency` });
        break;
      case "statusColor":
        history.push({ pathname: `${match.url}/statusColor` });
        break;
      case "projectTarget":
        history.push({ pathname: `${match.url}/projectTarget` });
        break;
      default:
        break;
    }
  }
  const getTablist = tabList => {
    if (user?.roleId === 21) {
      let temp = [];
      let includes = ["overviewProject", "jointReport", "apartment"];
      tabList.map(e => {
        if (includes.includes(e.key)) {
          temp.push(e);
        }
      });
      return temp;
    }
    return tabList;
  };
  const tabList = [
    {
      key: "overviewProject",
      tab: <IntlMessages id="overviewProject.title" />
    },
    {
      key: "documentProject",
      tab: <IntlMessages id="documentProject.title" />
    },
    {
      key: "projectMediaCustomer",
      tab: <IntlMessages id="projectMediaCustomer.title" />
    },
    {
      key: "projectVisitingCustomer",
      tab: <IntlMessages id="projectVisitingCustomer.title" />
    },
    {
      key: "jointReport",
      tab: <IntlMessages id="projectJointReport.title" />
    },
    {
      key: "saletarget",
      tab: <IntlMessages id="saleTarget.title" />
    },
    {
      key: "salePolicy",
      tab: <IntlMessages id="salePolicy.title" />
    },
    {
      key: "apartment",
      tab: <IntlMessages id="apartment.title" />
    },
    {
      key: "apartmentTemp",
      tab: <IntlMessages id="apartmentTemp.title" />
    },
    {
      key: "procedureProject",
      tab: <IntlMessages id="procedureProject.title" />
    },
    {
      key: "paymentTransaction",
      tab: <IntlMessages id="paymentTransaction.title" />
    },
    {
      key: "businessStaff",
      tab: <IntlMessages id="businessStaff.title" />
    },
    {
      key: "targetsDealer",
      tab: <IntlMessages id="targetsDealer.title" />
    },
    {
      key: "stackingPlanAgency",
      tab: <IntlMessages id="stackingPlanAgency.title" />
    },
    {
      key: "statusColor",
      tab: <IntlMessages id="statusColor.title" />
    },
    {
      key: "projectTarget",
      tab: <IntlMessages id="projectTarget.title" />
    }
  ];

  const getTabKey = () => {
    const url = match.url === "/" ? "" : match.url;
    const tabKey = location.pathname.replace(`${url}`, "");
    if (tabKey && tabKey !== "") {
      const tkey = tabKey.replace(/\//g, "");
      console.log(`🚀 ~ file: detail.js ~ line 171 ~ getTabKey ~ tkey`, tkey);
      if (tkey == "paymentTransactionImport") {
        return "paymentTransaction";
      }
      if (tkey == "procedureProjectImport") {
        return "procedureProject";
      }
      if (
        tkey == "jointReportcustomProject" ||
        tkey == "jointReportchartProject" ||
        tkey == "jointReportmediaCustomer" ||
        tkey == "jointReportVPBHCustomer" ||
        tkey == "jointReportcustomerBought" ||
        tkey == "jointReportreportFund" ||
        tkey == "jointReportresultSaleAgency" ||
        tkey == "jointReportsalesReport" ||
        tkey == "jointReportcompletedReport"
      ) {
      }
      return tkey;
    }
    return "overviewProject";
  };

  return (
    <Card
      className="gx-card"
      title={`Mã dự án ${match.params.id} - ${projectInfo.name || ""} `}
    >
      <Tabs
        // defaultActiveKey="1"
        onChange={callback}
        activeKey={getTabKey()}
      >
        {getTablist(tabList).map((item, index) => (
          <TabPane {...item} tab={item.tab} key={item.key || index} />
        ))}
      </Tabs>
      <Switch>
        <Redirect
          exact
          from={`${match.url}/`}
          to={`${match.url}/${getTabKey()}`}
        />
        <ProjectDetailRoute
          path={`${match.url}/salePolicy/:id`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../salePolicy/crud"))}
        />
        <ProjectDetailRoute
          path={`${match.url}/salePolicy`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../salePolicy/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectTarget/:id`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../projectTarget/crud"))}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectTarget`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../projectTarget/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/apartment/:id`}
          projectId={match.params.id}
          projectType={projectInfo.type}
          component={asyncComponent(() => import("../../apartment/crud"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/documentproject`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../documentProject/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/apartment`}
          projectType={projectInfo.type}
          projectId={match.params.id}
          component={asyncComponent(() =>
            import("../../apartment/list/manage-apartment")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/apartmentTemp`}
          projectType={projectInfo.type}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../bookingTemp"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/documentproject`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../documentProject/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectmediacustomer`}
          templateCustomerFile={projectInfo.templateCustomerFile}
          projectId={match.params.id}
          component={asyncComponent(() =>
            import("../../projectMediaCustomer/list")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectmediacustomer/:id`}
          projectId={match.params.id}
          component={asyncComponent(() =>
            import("../../projectMediaCustomer/crud")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectvisitingcustomer`}
          templateCustomerFile={projectInfo.templateCustomerFile}
          projectId={match.params.id}
          component={asyncComponent(() =>
            import("../../projectVisitingCustomer/list")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/projectvisitingcustomer/:id`}
          projectId={match.params.id}
          component={asyncComponent(() =>
            import("../../projectVisitingCustomer/crud")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/documentproject`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../documentProject/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/documentproject/:id`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../documentProject/crud"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/jointReport`}
          projectId={match.params.id}
          component={asyncComponent(() =>
            import("../../jointReport/detail/detail")
          )}
          exact={false}
        />
        <ProjectDetailRoute
          path={`${match.url}/overviewProject`}
          projectId={match.params.id}
          component={asyncComponent(() =>
            import("../../bookRequestPending/list")
          )}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/businessStaff`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../businessStaff/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/targetsDealer`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../targetsDealer/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/targetsDealer/:id`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../targetsDealer/crud"))}
        />
        <ProjectDetailRoute
          path={`${match.url}/statusColor`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../dealerTarget/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/statusColor/:id`}
          projectId={match.params.id}
          component={asyncComponent(() => import("../../dealerTarget/crud"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/stackingPlanAgency`}
          projectType={projectInfo.type}
          projectId={match.params.id}
          projectName={projectInfo.name}
          component={asyncComponent(() =>
            import("../../stackingPlanAgency/list")
          )}
          exact={true}
        />

        {/* begin saletarget */}
        <ProjectDetailRoute
          path={`${match.url}/saletarget`}
          projectId={match.params.id}
          projectName={projectInfo.name}
          component={asyncComponent(() => import("../../saleTarget/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/saletarget/:id`}
          component={asyncComponent(() => import("../../saleTarget/crud"))}
          projectId={match.params.id}
          projectName={projectInfo?.name}
        />
        {/* end saletarget */}
        {/* begin procedureProject */}
        <ProjectDetailRoute
          path={`${match.url}/procedureProject`}
          component={asyncComponent(() =>
            import("../../procedureProject/list")
          )}
          exact={true}
          projectId={match.params.id}
        />
        <ProjectDetailRoute
          path={`${match.url}/procedureProjectImport`}
          component={asyncComponent(() =>
            import("../../procedureProject/list/listImport")
          )}
          exact={true}
          projectId={match.params.id}
        />
        {/* end procedureProject */}
        {/* begin paymentTransaction */}
        <ProjectDetailRoute
          path={`${match.url}/paymentTransaction`}
          component={asyncComponent(() =>
            import("../../paymentTransaction/list")
          )}
          exact={true}
          projectId={match.params.id}
        />
        <ProjectDetailRoute
          path={`${match.url}/paymentTransactionImport`}
          component={asyncComponent(() =>
            import("../../paymentTransaction/list/listImport")
          )}
          exact={true}
          projectId={match.params.id}
        />
        {/* end paymentTransaction */}
      </Switch>
    </Card>
  );
};

export default ProjectInSide;
