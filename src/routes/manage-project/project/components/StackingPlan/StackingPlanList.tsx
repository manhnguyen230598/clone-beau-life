import React, { useState, useMemo } from 'react';
import { Tooltip, message, Switch } from 'antd';
import { TableProps } from 'antd/es/table';
import get from 'lodash/get';
import assign from 'lodash/assign';
import dayjs from 'dayjs';
import ProTable from '../../../../../packages/pro-table/Table';
import { getValue } from '../../../../../util/helpers';
import type { StackingPlanProps as AddStackingPlanProps } from './AddStackingPlan';
import * as stackingPlanServices from '../../../../../services/stackingPlan';
import type { ProColumnType } from '../../../../../packages/pro-table/Table';
import StackingPlanDataList from './StackingPlanDataList';
import { useEffect } from 'react';

interface IStackingPlan extends Omit<AddStackingPlanProps, "isAddStackingPlan" | "setIsAddStackingPlan" | "dispatch"> {
  id: number,
  createdAt: number,
  updatedAt: number,
  isActive: boolean
};

interface IStackingPlanList {
  limit?: number,
  projectId: number,
  type:string,
  projectName: string,
  modeChoose?: any,
  form: any,
  setIsAgencyDisable: (disabled: boolean) => void
}

type TableRowSelection = TableProps<any>['rowSelection'];

const StackingPlanList: React.FC<IStackingPlanList> = (props: IStackingPlanList) => {
  const {
    projectId,
    type,
    projectName,
    modeChoose = "radio",
    form,
    setIsAgencyDisable
  } = props;
  // const searchInput = useRef();
  const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  // const [formValues, setFormValues] = useState({});
  const [current, setCurrent] = useState(1);
  // const [listSearch, setListSearch] = useState({});
  const [data, setData] = useState<any>({
    list: [],
    pagination: {
      total: 0,
      current: 1,
      totalPages: 0
    },
  });

  const pagination = {
    ...(data && data.pagination),
    pageSize: props.limit || ((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ? Number(process.env.REACT_APP_PAGESIZE) : 5)
  };

  const fetchData = async (params: any, sort: any, filter: any) => {
    console.log('fetchData -> params, sort, filter', params, sort, filter);
    filter = Object.keys(filter).reduce((obj, key) => {
      const newObj = { ...obj };
      if(filter[key] !== null) newObj[key] = getValue(filter[key]);
      return newObj;
    }, {});

    const pageSize = Number(pagination.pageSize);
    let finalParams = assign(params, {
      skip: (Number(pagination.current) - 1) * Number(pagination.pageSize),
      limit: pageSize,
      populate: "agencyId"
    }, { queryInput: { ...filter, ...params.queryInput } });

    const response = await stackingPlanServices.getList(finalParams);
    let data = get(response, "data", {});
    let total = get(data, "count", 0);
    let list = [];
    if(data && data.data) {
      list = data.data.map((item: any) => {
        if(item.createdAt) {
          const createdAt = item.createdAt;
          item.createdAt = dayjs(createdAt);
        }
        if(item.updatedAt) {
          const updatedAt = item.updatedAt;
          item.updatedAt = dayjs(updatedAt);
        }
        return item;
      });
    }
    setCurrent(current + 1);
    const dataRes = {
      // list,
      pagination: {
        pageSize,
        total,
        totalPages: Math.floor((get(data, "count", 0) + pageSize - 1) / pageSize),
        current: params.current
      }
    };
    setData(dataRes);
    return {
      data: list,
      total,
      success: true
    };
  };

  const triggerChange = (key: any, data: any) => {
    /* if(onChange) {
      onChange({ key, data });
    } */
  };

  const handleSelectRows = (keys: any, rows: any) => {
    setSelectedRows(rows);
    setSelectedRowKeys(keys);
    if(props.modeChoose === 'radio') {
      if(keys.length > 0) {
        triggerChange(keys[0], rows[0]);
      }
    } else {
      triggerChange(keys, rows);
    }
  };

  const columns: ProColumnType<IStackingPlan>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 30,
      fixed: 'left',
      hideInSearch: true,
    },
    {
      title: 'Dự án',
      dataIndex: 'projectId',
      width: 200,
      hideInSearch: true,
      hideInTable: true
      // ...getColumnSearchProps('name'),
    },
    {
      title: 'Đại lý',
      dataIndex: ['agencyId', 'name'],
      width: 200,
      hideInSearch: true,
      // ...getColumnSearchProps('name'),
    },
    {
      title: 'Người dùng',
      dataIndex: 'userIds',
      width: 150,
      hideInSearch: true,
      hideInTable: true
      // ...getColumnSearchProps('name'),
    },
    {
      title: 'Trạng thái',
      dataIndex: 'isActive',
      width: 200,
      filters: true,
      valueEnum: {
        true: { "text": "Hoạt động", "status": "Processing", "color": "#ec3b3b", "isText": "true", },
        false: { "text": "Dừng", "status": "Default", "color": "#ec3b3b", "isText": "true", },
      },
      hideInSearch: true,
      render: (value: any, record: any) => {
        return (
          <>
            <Switch defaultChecked={record.isActive}
              checkedChildren="Hoạt động"
              unCheckedChildren="Dừng"
              onChange={(val) => {
                let data = {
                  isActive: val,
                  id: record.id
                };
                stackingPlanServices.update(record.id, data).then(res => {
                  console.log(`🚀 ~ file: StackingPlanList.tsx ~ line 143 ~ stackingPlanDataServices.update ~ res`, res);
                  if(res.status == 200) {
                    message.info("Cập nhật trạng thái thành công", 10);
                  } else {
                    message.error(res.data.message || 'Cập nhật thất bại', 10);
                  }
                }).catch(err => {
                  console.log(`🚀 ~ file: StackingPlanList.tsx ~ line 151 ~ stackingPlanDataServices.update ~ err`, err);
                  message.error(err.message || 'Cập nhật thất bại', 10);
                });
              }}
            />
          </>
        );
      }
    },
  ];

  const getParams = useMemo(() => {
    let params = {
      queryInput: {
        projectId
      }
    };
    return params;
  }, [projectId]);

  const expandedRowRender = (record: IStackingPlan) => {
    return <StackingPlanDataList
      type={type}
      stackingPlanId={record.id} 
    />;
  };

  useEffect(() => {
    console.log(`🚀 ~ file: StackingPlanList.tsx ~ line 34 ~ selectedRows`, selectedRows);
    console.log(`🚀 ~ file: StackingPlanList.tsx ~ line 181 ~ selectedRowKeys`, selectedRowKeys);
    if(selectedRowKeys && selectedRowKeys.length) {
      form.resetFields();
      form.setFieldsValue({
        agencyId: [get(selectedRows, "[0].agencyId.id", "")],
        userIds: get(selectedRows, "[0].userIds", [])
      });
      setIsAgencyDisable(true);
    }
  }, [selectedRowKeys]);

  const rowSelection: TableRowSelection = {
    type: modeChoose || 'checkbox',
    renderCell: (checked, record, index, originNode) => {
      return (
        <Tooltip title={"Chọn bảng hàng"}>
          {originNode}
        </Tooltip>
      );
    },
    onChange: handleSelectRows
  };

  const alertOptionRender = (props: { onCleanSelected: () => void }) => {
    const { onCleanSelected } = props;
    return [
      <a
        onClick={(e) => {
          e.preventDefault();
          form.resetFields();
          setIsAgencyDisable(false);
          onCleanSelected();
        }}
        key="0"
      >
        {`Bỏ chọn`}
      </a>,
    ];
  };

  return (
    <React.Fragment>
      <div className="nv-wrapper-select-table">
        <ProTable
          tableClassName="gx-table-responsive"
          request={fetchData}
          params={getParams}
          search={false}
          headerTitle={`Bảng hàng của dự án ${projectName}`}
          rowKey="id"
          toolBarRender={false}
          // tableAlertRender={false}
          tableAlertOptionRender={alertOptionRender}
          pagination={pagination}
          columns={columns}
          rowSelection={rowSelection}
          // onChange={handleStandardTableChange}
          dateFormatter="string"
          type="table"
          expandable={{ expandedRowRender }}
        />
      </div>
    </React.Fragment>
  );
};

export default React.memo(StackingPlanList);
