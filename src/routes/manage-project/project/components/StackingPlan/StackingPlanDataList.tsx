import React, { useState, useEffect } from "react";
import { Row, Col, Button, message, Badge, Divider, Affix } from "antd";
// import type { FilterDropdownProps } from 'antd/es/table/interface';
import { CheckCircleTwoTone } from "@ant-design/icons";
import { get, cloneDeep, uniq } from "lodash";
import dayjs from "dayjs";
import * as stackingPlanDataServices from "../../../../../services/stackingPlanData";
// import { any } from "prop-types";

interface IStackingPlanDataList {
  limit?: number;
  stackingPlanId: number;
  type: string;
}

const StackingPlanDataList: React.FC<IStackingPlanDataList> = (
  props: IStackingPlanDataList
) => {
  const { stackingPlanId, type } = props;
  //   const searchInput = useRef<any>();
  //   // const [, setSelectedRows] = useState([]);
  //   // const [, setSelectedRowKeys] = useState([]);
  //   // const [formValues, setFormValues] = useState({});
  //   const [listSearch, setListSearch] = useState({});
  const [data, setData] = useState<any>([]);
  const [rows, setRows] = useState<any>([]);
  const [columns, setColumn] = useState<any>([]);

  const fetchData = async () => {
    const response = await stackingPlanDataServices.getList({
      stackingPlanId: stackingPlanId
    });
    console.log(
      "🚀 ~ file: StackingPlanDataList.tsx ~ line 69 ~ fetchData ~ response",
      response
    );
    let data = get(response, "data", {});
    let list = [];
    if (data && data.data) {
      list = data.data.map((item: any) => {
        if (item.createdAt) {
          const createdAt = item.createdAt;
          item.createdAt = dayjs(createdAt);
        }
        if (item.updatedAt) {
          const updatedAt = item.updatedAt;
          item.updatedAt = dayjs(updatedAt);
        }
        return item;
      });
    }
    console.log(
      "🚀 ~ file: StackingPlanDataList.tsx ~ line 38 ~ fetchData ~ list",
      list
    );
    setData(list);
    setColumn(data.collumn);
    setRows(data.row);
  };

  useEffect(() => {
    fetchData();
  }, []);

  //   const handleSearchFilter = (selectedKeys: any, confirm: any, dataIndex: any) => {
  //     setListSearch({
  //       ...listSearch,
  //       [`search_${dataIndex}`]: selectedKeys[0],
  //     });
  //     confirm();
  //   };

  //   const handleReset = (clearFilters: any, confirm: any, dataIndex: any) => {
  //     clearFilters();
  //     setListSearch({
  //       ...listSearch,
  //       [`search_${dataIndex}`]: '',
  //     });
  //     confirm();
  //   };

  //   const getColumnSearchProps = (dataIndex: any) => ({
  //     filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }: FilterDropdownProps) => (
  //       <div style={{ padding: 8 }}>
  //         <Input
  //           ref={searchInput}
  //           placeholder={`Tìm kiếm ${dataIndex}`}
  //           value={selectedKeys[0]}
  //           onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
  //           onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
  //           onKeyDown={e => {
  //             if (e.key === 'Enter') {
  //               // onChange(e.target.value);
  //               handleSearchFilter(selectedKeys, confirm, dataIndex);
  //             }
  //           }}
  //           style={{ width: 188, marginBottom: 8, display: 'block' }}
  //         />
  //         <Space>
  //           <Button
  //             type="primary"
  //             onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
  //             icon={<SearchOutlined />}
  //             size="small"
  //             style={{ width: 90 }}
  //           >
  //             Tìm
  //           </Button>
  //           <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
  //             Reset
  //           </Button>
  //         </Space>
  //       </div>
  //     ),
  //     filterIcon: (filtered: any) => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
  //     onFilterDropdownVisibleChange: (visible: any) => {
  //       if(visible) {
  //         setTimeout(() => searchInput.current.select());
  //       }
  //     },
  //     onFilter: (value: any, record: any) => record
  //     /* onFilter: (value, record) =>
  //       record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
  //     render: text =>
  //       this.state.searchedColumn === dataIndex ? (
  //         <Highlighter
  //           highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
  //           searchWords={[this.state.searchText]}
  //           autoEscape
  //           textToHighlight={text.toString()}
  //         />
  //       ) : (
  //           text
  //         ), */
  //   });

  //   // const columns: ProColumnType<IStackingPlanData>[] = [
  //   //   {
  //   //     title: 'ID',
  //   //     dataIndex: 'id',
  //   //     key: 'id',
  //   //     width: 60,
  //   //     fixed: 'left',
  //   //     hideInSearch: true,
  //   //   },
  //   //   {
  //   //     title: 'Mã bảng hàng',
  //   //     dataIndex: 'stackingPlanId',
  //   //     width: 100,
  //   //     hideInSearch: true,
  //   //     hideInTable: true
  //   //     // ...getColumnSearchProps('name'),
  //   //   },
  //   //   {
  //   //     title: 'Mã căn hộ',
  //   //     dataIndex: 'fullCodeApartment',
  //   //     width: 150,
  //   //     hideInSearch: true,
  //   //     ...getColumnSearchProps('fullCodeApartment'),
  //   //   },
  //   //   {
  //   //     title: 'Trạng thái',
  //   //     dataIndex: 'status',
  //   //     width: 120,
  //   //     filters: true,
  //   //     valueEnum: {
  //   //       hide: { "text": "Ẩn", "status": "Processing", "color": "#ec3b3b", "isText": "true", },
  //   //       display: { "text": "Hiển thị", "status": "Default", "color": "#ec3b3b", "isText": "true", },
  //   //     },
  //   //     hideInSearch: true,
  //   //     render: (value: any, record: any) => {
  //   //       return (
  //   //         <>
  //   //           <Switch defaultChecked={record.status == 'display'}
  //   //             checkedChildren="Hiển thị"
  //   //             unCheckedChildren="Ẩn"
  //   //             onChange={(val) => {
  //   //               let data = {
  //   //                 status: val ? 'display' : 'hide',
  //   //                 id: record.id
  //   //               };
  //   //               stackingPlanDataServices.update(record.id, data).then(res => {
  //   //                 console.log(`🚀 ~ file: StackingPlanDataList.tsx ~ line 143 ~ stackingPlanDataServices.update ~ res`, res);
  //   //                 if(res.status == 200) {
  //   //                   message.info("Cập nhật trạng thái thành công", 10);
  //   //                 } else {
  //   //                   message.error(res.data.message || 'Cập nhật thất bại', 10);
  //   //                 }
  //   //               }).catch(err => {
  //   //                 console.log(`🚀 ~ file: StackingPlanDataList.tsx ~ line 151 ~ stackingPlanDataServices.update ~ err`, err);
  //   //                 message.error(err.message || 'Cập nhật thất bại', 10);
  //   //               });
  //   //             }}
  //   //           />
  //   //         </>
  //   //       );
  //   //     },
  //   //   },
  //   // ];

  //   const getParams = useMemo(() => {
  //     let params = {
  //       queryInput: {
  //         stackingPlanId
  //       }
  //     };
  //     return params;
  //   }, [stackingPlanId]);
  const [selectedItem, setSelectedItem] = useState<string[]>([]);
  const handleChangeStatus = (identity: string) => {
    let newArray = cloneDeep(selectedItem);
    if (newArray.includes(identity)) {
      newArray = newArray.filter(item => item !== identity);
      setSelectedItem(newArray);
    } else {
      newArray.push(identity);
      setSelectedItem(newArray);
    }
    console.log(newArray);

    // let newData = data?.map((item: any) => {
    //   if (item.fullCodeApartment === identity && type === "house") {
    //     let result = item.status === "display" ? { ...item, ["status"]: "hide" } : { ...item, ["status"]: "display" }
    //     return result
    //   }
    //   if (item.fullCodeApartment === identity && type === "resort") {
    //     let result = item.status === "display" ? { ...item, ["status"]: "hide" } : { ...item, ["status"]: "display" }
    //     return result
    //   }
    //   return item
    // })
    // console.log("🚀 ~ file: StackingPlanDataList.tsx ~ line 222 ~ newData ~ newData", newData)
    // setData(newData)
  };
  const handleChangeRowStatus = (row: string) => {
    let newArray = cloneDeep(selectedItem);
    data?.map((item: any) => {
      if (item.row === row) {
        newArray.push(item.fullCodeApartment);
      }
    });
    newArray = uniq(newArray);
    setSelectedItem(newArray);
    // setData(newData);
  };
  const onFinish = (status: string) => {
    // let displayFullCodeIds = data
    //   .filter((x: any) => x.status === "display")
    //   .map((item: any) => item.fullCodeApartment);

    let displayBody = {
      fullCodeIds: selectedItem,
      stackPlanId: stackingPlanId,
      status
    };
    stackingPlanDataServices.updateMulti(displayBody).then(res => {
      if (res.status == 200) {
        message.info("Cập nhật trạng thái thành công", 10);
      } else {
        message.error(res.data.message || "Cập nhật thất bại", 10);
      }
      setSelectedItem([]);
      fetchData();
    });
  };
  const getMaxColumn = () => {
    var MAX = 1;
    for (let i = 0; i < rows.length; i++) {
      let newMax = data.filter((item: any) => item.row === rows[i]).length;
      if (newMax > MAX) {
        MAX = newMax;
      }
    }
    return MAX;
  };

  return (
    <>
      <div style={{ alignItems: "center", marginTop: 30 }}>
        {type === "house" ? (
          <>
            <Divider orientation="left"></Divider>
            <Row>
              <Col lg={1}>
                <div style={{ marginTop: 40, marginLeft: 10 }}>
                  {rows?.map((item: any, index: number) => (
                    <p
                      onClick={() => handleChangeRowStatus(item)}
                      key={index}
                      style={{ height: 40, margin: 0 }}
                    >
                      <a>{item}</a>
                    </p>
                  ))}
                </div>
              </Col>
              <Col lg={23}>
                <div style={{ overflowX: "scroll" }}>
                  <div style={{ width: columns.length * 120 }}>
                    <div>
                      {columns?.map((item: any, index: number) => (
                        <p
                          key={index}
                          style={{
                            display: "inline-block",
                            width: 120,
                            textAlign: "center"
                          }}
                        >
                          {item}
                        </p>
                      ))}
                    </div>
                    {rows?.map((row: string) =>
                      columns?.map((column: string) =>
                        data?.map((item: any, index: number) =>
                          item.row == row && item.collumn == column ? (
                            <div
                              onClick={() =>
                                handleChangeStatus(item.fullCodeApartment)
                              }
                              key={index}
                              style={{
                                width: 120,
                                height: 40,
                                border: 1,
                                backgroundColor:
                                  item?.status === "display"
                                    ? "#45ffda"
                                    : "#f0d7d5",
                                borderStyle: "solid",
                                // textAlign:"center",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center"
                              }}
                            >
                              <p
                                style={{
                                  fontSize: 10,
                                  fontWeight: "bolder",
                                  color: "black",
                                  margin: 0
                                }}
                              >
                                <Badge
                                  count={
                                    selectedItem.includes(
                                      item.fullCodeApartment
                                    ) ? (
                                      <CheckCircleTwoTone
                                        style={{ color: "#f5222d" }}
                                      />
                                    ) : (
                                      ""
                                    )
                                  }
                                >
                                  {item.fullCodeApartment}
                                </Badge>
                              </p>
                            </div>
                          ) : (
                            ""
                          )
                        )
                      )
                    )}
                  </div>
                </div>
              </Col>
            </Row>
          </>
        ) : (
          <>
            <Divider orientation="left"></Divider>
            <Row>
              <div style={{ marginLeft: 10, width: 40, marginTop: 11 }}>
                {rows?.map((item: any, index: number) => (
                  <p
                    onClick={() => handleChangeRowStatus(item)}
                    key={index}
                    style={{ height: 40, margin: 0 }}
                  >
                    <a>{item}</a>
                  </p>
                ))}
              </div>
              <Col lg={23}>
                {/* <div>
               {data?.column?.map(item =>
                 <p style={{ display: "inline-block", width: customWidth(), textAlign: "center" }}>{item}</p>
               )}
             </div> */}
                <div style={{ overflowX: "scroll" }}>
                  <div style={{ width: getMaxColumn() * 100 }}>
                    {rows?.map((row: string) => {
                      return (
                        <>
                          {data
                            ?.filter((item: any) => item.row === row)
                            ?.map((item: any, index: any) => (
                              <div
                                onClick={() =>
                                  handleChangeStatus(item.fullCodeApartment)
                                }
                                key={index}
                                style={{
                                  width: 100,
                                  backgroundColor:
                                    item?.status === "display"
                                      ? "#45ffda"
                                      : "#f0d7d5",
                                  height: 40,
                                  border: 1,
                                  borderStyle: "solid",
                                  display: "inline-flex",
                                  alignItems: "center",
                                  justifyContent: "center"
                                }}
                              >
                                <p
                                  style={{
                                    fontWeight: "bolder",
                                    color: "black",
                                    margin: 0
                                  }}
                                >
                                  <Badge
                                    count={
                                      selectedItem.includes(
                                        item.fullCodeApartment
                                      ) ? (
                                        <CheckCircleTwoTone
                                          style={{ color: "#f5222d" }}
                                        />
                                      ) : (
                                        0
                                      )
                                    }
                                  >
                                    {item.fullCodeApartment}
                                  </Badge>
                                </p>
                              </div>
                            ))}
                          <br></br>
                        </>
                      );
                    })}
                  </div>
                </div>
              </Col>
            </Row>
          </>
        )}
        <Affix style={{ position: "absolute", top: -15, left: 100 }}>
          {/* <Row style={{ marginTop: 15 }}>
            <Col
              xl={{ span: 8, offset: 1 }}
              lg={{ span: 8, offset: 1 }}
              md={{ span: 8, offset: 1 }}
              sm={{ span: 8, offset: 1 }}
              xs={{ span: 8, offset: 1 }}
            >
              <Badge
                className="nv-badge-all"
                color={"#45ffda"}
                text={"Hiển thị"}
              />
            </Col>
            <Col xl={6} lg={6} md={6} sm={6} xs={6}>
              <Badge className="nv-badge-all" color={"#f0d7d5"} text={"Ẩn"} />
            </Col>
            {selectedItem.length > 0 ? (
              
                <Col
                  xl={{ span: 9, }}
                  lg={{ span: 9,  }}
                  md={{ span: 9,  }}
                  sm={{ span: 9,  }}
                  xs={{ span: 9,  }}
                >
                  <Button onClick={onFinish} type="primary">
                    Cập nhật bảng
                  </Button>
                  <Button onClick={onFinish} type="primary">
                    Cập nhật bảng
                  </Button>
                </Col>
              
            ) : (
              ""
            )}
          </Row> */}
          <div
            style={{
              display: "flex",
              marginTop: "20px",
              alignItems: "center"
            }}
          >
            <Badge
              className="nv-badge-all"
              color={"#45ffda"}
              text={"Hiển thị"}
            />
            <Badge className="nv-badge-all" color={"#f0d7d5"} text={"Ẩn"} />
            {selectedItem.length > 0 ? (
              <>
                <Button onClick={() => onFinish("display")} type="primary">
                  Hiển thị đã chọn
                </Button>
                <Button onClick={() => onFinish("hide")} type="ghost" danger>
                  Ẩn đã chọn
                </Button>
              </>
            ) : (
              ""
            )}
          </div>
        </Affix>
      </div>
    </>
  );
};

export default React.memo(StackingPlanDataList);
