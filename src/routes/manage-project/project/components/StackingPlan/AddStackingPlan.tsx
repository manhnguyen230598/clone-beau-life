import React, { FC, memo, useCallback, useState } from 'react';
import { Modal, Form, Button } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import AgencySelect from '../../../../../components/Select/Agency/ListAgency';
import UserSelect from '../../../../../components/Select/User/ListUser';
import StackingPlanList from './StackingPlanList';

type StackingPlanState = {
  isShow: boolean,
  projectId: number,
  projectName: string
}

export interface StackingPlanProps {
  projectId: number,
  projectName: string,
  type:string,
  agencyId: number,
  userIds: number[],
  isAddStackingPlan: boolean,
  setIsAddStackingPlan: (d: any) => void,
  dispatch: any
}

const AddStackingPlan1: FC<StackingPlanProps> = ({
  projectId, isAddStackingPlan, setIsAddStackingPlan, dispatch, projectName,type
}) => {
  const [form] = Form.useForm();
  const [agency,setAgency]=useState<any>();
  const [isAgencyDisable, setIsAgencyDisable] = useState<boolean>(false);
  const handleCancel = () => {
    setIsAddStackingPlan((origin: StackingPlanState) => ({
      ...origin,
      isShow: false
    }));
  };

  /* const handleOk = () => {
    form.submit();
  }; */

  const onFinish = useCallback((values: any) => {
    console.log(`🚀 ~ file: AddStackingPlan.tsx ~ line 22 ~ projectId`, projectId);
    console.log(`🚀 ~ file: AddStackingPlan.tsx ~ line 25 ~ onFinish ~ values`, values);
    let params = {
      agencyId: (values.agencyId || {}).id,
      userIds: (values.userIds || []).map((i: any) => i.id)
    };
    dispatch({
      type: 'project/addStackingPlan',
      payload: {
        projectId,
        ...params
      },
      callback: (res: any) => {
        console.log(`🚀 ~ file: AddStackingPlan.tsx ~ line 41 ~ onFinish ~ res`, res);
      }
    });
  }, [projectId]);

  const onFinishFailed = useCallback((errors: any) => {
    console.log(`🚀 ~ file: AddStackingPlan.tsx ~ line 30 ~ onFinishFailed ~ errors`, errors);

  }, []);


  return (
    <div>
      <Modal
        visible={isAddStackingPlan}
        // onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
        // okText="Thêm"
        cancelText="Đóng"
        title={`Thêm bảng hàng dự án ${projectName}`}
        width={"90%"}
        destroyOnClose
        style={{ maxHeight: '100vh' }}
      >
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Đại lý"
            name="agencyId"
            rules={[
              { required: true, message: 'Bạn phải chọn đại lý' }
            ]}
          >
            <AgencySelect disabled={isAgencyDisable} onChange={(value:any)=>{
               setAgency(value)
            }}/>
          </Form.Item>
          <Form.Item
            label="Người book căn"
            name="userIds"
            rules={[
              { required: true, message: 'Bạn chưa chọn người book căn' }
            ]}
          >
            <UserSelect modeChoose="checkbox" agencyId={agency?.id}/>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button
              type="primary"
              icon={<PlusCircleOutlined />}
              // onClick={handleOk}
              htmlType="submit"
            >{`Thêm mới`}</Button>
          </Form.Item>
        </Form>
        <StackingPlanList projectId={projectId} type={type} projectName={projectName} form={form} setIsAgencyDisable={setIsAgencyDisable} />
      </Modal>
    </div>
  );
};

export const AddStackingPlan: FC<StackingPlanProps> = memo<any>(connect(null)(AddStackingPlan1));
