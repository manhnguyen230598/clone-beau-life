import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Input,
  Button,
  Space,
  Switch,
  message,
  Modal,
  Card,
  Upload
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  UploadOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { union, cloneDeep } from "lodash";
import BusinessStaffDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import * as businessStaffServices from "services/businessStaff";
import AddUserToClassModal from "../AddUserToClassModal";
import AgencySelect from "../../../../components/Select/Agency/ListAgency";
const tranformBusinessStaffParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};
const UpLoadFile = Upload;
const RESOURCE = "businessStaff";

const BusinessStaffList = props => {
  const [form] = Form.useForm();
  const [formExcel] = Form.useForm();
  const [formAgency] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();
  const [isShowModal, setIsShowModal] = useState(false);
  const [isModalExcel, setIsModalExcel] = useState(false);
  const [isModalAgency, setIsModalAgency] = useState(false);
  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState();
  const onCancelModel = status => {
    setIsShowModal(status);
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformBusinessStaffParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: {
          ...initParams.filters,
          projectId
        },
        populate: "userId,agencyId"
      }
    });
  };
  const onCancelExcelModel = status => {
    setIsModalExcel(status);
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformBusinessStaffParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: {
          ...initParams.filters,
          projectId
        },
        populate: "userId,agencyId"
      }
    });
  };

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    projectId
  } = props;
  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformBusinessStaffParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: {
          ...initParams.filters,
          projectId
        },
        populate: "userId,agencyId"
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});
    if (filters) {
      if (filters.isApprove) {
        filters.isApprove = filters.isApprove == "true" ? true : false;
      }
      if (filters.agencyId) {
        filters.agencyId = filters.agencyId?.id;
      }
      if (filters.roleId) {
        filters.roleId = filters.roleId?.id;
      }
    }
    const initParams = tranformBusinessStaffParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        projectId
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize),
      populate: "userId,agencyId"
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });
        if (typeof values.agencyId == "object") {
          values.agencyId = values.agencyId?.id;
        }
        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", "false");
          values = {};
        }
        const initParams = tranformBusinessStaffParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters,
              projectId
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }]),
            populate: "userId,agencyId"
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    setIsShowModal(true);
  };
  const handleAddExcelClick = () => {
    setIsModalExcel(true);
  };

  /* const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  }; */

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Mã người dùng",
      dataIndex: "userId",
      width: 70,
      render: val => {
        return <span>{val ? val.id : "-"}</span>;
      }
    },
    {
      title: "Người dùng",
      dataIndex: "userId",
      width: 120,
      render: val => {
        return <span>{val ? val.name : "-"}</span>;
      }
    },
    {
      title: "Đại lý",
      dataIndex: "agencyId",
      width: 120,
      render: val => {
        return <span>{val ? val.name : "-"}</span>;
      },
      renderFormItem: (_, { type, defaultRender, ...rest }, form) => {
        if (type === "form") {
          return null;
        }
        return <AgencySelect mode="radio" />;
        // return defaultRender(_);
      }
    },
    {
      title: "Trạng thái",
      dataIndex: "isActive",
      width: 60,
      filters: true,
      valueEnum: {
        true: {
          text: "Hoạt động",
          status: "Processing",
          color: "#ec3b3b",
          isText: "true"
        },
        false: {
          text: "Dừng",
          status: "Default",
          color: "#ec3b3b",
          isText: "true"
        }
      },
      ...getColumnSearchProps("isActive"),
      render: (val, record) => {
        const onChangeState = async checked => {
          let rs = await businessStaffServices.update(record.id, {
            isActive: checked
          });
          if (rs && rs.status == 200) {
            message.success("Thay đổi trạng thái thành công");
          } else {
            message.error("Thay đổi trạng thái thất bại");
          }
        };
        return (
          <Switch
            defaultChecked={record.isActive ? true : false}
            onChange={e => onChangeState(e)}
          />
        );
      }
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdAt",
      valueType: "dateTime",
      width: 150,
      hideInSearch: true
    }
  ];
  const [fileList, setFileList] = useState([]);
  const uploadProps = {
    maxCount: 1,
    onRemove: file => {
      setFileList(fileList => {
        const index = fileList.indexOf(file);
        const newFileList = fileList.slice();
        newFileList.splice(index, 1);
        return newFileList;
      });
    },
    beforeUpload: file => {
      if (fileList.length > 0) {
        message.warn("Tối đa một file");
        return;
      }
      setFileList(fileList => [...fileList, file]);
      return false;
    },
    fileList
  };
  const onSubmitImport = async value => {
    const data = cloneDeep(value);
    if (props.projectId) {
      data.projectId = props.projectId;
    }
    if (fileList.length > 0) {
      data.file = fileList;
    }
    let rs = await businessStaffServices.importExcel(data);
    if (rs && rs.status == 200) {
      let data = rs.data;
      Modal.success({
        width: "46vw",
        title: "Kết quả import dữ liệu",
        content: (
          <div>
            <p style={{ color: "green" }}>Thành công:{data?.success.length}</p>
            <ul>
              {data?.success.map(e => {
                return <li>{e.phone}</li>;
              })}
            </ul>
            <p style={{ color: "red" }}>Thất bại:{data?.fail.length}</p>
            <ul>
              {data?.fail.map(e => {
                return (
                  <li>
                    {e.phone} : {e.message}
                  </li>
                );
              })}
            </ul>
          </div>
        )
      });
    } else {
      message.error(rs.data.message);
    }
  };
  const onSubmitAgency = async value => {
    let data = cloneDeep(value);
    data.agencyIds = data.agencyIds.map(e => e.id);
    data.projectId = projectId;
    // console.log(data);
    businessStaffServices.approveAgency(data).then(res => {
      if (res.status == 200) {
        message.success(res.data.message);
      }
      let initParams = {};
      let pagi = Object.assign(pagination, tableChange?.pagination || {});
      const defaultCurrent = pagi.current || 1;
      initParams = tranformBusinessStaffParams(
        tableChange?.filtersArg,
        tableChange?.formValues,
        tableChange?.sort
      );

      dispatch({
        type: `${RESOURCE}/fetch`,
        payload: {
          page: tableChange?.pagination?.current || current,
          current: tableChange?.pagination?.current || current,
          skip: (defaultCurrent - 1) * pagi.pageSize,
          limit: pagi.pageSize || limit,
          pageSize: pagi.pageSize || limit,
          sort: initParams.sort
            ? initParams.sort.length <= 0
              ? [{ createdAt: "desc" }]
              : initParams.sort
            : [{ createdAt: "desc" }],
          queryInput: {
            ...initParams.filters,
            projectId
          },
          populate: "userId,agencyId"
        }
      });
    });
  };
  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Nhân viên"
        actionRef={actionRef}
        formRef={form}
        form={{
          initialValues: formValues
        }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => setIsModalAgency(true)}
          >
            {`Thêm mới theo đại lý`}
          </Button>,
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
          >
            {`Thêm mới`}
          </Button>,
          <Button
            icon={<PlusOutlined />}
            type="default"
            onClick={() => handleAddExcelClick()}
          >
            {`Thêm bằng file excel`}
          </Button>
        ]}
      />
      <AddUserToClassModal
        isShowModal={isShowModal}
        setIsShowModal={onCancelModel}
        projectId={projectId}
      />
      <Form form={formExcel} onFinish={onSubmitImport}>
        <Modal
          visible={isModalExcel}
          onOk={() => {
            formExcel.submit();
          }}
          onCancel={() => onCancelExcelModel(false)}
        >
          <Card
            title="Import file Excel danh sách nhân viên dự án"
            // className={styles.cardCheckCodeResult}
          >
            <div style={{ textAlign: "center" }}>
              <div>
                <p>
                  Tải file mẫu{" "}
                  <a
                    href={`/static/templates/Import nhan vien kinh doanh.xlsx`}
                    download
                  >
                    tại đây
                  </a>
                </p>
              </div>
              <Form.Item name="fileExcel">
                <UpLoadFile {...uploadProps}>
                  <Button icon={<UploadOutlined />}>Upload</Button>
                </UpLoadFile>
              </Form.Item>
            </div>
          </Card>
        </Modal>
      </Form>
      <Form form={formAgency} onFinish={onSubmitAgency}>
        <Modal
          visible={isModalAgency}
          onOk={() => {
            formAgency.submit();
          }}
          onCancel={() => setIsModalAgency(false)}
        >
          <Card
            title="Thêm nhân viên dự án theo đại lý"
            // className={styles.cardCheckCodeResult}
          >
            <Form.Item name="agencyIds">
              <AgencySelect modeChoose={"checkbox"} />
            </Form.Item>
          </Card>
        </Modal>
      </Form>

      <BusinessStaffDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ businessStaff, loading }) => ({
  businessStaff,
  loading: loading.models.businessStaff
}))(BusinessStaffList);
