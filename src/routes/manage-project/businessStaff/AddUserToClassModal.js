import React, { useState, useEffect } from "react";
import { Modal, Form, Tag, message } from "antd";
import cloneDeep from "lodash/cloneDeep";
import get from "lodash/get";
import { connect } from "dva";
import TransferUser, {
  rightTableColumns
} from "../../agency/classroom/crud/components/Step4/components/TransferUser";
import * as businessStaffServices from "services/businessStaff";

const RESOURCE = "userclass";
const leftTableColumns = [
  {
    dataIndex: "id",
    title: "ID"
  },
  {
    dataIndex: "name",
    title: "Tên"
  },
  {
    dataIndex: "phone",
    title: "SĐT",
    render: val => <>{val}</>
  },
  {
    dataIndex: "agencyId",
    title: "Đại lý",
    render: val => <>{val?.name ? val.name : "-"}</>
  },
];
const AddUserToClassModal = ({
  isShowModal = false,
  setIsShowModal,
  projectId,
  dispatch
}) => {
  const [form] = Form.useForm();
  const [users, setUsers] = useState([]);
  const onFinish = values => {
    const data = cloneDeep(values);
    data.projectId = projectId;
    businessStaffServices.approveStaff(data ).then(res => {
      if (res.status == 200) {
        message.success(res.data.message);
      }
      businessStaffServices.getUserNotInClass({ projectId }).then(res => {
        if (res.status == 200) {
          setUsers(get(res, "data.data", []));
        }
      });
    });

  };

  useEffect(() => {
    if (projectId) {
      businessStaffServices.getUserNotInClass({ projectId }).then(res => {
        if (res.status == 200) {
          setUsers(get(res, "data.data", []));
        }
      });
    }
  }, [projectId]);

  return (
    <React.Fragment>
      <Modal
        width={1200}
        visible={isShowModal}
        okText="Thêm nhân viên"
        onCancel={() => setIsShowModal(false)}
        onOk={() => form.submit()}
        title={"Thêm nhân viên"}
      >
        <Form
          className="ant-advanced-search-form"
          form={form}
          layout="vertical"
          onFinish={onFinish}
          hideRequiredMark
        >
          <Form.Item
            // label="nhân viên"
            name="userIds"
            required={false}
            shouldUpdate={(prevValues, curValues) =>
              prevValues.type !== curValues.type
            }
          >
            <TransferUser
              dataSource={users}
              disabled={false}
              showSearch={true}
              filterOption={(inputValue, item) =>
                item.agencyId?.name
                  ?.toLowerCase()
                  .indexOf(inputValue?.toLowerCase()) !== -1 ||
                item.name?.toLowerCase().indexOf(inputValue?.toLowerCase()) !==
                  -1 ||
                item.phone?.toLowerCase().indexOf(inputValue?.toLowerCase()) !==
                  -1
              }
              leftColumns={leftTableColumns}
              rightColumns={rightTableColumns}
            />
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};

export default connect(null)(AddUserToClassModal);
