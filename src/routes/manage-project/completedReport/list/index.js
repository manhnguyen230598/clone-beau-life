/* eslint-disable array-callback-return */
import React, { useCallback, useEffect, useState } from "react";
import {
  ComposedChart,
  Line,
  Bar,
  CartesianGrid,
  XAxis,
  YAxis,
  // Tooltip,
  Legend,
  ResponsiveContainer
} from "recharts";
import { BarChart } from "recharts";
import { PieChart, Pie, Sector, Cell } from "recharts";
import { Row, Card, Button, Tooltip } from "antd";
import { Col } from "antd";
import Widget from "src/components/Widget";
import * as project from "src/services/project";
import { Space, DatePicker } from "antd";
import ReactDOM from "react-dom";

var cml = new Date();
var currentMonth2 = cml.getMonth() + 1;
var currentYear2 = cml.getFullYear();

const CompletedReport = props => {
  const { RangePicker } = DatePicker;
  const [dataChart, setDataChart] = useState();

  let [fromTimeExchange, setFromTimeExchange] = useState(""),
    [fromTimeAccumulate, setFromTimeAccumulate] = useState(""),
    [monthYearInput, setMonthYearInput] = useState(new Date());

  const getData = async () => {
    if (fromTimeAccumulate) {
      let dateStart = fromTimeAccumulate[0].split("-");
      let dateEnd = fromTimeAccumulate[1].split("-");
      let api = await project.completedReport(
        props.projectId,
        parseInt(dateStart[0]),
        parseInt(dateEnd[0]),
        parseInt(dateStart[1]),
        parseInt(dateEnd[1])
      );
      setDataChart(api.data);
    } else {
      alert("Vui lòng nhập ngày tháng");
    }
  };

  // useEffect(() => {
  //     getData();
  //   }, [month, endMonth , year, endYear])

  const renderTick = props => {
    const { x, y, index } = props;
    if (index === 8) {
      return (
        <>
          <path d={`M${x - 40.5},${y - 38}v${-150}`} stroke="black" />
          <path d={`M${x + 40.5},${y - 38}v${-150}`} stroke="black" />
        </>
      );
    } else {
      return <path d={`M${x - 40.5},${y - 38}v${-150}`} stroke="black" />;
    }
  };

  const renderCustomizedLabel = props => {
    const { x, y, stroke, value } = props;
    return (
      <text
        x={x}
        y={y}
        dy={-10}
        fill={stroke}
        fontSize={15}
        textAnchor="middle"
      >
        {value}%
      </text>
    );
  };
  const style = {
    left: 60,
    bottom: 79,
    lineHeight: 2,
    height: 122,
    border: "2px solid #a6a6a6",
    paddingTop: 4,
    paddingLeft: 5,
    borderRight: "none"
  };
  //---------------------------
  return (
    <>
      <Card
        title={
          <span style={{ marginLeft: 10 }}>Báo cáo hoàn thành chỉ tiêu</span>
        }
      >
        <div style={{ display: "flex" }}>
          <Space direction="vertical" size={12}>
            <RangePicker
              picker="month"
              selected={monthYearInput}
              dateFormat="MM/yyyy"
              showMonthYearPicker
              onChange={(date, datrString) => {
                setFromTimeAccumulate(datrString);
                setMonthYearInput(datrString);
              }}
            />
          </Space>
          <Tooltip title="Báo cáo chỉ hiển thị những tháng đã được tạo chỉ tiêu trong phần Chỉ tiêu dự án">
            <Button
              style={{ marginLeft: "10px" }}
              onClick={() => getData()}
              type="primary"
            >
              Lọc
            </Button>
          </Tooltip>
        </div>
        {dataChart ? (
          <ResponsiveContainer width={980} height={500}>
            <ComposedChart
              width={900}
              height={600}
              //  data={data}
              data={dataChart ? dataChart.data : null}
              margin={{
                top: 100,
                right: 20,
                bottom: 20,
                left: 0
              }}
            >
              <YAxis
                ticks={[
                  0,
                  10,
                  20,
                  30,
                  40,
                  50,
                  60,
                  70,
                  80,
                  90,
                  100,
                  110,
                  120,
                  130,
                  140,
                  150,
                  160
                ]}
                domain={[0, 160]}
              />
              <YAxis
                yAxisId="right"
                type="number"
                dataKey="PCV"
                name="pcv"
                unit="%"
                orientation="right"
                stroke="#82ca9d"
                ticks={[0, 20, 40, 60, 80, 100, 120, 140, 160]}
                domain={[0, 160]}
              />
              <CartesianGrid stroke="#f5f5f5" />
              <Tooltip />
              <Legend
              // align="left"
              // layout="vertical"
              // wrapperStyle={style}
              />
              <XAxis
                dataKey="name"
                scale="auto"
                tickLine={false}
                interval={0}
              />

              <Bar
                dataKey="result"
                barSize={20}
                fill="#413ea0"
                name="Tổng số căn bán thành công"
              />
              <Bar
                dataKey="target"
                barSize={20}
                fill="#e19129"
                name="Chỉ tiêu bán theo kế hoạch CĐT"
              />
              <Bar
                dataKey="targetWeland"
                barSize={20}
                fill="#ba68c8"
                name="Chỉ tiêu bán theo kế hoạch Weland"
              />
              {/* <Line dataKey="PCV" yAxisId="right" stroke="#ff7300" dot={true} /> */}
              <Line
                dataKey="percent"
                yAxisId="right"
                stroke="#ff7300"
                name="% thực hiện so với KH CĐT"
                dot={false}
                label={renderCustomizedLabel}
              />
              <Line
                dataKey="percentWeland"
                yAxisId="right"
                stroke="#81c784"
                name="% thực hiện so với KH Weland"
                dot={false}
                label={renderCustomizedLabel}
              />
            </ComposedChart>
          </ResponsiveContainer>
        ) : (
          ""
        )}
      </Card>
    </>
  );
};

export default CompletedReport;
