import React, { FC, useCallback, memo } from 'react';
import { Modal, Form } from 'antd';
import { DownCircleOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import XLSX from 'xlsx';
import { v4 as uuidv4 } from 'uuid';
import dayjs from 'dayjs';
import cloneDeep from 'lodash/cloneDeep';
// import { useIntl } from '../../../../packages/pro-table/component/intlContext';
import UploadFile from '../../../../components/UploadFile';
import { fromExcelDate } from '../../../../util/helpers';

const handleFile = (file: any, onChange: (data: any) => any) => {
  const reader = new FileReader();
  reader.onload = (e: any) => {
    try {
      const dataBit = new Uint8Array(e.target.result);
      const wb = XLSX.read(dataBit, { type: 'array' });
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      const data = XLSX.utils.sheet_to_json(ws);
      // const cols = make_cols(ws['!ref']);
      // setGodens(origin => data);
      if(onChange) {
        onChange(data);
      }
    } catch(error) {
      console.log(`🚀 ~ file: model:procedureProject ~ line 41 ~ handleFile ~ error`, error);
    }
  };
  reader.readAsArrayBuffer(file);
};

interface ProcedureProjectImportProp {
  projectId: number
  importVisible: boolean
  setImportVisible: (visible: boolean) => void
  dispatch: any,
  history: any,
  match: any
}

const ProcedureProjectImportComp: FC<ProcedureProjectImportProp> = (props: ProcedureProjectImportProp) => {
  const {
    dispatch,
    projectId,
    importVisible,
    setImportVisible,
    history,
    match
  } = props;
  const [form] = Form.useForm();

  const handleCancel = useCallback(() => {
    setImportVisible(false);
  }, [setImportVisible]);

  const onFinish = useCallback((values) => {
    const data = cloneDeep(values);

    const pathname = match.url.replace('procedureProject', 'procedureProjectImport');
    if(data.xlsx[0]) {
      handleFile(data.xlsx[0].originFileObj, (res) => {
        const params = {
          data: (res || []).map((i: any) => {
            return {
              "fullCode": i["Mã căn"],
              "name": i["Tình trạng HĐ"],
              "dayTTDC": dayjs(fromExcelDate(i["Ngày ký HĐVV/TTĐC"])).valueOf(),
              "dayHDMB": dayjs(fromExcelDate(i["Ngày ký HĐMB"])).valueOf(),
              "totalPriceCalcu": Number(i["Tổng giá trị chưa VAT đưa vào HĐMB"] || "0"),
              "vat": Number(i["VAT"] || "0"),
              "salePolicyName": i["Chính sách bán hàng"],
              id: uuidv4()
            };
          }),
          projectId
        };
        dispatch({
          type: "procedureProject/preImportData",
          payload: params,
          callback: () => {
            history.push({ pathname });
          }
        });
      });
    }
  }, [projectId]);

  const onFinishFailed = useCallback((errors) => {
    console.log(`🚀 ~ file: ProcedureProjectImport.tsx ~ line 33 ~ onFinishFailed ~ errors`, errors);
  }, []);

  return (
    <React.Fragment>
      <Modal
        title="Import thủ tục thu tiền căn hộ"
        visible={importVisible}
        onCancel={handleCancel}
        onOk={() => form.submit()}
      >
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label={"Chọn file excel"}
          // noStyle
          >
            <div className="gx-d-flex gx-flex-nowrap gx-justify-content-start gx-align-items-center">
              <Form.Item
                name="xlsx"
                rules={[
                  { required: true, message: 'Vui lòng chọn file dữ liệu để import!' }
                ]}
                noStyle
              >
                <UploadFile autoUpload={false} />
              </Form.Item>
              <a className="gx-ml-3" href={`/static/templates/procedureProject.xlsx`} download><DownCircleOutlined />&nbsp;Tải File mẫu</a>
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};

const ProcedureProjectImport: FC<ProcedureProjectImportProp> = memo<any>(connect(null)(ProcedureProjectImportComp));

export default ProcedureProjectImport;
