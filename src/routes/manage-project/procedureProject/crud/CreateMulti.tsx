import React, { FC, useCallback, memo } from 'react';
import { Modal, Form } from 'antd';
import { connect } from 'dva';
import dayjs from 'dayjs';
import cloneDeep from 'lodash/cloneDeep';
// import { DatePicker } from '../../../../packages/pro-component/index';
import { FormInputRender } from '../../../../packages/pro-table/form';
import { useIntl } from '../../../../packages/pro-table/component/intlContext';
import Upload from '../../../../components/Upload';
import ApartmentSelect from '../../../../components/Select/Apartment/ListApartment';

interface ProcedureProjectCreateMultiProp {
  projectId: number
  createMultiVisible: boolean
  setCreateMultiVisible: (visible: boolean) => void
  dispatch: any
}

const ProcedureProjectCreateMultiComp: FC<ProcedureProjectCreateMultiProp> = (props: ProcedureProjectCreateMultiProp) => {
  const {
    dispatch,
    projectId,
    createMultiVisible,
    setCreateMultiVisible
  } = props;
  const [form] = Form.useForm();
  const intl = useIntl();

  const handleCancel = useCallback(() => {
    setCreateMultiVisible(false);
  }, [setCreateMultiVisible]);

  const onFinish = useCallback((values) => {
    const data = cloneDeep(values);
    if(data.images) {
      data.images =
        data.images && data.images.length > 0
          ? data.images
            .map(
              (i: any) =>
                i.url ||
                (i.response && i.response.length > 0
                  ? i.response[0].url
                  : ''),
            )
            .toString()
          : '';
    }
    if(data.apartmentId && typeof data.apartmentId == 'object') {
      data.apartmentId = data.apartmentId.id;
    }
    if(data.timeSign) {
      data.timeSign = dayjs(data.timeSign).valueOf();
    }
    const params = {
      ...data,
      projectId
    };

    console.log(`🚀 ~ file: ProcedureProjectCreateMulti.tsx ~ line 32 ~ onFinish ~ params`, params);
    dispatch({
      type: "procedureProject/createMulti",
      payload: params,
      callback: (res: any) => {
        console.log(`🚀 ~ file: ProcedureProjectCreateMulti.tsx ~ line 39 ~ onFinish ~ res`, res);
      }
    });
  }, [projectId]);

  const onFinishFailed = useCallback((errors) => {
    console.log(`🚀 ~ file: ProcedureProjectCreateMulti.tsx ~ line 33 ~ onFinishFailed ~ errors`, errors);
  }, []);

  return (
    <React.Fragment>
      <Modal
        title="Thêm mới thủ tục căn hộ"
        visible={createMultiVisible}
        onCancel={handleCancel}
        onOk={() => form.submit()}
      >
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label={"Tên thủ tục"}
            name="name"
            rules={[
              { required: true, message: 'Tên thủ tục không được trống' }
            ]}
          >
            <FormInputRender
              item={{
                "title": "Ngày kí", "dataIndex": "name", "width": 120,
                formItemProps: {},
                "valueEnum": {
                  "Hợp đồng mua bán": { "text": "Hợp đồng mua bán", "status": "Processing", "color": "#ec3b3b", "isText": true },
                  "Hợp đồng vay vốn": { "text": "Hợp đồng vay vốn", "status": "Default", "color": "#ec3b3b", "isText": true },
                  "Thỏa thuận đặt cọc": { "text": "Thỏa thuận đặt cọc", "status": "Success", "color": "#ec3b3b", "isText": true }
                },
              }}
              intl={intl}
              type="form"
            />
          </Form.Item>
          <Form.Item
            label={"Ngày kí"}
            name="timeSign"
            rules={[
              { required: true, message: 'Ngày kí không được trống' }
            ]}
          >
            <FormInputRender
              item={{
                "title": "Ngày kí", "dataIndex": "timeSign", "width": 120,
                formItemProps: {}, valueType: "dateTime"
              }}
              intl={intl}
              type="form"
            />
          </Form.Item>
          <Form.Item
            label={"Ảnh"}
            name="images"
            rules={[
              { required: false, message: 'Tên thủ tục không được trống' }
            ]}
          >
            <Upload />
          </Form.Item>
          <Form.Item
            label={"Căn hộ"}
            name="apartmentId"
            rules={[
              { required: true, message: 'Căn hộ không được trống' }
            ]}
          >
            <ApartmentSelect type="radio" paramsList={{ projectId }} />
          </Form.Item>
          <Form.Item
            label={"Giá"}
            name="totalPriceCalcu"
            rules={[
              { required: true, message: 'Giá không được trống' }
            ]}
          >
            <FormInputRender
              item={{
                "title": "Giá", "dataIndex": "totalPriceCalcu", "width": 120,
                formItemProps: {}, valueType: "digit"
              }}
              intl={intl}
              type="form"
            />
          </Form.Item>
          <Form.Item
            label={"VAT"}
            name="vat"
            rules={[
              { required: true, message: 'VAT không được trống' }
            ]}
          >
            <FormInputRender
              item={{
                "title": "VAT", "dataIndex": "vat", "width": 120,
                formItemProps: {}, valueType: "digit"
              }}
              intl={intl}
              type="form"
            />
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};

const ProcedureProjectCreateMulti: FC<ProcedureProjectCreateMultiProp> = memo<any>(connect(null)(ProcedureProjectCreateMultiComp));

export default ProcedureProjectCreateMulti;
