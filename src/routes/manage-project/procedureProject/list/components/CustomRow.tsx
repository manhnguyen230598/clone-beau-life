import React, { } from 'react';

interface CustomRowProps {
  index: number;
  children: any;
  style?: object | any
}

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const rKey = props.children.find((i: any) => i.key == "RC_TABLE_KEY")?.props?.record;
  const style = rKey ?. isSuccess ?. value == false ? { background:'#fad7d7' } : {};
  return (
    <React.Fragment>
      <tr {...props} style={{ ...(props?.style), ...style }} />
    </React.Fragment>
  );
};

export default CustomRow;
