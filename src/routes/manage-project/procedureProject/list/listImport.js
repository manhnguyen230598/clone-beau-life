import React, { useState, useEffect } from 'react';
import { Form, Button, Popconfirm, Typography, Tooltip } from 'antd';
import { PlusOutlined, CloseCircleOutlined, CheckCircleOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import ProTable from 'packages/pro-table/Table';
import EditableCell from './components/EditableCell';
import CustomRow from './components/CustomRow';
import local from 'util/local';

const RESOURCE = "procedureProject";
const ProcedureProjectImportList = (props) => {
  const {
    dispatch,
    [RESOURCE]: { import: { data: originData, projectId } },
  } = props;
  const [form] = Form.useForm();
  const [data, setData] = useState(originData);
  const [editingKey, setEditingKey] = useState('');
  const [columnsStateMap, setColumnsStateMap] = useState({ "id": { "show": false, "fixed": "left" }, "isSuccess": { "show": false } });
  const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  useEffect(() => {
    setData(originData);
  }, [originData]);

  const isEditing = (record) => record.id === editingKey;

  const edit = (record) => {
    form.setFieldsValue({
      name: '',
      age: '',
      address: '',
      ...record,
    });
    setEditingKey(record.id);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key) => {
    try {
      const row = await form.validateFields();
      const newData = [...data];
      const index = newData.findIndex((item) => key === item.id);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
        setData(newData);
        setEditingKey('');
      } else {
        newData.push(row);
        setData(newData);
        setEditingKey('');
      }
      local.set("procedureProjectImport", JSON.stringify({
        data: newData,
        projectId
      }));
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const doImport = (dataImport) => {
    dispatch({
      type: "procedureProject/importData",
      payload: {
        projectId,
        data: dataImport
      },
      callback: (res) => {
        console.log(`🚀 ~ file: ProcedureProjectCreateMulti.tsx ~ line 39 ~ onFinish ~ res`, res);
        if (res?.error) {

        } else {
          setData(origin => {
            return origin.map(item => {
              const isFail = ((res?.data?.fail) ?? []).find(i => i.id == item.id);
              if (isFail) {
                return {
                  ...item,
                  isSuccess: {
                    value: false,
                    message: isFail.message
                  }
                };
              }
              return {
                ...item,
                isSuccess: {
                  value: true,
                  message: "Thành công"
                }
              };
            });
          });
          setColumnsStateMap(origin => {
            return {
              ...origin,
              "isSuccess": { "show": true }
            };
          });
        }
      }
    });
  };

  const handleAddClick = () => {
    doImport(data);
  };

  const triggerChange = (key, data) => {
    /* if(onChange) {
      onChange({ key, data });
    } */
  };

  const handleSelectRows = (keys, rows) => {
    setSelectedRows(rows);
    setSelectedRowKeys(keys);
    if (props.modeChoose === 'radio') {
      if (keys.length > 0) {
        triggerChange(keys[0], rows[0]);
      }
    } else {
      triggerChange(keys, rows);
    }
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Mã căn',
      dataIndex: 'fullCode',
      width: 200,
      editable: true,
    },
    {
      title: 'Tình trạng HĐ',
      dataIndex: 'name',
      width: 200,
      editable: true,
    },
    {
      title: 'Ngày ký HĐVV/TTĐC',
      dataIndex: 'dayTTDC',
      valueType: 'date',
      width: 150,
      sorter: true,
      editable: true,
    },
    {
      title: 'Ngày ký HĐMB',
      dataIndex: 'dayHDMB',
      valueType: 'date',
      width: 150,
      sorter: true,
      editable: true,
    },
    {
      title: 'Tổng giá chưa VAT',
      dataIndex: 'totalPriceCalcu',
      valueType: 'money',
      width: 200,
      editable: true,
    },
    {
      title: 'VAT',
      dataIndex: 'vat',
      valueType: 'money',
      width: 200,
      editable: true,
    },
    {
      title: 'Chính sách bán hàng',
      dataIndex: 'salePolicyName',
      width: 200,
      editable: true,
    },
    {
      title: 'Đồng bộ',
      dataIndex: 'isSuccess',
      valueType: 'switch',
      width: 100,
      render: (val, record) => {
        if (typeof val == 'object' && val?.value == false) {
          return (
            <Tooltip title={val?.message ?? ''}>
              <CloseCircleOutlined
                style={{ color: "red" }}
                onClick={() => {
                  doImport([record]);
                }}
              />
            </Tooltip>
          );
        } else if (typeof val == 'object' && val?.value == true) {
          return <CheckCircleOutlined style={{ color: "#15eb20" }} />;
        }
        return null;
      }
    },
    {
      title: 'Thao tác',
      dataIndex: 'operation',
      width: 120,
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <a
              href="javascript:;"
              onClick={() => save(record.id)}
              style={{
                marginRight: 8,
              }}
            >
              Lưu
            </a>
            <Popconfirm title="Bạn chắc chắn?" onConfirm={cancel}>
              <a>Hủy</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
            Sửa
          </Typography.Link>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.valueType === 'digit' ? 'number' : (col.valueType === 'date' ? 'date' : 'text'),
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  const rowSelection = {
    type: 'checkbox',
    renderCell: (checked, record, index, originNode) => {
      return (
        <Tooltip title={"Chọn bản ghi"}>
          {originNode}
        </Tooltip>
      );
    },
    onChange: handleSelectRows
  };

  console.log(`🚀 ~ file: listImport.js ~ line 18 ~ ProcedureProjectImportList ~ data`, data);
  return (
    <>
      <Form form={form} component={false}>
        <ProTable
          tableClassName="gx-table-responsive"
          type="table"
          rowKey="id"
          search={false}
          headerTitle="Danh sách import"
          components={{
            body: {
              cell: EditableCell,
              row: CustomRow
            },
          }}
          bordered
          dataSource={data}
          columns={mergedColumns}
          columnsStateMap={columnsStateMap}
          onColumnsStateChange={(mapCols) => {
            setColumnsStateMap(mapCols);
          }}
          rowClassName="nv-editable-row"
          // pagination={{
          //   onChange: cancel,
          // }}
          rowSelection={rowSelection}
          pagination={false}
          toolBarRender={(action, { selectedRows }) => [
            <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
              {`Cập nhật toàn bộ`}
            </Button>,
            selectedRows?.length && <Button icon={<PlusOutlined />} type="default" onClick={() => doImport(selectedRows)}>
              {`Cập nhật đã chọn`}
            </Button>
          ]}
        />
      </Form>
    </>
  );
};

export default connect(({ procedureProject, loading }) => ({
  procedureProject,
  loading: loading.models.procedureProject
}))(ProcedureProjectImportList);
