/* eslint-disable array-callback-return */
import React, { useCallback, useEffect, useState } from "react";
import {
  ComposedChart,
  Line,
  Bar,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer
} from "recharts";
import { BarChart } from "recharts";
import { PieChart, Pie, Sector, Cell } from "recharts";
import { Row, Card, Divider } from "antd";
import { Col } from "antd";
import Widget from "src/components/Widget";
import * as project from "src/services/project";

var date1 = new Date();
var currentMonth2 = date1.getMonth() + 1;
var currentYear2 = date1.getFullYear();

const CustomerBought = props => {
  const [month, setMonth] = useState(currentMonth2);
  const [year, setYear] = useState(currentYear2);
  const [pieMedia, setPieMedia] = useState([]);
  const [activeIndex, setActiveIndex] = useState(0);
  const onPieEnter = useCallback(
    (_, index) => {
      setActiveIndex(index);
    },
    [setActiveIndex]
  );

  const getDataMeida = async () => {
    let api = await project.customerBought(props.projectId, month, year);
    setPieMedia(api.data.data);
  };
  useEffect(() => {
    getDataMeida();
  }, [month, year]);

  //---------------------------
  const COLORS = [
    "#0088FE",
    "#00C49F",
    "#FFBB28",
    "#FF8042",
    "#33FF66",
    "#FF99FF"
  ];
  const RADIAN = Math.PI / 180;

  const renderCustomizedLabel1 = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill="white"
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };
  const renderActiveShape = props => {
    const {
      cx,
      cy,
      midAngle,
      innerRadius,
      outerRadius,
      startAngle,
      endAngle,
      fill,
      percent,
      value
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? "start" : "end";

    return (
      <>
        <g>
          {/* <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
          {payload.name}
        </text> */}
          <Sector
            cx={cx}
            cy={cy}
            innerRadius={innerRadius}
            outerRadius={outerRadius}
            startAngle={startAngle}
            endAngle={endAngle}
            fill={fill}
          />
          <Sector
            cx={cx}
            cy={cy}
            startAngle={startAngle}
            endAngle={endAngle}
            innerRadius={outerRadius + 6}
            outerRadius={outerRadius + 10}
            fill={fill}
          />
          <path
            d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
            stroke={fill}
            fill="none"
          />

          <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
          <text
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            textAnchor={textAnchor}
            fill="#333"
          >{`PV ${value}`}</text>
          <text
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            dy={18}
            textAnchor={textAnchor}
            fill="#999"
          >
            {`(Rate ${(percent * 100).toFixed(2)}%)`}
          </text>
        </g>
      </>
    );
  };

  return (
    <>
      <div
        className="nv-group-calender"
        style={{ display: "flex", float: "right", position: "static" }}
      >
        <div className="nv-select-month">
          <select
            name="mdz"
            defaultValue={currentMonth2}
            onChange={e => setMonth(e.target.value)}
          >
            <option value="1">Tháng 1</option>
            <option value="2">Tháng 2</option>
            <option value="3">Tháng 3</option>
            <option value="4">Tháng 4</option>
            <option value="5">Tháng 5</option>
            <option value="6">Tháng 6</option>
            <option value="7">Tháng 7</option>
            <option value="8">Tháng 8</option>
            <option value="9">Tháng 9</option>
            <option value="10">Tháng 10</option>
            <option value="11">Tháng 11</option>
            <option value="12">Tháng 12</option>
          </select>
        </div>
        <div className="nv-select-month">
          <select
            name="mdz"
            defaultValue={currentYear2}
            onChange={e => setYear(e.target.value)}
          >
            <option value="2018">Năm 2018</option>
            <option value="2019">Năm 2019</option>
            <option value="2020">Năm 2020</option>
            <option value="2021">Năm 2021</option>
          </select>
        </div>
      </div>
      <div style={{ marginTop: "50px" }}>
        <Widget styleName="gx-card-full is-build-chartclass">
          <div className="nv-namechart">
            <span>Về địa phương</span>
          </div>
          <Divider />
          <Row>
            <Col xl={12} lg={12} md={12} sm={12} xs={24}>
              <div className="nv-namechart">
                <span>Định hướng</span>
              </div>
              <Row>
                <Col xl={16} lg={16} md={16} sm={16} xs={24}>
                  <ResponsiveContainer width="100%" height={350}>
                    <PieChart>
                      <Pie
                        // activeIndex={activeIndex}
                        activeShape={renderActiveShape}
                        data={pieMedia ? pieMedia.pieAreaTarget : null}
                        cx={"50%"}
                        cy={"50%"}
                        labelLine={false}
                        label={renderCustomizedLabel1}
                        outerRadius={80}
                        fill="#8884d8"
                        dataKey="value"
                        // onMouseEnter={onPieEnter}
                      >
                        {pieMedia
                          ? (
                              pieMedia.pieAreaTarget || []
                            ).map((entry, index) => (
                              <Cell
                                key={`cell-${index}`}
                                fill={COLORS[index % COLORS.length]}
                              />
                            ))
                          : null}
                      </Pie>
                      <Legend />
                    </PieChart>
                  </ResponsiveContainer>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={24}>
                  {pieMedia
                    ? (pieMedia.pieAreaTarget || []).map((item, index) => {
                        return (
                          <>
                            <div className="nv-info-classroom">
                              <span key={index}>{item.name}: </span>
                              <span key={index}>{item.value} KH</span>
                            </div>
                          </>
                        );
                      })
                    : null}
                </Col>
              </Row>
            </Col>
            <Col
              xl={12}
              lg={12}
              md={12}
              sm={12}
              xs={24}
              style={{ borderLeft: "solid 1px #eceff1" }}
            >
              <div className="nv-namechart">
                <span>Thực tế</span>
              </div>
              <Row>
                <Col xl={16} lg={16} md={16} sm={16} xs={24}>
                  <ResponsiveContainer width="100%" height={350}>
                    <PieChart width={600} height={350}>
                      <Pie
                        // activeIndex={activeIndex}
                        activeShape={renderActiveShape}
                        data={pieMedia ? pieMedia.pieAreaNow : null}
                        cx={"50%"}
                        cy={"50%"}
                        labelLine={false}
                        label={renderCustomizedLabel1}
                        outerRadius={80}
                        fill="#8884d8"
                        dataKey="value"
                        // onMouseEnter={onPieEnter}
                      >
                        {pieMedia
                          ? (pieMedia.pieAreaNow || []).map((entry, index) => (
                              <Cell
                                key={`cell-${index}`}
                                fill={COLORS[index % COLORS.length]}
                              />
                            ))
                          : null}
                      </Pie>
                      <Legend />
                    </PieChart>
                  </ResponsiveContainer>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={24}>
                  {pieMedia
                    ? (pieMedia.pieAreaNow || []).map((item, index) => {
                        return (
                          <>
                            <div className="nv-info-classroom">
                              <span key={index}>{item.name}: </span>
                              <span key={index}>{item.value} KH</span>
                            </div>
                          </>
                        );
                      })
                    : null}
                </Col>
              </Row>
            </Col>
          </Row>
        </Widget>
        <Widget styleName="gx-card-full is-build-chartclass">
          <div className="nv-namechart">
            <span>Về độ tuổi</span>
          </div>
          <Divider />
          <Row>
            <Col xl={12} lg={12} md={12} sm={12} xs={24}>
              <div className="nv-namechart">
                <span>Định hướng</span>
              </div>
              <Row>
                <Col xl={16} lg={16} md={16} sm={16} xs={24}>
                  <ResponsiveContainer width="100%" height={350}>
                    <PieChart width={600} height={350}>
                      <Pie
                        // activeIndex={activeIndex}
                        activeShape={renderActiveShape}
                        data={pieMedia ? pieMedia.pieAgeTarget : null}
                        cx={"50%"}
                        cy={"50%"}
                        labelLine={false}
                        label={renderCustomizedLabel1}
                        outerRadius={80}
                        fill="#8884d8"
                        dataKey="value"
                        // onMouseEnter={onPieEnter}
                      >
                        {pieMedia
                          ? (pieMedia.pieAgeTarget || []).map((entry, index) => (
                              <Cell
                                key={`cell-${index}`}
                                fill={COLORS[index % COLORS.length]}
                              />
                            ))
                          : null}
                      </Pie>
                      <Legend />
                    </PieChart>
                  </ResponsiveContainer>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  {pieMedia
                    ? (pieMedia.pieAgeTarget || []).map((item, index) => {
                        return (
                          <>
                            <div className="nv-info-classroom">
                              <span key={index}>{item.name}: </span>
                              <span key={index}>{item.value} KH</span>
                            </div>
                          </>
                        );
                      })
                    : null}
                </Col>
              </Row>
            </Col>
            <Col
              xl={12}
              lg={12}
              md={12}
              sm={12}
              xs={24}
              style={{ borderLeft: "solid 1px #eceff1" }}
            >
              <div className="nv-namechart">
                <span>Thực tế</span>
              </div>
              <Row>
                <Col xl={16} lg={16} md={16} sm={16} xs={24}>
                  <ResponsiveContainer width="100%" height={350}>
                    <PieChart width={600} height={350}>
                      <Pie
                        // activeIndex={activeIndex}
                        activeShape={renderActiveShape}
                        data={pieMedia ? pieMedia.pieAgeNow : null}
                        cx={"50%"}
                        cy={"50%"}
                        labelLine={false}
                        label={renderCustomizedLabel1}
                        outerRadius={80}
                        fill="#8884d8"
                        dataKey="value"
                        // onMouseEnter={onPieEnter}
                      >
                        {pieMedia
                          ? (pieMedia.pieAgeNow || []).map((entry, index) => (
                              <Cell
                                key={`cell-${index}`}
                                fill={COLORS[index % COLORS.length]}
                              />
                            ))
                          : null}
                      </Pie>
                      <Legend />
                    </PieChart>
                  </ResponsiveContainer>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={24}>
                  {pieMedia
                    ? (pieMedia.pieAgeNow || []).map((item, index) => {
                        return (
                          <>
                            <div className="nv-info-classroom">
                              <span key={index}>{item.name}: </span>
                              <span key={index}>{item.value} KH</span>
                            </div>
                          </>
                        );
                      })
                    : null}
                </Col>
              </Row>
            </Col>
          </Row>
        </Widget>
        <Widget styleName="gx-card-full is-build-chartclass">
          <Row>
            <Col xl={12} lg={12} md={12} sm={12} xs={24}>
              <div className="nv-namechart">
                <span>Sản phẩm lựa chọn</span>
              </div>
              <Row>
                <Col xl={16} lg={16} md={16} sm={16} xs={24}>
                  <ResponsiveContainer width="100%" height={350}>
                    <PieChart width={600} height={350}>
                      <Pie
                        // activeIndex={activeIndex}
                        activeShape={renderActiveShape}
                        data={pieMedia ? pieMedia.pieRoom : null}
                        cx={"50%"}
                        cy={"50%"}
                        labelLine={false}
                        label={renderCustomizedLabel1}
                        outerRadius={80}
                        fill="#8884d8"
                        dataKey="value"
                        // onMouseEnter={onPieEnter}
                      >
                        {pieMedia
                          ? (pieMedia.pieRoom || []).map((entry, index) => (
                              <Cell
                                key={`cell-${index}`}
                                fill={COLORS[index % COLORS.length]}
                              />
                            ))
                          : null}
                      </Pie>
                      <Legend />
                    </PieChart>
                  </ResponsiveContainer>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  {pieMedia
                    ? (pieMedia.pieRoom || []).map((item, index) => {
                        return (
                          <>
                            <div className="nv-info-classroom">
                              <span key={index}>{item.name}: </span>
                              <span key={index}>{item.value} KH</span>
                            </div>
                          </>
                        );
                      })
                    : null}
                </Col>
              </Row>
            </Col>
            <Col
              xl={12}
              lg={12}
              md={12}
              sm={12}
              xs={24}
              style={{ borderLeft: "solid 1px #eceff1" }}
            >
              <div className="nv-namechart">
                <span>CSBH lựa chọn</span>
              </div>
              <Row>
                <Col xl={16} lg={16} md={16} sm={16} xs={24}>
                  <ResponsiveContainer width="100%" height={350}>
                    <PieChart width={600} height={350}>
                      <Pie
                        // activeIndex={activeIndex}
                        activeShape={renderActiveShape}
                        data={pieMedia ? pieMedia.piePolicy : null}
                        cx={"50%"}
                        cy={"50%"}
                        labelLine={false}
                        label={renderCustomizedLabel1}
                        outerRadius={80}
                        fill="#8884d8"
                        dataKey="value"
                        // onMouseEnter={onPieEnter}
                      >
                        {pieMedia
                          ? (pieMedia.piePolicy || []).map((entry, index) => (
                              <Cell
                                key={`cell-${index}`}
                                fill={COLORS[index % COLORS.length]}
                              />
                            ))
                          : null}
                      </Pie>
                      <Legend />
                    </PieChart>
                  </ResponsiveContainer>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={24}>
                  {pieMedia
                    ? (pieMedia.piePolicy || []).map((item, index) => {
                        return (
                          <>
                            <div className="nv-info-classroom">
                              <span key={index}>{item.name || "Không chọn"}: </span>
                              <span key={index}>{item.value} KH</span>
                            </div>
                          </>
                        );
                      })
                    : null}
                </Col>
              </Row>
            </Col>
          </Row>
        </Widget>
        
      </div>
    </>
  );
};

export default CustomerBought;
