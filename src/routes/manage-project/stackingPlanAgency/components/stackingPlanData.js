import React, { useState, useEffect, useCallback } from "react";
import {
  Modal,
  Form,
  Button,
  Row,
  Col,
  Radio,
  Select,
  Badge,
  Divider,
  message
} from "antd";
import { PlusCircleOutlined, CheckCircleTwoTone } from "@ant-design/icons";
import { connect } from "dva";
import AgencySelect from "src/components/Select/Agency/ListAgency";
import UserSelect from "src/components/Select/User/ListUser";
import * as stackingPlanDataServices from "src/services/stackingPlanData";
import { get, cloneDeep, uniq } from "lodash";
import { formatMoney } from "src/util/utils";
import Loading from "src/components/Loading";
import { v4 as uuid } from "uuid";
const { Option } = Select;
const colorMap = {
  hide: "#e0e0e0",
  display: "#aee571"
};
const StackingPlanData = ({
  item,
  isAddStackingPlan,
  projectName,
  setIsViewModal,
  projectId,
  dispatch,
  type
}) => {
  const [form] = Form.useForm();
  const [agency, setAgency] = useState();
  const [isAgencyDisable, setIsAgencyDisable] = useState(false);
  const [dataRow, setDataRow] = useState([]);
  const [dataCollumn, setDataCollumn] = useState([]);
  const [listApt, setListApt] = useState([]);

  const [buildingCode, setbuildingCode] = useState();
  const [list, setList] = useState([]);
  const [height, setHeight] = useState(40);
  const [width, setWidth] = useState(80);
  const [selectedItem, setSelectedItem] = useState([]);
  const [aptRow, setAptRow] = useState({});
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();
  useEffect(() => {
    if (item && item.id) {
      fetchData(item.id);
    }
  }, [item]);
  const fetchData = async id => {
    setLoading(true);
    const response = await stackingPlanDataServices.getList({
      stackingPlanId: id
    });
    setLoading(false);
    let data = get(response, "data", {});
    setData(data);
    let list = [];
    setDataRow(data.row);
    setDataCollumn(data.column);

    setList(data.data);
    if (type === "house") {
      let build = data?.building?.at(0);
      setbuildingCode(build || "");
      let newArr = data?.data?.filter(item => item.buildingCode === build);
      setListApt(newArr);
    } else {
      setListApt(data.data);
    }
  };
  useEffect(() => {
    let temp = {};
    if (listApt.length > 0) {
      if (type === "house") {
        listApt.map(e => {
          if (!temp[e.floorCode]) temp[e.floorCode] = [];
          temp[e.floorCode].push(e);
        });
      } else {
        listApt.map(e => {
          if (!temp[e.row]) temp[e.row] = [];
          temp[e.row].push(e);
        });
      }
    }
    setAptRow(temp);
  }, [listApt]);
  useEffect(() => {
    if (type === "house") {
      let row = new Set();
      let collumn = new Set();
      listApt.map(e => {
        row.add(e.floorCode);
        collumn.add(e.apartmentCode);
      });
      row = [...row].sort((a, b) => {
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }

        // names must be equal
        return 0;
      });
      collumn = [...collumn].sort((a, b) => {
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }

        // names must be equal
        return 0;
      });
      setDataRow([...row]);
      setDataCollumn([...collumn]);
    }
  }, [listApt]);
  const onFinish = useCallback(
    values => {
      let params = {
        agencyId: (values.agencyId || {}).id,
        userIds: (values.userIds || []).map(i => i.id)
      };
      dispatch({
        type: "project/addStackingPlan",
        payload: {
          projectId,
          ...params
        },
        callback: res => {
          console.log(
            `🚀 ~ file: AddStackingPlan.tsx ~ line 41 ~ onFinish ~ res`,
            res
          );
        }
      });
    },
    [projectId]
  );

  const onFinishFailed = useCallback(errors => {
    console.log(
      `🚀 ~ file: AddStackingPlan.tsx ~ line 30 ~ onFinishFailed ~ errors`,
      errors
    );
  }, []);
  const handleOk = () => {
    // form.submit();
  };
  const onFinishUpdate = status => {
    console.log(selectedItem);
    let displayBody = {
      fullCodeIds: selectedItem,
      stackPlanId: item.id,
      status
    };
    stackingPlanDataServices.updateMulti(displayBody).then(res => {
      if (res.status == 200) {
        message.info("Cập nhật trạng thái thành công", 10);
      } else {
        message.error(res.data.message || "Cập nhật thất bại", 10);
      }
      setSelectedItem([]);
      fetchData(item.id);
    });
  };
  function handleChange(value) {
    if (value === "all") {
      setDataRow(data?.row);
      return;
    }
    setDataRow(data?.row?.filter(item => item === value));
  }
  function handleChangeBuilding(value) {
    setbuildingCode(value.target.value);
    let newArr = list.filter(item => item.buildingCode === value.target.value);
    setListApt(newArr);
  }
  const getMaxColumn = () => {
    let MAX = 1;
    for (let i = 0; i < data?.row?.length; i++) {
      let newMax = data?.data?.filter(item => item.row === data?.row[i]).length;
      if (newMax > MAX) {
        MAX = newMax;
      }
    }
    return MAX;
  };
  const hangdelSelectItem = identity => {
    let newArray = cloneDeep(selectedItem);
    if (newArray.includes(identity)) {
      newArray = newArray.filter(item => item !== identity);
      setSelectedItem(newArray);
    } else {
      newArray.push(identity);
      setSelectedItem(newArray);
    }
  };
  function handleChangeSubdivision(value) {
    if (value === "all") {
      setListApt(data?.list);
      return;
    }
    setListApt(list?.filter(item => item.subdivision === value));
  }
  function handleChangeWidth(value) {
    setWidth(value);
  }
  const handleChangeRowStatus = row => {
    let newArray = cloneDeep(selectedItem);
    let temp;
    if (type === "house") {
      temp = listApt.filter(
        e => e.floorCode === row && e.buildingCode === buildingCode
      );
    } else {
      temp = listApt.filter(e => e.row === row);
    }
    temp.map(e => {
      newArray.push(e.fullCode);
    });
    // newArray.push(temp);
    newArray = uniq(newArray);
    setSelectedItem(newArray);
  };
  return (
    <Modal
      visible={isAddStackingPlan}
      // onOk={handleOk}
      onCancel={() => {
        setIsViewModal(false);
      }}
      footer={null}
      // okText="Thêm"
      cancelText="Đóng"
      title={`Bảng hàng đại lý ${item?.agencyId?.name}`}
      width={"90%"}
      destroyOnClose
      style={{ maxHeight: "100vh" }}
    >
      {loading ? <Loading /> : ""}
      <Divider orientation="left">Thao tác</Divider>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div>
          <Button
            disabled={selectedItem.length === 0}
            onClick={() => {
              onFinishUpdate("display");
            }}
            type="primary"
          >
            Hiển thị đã chọn
          </Button>
          <Button
            onClick={() => {
              onFinishUpdate("hide");
            }}
            disabled={selectedItem.length === 0}
          >
            {" "}
            Ẩn đã chọn
          </Button>

          <Button
            danger
            onClick={() => {
              setSelectedItem([]);
            }}
            disabled={selectedItem.length === 0}
          >
            {" "}
            <i className="icon icon-close" />
            Bỏ chọn
          </Button>
        </div>
        <div>
          <span
            style={{
              width: "70px",
              height: "34px",
              border: "solid 1px",
              display: "inline-block",
              backgroundColor: colorMap["display"],
              borderRadius: "5px",
              textAlign: "center",
              lineHeight: "34px",
              fontWeight: 600
            }}
          >
            Hiển thị
          </span>

          <span
            style={{
              width: "70px",
              height: "34px",
              border: "solid 1px",
              margin: "0 10px",
              display: "inline-block",
              backgroundColor: colorMap["hide"],
              borderRadius: "5px",
              textAlign: "center",
              lineHeight: "34px",
              fontWeight: 600
            }}
          >
            Ẩn
          </span>
        </div>
      </div>
      <Divider orientation="left">Thông tin căn</Divider>
      {type === "house" ? (
        <>
          <div>
            <Row>
              <Col
                lg={{ span: 5, offset: 1 }}
                style={{ display: "flex", alignItems: "center" }}
              >
                <label style={{ marginRight: 5, fontWeight: 600 }}>
                  Chọn tòa:
                </label>
                <Radio.Group
                  onChange={handleChangeBuilding}
                  value={buildingCode}
                >
                  {data?.building?.map((item, index) => (
                    <Radio key={index} value={item}>
                      Tòa {item}
                    </Radio>
                  ))}
                </Radio.Group>
              </Col>
              <Col lg={{ span: 5, offset: 1 }}>
                <label style={{ fontWeight: 600 }}>Chọn tầng:</label>
                <Select
                  defaultValue="all"
                  style={{ width: 200, marginLeft: 10 }}
                  onChange={handleChange}
                >
                  <Option key={"all"} value="all">
                    Tất cả
                  </Option>
                  {data?.row?.map((item, index) => (
                    <Option key={index} value={item}>
                      Tầng {item}
                    </Option>
                  ))}
                </Select>
              </Col>
              <Col lg={{ span: 4, offset: 7 }}>
                <label style={{ fontWeight: 600 }}>Tỉ lệ:</label>
                <Select
                  defaultValue={"80"}
                  style={{ width: 100, marginLeft: 10 }}
                  onChange={handleChangeWidth}
                >
                  <Option value="80">100%</Option>
                  <Option value="40">50%</Option>
                </Select>
              </Col>
            </Row>
          </div>

          <div style={{ alignItems: "center", margin: "30px 0px" }}>
            <Row>
              <Col lg={1}>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px"
                  }}
                >
                  <i className="icon icon-company" />
                </p>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px",
                    overflow: "hidden",
                    whiteSpace: "nowrap"
                  }}
                >
                  <i className="icon icon-home" />
                </p>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px",
                    overflow: "hidden",
                    whiteSpace: "nowrap"
                  }}
                >
                  <i className=" icon icon-map-directions" />
                </p>
                <div style={{ height: "100%" }}>
                  {dataRow?.map((item, index) => (
                    <p
                      key={index}
                      style={{
                        height: `${height}px`,
                        margin: 0,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        handleChangeRowStatus(item);
                      }}
                    >
                      {item}
                    </p>
                  ))}
                </div>
              </Col>
              <Col lg={23}>
                <div style={{ overflowX: "scroll" }}>
                  <div style={{ width: (dataCollumn?.length || 0) * width }}>
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px"
                        }}
                      >
                        {item}
                      </p>
                    ))}
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px",
                          // overflow: "hidden",
                          whiteSpace: "nowrap"
                        }}
                      >
                        {data?.rowInfo[item]?.numberRooms || "-"}
                      </p>
                    ))}
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px",
                          overflow: "hidden",
                          whiteSpace: "nowrap"
                        }}
                      >
                        {data?.rowInfo[item]?.direction || "-"}
                      </p>
                    ))}

                    {dataRow?.map(row =>
                      dataCollumn?.map(column => {
                        let item = aptRow[row]?.find(ele => {
                          return (
                            ele.floorCode == row && ele.apartmentCode == column
                          );
                        });
                        if (item) {
                          return (
                            <div
                              key={item.id}
                              style={{
                                width: `${width}px`,
                                backgroundColor: colorMap[item.statusView],
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                cursor: "pointer",
                                position: "relative"
                              }}
                              onClick={() => hangdelSelectItem(item.fullCode)}
                            >
                              <p
                                style={{
                                  fontWeight: "bolder",
                                  color: "black",
                                  margin: 0
                                }}
                              >
                                <Badge
                                  count={
                                    selectedItem.includes(item.fullCode) ? (
                                      <CheckCircleTwoTone
                                        style={{ color: "#f5222d" }}
                                      />
                                    ) : (
                                      0
                                    )
                                  }
                                >
                                  {width > 40 ? (
                                    formatMoney(item.totalPrice)
                                  ) : (
                                    <i className="icon icon-home" />
                                  )}
                                </Badge>
                              </p>
                            </div>
                          );
                        } else {
                          return (
                            <div
                              key={uuid()}
                              style={{
                                width: `${width}px`,
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                backgroundImage:
                                  "repeating-linear-gradient( 45deg, black, black 2px, transparent 2px, transparent 5px)"
                              }}
                            >
                              <span style={{ marginBottom: "2px" }}>Trống</span>
                            </div>
                          );
                        }
                      })
                    )}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </>
      ) : (
        <>
          <div>
            <Row>
              <Col lg={{ span: 5, offset: 1 }}>
                <label>Chọn dãy:</label>
                <Select
                  defaultValue="all"
                  style={{ width: 200, marginLeft: 10 }}
                  onChange={handleChange}
                >
                  <Option key={"all"} value="all">
                    Tất cả
                  </Option>
                  {data?.row?.map((item, index) => (
                    <Option key={index} value={item}>
                      Dãy {item}
                    </Option>
                  ))}
                </Select>
              </Col>
              <Col lg={{ span: 4, offset: 14 }}>
                <label style={{ fontWeight: 600 }}>Tỉ lệ:</label>
                <Select
                  defaultValue={"80"}
                  style={{ width: 100, marginLeft: 10 }}
                  onChange={handleChangeWidth}
                >
                  <Option value="80">100%</Option>
                  <Option value="40">50%</Option>
                </Select>
              </Col>
            </Row>
          </div>

          <div style={{ alignItems: "center", marginTop: 30 }}>
            <Row>
              <Col lg={1}>
                <div style={{ height: "100%" }}>
                  <p
                    style={{
                      textAlign: "center",
                      border: "solid 1px",
                      overflow: "hidden",
                      whiteSpace: "nowrap"
                    }}
                  >
                    <i className="icon icon-home" />
                  </p>
                  {dataRow?.map((item, index) => (
                    <span
                      key={index}
                      style={{
                        height: `${height}px`,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        cursor : "pointer"
                      }}
                      onClick={() => {
                        handleChangeRowStatus(item);
                      }}
                    >
                      {item}
                    </span>
                  ))}
                </div>
              </Col>
              <Col lg={23}>
                {/* <div>
                    {data?.column?.map(item =>
                      <p style={{ display: "inline-block", width: customWidth(), textAlign: "center" }}>{item}</p>
                    )}
                  </div> */}
                <div style={{ overflowX: "scroll" }}>
                  <div
                    style={{ width: (dataCollumn?.length || 0) * width + 10 }}
                  >
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px"
                        }}
                      >
                        {item}
                      </p>
                    ))}
                  </div>
                  <div
                    style={{ width: (dataCollumn?.length || 0) * width + 10 }}
                  >
                    {dataRow?.map(row =>
                      dataCollumn?.map(column => {
                        let item = aptRow[row]?.find(ele => {
                          return ele.row == row && ele.apartmentCode == column;
                        });
                        if (item) {
                          return (
                            <div
                              key={item.id}
                              style={{
                                width: `${width}px`,
                                backgroundColor: colorMap[item.statusView],
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                cursor: "pointer",
                                position: "relative"
                              }}
                              onClick={() => hangdelSelectItem(item.fullCode)}
                            >
                              <p
                                style={{
                                  fontWeight: "bolder",
                                  color: "black",
                                  margin: 0
                                }}
                              >
                                <Badge
                                  count={
                                    selectedItem.includes(item.fullCode) ? (
                                      <CheckCircleTwoTone
                                        style={{ color: "#f5222d" }}
                                      />
                                    ) : (
                                      0
                                    )
                                  }
                                >
                                  {width > 40 ? (
                                    formatMoney(item.totalPrice)
                                  ) : (
                                    <i className="icon icon-home" />
                                  )}
                                </Badge>
                              </p>
                            </div>
                          );
                        } else {
                          return (
                            <div
                              key={uuid()}
                              style={{
                                width: `${width}px`,
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                backgroundImage:
                                  "repeating-linear-gradient( 45deg, black, black 2px, transparent 2px, transparent 5px)"
                              }}
                            >
                              <span style={{ marginBottom: "2px" }}>Trống</span>
                            </div>
                          );
                        }
                      })
                    )}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </>
      )}
    </Modal>
  );
};
export default StackingPlanData;
