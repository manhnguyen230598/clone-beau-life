import React, { useState, useEffect, useCallback } from "react";
import { Modal, Form, Button } from "antd";
import { PlusCircleOutlined } from "@ant-design/icons";
import { connect } from "dva";
import AgencySelect from "src/components/Select/Agency/ListAgency";
import UserSelect from "src/components/Select/User/ListUser";
const FixStackingPlan = ({
  data,
  isAddStackingPlan,
  mode,
  projectName,
  setIsViewModal,
  projectId,
  dispatch,
  fetchData
}) => {
  const [form] = Form.useForm();
  const [agency, setAgency] = useState();
  const [isAgencyDisable, setIsAgencyDisable] = useState(false);
  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...data
    });
    setAgency(data?.agencyId);
  }, [data]);
  const onFinish = useCallback(
    values => {
      let params = {
        agencyId: (values.agencyId || {}).id,
        userIds: (values.userIds || []).map(i => {
          if (typeof i === "object") return i.id;
          return i;
        })
      };
        dispatch({
          type: "project/addStackingPlan",
          payload: {
            projectId,
            ...params
          },
          callback: res => {
            fetchData()
          }
        });
    },
    [projectId]
  );

  const onFinishFailed = useCallback(errors => {
    console.log(
      `🚀 ~ file: AddStackingPlan.tsx ~ line 30 ~ onFinishFailed ~ errors`,
      errors
    );
  }, []);
  const handleOk = () => {
    // form.submit();
  };
  return (
    <Modal
      visible={isAddStackingPlan}
      // onOk={handleOk}
      onCancel={() => {
        setIsViewModal(false);
      }}
      footer={null}
      // okText="Thêm"
      cancelText="Đóng"
      title={`Bảng hàng dự án ${projectName}`}
      //   width={"90%"}
      destroyOnClose
      style={{ maxHeight: "100vh" }}
    >
      <Form
        form={form}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Đại lý"
          name="agencyId"
          rules={[{ required: true, message: "Bạn phải chọn đại lý" }]}
        >
          <AgencySelect
            disabled={mode === "edit"}
            onChange={value => {
              setAgency(value);
            }}
          />
        </Form.Item>
        <Form.Item
          label="Người book căn"
          name="userIds"
          rules={[{ required: true, message: "Bạn chưa chọn người book căn" }]}
        >
          <UserSelect modeChoose="checkbox" agencyId={agency?.id} />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          {mode === "add" ? (
            <Button
              type="primary"
              icon={<PlusCircleOutlined />}
              //   onClick={handleOk}
              htmlType="submit"
            >{`Thêm mới`}</Button>
          ) : (
            <Button
              type="primary"
              icon={<PlusCircleOutlined />}
              //   onClick={handleOk}
              htmlType="submit"
            >{`Sửa`}</Button>
          )}
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default FixStackingPlan;
