import React, { useState, useEffect, useRef } from "react";
import { Form, Input, Button, Space, Tooltip, Divider, Modal } from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  DotChartOutlined
} from "@ant-design/icons";
import { connect, routerRedux } from "dva";
import ProTable from "packages/pro-table/Table";
import { union } from "lodash";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import FixStackingPlan from "../components/fixStackIngPlan";
import StackingPlanData from "../components/stackingPlanData";
const tranformDocumentProjectParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "stackingPlan";
const StackingPlanList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();
  const [isViewModal, setIsViewModal] = useState(false);
  const [mode, setMode] = useState();
  const [dataItem, setData] = useState();
  const [dataChecklist, setDataChecklist] = useState({});
  //
  const [isViewStackingPlan, setIsViewStackingPlan] = useState(false);
  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    projectId,
    projectName,
    projectType
  } = props;

  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = () => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformDocumentProjectParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: union(initParams.sort || [], [{ id: "desc" }]),
        queryInput: {
          ...initParams.filters,
          projectId
        },
        populate: "agencyId"
      }
    });
  };
  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformDocumentProjectParams(
      filters,
      formValues,
      sorter
    );

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        projectId
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize),
      populate: "projectId, agencyId"
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", 'false');
          values = {};
        }
        const initParams = tranformDocumentProjectParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters,
              projectId
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            populate: "agencyId"
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    setData({});
    setMode("add");
    setIsViewModal(true);
  };

  const handleEditClick = item => {
    setData(item);
    setMode("edit");
    setIsViewModal(true);
  };
  const handleEditStacking = item => {
    setDataChecklist(item);
    setIsViewStackingPlan(true);
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 60,
      fixed: "left",
      hideInSearch: true
    },
    {
      title: "Tên đại lý",
      dataIndex: "agencyId",
      width: 250,
      render: (val, record) => {
        return <span>{val ? val.name : ""}</span>;
      }
    },
    {
      title: "Id user book căn",
      dataIndex: "userIds",
      hideInSearch: true,
      width: 250,
      render: (val, record) => {
        return <span>{Array.isArray(val) ? val.join() : ""}</span>;
      }
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdAt",
      valueType: "dateTime",
      width: 150,
      hideInSearch: true
    },
    {
      title: "Ngày sửa",
      dataIndex: "updatedAt",
      valueType: "dateTime",
      width: 150,
      hideInTable: true,
      hideInSearch: true
    },
    {
      title: "Thao tác",
      width: 150,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
          <Divider type="vertical" />
          <Tooltip title={`Thông tin bảng hàng`}>
            <DotChartOutlined onClick={() => { handleEditStacking(record); }} />
          </Tooltip>
        </>
      )
    }
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={false}
        headerTitle="Đại ly"
        actionRef={actionRef}
        formRef={form}
        form={{
          initialValues: formValues
        }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
          >
            {`Thêm mới`}
          </Button>
        ]}
      />
      <FixStackingPlan
        isAddStackingPlan={isViewModal}
        mode={mode}
        projectName={projectName}
        setIsViewModal={setIsViewModal}
        data={dataItem}
        projectId={projectId}
        dispatch={dispatch}
        fetchData={fetchData}
      />
      <StackingPlanData
        isAddStackingPlan={isViewStackingPlan}
        projectName={projectName}
        setIsViewModal={setIsViewStackingPlan}
        projectId={projectId}
        dispatch={dispatch}
        item={dataChecklist}
        type={projectType}
      />
    </>
  );
};

export default connect(({ stackingPlan, loading }) => ({
  stackingPlan,
  loading: loading.models.stackingPlan
}))(StackingPlanList);
