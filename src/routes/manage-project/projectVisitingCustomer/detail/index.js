
import React from 'react';
import { Image, Drawer, Row, Col, Divider } from 'antd';
import TableStatus from 'packages/pro-table/component/status';
import get from 'lodash/get';
import * as enums from 'util/enums';
import { formatDate } from 'util/helpers';

const DescriptionItem = ({ title, content }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const ImagesItem = ({ title, images }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <div>
      {images.map(item => (
        <Image width={80} src={item.url} />
      ))}
    </div>
  </div>
);

const ProjectVisitingCustomerDetail = ({ record, drawerVisible, onChange }) => {
  const triggerChange = (v) => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };
  /*   const genderStatus = get(enums.genderEnum[get(record, "gender", "-")], "status", null);
    const Status = TableStatus[genderStatus || 'Init'];
    const genderText = get(enums.genderEnum[get(record, "gender", "-")], "text", "-"); */

  return (
    <Drawer
      width={640}
      style={{ width: "640px !important" }}
      className="nv-drawer-detail"
      title={<span style={{ color: "#f09b1b" }}>{`Chi tiết ${get(record, "name", "-").toUpperCase()}`}</span>}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Divider orientation="left" plain>Thông tin môi giới</Divider>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tên người bán" content={get(record, "saleName", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="SĐT người bán" content={get(record, "salePhone", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tên đại lý" content={get(record, "agencyName", "-")} />
        </Col>
      </Row>
      <Divider orientation="left" plain>Thông tin cá nhân</Divider>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="ID" content={get(record, "id", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tên khách" content={get(record, "name", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Giới tính" content={get(record, "gender", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Khoảng tuổi" content={get(record, "ageRange", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="SĐT khách" content={get(record, "phone", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Cộng tác viên phụ trách" content={get(record, "consultantInCharge", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Số lượt theo dõi" content={get(record, "peopleFollow", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Địa chỉ" content={get(record, "address", "-")} />
        </Col>
      </Row>
      <Divider orientation="left" plain>Nhu cầu của khách hàng</Divider>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Kênh" content={get(record, "channel", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Mục đích" content={get(record, "purpose", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Nhóm" content={get(record, "groupType", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Trạng thái" content={get(record, "state", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
          <DescriptionItem title="Bình luận" content={get(record, "customerReply", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
          <DescriptionItem title="Sản phẩm quan tâm" content={get(record, "productTypeCare", "-")} />
        </Col>
      </Row>

    </Drawer>
  );
};

export default ProjectVisitingCustomerDetail;
