import React, { useEffect, useState } from 'react';
import { Card, Button, Form, Col, Row, Popover, Input ,Select} from 'antd';
import _ from 'lodash';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import * as enums from 'util/enums';
import { camelCaseToDash } from 'util/helpers';
import ListAgencySelect from 'src/components/Select/Agency/ListAgencyName';
const {Option} = Select
const RESOURCE = "projectVisitingCustomer";
const fieldLabels = {
  "name": "Tên khách hàng",
  "peopleFollow": "Số người đi cùng ",
  "address": "Địa chỉ",
  "phone": "Số điện thoại",
  "gender": "Giới tính",
  "ageRange": "Khoảng tuổi",
  "productTypeCare": "Sản phẩm quan tâm",
  "state": "Trạng thái",
  "groupType": "Nhóm",
  "customerReply": "Khách phản hồi",
  "channel": "Kênh biết đến sản phẩm",
  "purpose": "Mục đích",
  "agencyName": "Tên đại lý",
  "saleName": "Tên người bán",
  "salePhone": "SĐT người bán",
  "count" : "Số lượt đã lên VPBH"
};
const { TextArea } = Input
const ProjectVisitingCustomerForm = ({ projectVisitingCustomer: { formTitle, formData }, projectId, dispatch, submitting, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');
  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    data.incomeDate = Date.now()
    data.projectId = projectId;
    if(typeof data.agencyName === "object"){
      data.agencyName = data.agencyName?.name
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: (res) => {
        if (res && !res.error) {
          history.goBack();
        }
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  }, [])

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  })
  const layout = {
    labelCol: { span: 12 },
    wrapperCol: { span: 16 }
  };

  if (params.id === 'add' || (formData && formData.id && formData.id !== 'add')) {
    return (
      <Form
        {...layout}
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin môi giới">
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["saleName"]}
                name="saleName"
              >
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["salePhone"]} name="salePhone">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["agencyName"]} name="agencyName">
                <ListAgencySelect mode="radio"/>
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title={"Thông tin cá nhân"}>

          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["name"]}
                name="name"
              >
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["address"]} name="address">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["phone"]}
                name="phone"
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["peopleFollow"]}
                name="peopleFollow"
              >
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["ageRange"]} name="ageRange">
                <Select>
                  <Option value="Dưới 25">Dưới 25</Option>
                  <Option value="Từ 25-35">Từ 25-35</Option>
                  <Option value="Từ 35-45">Từ 35-45</Option>
                  <Option value="Từ 45-55">Từ 45-55</Option>
                  <Option value="Trên 55">Trên 55</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["gender"]}
                name="gender"
              >
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      "Nam": { text: "Nam" },
                      "Nữ": { text: "Nữ" },
                      "Khác": { text: "Khác" }
                    },
                    hideInTable: false,
                    hideInSearch: false
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["count"]}
                name="count"
              >
               <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title={"Nhu cầu khách hàng"}>
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["purpose"]}
                name="purpose"
              >
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      "Để ở": { text: "Để ở" },
                      "Để đầu tư": { text: "Để đầu tư" }
                    },
                    hideInTable: false,
                    hideInSearch: false
                  }}
                  intl={intl}
                />

              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["productTypeCare"]} name="productTypeCare">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["state"]}
                name="state"
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["channel"]}
                name="channel"
              >
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["customerReply"]} name="customerReply">
                <TextArea
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>

          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack() }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ projectVisitingCustomer, loading, router }) => ({
  submitting: loading.effects['projectVisitingCustomer/submit'],
  projectVisitingCustomer,
  router
}))(ProjectVisitingCustomerForm);
