import React, { useEffect, useState } from 'react';
import { Card, Button, Form, Col, Row, Popover,DatePicker, Switch } from 'antd';
import _ from 'lodash';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import * as enums from 'util/enums';
import moment from 'moment'
import UploadFile from '../../../../components/UploadFile';

import { camelCaseToDash } from 'util/helpers';

const RESOURCE = "salePolicy";
const fieldLabels = {"name":"Tên chính sách","description":"Mô tả","isActive":"Trạng thái","startDate":"Ngày bắt đầu","endDate":"Ngày kết thúc","file":"File","projectId":"proJect"};
const SalePolicyForm = ({ salePolicy: { formTitle, formData }, dispatch, submitting,projectId, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');
  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    if(data.startDate){
      data.startDate = moment(data.startDate).unix()*1000
    }
    if(data.endDate){
      data.endDate = moment(data.endDate).unix()*1000
    };
    data.projectId = projectId;
    if (data.file ) {
      if (data.file) {
        data.file =
          data.file && data.file.length > 0
            ? data.file
              .map(
                (i) =>
                  i.url ||
                  (i.response && i.response.length > 0
                    ? i.response[0].url
                    : ''),
              )
              .toString()
            : '';
      }
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
			callback: (res) => {
				if (res && !res.error) {
          // history.goBack();
        }
			}
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  },[])

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  },[formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  })

  // const handleChangeFile = (file) => {
  //   const curFile = file;
  //   setValue(origin => {
  //     if (onChange) {
  //       onChange({
  //         ...origin,
  //         link: curFile[0] || {}
  //       });
  //     }
  //     return {
  //       ...origin,
  //       link: curFile[0] || {}
  //     };
  //   });
  // };


  if(params.id === 'add' || (formData && formData.id && formData.id !== 'add')){
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin cơ bản" className="gx-card" bordered={false}>
          <Row gutter={24}>
              <Col xxl={8} xl={8} lg={8} md={12} sm={24} xs={24}>
                <Form.Item
                  label={fieldLabels['name']}
                  name="name"
                >
                  <FormInputRender
                    item={ {"title":"Tên chính sách","dataIndex":"name","width":200} }
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xxl={{ span: 8, offset: 0 }} xl={{ span: 8, offset: 0}} lg={{ span: 8 }} md={{ span: 12 }} sm={24} xs={24}>
                <Form.Item
                  label={fieldLabels['description']}
                  name="description"
                >
                  <FormInputRender
                    item={ {"title":"Mô tả","dataIndex":"description","width":200} }
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xxl={{ span: 8, offset: 0 }} xl={{ span: 8, offset: 0 }} lg={{ span: 8 }} md={{ span: 24 }} sm={24} xs={24}>
                <Form.Item
                  label={fieldLabels['isActive']}
                  name="isActive"
                  
                >
                  <Switch
                    defaultChecked={formData?.isActive}
                    item={ {"title":"Trạng thái","dataIndex":"isActive","width":200} }
                    intl={intl}
                  />
                </Form.Item>
              </Col>
          </Row>
          <Row gutter={24}>
              {/* <Col xxl={8} xl={8} lg={8} md={12} sm={24} xs={24}>
                <Form.Item
                  label={fieldLabels['startDate']}
                  name="startDate"
                >
                  <DatePicker
                    item={ {"title":"Ngày bắt đầu","dataIndex":"startDate","width":200} }
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xxl={{ span: 8, offset: 0 }} xl={{ span: 8, offset: 0}} lg={{ span: 8 }} md={{ span: 12 }} sm={24} xs={24}>
                <Form.Item
                  label={fieldLabels['endDate']}
                  name="endDate"
                >
                  <DatePicker
                    item={ {"title":"Ngày kết thúc","dataIndex":"endDate","width":200} }
                    intl={intl}
                  />
                </Form.Item>
              </Col> */}
              <Col xxl={{ span: 8, offset: 0 }} xl={{ span: 8, offset: 0 }} lg={{ span: 8 }} md={{ span: 24 }} sm={24} xs={24}>
                <Form.Item
                  label={fieldLabels['file']}
                  name="file"
                  // initialValue={formData?.file || ""}
                >
                   <UploadFile autoUpload={true} accept=".xlsx,.pdf,.csv" />
                </Form.Item>
              </Col>
          </Row>

        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack() }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ salePolicy, loading, router }) => ({
  submitting: loading.effects['salePolicy/submit'],
  salePolicy,
	router
}))(SalePolicyForm);
