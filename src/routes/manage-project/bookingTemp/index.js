import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Input,
  Button,
  Space,
  Select,
  Tooltip,
  Avatar,
  Row,
  Col,
  Upload,
  Card,
  message,
  Divider,
  Modal,
  Popover
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  UploadOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { get, union, cloneDeep } from "lodash";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import ApartmentListTable from "./apartmentList";
import Loading from "src/components/Loading";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
const UpLoadFile = Upload;
const enumStatus = {
  "Chưa mở bán": "yellow",
  "Mở bán": "green",
  Lock: "red"
};
const { Option } = Select;

const RESOURCE = "apartment";
const ApartmentList = props => {
  console.log(props);
  const intl = useIntl();
  const [form] = Form.useForm();
  const [isShowModal, setIsShowModal] = useState(false);
  const [fileList, setFileList] = useState([]);
  const [projectType, setProjectType] = useState(props.projectType);

  const [colorStatus, setColorStatus] = useState([]);
  const [colorEnum, setColorEnum] = useState({
    notsale: "#e0e0e0",
    saled: "#66bb6a",
    pending: "#fff176"
  });
  const [countStatus, setCountStatus] = useState({});
  const [mapStatusCount, setMapStatusCount] = useState({});
  const [list, setList] = useState([]);
  const [loadingModal, setLoadingModal] = useState(false);
  const {
    dispatch,
    [RESOURCE]: { data }
    // loading,
    // history
  } = props;

  const fetchData = () => {
    dispatch({
      type: `${RESOURCE}/fetchTemp`,
      payload: {
        projectId: props.projectId
      }
    });
  };
  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    setList(data.list);
  }, [data]);
  const countStatusData = arr => {
    let temp = {
      "Chưa mở bán": 0,
      "Mở bán": 0,
      Lock: 0
    };
    if (Array.isArray(list)) {
      list.map(e => {
        temp[e.status]++;
      });
      setMapStatusCount(temp);
    }
  };
  useEffect(() => {
    countStatusData(list);
  }, [list]);

  if (!colorStatus) return <Loading />;
  return (
    <>
      <Card style={{ backgroundColor: "#e1f5fe", border: "solid 1px" }}>
        <Row>
          <Col key={`status_total_can`} xl={6} lg={6} md={6} sm={6} xs={12}>
            {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
             */}
            <span
              style={{
                width: "40px",
                height: "15px",
                backgroundColor: "transparent",
                marginRight: "8px",
                display: "inline-block",
                border: "solid 1px"
              }}
            ></span>
            <span style={{ fontWeight: 600 }}>
              TỔNG SỐ CĂN ({data?.list?.length || 0} căn)
            </span>
          </Col>
          <Col xl={6} lg={6} md={6} sm={6} xs={12}>
            {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
             */}
            <span
              style={{
                width: "40px",
                height: "15px",
                backgroundColor: "#e0e0e0",
                marginRight: "8px",
                display: "inline-block",
                border: "solid 1px"
              }}
            ></span>
            <Popover
              content={
                <span>
                  Tổng số căn chưa được tính chỉ tiêu trong bảng hàng nội bộ.
                </span>
              }
            >
              <span style={{ fontWeight: 600 }}>
                Chưa bán ({data?.countNotSaled || 0} căn)
              </span>
            </Popover>
          </Col>
          <Col xl={6} lg={6} md={6} sm={6} xs={12}>
            {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
             */}
            <span
              style={{
                width: "40px",
                height: "15px",
                backgroundColor: "#66bb6a",
                marginRight: "8px",
                display: "inline-block",
                border: "solid 1px"
              }}
            ></span>
            <Popover
              content={
                <span>
                  Tổng số căn đã được tính chỉ tiêu trong bảng hàng nội bộ.
                </span>
              }
            >
            <span style={{ fontWeight: 600 }}>
              Đã bán ({data?.countSaled || 0} căn)
            </span>
            </Popover>
          </Col>
          <Col xl={6} lg={6} md={6} sm={6} xs={12}>
            {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
             */}
            <span
              style={{
                width: "40px",
                height: "15px",
                backgroundColor: "#fff176",
                marginRight: "8px",
                display: "inline-block",
                border: "solid 1px"
              }}
            ></span>
            <Popover
              content={
                <span>
                  Tổng số đang đợi duyệt lần cuối để tính chỉ tiêu cho đại lý trong bảng hàng nội bộ.
                </span>
              }
            >
            <span style={{ fontWeight: 600 }}>
              Đợi duyệt nội bộ ({data?.countPending || 0} căn)
            </span>
            </Popover>
          </Col>
        </Row>
      </Card>

      <ApartmentListTable
        type={projectType}
        data={data}
        projectId={props.projectId}
        colorEnum={colorEnum}
        history={props.history}
        match={props.match}
        setIsShowModal={setIsShowModal}
        fetchData={fetchData}
      />
    </>
  );
};

export default connect(({ apartment, loading }) => ({
  apartment,
  loading: loading.models.apartment
}))(ApartmentList);
