import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Input,
  Button,
  Space,
  Tooltip,
} from "antd";
import {
  SearchOutlined,
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { union } from "lodash";
import ReportFundDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
// import { saveAs } from "file-saver";
// import omit from "lodash/omit";
// import { exportExcel } from "src/services/project";

const tranformReportFundParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "reportFund";
const ReportFundList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState();

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;

  //------

  const accessTokenObj = localStorage.getItem("token");

  /* const handleExportClick = () => {
    exportExcel(props.projectId, accessTokenObj)
      .then(res => {
        if (res && res.data) {
          const blob = new Blob([res.data], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          });
          try {
            saveAs(blob, "thong_ke.xlsx");
          } catch (error) {
            console.log("error: ", error);
            message.error("Không xuất được báo cáo!");
          }
        } else {
          message.error("Không xuất được báo cáo!");
        }
      })
      .catch(error => {
        console.log("error: ", error.message);
        message.error("Không xuất được báo cáo!");
      });
  }; */

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformReportFundParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        projectId: props.projectId
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformReportFundParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", 'false');
          values = {};
        }
        const initParams = tranformReportFundParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  };

  const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "STT",
      width: 30,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Toà nhà/phân khu",
      dataIndex: "buildingCode-subdivision",
      width: 180
      // ...getColumnSearchProps('buildingCode-subdivision'),
    },
    {
      title: "Tầng/hàng",
      dataIndex: "floorCode-row",
      width: 70,
      hideInSearch: true
    },
    {
      title: "Số nhà",
      dataIndex: "apartmentCode",
      width: 90,
      hideInSearch: true
    },
    {
      title: "Mã căn",
      dataIndex: "fullCode",
      width: 110
    },
    {
      title: "Tình trạng",
      dataIndex: "status",
      width: 200,
      hideInTable: true
    },
    {
      title: "Tổng giá trị căn",
      dataIndex: "totalPrice",
      width: 150
    },
    {
      title: "Tổng giá theo BCTT đê tính tỷ lệ thanh toán",
      dataIndex: "totalPriceWithProcedure",
      width: 100
    },
    {
      title: "CD SALES",
      dataIndex: "directorSale",
      width: 100
    },
    {
      title: "Đại lý nhận quỹ",
      dataIndex: "agencyName",
      width: 100
    },
    {
      title: "Tổng thu",
      dataIndex: "totalCollect",
      width: 150
    },
    {
      title: "%Thu tiền",
      dataIndex: "percentCollect",
      width: 100
    },
    {
      title: "Ngày báo bán",
      dataIndex: "daySaled",
      // valueType: 'dateTime',
      width: 150
    },
    {
      title: "Ngày đủ cọc",
      dataIndex: "dayEnoughDeposit",
      // valueType: 'dateTime',
      width: 150
    },
    {
      title: "Ngày TTDC",
      dataIndex: "dayTTDC",
      // valueType: "dateTime",
      width: 150
    },
    {
      title: "Ngày hợp đồng mua bán",
      dataIndex: "dayHDMB",
      // valueType: "dateTime",
      width: 150
    },
    {
      title: "Tình trạng thủ tục",
      dataIndex: "statusProcedure",
      // valueType: "dateTime",
      width: 150
    },
    {
      title: "Cảnh báo chậm thủ tục quá 7 ngày",
      dataIndex: "warning7Days",
      width: 150
    },
    {
      title: "Tình trạng chi tiết",
      dataIndex: "statusDetail",
      width: 150
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      width: 150
    }
    // {
    //   title: 'Thao tác',
    //   width: 150,
    //   fixed: 'right',
    //   render: (text, record) => (
    //     <>
    //       <Tooltip title={`Sửa`}>
    //         <EditOutlined onClick={()=> handleEditClick(record)} />
    //       </Tooltip>
    //     </>
    //   ),
    // },
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={false}
        headerTitle="Căn hộ"
        actionRef={actionRef}
        formRef={form}
        form={{
          initialValues: formValues
        }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Tooltip title="Xuất báo cáo excel">
            {/* <FileExcelOutlined style={{ fontSize: '16px', lineHeight: '16px', verticalAlign: 'middle' }} onClick={() => handleExportClick()} /> */}
            <a
              style={{
                border: "1px solid gray",
                padding: "5px",
                borderRadius: "5px"
              }}
              target="_blank"
              href={`${process.env.REACT_APP_URL}/api/admin/apartment/dowload-report-fund?projectId=${props.projectId}&accesstoken=${accessTokenObj}`}
            >
              Tải về
            </a>
          </Tooltip>
        ]}
      />
      <ReportFundDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ reportFund, loading }) => ({
  reportFund,
  loading: loading.models.reportFund
}))(ReportFundList);
