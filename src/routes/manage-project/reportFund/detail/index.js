
import React from 'react';
import { Image, Drawer, Row, Col } from 'antd';
import TableStatus from 'packages/pro-table/component/status';
import get from 'lodash/get';
import * as enums from 'util/enums';
import { formatDate } from 'util/helpers';

const DescriptionItem = ({ title, content }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const ImagesItem = ({ title, images }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <div>
      {images.map(item => (
        <Image width={80} src={item.url} />
      ))}
    </div>
  </div>
);

const ReportFundDetail = ({ record, drawerVisible, onChange }) => {
  const triggerChange = (v) => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };
/*   const genderStatus = get(enums.genderEnum[get(record, "gender", "-")], "status", null);
  const Status = TableStatus[genderStatus || 'Init'];
  const genderText = get(enums.genderEnum[get(record, "gender", "-")], "text", "-"); */

  return (
    <Drawer
      width={640}
      style={{ width: "640px !important" }}
      className="nv-drawer-detail"
      title={<span style={{ color: "#f09b1b" }}>{`Chi tiết ${get(record, "name", "-").toUpperCase()}`}</span>}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="ID" content={get(record, "id" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Toà nhà/phân khu" content={get(record, "buildingCode-subdivision" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tầng/hàng" content={get(record, "floorCode-row" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Số nhà" content={get(record, "apartmentCode" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Mã căn" content={get(record, "fullCode" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tình trạng" content={get(record, "status" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tổng giá trị căn" content={get(record, "totalPrice" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tổng giá theo BCTT đê tính tỷ lệ thanh toán" content={get(record, "totalPriceWithProcedure" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="CD SALES" content={get(record, "directorSale" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Đại lý nhận quỹ" content={get(record, "agencyName" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tổng thu" content={get(record, "totalCollect" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="%Thu tiền" content={get(record, "percentCollect" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Ngày báo bán" content={get(record, "daySaled" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Ngày đủ cọc" content={get(record, "dayEnoughDeposit" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Ngày TTDC" content={get(record, "dayTTDC" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Ngày hợp đồng mua bán" content={get(record, "dayHDMB" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tình trạng thủ tục" content={get(record, "statusProcedure" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Cảnh báo chậm thủ tục quá 7 ngày" content={get(record, "warning7Days" , "-" )} />
        </Col>
      </Row>
    </Drawer>
  );
};

export default ReportFundDetail;
