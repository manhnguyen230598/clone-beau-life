/* eslint-disable array-callback-return */
import React, { useCallback, useEffect, useState } from "react";
import {
  ComposedChart,
  Line,
  Bar,
  CartesianGrid,
  XAxis,
  YAxis,
  
  Legend,
  ResponsiveContainer
} from "recharts";
import { BarChart } from "recharts";
import { PieChart, Pie, Sector, Cell } from "recharts";
import { Row , Tooltip} from "antd";
import { Col, Card, Button } from "antd";
import Widget from "src/components/Widget";
import moment from "moment";
import * as project from "src/services/project";
import { Space, DatePicker } from "antd";
import ReactDOM from "react-dom";

var rps = new Date();
var currentMonth1 = rps.getMonth() + 1;
var currentYear1 = rps.getFullYear();

const ReportSales = props => {
  const { RangePicker } = DatePicker;

  const data = [
    {
      name: "Dai ly A",
      tuan1: 10,
      tuan2: 20,
      tuan3: 20,
      tuan4: 30,
      result: 80
    },
    {
      name: "Dai ly B",
      tuan1: 30,
      tuan2: 10,
      tuan3: 20,
      tuan4: 40,
      result: 80
    },
    {
      name: "Dai ly C",
      tuan1: 40,
      tuan2: 30,
      tuan3: 40,
      tuan4: 50,
      result: 80
    },
    {
      name: "Dai ly D",
      tuan1: 20,
      tuan2: 40,
      tuan3: 10,
      tuan4: 15,
      result: 80
    }
  ];

  const [dataChart, setDataChart] = useState();

  let [fromTimeExchange, setFromTimeExchange] = useState(""),
    [fromTimeAccumulate, setFromTimeAccumulate] = useState(""),
    [monthYearInput, setMonthYearInput] = useState(new Date());

  const getData = async () => {
    if (fromTimeAccumulate) {
      let dateStart = fromTimeAccumulate[0].split("-");
      let dateEnd = fromTimeAccumulate[1].split("-");
      let api = await project.reportSale(
        props.projectId,
        parseInt(dateStart[0]),
        parseInt(dateEnd[0]),
        parseInt(dateStart[1]),
        parseInt(dateEnd[1])
      );
      setDataChart(api.data);
    } else {
      alert("Vui lòng nhập ngày tháng");
    }
  };

  // useEffect(() => {
  //   getData();
  // }, [month, endMonth , year, endYear])

  const renderTick = props => {
    const { x, y, index } = props;
    if (index === 8) {
      return (
        <>
          <path d={`M${x - 40.5},${y - 38}v${-150}`} stroke="black" />
          <path d={`M${x + 40.5},${y - 38}v${-150}`} stroke="black" />
        </>
      );
    } else {
      return <path d={`M${x - 40.5},${y - 38}v${-150}`} stroke="black" />;
    }
  };

  const renderCustomizedLabel = props => {
    const { x, y, stroke, value } = props;
    return (
      <text
        x={x}
        y={y}
        dy={-10}
        fill={stroke}
        fontSize={15}
        textAnchor="middle"
      >
        {value}%
      </text>
    );
  };
  const style = {
    right: 150,
    top: 79,
    lineHeight: 2,
    height: 122,
    paddingTop: 4,
    paddingLeft: 5
  };
  //---------------------------

  return (
    <>
      <Card title={<span style={{ marginLeft: 10 }}>Báo cáo bán hàng</span>}>
        <div style={{ display: "flex" }}>
          <Space direction="vertical" size={12}>
            <RangePicker
              picker="month"
              selected={monthYearInput}
              dateFormat="MM/yyyy"
              showMonthYearPicker
              onChange={(date, datrString) => {
                setFromTimeAccumulate(datrString);
                setMonthYearInput(datrString);
              }}
            />
          </Space>
          <Tooltip title="Báo cáo chỉ hiển thị những tháng đã được tạo chỉ tiêu trong phần Chỉ tiêu dự án">
            <Button
              style={{ marginLeft: "10px" }}
              onClick={() => getData()}
              type="primary"
            >
              Lọc
            </Button>
          </Tooltip>
        </div>
        {dataChart ? (
          <ResponsiveContainer width={1100} height={500}>
            <ComposedChart
              width={1100}
              height={600}
              //  data={data}
              data={dataChart ? dataChart.data : []}
              margin={{
                top: 100,
                right: 20,
                bottom: 20,
                left: 0
              }}
            >
              <YAxis
                ticks={[
                  0,
                  10,
                  20,
                  30,
                  40,
                  50,
                  60,
                  70,
                  80,
                  90,
                  100,
                  110,
                  120,
                  130,
                  140,
                  150
                ]}
                domain={[0, 150]}
              />

              <CartesianGrid stroke="#f5f5f5" />

              <Legend align="right" layout="vertical" wrapperStyle={style} />
              <XAxis
                dataKey="name"
                scale="auto"
                tickLine={false}
                interval={0}
              />

              <Bar dataKey="Tuần 1" barSize={20} fill="#413ea0" />
              <Bar dataKey="Tuần 2" barSize={20} fill="#e19129" />
              <Bar dataKey="Tuần 3" barSize={20} fill="#808080" />
              <Bar dataKey="Tuần 4" barSize={20} fill="#314313" />
              {/* <Line dataKey="PCV" yAxisId="right" stroke="#ff7300" dot={true} /> */}
              <Line
                dataKey="result"
                stroke="#ff7300"
                name="Tổng căn bán thành công"
                dot={false}
                label={renderCustomizedLabel}
              />
            </ComposedChart>
          </ResponsiveContainer>
        ) : (
          ""
        )}
      </Card>
    </>
  );
};

export default ReportSales;
