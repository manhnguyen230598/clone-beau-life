import React, { useState, useCallback } from 'react';
import dayjs from 'dayjs';
// import { DatePicker } from 'antd';
// import moment from 'moment';
import { DatePicker } from '../../../../../packages/pro-component';

interface DatePickerProps {
  value?: any,
  onChange?: (value: any) => void
}

const DatePickerComp: React.FC<DatePickerProps> = (props: DatePickerProps) => {
  const {
    value: initValue,
    onChange: superChange
  } = props;
  const [, setValue] = useState();
  // const preValue = useMemo(value);

  const onChange = useCallback((val: any) => {
    let val1 = val;
    setValue(val1);
    if(superChange) {
      superChange(val1);
    }
  }, [initValue]);

  return (
    <>
      <DatePicker defaultValue={dayjs(initValue)} onChange={onChange} />
    </>
  );
};

export default DatePickerComp;
