import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Space, /* Tooltip, Avatar */ } from 'antd';
import { SearchOutlined, PlusOutlined, FileExcelOutlined /* EditOutlined, EyeOutlined */ } from '@ant-design/icons';
import { connect } from 'dva';
import ProTable from 'packages/pro-table/Table';
import { /* get,  */union } from 'lodash';
import PaymentTransactionDetail from '../detail';
import { getValue, setTableChange, getTableChange } from 'util/helpers';
import CreateMulti from '../crud/CreateMulti';
import ImportModal from '../crud/ImportModal';
import dayjs from 'dayjs';


const tranformPaymentTransactionParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "paymentTransaction";
const PaymentTransactionList = (props) => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ?
    Number(process.env.REACT_APP_PAGESIZE) : 10);
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState((origin) => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState({ "id": { "show": false, "fixed": "left" }, "projectId": { "show": false }, "timeSign": { "show": true }, "fullCodeApartment": { "show": true }, "shortCodeApartment": { "show": true }, "customerId": { "show": true }, "evidenceImage": { "show": false }, "totalPrice": { "show": true }, "cancelReason": { "show": false }, "userConfirm": { "show": false }, "apartmentId": { "show": false }, "status": { "show": false }, "createdAt": { "show": false }, "updatedAt": { "show": false } });
  const [createMultiVisible, setCreateMultiVisible] = useState(false);
  const [importVisible, setImportVisible] = useState(false);

  const {
    dispatch,
    [RESOURCE]: { data },
    loading, history, match,
    projectId
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, (tableChange?.pagination) || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformPaymentTransactionParams(tableChange?.filtersArg, tableChange?.formValues, tableChange?.sort);

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort ? (initParams.sort.length <= 0 ? [{ "createdAt": "desc" }] : initParams.sort) : [{ "createdAt": "desc" }],
        queryInput: {
          ...initParams.filters,
          projectId
        }
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformPaymentTransactionParams(filters, formValues, sorter);

    if (sessionStorage.getItem('isReset') === 'true') {
      sessionStorage.setItem('isReset', false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        projectId
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ "createdAt": "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current.validateFields().then(fieldsValue => {
      console.log('handleSearch -> fieldsValue', fieldsValue);
      let values = {};
      Object.keys(fieldsValue).forEach(key => {
        if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
          values[key] = fieldsValue[key];
        }
      });
      if (values.timeSign && Array.isArray(values.timeSign)) {
        values.timeSign = { ">=": new Date(values.timeSign[0]).getTime(), "<=": new Date(values.timeSign[1]).getTime() };
      }
      if (sessionStorage.getItem('isReset') === 'true') {
        sessionStorage.setItem('isReset', 'false');
        values = {};
      }
      const initParams = tranformPaymentTransactionParams(tableChange?.filters, values, tableChange?.sort);

      setTableChange(RESOURCE, {
        ...tableChange,
        pagination: {
          ...tableChange.pagination,
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit
        },
        formValues: values
      });
      setFormValues(() => values);
      dispatch({
        type: `${RESOURCE}/fetch`,
        payload: {
          queryInput: {
            ...initParams.filters,
            projectId
          },
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit,
          sort: union(initParams.sort || [], [{ "createdAt": "desc" }])
        }
      });
    }).catch(err => {
      if (err) return;
    });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0],
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: '',
    });
    confirm();
  };

  const handleAddClick = () => {
    // history.push({ pathname: `${props.match.url}/add` });
    setCreateMultiVisible(true);
  };

  const handleImportClick = () => {
    // history.push({ pathname: `${props.match.url}/add` });
    setImportVisible(true);
  };

  /* const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  }; */

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
    },
    {
      title: 'Dự án',
      dataIndex: 'projectId',
      width: 200,
      hideInSearch: true,
      ...getColumnSearchProps('projectId'),
    },
    {
      title: 'Ngày thu',
      dataIndex: 'timeSign',
      valueType: 'dateTimeRange',
      width: 150,
      sorter: true,
      render: val => <span>{dayjs(val).format("DD/MM/YYYY")}</span>
    },
    {
      title: 'Mã căn',
      dataIndex: 'fullCodeApartment',
      width: 200,
      ...getColumnSearchProps('fullCodeApartment'),
    },
    {
      title: 'Mã căn rút gọn',
      dataIndex: 'shortCodeApartment',
      width: 200,
      filters: true,
      ...getColumnSearchProps('shortCodeApartment'),
    },
    {
      title: 'Khách hàng',
      dataIndex: 'customerId',
      width: 120,
      filters: true,
      hideInTable: true,
    },
    {
      title: 'Ảnh HĐ',
      dataIndex: 'evidenceImage',
      width: 200,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Số tiền đã thu',
      dataIndex: 'totalPrice',
      valueType: 'money',
      width: 200,
      hideInSearch: true,
      hideInTable: false,
    },
    {
      title: 'Lý do hủy',
      dataIndex: 'cancelReason',
      width: 120,
      hideInSearch: true,
      hideInTable: true,
      filters: true,
    },
    {
      title: 'Khác xác nhận',
      dataIndex: 'userConfirm',
      width: 120,
      hideInSearch: true,
      hideInTable: true,
      filters: true,
    },
    {
      title: 'Id căn hộ',
      dataIndex: 'apartmentId',
      width: 120,
      hideInSearch: true,
      hideInTable: true,
      filters: true,
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      width: 120,
      filters: true,
      hideInSearch: true,
      valueEnum: {
        "success": { "text": "Thành công", "status": "Processing", "color": "#ec3b3b", "isText": "true", },
        "false": { "text": "Dừng", "status": "Default", "color": "#ec3b3b", "isText": "true", },
      }
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
      width: 150,
      sorter: true,
      hideInSearch: true,
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updatedAt',
      valueType: 'dateTime',
      width: 150,
      sorter: true,
      hideInSearch: true,
    },
    /* {
      title: 'Thao tác',
      width: 150,
      fixed: 'right',
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      ),
    }, */
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Lịch sử thu tiền căn hộ"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={(mapCols) => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={(searchParams) => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem('isReset', 'true');
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit,
            },
            filtersArg: {}, sorter: {}, formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
            {`Thêm mới`}
          </Button>,
          <Button icon={<FileExcelOutlined />} type="default" onClick={() => handleImportClick()}>
            {`Import excel`}
          </Button>
        ]}
      />
      <PaymentTransactionDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={(v) => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
      <CreateMulti
        projectId={projectId}
        createMultiVisible={createMultiVisible}
        setCreateMultiVisible={setCreateMultiVisible}
      />
      <ImportModal
        projectId={projectId}
        importVisible={importVisible}
        setImportVisible={setImportVisible}
        history={history}
        match={match}
      />
    </>
  );
};

export default connect(({ paymentTransaction, loading }) => ({
  paymentTransaction,
  loading: loading.models.paymentTransaction
}))(PaymentTransactionList);
