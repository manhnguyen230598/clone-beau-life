import React, { FC, useCallback, memo } from 'react';
import { Modal, Form } from 'antd';
import { connect } from 'dva';
import dayjs from 'dayjs';
import cloneDeep from 'lodash/cloneDeep';
// import { DatePicker } from '../../../../packages/pro-component/index';
import { FormInputRender } from '../../../../packages/pro-table/form';
import { useIntl } from '../../../../packages/pro-table/component/intlContext';
import Upload from '../../../../components/Upload';
import ApartmentSelect from '../../../../components/Select/Apartment/ListApartment';

interface PaymentTransactionCreateMultiProp {
  projectId: number
  createMultiVisible: boolean
  setCreateMultiVisible: (visible: boolean) => void
  dispatch: any
}

const PaymentTransactionCreateMultiComp: FC<PaymentTransactionCreateMultiProp> = (props: PaymentTransactionCreateMultiProp) => {
  const {
    dispatch,
    projectId,
    createMultiVisible,
    setCreateMultiVisible
  } = props;
  const [form] = Form.useForm();
  const intl = useIntl();

  const handleCancel = useCallback(() => {
    setCreateMultiVisible(false);
  }, [setCreateMultiVisible]);

  const onFinish = useCallback((values) => {
    const data = cloneDeep(values);
    if(data.evidenceImage) {
      data.evidenceImage =
        data.evidenceImage && data.evidenceImage.length > 0
          ? data.evidenceImage
            .map(
              (i: any) =>
                i.url ||
                (i.response && i.response.length > 0
                  ? i.response[0].url
                  : ''),
            )
            .toString()
          : '';
    }
    if(data.apartmentId && typeof data.apartmentId == 'object') {
      data.apartmentId = data.apartmentId.id;
    }
    if(data.timeSign) {
      data.timeSign = dayjs(data.timeSign).valueOf();
    }
    const params = {
      ...data,
      projectId
    };

    console.log(`🚀 ~ file: PaymentTransactionCreateMulti.tsx ~ line 32 ~ onFinish ~ params`, params);
    dispatch({
      type: "paymentTransaction/createMulti",
      payload: params,
      callback: (res: any) => {
        console.log(`🚀 ~ file: PaymentTransactionCreateMulti.tsx ~ line 39 ~ onFinish ~ res`, res);
      }
    });
  }, [projectId]);

  const onFinishFailed = useCallback((errors) => {
    console.log(`🚀 ~ file: PaymentTransactionCreateMulti.tsx ~ line 33 ~ onFinishFailed ~ errors`, errors);
  }, []);

  return (
    <React.Fragment>
      <Modal
        title="Thêm mới hóa đơn thu tiền căn hộ"
        visible={createMultiVisible}
        onCancel={handleCancel}
        onOk={() => form.submit()}
      >
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label={"Ngày thu"}
            name="timeSign"
            rules={[
              { required: true, message: 'Ngày kí không được trống' }
            ]}
          >
            <FormInputRender
              item={{
                "title": "Ngày kí", "dataIndex": "timeSign", "width": 120,
                formItemProps: {}, valueType: "dateTime"
              }}
              intl={intl}
              type="form"
            />
          </Form.Item>
          <Form.Item
            label={"Ảnh"}
            name="evidenceImage"
            rules={[
              { required: false, message: 'Ảnh thủ tục không được trống' }
            ]}
          >
            <Upload mode="simple" />
          </Form.Item>
          <Form.Item
            label={"Căn hộ"}
            name="apartmentId"
            rules={[
              { required: true, message: 'Căn hộ không được trống' }
            ]}
          >
            <ApartmentSelect type="radio" paramsList={{ projectId }} />
          </Form.Item>
          <Form.Item
            label={"Số tiền thu"}
            name="totalPrice"
            rules={[
              { required: true, message: 'Số tiền không được trống' }
            ]}
          >
            <FormInputRender
              item={{
                "title": "Giá", "dataIndex": "totalPrice", "width": 120,
                formItemProps: {}, valueType: "digit"
              }}
              intl={intl}
              type="form"
            />
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};

const PaymentTransactionCreateMulti: FC<PaymentTransactionCreateMultiProp> = memo<any>(connect(null)(PaymentTransactionCreateMultiComp));

export default PaymentTransactionCreateMulti;
