import React, { FC, useCallback, memo } from 'react';
import { Modal, Form } from 'antd';
import { DownCircleOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import XLSX from 'xlsx';
import { v4 as uuidv4 } from 'uuid';
import dayjs from 'dayjs';
import cloneDeep from 'lodash/cloneDeep';
// import { useIntl } from '../../../../packages/pro-table/component/intlContext';
import UploadFile from '../../../../components/UploadFile';
import { fromExcelDate } from '../../../../util/helpers';

const handleFile = (file: any, onChange: (data: any) => any) => {
  const reader = new FileReader();
  reader.onload = (e: any) => {
    try {
      const dataBit = new Uint8Array(e.target.result);
      const wb = XLSX.read(dataBit, { type: 'array' });
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      const data = XLSX.utils.sheet_to_json(ws);
      // const cols = make_cols(ws['!ref']);
      // setGodens(origin => data);
      if(onChange) {
        onChange(data);
      }
    } catch(error) {
      console.log(`🚀 ~ file: model:paymentTransaction ~ line 41 ~ handleFile ~ error`, error);
    }
  };
  reader.readAsArrayBuffer(file);
};

interface PaymentTransactionImportProp {
  projectId: number
  importVisible: boolean
  setImportVisible: (visible: boolean) => void
  dispatch: any,
  history: any,
  match: any
}

const PaymentTransactionImportComp: FC<PaymentTransactionImportProp> = (props: PaymentTransactionImportProp) => {
  const {
    dispatch,
    projectId,
    importVisible,
    setImportVisible,
    history,
    match
  } = props;
  const [form] = Form.useForm();

  const handleCancel = useCallback(() => {
    setImportVisible(false);
  }, [setImportVisible]);

  const onFinish = useCallback((values) => {
    const data = cloneDeep(values);

    const pathname = match.url.replace('paymentTransaction', 'paymentTransactionImport');
    if(data.xlsx[0]) {
      handleFile(data.xlsx[0].originFileObj, (res) => {
        let response = Object.assign(res, {});
        (response || []).forEach((ele:any)=>{
          Object.keys(ele).forEach(e=>{
            ele[e.trim()] = ele[e];
            if(e.trim() !== e){
              delete ele[e]
            }
          })
        })

        const params = {
          data: (response || []).map((i: any) => {
            return {
              // ...i,
              "fullCode": i["Mã căn"],
              "timeSign": dayjs(fromExcelDate(i["Ngày tháng (DD/MM/YYYY)"])).valueOf(),
              "totalPrice": Number(i["Tiền thu căn hộ"] || "0"),
              id: uuidv4()
            };
          }),
          projectId
        };
        dispatch({
          type: "paymentTransaction/preImportData",
          payload: params,
          callback: () => {
            history.push({ pathname });
          }
        });
      });
    }
  }, [projectId]);

  const onFinishFailed = useCallback((errors) => {
    console.log(`🚀 ~ file: PaymentTransactionImport.tsx ~ line 33 ~ onFinishFailed ~ errors`, errors);
  }, []);

  return (
    <React.Fragment>
      <Modal
        title="Import lịch sử thu tiền căn hộ"
        visible={importVisible}
        onCancel={handleCancel}
        onOk={() => form.submit()}
      >
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label={"Chọn file excel"}
          // noStyle
          >
            <div className="gx-d-flex gx-flex-nowrap gx-justify-content-start gx-align-items-center">
              <Form.Item
                name="xlsx"
                rules={[
                  { required: true, message: 'Vui lòng chọn file dữ liệu để import!' }
                ]}
                noStyle
              >
                <UploadFile autoUpload={false} />
              </Form.Item>
              <a className="gx-ml-3" href={`/static/templates/paymentTransaction.xlsx`} download><DownCircleOutlined />&nbsp;Tải File mẫu</a>
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};

const PaymentTransactionImport: FC<PaymentTransactionImportProp> = memo<any>(connect(null)(PaymentTransactionImportComp));

export default PaymentTransactionImport;
