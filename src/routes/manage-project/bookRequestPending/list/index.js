import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Input,
  Button,
  Space,
  Tooltip,
  Avatar,
  Row,
  Col,
  Table,
  Timeline,
  Popconfirm,
  Modal,
  Card,
  Image
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  UpCircleTwoTone,
  DownCircleTwoTone
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { get, union } from "lodash";
import BookRequestPendingDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import Widget from "src/components/Widget";
import * as project from "src/services/project";
import { Switch } from "react-router-dom";
import dayjs from "dayjs";
import locations from "./../../../../locations.json";
import * as customerServices from "src/services/customer";
const tranformBookRequestPendingParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "bookRequestPending";
const BookRequestPendingList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  const formatDate = value => {
    return dayjs(value).format("DD-MM-YYYY HH:MM");
    /* const date = new Date(value);
    return `${date.getMonth()}-${date.getFullYear()}` */
  };

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState();
  const [dataBooking, setDataBooking] = useState([]);
  const [dataHistory, setDataHistory] = useState([]);

  const [isViewModal, setIsViewModal] = useState(false);
  const [activeUser, setActiveUser] = useState({});
  const [activeUserInfos, setActiveUserInfos] = useState([]);
  const getDataBooking = async () => {
    let api = await project.countBooking(props.projectId);
    setDataBooking(api.data.data);
  };
  const getDataHistory = async () => {
    let api = await project.historyActivity(props.projectId, 0, 15);
    setDataHistory(api.data.data);
  };
  const ImagesItem = ({ title, images }) => (
    <div className="nv-detail-item-wrapper">
      {/* <p className="nv-detail-item-label">{title}:</p> */}
      <div>
        {images.map((item, index) => (
          <Image key={index} width={80} src={item} />
        ))}
      </div>
    </div>
  );

  useEffect(() => {
    getDataBooking();
    getDataHistory();
  }, []);
  useEffect(() => {
    if (activeUser?.customerIds) {
      customerServices
        .getList({
          queryInput: {
            id: activeUser?.customerIds
          }
        })
        .then(rs => {
          setActiveUserInfos(rs?.data?.data);
        });
    } else {
      setActiveUserInfos([activeUser?.customerId]);
    }
  }, [activeUser]);
  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;

  const fetchData = () => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformBookRequestPendingParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        projectId: props.projectId
      }
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformBookRequestPendingParams(
      filters,
      formValues,
      sorter
    );

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        const values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", false);
          values = {};
        }
        const initParams = tranformBookRequestPendingParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(fieldsValue);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  };

  const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  };
  const getProvice = provinceId => {
    let find = locations.provinces.find(e => Number(e.id) === provinceId);
    console.log(find, provinceId);
    return find?.name || "";
  };
  const getDistrict = provinceId => {
    let find = locations.districts.find(e => Number(e.id) === provinceId);
    console.log(find, provinceId);
    return find?.name || "";
  };
  const getWard = provinceId => {
    let find = locations.wards.find(e => Number(e.id) === provinceId);
    console.log(find, provinceId);
    return find?.name || "";
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 30,
      fixed: "left",
      sorter: false,
      hideInSearch: false
    },
    {
      title: "Ngày book căn",
      dataIndex: "createdAt",
      width: 100,
      hideInSearch: true,
      render: value => <span>{formatDate(value)}</span>
    },
    {
      title: "Mã căn hộ",
      dataIndex: "fullCode",
      width: 100
      // ...getColumnSearchProps("fullCode")
    },
    {
      title: "Số tiền đã thu",
      dataIndex: "moneyCollect",
      width: 100,
      render: value => <span>{value.toLocaleString()}đ</span>
    },
    {
      title: "Thông tin khách hàng",
      dataIndex: "customerIds",
      width: 120,
      render: (val, record) => {
        return (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start"
            }}
          >
            {/* <div style={{ textTransform: "uppercase" }}>
                <span style={{ marginLeft: "5px" }}>{val}</span>
              </div> */}
            <a
              style={{ fontSize: "12px", marginLeft: "5px" }}
              onClick={() => {
                setActiveUser(record);
                setIsViewModal(true);
              }}
            >
              <Tooltip title="Chi tiết">
                <span style={{ marginRight: "5px" }}>Xem đầy đủ</span>
                <EyeOutlined />
              </Tooltip>
            </a>
          </div>
        );
      }
      // ...getColumnSearchProps("customerId")
    },
    // {
    //   title: 'Thao tác',
    //   width: 50,
    //   fixed: 'right',
    //   render: (text, record) => (
    //     <>
    //       <Tooltip title={`duyet`}>
    //         <EditOutlined onClick={()=> handleEditClick(record)} />
    //       </Tooltip>
    //     </>
    //   ),
    // },
    {
      title: "Thao tác",
      width: 50,
      fixed: "right",
      render: (value, record) => {
        return (
          <div className="c-group-btn">
            <Popconfirm
              title="Bạn chắc chắn muốn duyệt yêu cầu này?"
              onConfirm={() => {
                let data = {
                  status: "success",
                  id: record.id
                };
                dispatch({
                  type: `${RESOURCE}/updateStatus`,
                  payload: data,
                  callback: res => {
                    if (res && !res.error) {
                      fetchData();
                      getDataHistory();
                    }
                  }
                });
              }}
              okText="Có"
              cancelText="Không"
            >
              <div
                className="nv-btn-duyet"
                style={{
                  width: "20px",
                  display: "inline-block",
                  marginRight: "5px"
                }}
                // onClick={val => {
                //   let data = {
                //     status: "success",
                //     id: record.id
                //   };
                //   dispatch({
                //     type: `${RESOURCE}/updateStatus`,
                //     payload: data,
                //     callback: res => {
                //       if (res && !res.error) {
                //         fetchData();
                //       }
                //     }
                //   });
                // }}
              >
                <img
                  src={require("assets/images/img-success.jpg")}
                  alt="framed"
                />
              </div>
            </Popconfirm>
            <Popconfirm
              title="Bạn chắc chắn muốn hủy yêu cầu này?"
              onConfirm={() => {
                let data = {
                  status: "cancel",
                  id: record.id
                };
                dispatch({
                  type: `${RESOURCE}/cancelStatus`,
                  payload: data,
                  callback: res => {
                    if (res && !res.error) {
                      fetchData();
                      getDataHistory();
                    }
                  }
                });
              }}
              okText="Có"
              cancelText="Không"
            >
              <div
                className="nv-btn-huy"
                style={{ width: "20px", display: "inline-block" }}
                // onClick={val => {
                //   let data = {
                //     status: "cancel",
                //     id: record.id
                //   };
                //   dispatch({
                //     type: `${RESOURCE}/cancelStatus`,
                //     payload: data,
                //     callback: res => {
                //       if (res && !res.error) {
                //         fetchData();
                //       }
                //     }
                //   });
                // }}
              >
                <img
                  src={require("assets/images/img-cancel.jpeg")}
                  alt="framed"
                />
              </div>
            </Popconfirm>
            <div className="c-group-btn"></div>
          </div>
        );
      }
    }
  ];

  return (
    <>
      <Row>
        <Col
          xl={16}
          lg={16}
          md={16}
          sm={16}
          xs={24}
          style={{ padding: "10px" }}
        >
          <Widget
            styleName="gx-card-widget is-build-chartclass"
            title="Số lượt book căn ngày hôm nay"
          >
            <div className="nv-info-booking" style={{ marginBottom: "25px" }}>
              <span
                style={{
                  marginRight: "25px",
                  paddingRight: "25px",
                  display: "inline-flex",
                  alignItems: "center",
                  height: "50px",
                  borderRight: "1px solid gray"
                }}
              >
                Số lượt book căn hôm nay:{" "}
                <span
                  style={{
                    fontSize: "200%",
                    marginLeft: "3px",
                    color: "#00c853"
                  }}
                >
                  {dataBooking ? dataBooking.countNow : "0"}
                </span>
              </span>
              <span
                style={{
                  display: "inline-flex",
                  alignItems: "center",
                  height: "50px"
                }}
              >
                So với hôm qua:{" "}
                {dataBooking ? (
                  <div>
                    {dataBooking.growthRate > 0 ? (
                      <span style={{ marginLeft: "5px" }}>
                        <UpCircleTwoTone
                          twoToneColor="green"
                          style={{ fontSize: "150%" }}
                        />
                        <span
                          style={{
                            color: "green",
                            fontSize: "200%",
                            marginLeft: "5px"
                          }}
                        >
                          {dataBooking.growthRate}%
                        </span>
                      </span>
                    ) : (
                      <span style={{ marginLeft: "5px" }}>
                        <DownCircleTwoTone
                          twoToneColor="red"
                          style={{ fontSize: "150%" }}
                        />
                        <span
                          style={{
                            color: "red",
                            fontSize: "200%",
                            marginLeft: "5px"
                          }}
                        >
                          {dataBooking.growthRate}%
                        </span>
                      </span>
                    )}
                  </div>
                ) : (
                  "0%"
                )}
              </span>
            </div>
          </Widget>
          <Widget title="Lượt book căn chưa xác nhận">
            <Table
              tableClassName="gx-table-responsive"
              type="table"
              rowKey="id"
              search={false}
              // headerTitle="Người dùng"
              actionRef={actionRef}
              formRef={form}
              form={{
                initialValues:
                  sessionStorage.getItem("isReset") == "true" ? {} : formValues,
                preserve: false
              }}
              dataSource={data && data.list}
              pagination={data.pagination}
              columns={columns}
              columnsStateMap={columnsStateMap}
              onColumnsStateChange={mapCols => {
                setColumnsStateMap(mapCols);
              }}
              loading={loading}
              // scroll={{ x: 1500, y: 300 }}
              onChange={handleTableChange}
              onSubmit={handleTableSearch}
              beforeSearchSubmit={searchParams => {
                return {};
              }}
            />
          </Widget>
        </Col>
        <Col xl={8} lg={8} md={8} sm={8} xs={24} style={{ padding: "10px" }}>
          <Widget styleName="gx-card-widget is-build-chartclass nv-full-height">
            <div
              className="nv-title-history"
              style={{ textAlign: "center", marginBottom: "15px" }}
            >
              <span style={{ fontWeight: "700", fontSize: "1.2rem" }}>
                Các hoạt động gần đây
              </span>
            </div>
            <div className="nv-group-history" style={{ marginTop: "10px" }}>
              {/* <ul>
                {dataHistory
                  ? dataHistory.map((item, index) => {
                      return (
                        <>
                          <li key={index} style={{ marginBottom: "10px" }}>
                            <span style={{ color: "blue" }}>
                              {formatDate(item.createdAt)}&nbsp;
                            </span>
                            <span>{item.description}</span>
                          </li>
                        </>
                      );
                    })
                  : null}
              </ul> */}
              <Timeline style={{ marginTop: "10px" }}>
                {dataHistory
                  ? dataHistory.map((item, index) => (
                      <div key={index}>
                        <Timeline.Item>
                          <span>{item.description}&nbsp;</span>
                          <span style={{ color: "red" }}>
                            {formatDate(item.createdAt)}
                          </span>
                        </Timeline.Item>
                      </div>
                    ))
                  : null}
              </Timeline>
            </div>
          </Widget>
        </Col>
      </Row>
      <Modal
        title="Thông tin book căn"
        visible={isViewModal}
        onOk={() => {
          setIsViewModal(false);
        }}
        onCancel={() => {
          setIsViewModal(false);
        }}
        width={"60%"}
      >
        <Card
          title={
            <div>
              <span style={{ marginRight: "5px" }}>Thông tin khách hàng</span>
              <i className="icon icon-user" />
            </div>
          }
        >
          {activeUserInfos.map(ele => {
            return (
              <>
                <Row gutter={[24, 16]}>
                  <Col span={8}>
                    <span className="title-customer">Tên khách hàng:</span>
                    <span className="value-customer">{ele?.name}</span>
                  </Col>
                  <Col span={8}>
                    <span className="title-customer">Số điện thoại:</span>
                    <span className="value-customer">{ele?.phone}</span>
                  </Col>
                  <Col span={8}>
                    <span className="title-customer">Giới tính:</span>
                    <span className="value-customer">{ele?.gender}</span>
                  </Col>
                </Row>
                <Row gutter={[24, 16]}>
                  <Col span={8}>
                    <span className="title-customer">
                      Số chứng minh nhân dân:
                    </span>
                    <span className="value-customer">
                      {ele?.indentityNumber}
                    </span>
                  </Col>
                  <Col span={8}>
                    <span className="title-customer">Nơi cấp:</span>
                    <span className="value-customer">
                      {ele?.indentityProvince}
                    </span>
                  </Col>
                  <Col span={8}>
                    <span className="title-customer">Ngày cấp:</span>
                    <span className="value-customer">
                      {ele?.indentityIssueDate
                        ? formatDate(ele?.indentityIssueDate)
                        : ""}
                    </span>
                  </Col>
                </Row>
                <Row gutter={[24, 16]}>
                  <Col span={8}>
                    <span className="title-customer">Địa chỉ:</span>
                    <span className="value-customer">{ele?.address}</span>
                  </Col>
                </Row>
                <Row gutter={[24, 16]}>
                  <Col span={8}>
                    <span className="title-customer">Tỉnh:</span>
                    <span className="value-customer">
                      {getProvice(ele?.provinceId)}
                    </span>
                  </Col>
                  <Col span={8}>
                    <span className="title-customer">Huyện:</span>
                    <span className="value-customer">
                      {getDistrict(ele?.districtId)}
                    </span>
                  </Col>
                  <Col span={8}>
                    <span className="title-customer">Xã:</span>
                    <span className="value-customer">
                      {getWard(ele?.wardId)}
                    </span>
                  </Col>
                </Row>
                <div style={{ textAlign: "end" }}>
                  <a href={`/#/main/customer/${ele?.id}`} target="_blank">
                    Sửa thông tin <EditOutlined />
                  </a>
                </div>

                <div
                  style={{
                    margin: "10px 5px",
                    borderBottom: "solid 1px #9999"
                  }}
                ></div>
              </>
            );
          })}
        </Card>
        <Card
          title={
            <div>
              <span style={{ marginRight: "5px" }}>Thông tin book căn</span>
              <i className="icon icon-pricing-table" />
            </div>
          }
        >
          <Row gutter={[24, 16]}>
            <Col span={8}>
              <span className="title-customer">Mã căn:</span>
              <span className="value-customer">{activeUser?.fullCode}</span>
            </Col>
            <Col span={8}>
              <span className="title-customer">Số tiền đã thu:</span>
              <span className="value-customer">
                {activeUser?.moneyCollect?.toLocaleString() || 0}đ
              </span>
            </Col>
            <Col span={8}>
              <span className="title-customer">Đại lý:</span>
              <span className="value-customer">
                {activeUser?.agencyId?.name || "Không rõ"}
              </span>
            </Col>
          </Row>
          <Row gutter={[24, 16]}>
            <Col span={12}>
              <span className="title-customer">Hình ảnh xác minh:</span>
              <ImagesItem images={activeUser?.images || []} />
            </Col>
          </Row>
          <Row gutter={[24, 16]}>
            <Col span={12}>
              <span className="title-customer">Ngày book:</span>
              <span className="value-customer">
                {formatDate(activeUser?.createdAt)}
              </span>
            </Col>
          </Row>
        </Card>
      </Modal>
    </>
  );
};

export default connect(({ bookRequestPending, loading }) => ({
  bookRequestPending,
  loading: loading.models.bookRequestPending
}))(BookRequestPendingList);
