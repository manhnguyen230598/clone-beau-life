import React, { useEffect, useState } from "react";
import {
  Card,
  Button,
  Form,
  Col,
  Row,
  Popover,
  Popconfirm,
  message,
  Modal
} from "antd";
import _ from "lodash";
import { CloseCircleOutlined, EditOutlined } from "@ant-design/icons";
import { connect } from "dva";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import FooterToolbar from "packages/FooterToolbar";
import * as apartmentServices from "services/apartment";
import locations from "./../../../../locations.json";
import dayjs from "dayjs";
import * as customerServices from "src/services/customer";
const RESOURCE = "apartment";
const fieldLabelsHigh = {
  numberRooms: "Số phòng ngủ",
  view: "View",
  cornerApartment: "Căn góc",
  areaHeartWall: "Diện tích tim tường",
  area: "Diện tích thông thủy",
  totalPrice: "Tổng giá bán (gồm VAT & KPBT)",
  totalPriceNotVAT: "Tổng giá bán (chưa VAT & KBPT)",
  pricePerNotVAT: "Đơn giá bán/m2 thông thủy (chưa VAT & KPBT)",
  note: "Ghi chú"
};

const fieldLabelsLow = {
  areaLand: "Diện tích đất",
  areaBuilding: "Diện tích xây dựng",
  cornerApartment: "Vị trí",
  totalPrice: "Tổng giá bán"
};

const ApartmentForm = ({
  apartment: { formTitle, formData },
  projectType,
  dispatch,
  submitting,
  match: { params },
  history,
  ...rest
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState("100%");
  const [isViewModal, setIsViewModal] = useState(false);
  const [activeUserInfos, setActiveUserInfos] = useState([]);
  const formatDate = value => {
    return dayjs(value).format("DD-MM-YYYY HH:MM");
    /* const date = new Date(value);
    return `${date.getMonth()}-${date.getFullYear()}` */
  };
  const getProvice = provinceId => {
    let find = locations.provinces.find(e => Number(e.id) === provinceId);
    console.log(find, provinceId);
    return find?.name || "";
  };
  const getDistrict = provinceId => {
    let find = locations.districts.find(e => Number(e.id) === provinceId);
    console.log(find, provinceId);
    return find?.name || "";
  };
  const getWard = provinceId => {
    let find = locations.wards.find(e => Number(e.id) === provinceId);
    console.log(find, provinceId);
    return find?.name || "";
  };
  const getErrorInfo = errors => {
    const errorCount = errors.filter(item => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = fieldKey => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map(err => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      switch (projectType) {
        case "house":
          return (
            <li
              key={key}
              className="nv-error-list-item"
              onClick={() => scrollToField(key)}
            >
              <CloseCircleOutlined className="nv-error-icon" />
              <div className="nv-error-message">{err.errors[0]}</div>
              <div className="nv-error-field">{fieldLabelsHigh[key]}</div>
            </li>
          );
        case "resort":
          return (
            <li
              key={key}
              className="nv-error-list-item"
              onClick={() => scrollToField(key)}
            >
              <CloseCircleOutlined className="nv-error-icon" />
              <div className="nv-error-message">{err.errors[0]}</div>
              <div className="nv-error-field">{fieldLabelsLow[key]}</div>
            </li>
          );
        default:
          break;
      }
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={trigger => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = values => {
    setError([]);
    const data = _.cloneDeep(values);
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: res => {
        if (res && !res.error) {
          // history.goBack();
        }
      }
    });
  };

  const onFinishFailed = errorInfo => {
    setError(errorInfo.errorFields);
  };
  const onchangeStatus = async status => {
    let rs = await apartmentServices.updateStatus({
      id: params.id,
      status
    });
    if (rs && rs.data) {
      if (rs.data.code == 0) {
        message.success(rs.data.message);
      } else {
        message.error(rs.data.message);
      }
    }
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  }, []);

  useEffect(() => {
    if (isViewModal) {
      customerServices
        .getList({
          queryInput: {
            id:
              formData?.customerIds?.length > 0
                ? formData?.customerIds
                : formData?.customerId
          }
        })
        .then(rs => {
          setActiveUserInfos(rs?.data?.data);
        });
    }
  }, [isViewModal]);
  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll(".ant-layout-sider")[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    }
    window.addEventListener("resize", resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener("resize", resizeFooterToolbar);
    };
  });
  const PopAction = props => {
    const [visible, setVisible] = useState(false);
    return (
      <Popconfirm
        title={props.title}
        visible={visible}
        onConfirm={() => {
          onchangeStatus(props.titleButton);
          setVisible(false);
        }}
        // okButtonProps={{ loading: confirmLoading }}
        onCancel={() => setVisible(false)}
        okText="Đồng ý"
      >
        <Button style={props.style} onClick={() => setVisible(true)}>
          {props.titleButton}
        </Button>
      </Popconfirm>
    );
  };
  const layout = {
    labelCol: { span: 12 },
    wrapperCol: { span: 16 }
  };

  if (
    params.id === "add" ||
    (formData && formData.id && formData.id !== "add")
  ) {
    return (
      <Form
        {...layout}
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        {params.id !== "add" ? (
          <Card title={`Thao tác với căn hộ ${formData["fullCode"]}`}>
            {/* <Button style={{color:"green", borderColor:"green"}}>Mở bán căn</Button>
          <Button>Khóa căn</Button>
          <Button danger>Hủy HĐV/HĐMB của căn</Button> */}
            <PopAction
              title={"Bạn có chắc chắn mở bán căn hộ này."}
              titleButton="Mở bán"
              style={{ color: "green", borderColor: "green" }}
            />
            <PopAction
              title={"Bạn có chắc chắn khóa căn hộ này."}
              titleButton="LOCK căn"
              style={{ color: "black", borderColor: "black" }}
            />
            <PopAction
              title={"Bạn có chắc chắn hủy HDV/HĐMB căn hộ này."}
              titleButton="Hủy HĐV/HĐMB"
              style={{ color: "red", borderColor: "red" }}
            />
          </Card>
        ) : null}

        <Card title={"Thông tin chính"}>
          {projectType === "house" ? (
            <>
              <Row>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <Form.Item
                    label={fieldLabelsHigh["cornerApartment"]}
                    name="cornerApartment"
                  >
                    <FormInputRender
                      item={{
                        title: "Tên căn hộ",
                        dataIndex: "name",
                        width: 100
                      }}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <Form.Item label={fieldLabelsHigh["view"]} name="view">
                    <FormInputRender
                      item={{
                        title: "Tên căn hộ",
                        dataIndex: "name",
                        width: 100
                      }}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <Form.Item
                    label={fieldLabelsHigh["numberRooms"]}
                    name="numberRooms"
                  >
                    <FormInputRender
                      item={{
                        title: "Tên căn hộ",
                        dataIndex: "name",
                        width: 100
                      }}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <Form.Item
                    label={fieldLabelsHigh["areaHeartWall"]}
                    name="areaHeartWall"
                  >
                    <FormInputRender
                      item={{
                        title: "Tên căn hộ",
                        dataIndex: "name",
                        width: 100
                      }}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <Form.Item label={fieldLabelsHigh["area"]} name="area">
                    <FormInputRender
                      item={{
                        title: "Tên căn hộ",
                        dataIndex: "name",
                        width: 100
                      }}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <Form.Item
                    label={fieldLabelsHigh["pricePerNotVAT"]}
                    name="pricePerNotVAT"
                  >
                    <FormInputRender
                      item={{
                        title: "Tên căn hộ",
                        dataIndex: "name",
                        width: 100,
                        valueType: "money"
                      }}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <Form.Item
                    label={fieldLabelsHigh["totalPriceNotVAT"]}
                    name="totalPriceNotVAT"
                  >
                    <FormInputRender
                      item={{
                        title: "Tên căn hộ",
                        dataIndex: "name",
                        width: 100,
                        valueType: "money"
                      }}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <Form.Item
                    label={fieldLabelsHigh["totalPrice"]}
                    name="totalPrice"
                  >
                    <FormInputRender
                      item={{
                        title: "Tên căn hộ",
                        dataIndex: "name",
                        width: 100,
                        valueType: "money"
                      }}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col xl={12} lg={12} md={12} sm={24} xs={8}>
                  <Form.Item label={fieldLabelsHigh["note"]} name="note">
                    <FormInputRender
                      type="form"
                      item={{
                        title: "Mô tả dự án",
                        dataIndex: "defaultsTo",
                        valueType: "textarea"
                      }}
                      placeHolder={"Nhập mô tả dự án"}
                      intl={intl}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </>
          ) : (
            <Row>
              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                <Form.Item label={fieldLabelsLow["areaLand"]} name="areaLand">
                  <FormInputRender
                    item={{
                      title: "Tên căn hộ",
                      dataIndex: "name",
                      width: 100
                    }}
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                <Form.Item
                  label={fieldLabelsLow["areaBuilding"]}
                  name="areaBuilding"
                >
                  <FormInputRender
                    item={{
                      title: "Tên căn hộ",
                      dataIndex: "name",
                      width: 100
                    }}
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                <Form.Item label={"Hướng cửa"} name="direction">
                  <FormInputRender
                    item={{
                      title: "Tên căn hộ",
                      dataIndex: "note",
                      width: 100
                    }}
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                <Form.Item label={"Loại căn hộ"} name="numberRooms">
                  <FormInputRender
                    item={{
                      title: "Tên căn hộ",
                      dataIndex: "note",
                      width: 100
                    }}
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                <Form.Item label={"Tầng cao"} name="numberOfFloor">
                  <FormInputRender
                    item={{
                      title: "Tên căn hộ",
                      dataIndex: "note",
                      width: 100
                    }}
                    intl={intl}
                  />
                </Form.Item>
              </Col>

              {/* <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                <Form.Item label={fieldLabelsLow["numberRooms"]} name="numberRooms">
                  <FormInputRender
                    item={{ "title": "Tên căn hộ", "dataIndex": "name", "width": 100 }}
                    intl={intl}
                  />
                </Form.Item>
              </Col> */}
              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                <Form.Item
                  label={fieldLabelsLow["totalPrice"]}
                  name="totalPrice"
                >
                  <FormInputRender
                    item={{
                      title: "Tên căn hộ",
                      dataIndex: "name",
                      width: 100,
                      valueType: "money"
                    }}
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xl={12} lg={12} md={12} sm={8} xs={8}>
                <Form.Item label={"Ghi chú"} name="note">
                  <FormInputRender
                    type="form"
                    item={{
                      title: "Mô tả dự án",
                      dataIndex: "defaultsTo",
                      valueType: "textarea"
                    }}
                    placeHolder={"Nhập mô tả dự án"}
                    intl={intl}
                  />
                </Form.Item>
              </Col>
            </Row>
          )}
        </Card>
        {params.id !== "add" ? (
          <>
            <Card title={"Trạng thái"}>
              <Row gutter={[16, 16]}>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <span>Trạng thái căn:</span>
                  <span
                    style={{
                      fontWeight: 600,
                      marginLeft: "4px",
                      fontSize: "1.1em"
                    }}
                  >
                    {formData["status"]}
                  </span>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <span>Trạng thái bảng hàng:</span>
                  <span
                    style={{
                      fontWeight: 600,
                      marginLeft: "4px",
                      fontSize: "1.1em"
                    }}
                  >
                    {formData["statusSale"]}
                  </span>
                </Col>
                <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                  <span>Trạng thái báo cáo quỹ căn:</span>
                  <span
                    style={{
                      fontWeight: 600,
                      marginLeft: "4px",
                      fontSize: "1.1em"
                    }}
                  >
                    {formData["statusDetail"]}
                  </span>
                </Col>
              </Row>
            </Card>
            <Card title={"Xem thông tin khách hàng"}>
              <Button type="primary" onClick={() => setIsViewModal(true)}>
                Xem đầy đủ thông tin khách hàng
              </Button>
              <Modal
                title="Thông tin book căn"
                visible={isViewModal}
                onOk={() => {
                  setIsViewModal(false);
                }}
                onCancel={() => {
                  setIsViewModal(false);
                }}
                width={"60%"}
              >
                <Card
                  title={
                    <div>
                      <span style={{ marginRight: "5px" }}>
                        Thông tin khách hàng
                      </span>
                      <i className="icon icon-user" />
                    </div>
                  }
                >
                  {activeUserInfos.map(ele => {
                    return (
                      <>
                        <Row gutter={[24, 16]}>
                          <Col span={8}>
                            <span className="title-customer">
                              Tên khách hàng:
                            </span>
                            <span className="value-customer">{ele?.name}</span>
                          </Col>
                          <Col span={8}>
                            <span className="title-customer">
                              Số điện thoại:
                            </span>
                            <span className="value-customer">{ele?.phone}</span>
                          </Col>
                          <Col span={8}>
                            <span className="title-customer">Giới tính:</span>
                            <span className="value-customer">
                              {ele?.gender}
                            </span>
                          </Col>
                        </Row>
                        <Row gutter={[24, 16]}>
                          <Col span={8}>
                            <span className="title-customer">
                              Số chứng minh nhân dân:
                            </span>
                            <span className="value-customer">
                              {ele?.indentityNumber}
                            </span>
                          </Col>
                          <Col span={8}>
                            <span className="title-customer">Nơi cấp:</span>
                            <span className="value-customer">
                              {ele?.indentityProvince}
                            </span>
                          </Col>
                          <Col span={8}>
                            <span className="title-customer">Ngày cấp:</span>
                            <span className="value-customer">
                              {ele?.indentityIssueDate
                                ? formatDate(ele?.indentityIssueDate)
                                : ""}
                            </span>
                          </Col>
                        </Row>
                        <Row gutter={[24, 16]}>
                          <Col span={8}>
                            <span className="title-customer">Địa chỉ:</span>
                            <span className="value-customer">
                              {ele?.address}
                            </span>
                          </Col>
                        </Row>
                        <Row gutter={[24, 16]}>
                          <Col span={8}>
                            <span className="title-customer">Tỉnh:</span>
                            <span className="value-customer">
                              {getProvice(ele?.provinceId)}
                            </span>
                          </Col>
                          <Col span={8}>
                            <span className="title-customer">Huyện:</span>
                            <span className="value-customer">
                              {getDistrict(ele?.districtId)}
                            </span>
                          </Col>
                          <Col span={8}>
                            <span className="title-customer">Xã:</span>
                            <span className="value-customer">
                              {getWard(ele?.wardId)}
                            </span>
                          </Col>
                        </Row>
                        <div style={{ textAlign: "end" }}>
                          <a
                            href={`/#/main/customer/${ele?.id}`}
                            target="_blank"
                          >
                            Sửa thông tin <EditOutlined />
                          </a>
                        </div>

                        <div
                          style={{
                            margin: "10px 5px",
                            borderBottom: "solid 1px #9999"
                          }}
                        ></div>
                      </>
                    );
                  })}
                </Card>
              </Modal>
            </Card>
          </>
        ) : null}

        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={submitting}
          >
            {params.id === "add" ? "Thêm mới" : "Chỉnh sửa thông tin chính"}
          </Button>
          <Button
            type="default"
            style={{ color: "#fa5656" }}
            onClick={() => {
              history.goBack();
            }}
          >
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ apartment, loading, router }) => ({
  submitting: loading.effects["apartment/submit"],
  apartment,
  router
}))(ApartmentForm);
