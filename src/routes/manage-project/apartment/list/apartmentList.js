import React, { useState, useEffect } from "react";
import {
  Select,
  Row,
  Col,
  Popover,
  Radio,
  Divider,
  Button,
  Badge,
  Modal,
  Empty,
  Switch
} from "antd";
import {
  CheckCircleTwoTone,
  ExclamationCircleOutlined
} from "@ant-design/icons";
import { LockOutlined, PlusOutlined } from "@ant-design/icons";
import * as apartmentServices from "services/apartment";
import { v4 as uuid } from "uuid";
import { formatMoney } from "src/util/utils";
import { get, cloneDeep, uniq } from "lodash";
const enumStatus = {
  "Chưa mở bán": "yellow",
  "Mở bán": "green",
  Lock: "red"
};
const { Option } = Select;
const { confirm, info } = Modal;
const ApartmentList = props => {
  const { data, type, projectId, colorEnum, history, fetchData } = props;
  const [dataRow, setDataRow] = useState([]);
  const [dataCollumn, setDataCollumn] = useState([]);
  const [listApt, setListApt] = useState([]);

  const [buildingCode, setbuildingCode] = useState();
  const [list, setList] = useState([]);
  const [height, setHeight] = useState(40);
  const [width, setWidth] = useState(80);
  const [selectedItem, setSelectedItem] = useState([]);
  const [aptRow, setAptRow] = useState({});
  const [viewPrice, setViewPrice] = useState(true);
  const [disabledControl, setDisabledControl] = useState(false);
  useEffect(() => {
    setDataRow(data.row);
    setDataCollumn(data.column);
    setList(data.list);
    if (type === "house") {
      let build = data?.building?.at(0);
      setbuildingCode(build || "");
      let newArr = data?.list?.filter(item => item.buildingCode === build);
      setListApt(newArr);
    } else {
      setListApt(data.list);
    }
    const currentUser = JSON.parse(localStorage.getItem("user_id"));
    if (currentUser?.roleId === 21) {
      setDisabledControl(true);
    }
  }, [data]);
  useEffect(() => {
    let temp = {};
    if (listApt.length > 0) {
      if (type === "house") {
        listApt.map(e => {
          if (!temp[e.floorCode]) temp[e.floorCode] = [];
          temp[e.floorCode].push(e);
        });
      } else {
        listApt.map(e => {
          if (!temp[e.row]) temp[e.row] = [];
          temp[e.row].push(e);
        });
      }
    }
    setAptRow(temp);
  }, [listApt]);
  useEffect(() => {
    if (type === "house") {
      let row = new Set();
      let collumn = new Set();
      listApt.map(e => {
        row.add(e.floorCode);
        collumn.add(e.apartmentCode);
      });
      row = [...row].sort((a, b) => {
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }

        // names must be equal
        return 0;
      });
      collumn = [...collumn].sort((a, b) => {
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }

        // names must be equal
        return 0;
      });
      setDataRow([...row]);
      setDataCollumn([...collumn]);
    }
  }, [listApt]);

  function handleChange(value) {
    if (value === "all") {
      setDataRow(data?.row);
      return;
    }
    setDataRow(data?.row?.filter(item => item === value));
  }
  function handleChangeBuilding(value) {
    setbuildingCode(value.target.value);
    let newArr = list.filter(item => item.buildingCode === value.target.value);
    setListApt(newArr);
  }
  function handleChangeSubdivision(value) {
    if (value === "all") {
      setListApt(data?.list);
      return;
    }
    setListApt(list?.filter(item => item.subdivision === value));
  }
  function handleChangeWidth(value) {
    setWidth(value);
  }
  const getMaxColumn = () => {
    let MAX = 1;
    for (let i = 0; i < data?.row?.length; i++) {
      let newMax = data?.list?.filter(item => item.row === data?.row[i]).length;
      if (newMax > MAX) {
        MAX = newMax;
      }
    }
    return MAX;
  };
  const hangdelSelectItem = identity => {
    let newArray = cloneDeep(selectedItem);
    if (newArray.includes(identity)) {
      newArray = newArray.filter(item => item !== identity);
      setSelectedItem(newArray);
    } else {
      newArray.push(identity);
      setSelectedItem(newArray);
    }
  };
  const handleChangeRowStatus = row => {
    let newArray = cloneDeep(selectedItem);
    let temp;
    if (type === "house") {
      temp = listApt.filter(
        e => e.floorCode === row && e.buildingCode === buildingCode
      );
    } else {
      temp = listApt.filter(e => e.row === row);
    }

    temp.map(e => {
      newArray.push(e.id);
    });
    // newArray.push(temp);
    newArray = uniq(newArray);
    setSelectedItem(newArray);
  };
  const handleUpdate = () => {
    confirm({
      title: `Bạn có muốn mở bán ${selectedItem.length} căn đã chọn?`,
      icon: <ExclamationCircleOutlined />,
      // content: selectedItem.join(),
      okText: "Đồng ý",
      cancelText: "Hủy bỏ",
      onOk: () => {
        apartmentServices
          .updateStatusMulti({ ids: selectedItem, projectId, status: "Mở bán" })
          .then(rs => {
            if (rs?.data?.code === 0) {
              fetchData();
              Modal.success({
                title: "Kết quả update mở bán căn",
                content: (
                  <div>
                    <p style={{ color: "green" }}>
                      Thành công:{rs?.data?.success.length}
                    </p>
                    <ul>
                      {rs?.data?.success.map(e => {
                        return <li>{e.fullCode}</li>;
                      })}
                    </ul>
                    <p style={{ color: "red" }}>
                      Thất bại:{rs?.data?.fail.length}
                    </p>
                    <ul>
                      {rs?.data?.fail.map(e => {
                        return (
                          <li>
                            {e.fullCode} : {e.message}
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                )
              });
              setSelectedItem([]);
            }
          });
      }
    });
  };
  return (
    <>
      <Divider orientation="left">Thao tác</Divider>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div>
          <Button
            disabled={selectedItem.length !== 1 || disabledControl}
            onClick={() => {
              history.push({
                pathname: `${props.match.url}/${selectedItem[0]}`
              });
            }}
          >
            <i className="icon icon-edit" /> Sửa
          </Button>
          <Button onClick={handleUpdate} disabled={selectedItem.length === 0 || disabledControl}>
            {" "}
            <i className="icon icon-pricing-table" /> Mở bán
          </Button>

          <Button
            danger
            onClick={() => {
              setSelectedItem([]);
            }}
            disabled={selectedItem.length === 0}
          >
            {" "}
            <i className="icon icon-close" />
            Bỏ chọn
          </Button>
        </div>
        <div>
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => props.setIsShowModal(true)}
            disabled={disabledControl}
          >
            {`Import`}
          </Button>
          <Button
            style={{
              border: "1px solid gray",
              // padding: "8px",
              borderRadius: "5px",
              // height : "34px",
              // lineHeight : "34px",
              color: "green"
            }}
            disabled={type === "resort"}
          >
            <a
              target="_blank"
              href={`${
                process.env.REACT_APP_URL
              }/api/admin/apartment/export-stacking-plan?projectId=${projectId}&accesstoken=${localStorage.getItem(
                "token"
              )}`}
            >
              <i className="icon icon-long-arrow-down" />
              Xuất excel
            </a>
          </Button>
        </div>
      </div>
      <Divider orientation="left">
        Danh sách các căn ({list?.length} căn)
      </Divider>
      {type === "house" ? (
        <>
          <div>
            <Row>
              <Col
                lg={{ span: 5, offset: 1 }}
                style={{ display: "flex", alignItems: "center" }}
              >
                <label style={{ marginRight: 5, fontWeight: 600 }}>
                  Chọn tòa:
                </label>
                <Radio.Group
                  onChange={handleChangeBuilding}
                  value={buildingCode}
                >
                  {data?.building?.map((item, index) => (
                    <Radio key={index} value={item}>
                      Tòa {item}
                    </Radio>
                  ))}
                </Radio.Group>
              </Col>
              <Col lg={{ span: 5, offset: 1 }}>
                <label style={{ fontWeight: 600 }}>Chọn tầng:</label>
                <Select
                  defaultValue="all"
                  style={{ width: 200, marginLeft: 10 }}
                  onChange={handleChange}
                >
                  <Option key={"all"} value="all">
                    Tất cả
                  </Option>
                  {data?.row?.map((item, index) => (
                    <Option key={index} value={item}>
                      Tầng {item}
                    </Option>
                  ))}
                </Select>
              </Col>
              <Col lg={{ span: 4, offset: 3 }}>
                <label style={{ fontWeight: 600 }}>Tỉ lệ:</label>
                <Select
                  defaultValue={"80"}
                  style={{ width: 100, marginLeft: 10 }}
                  onChange={handleChangeWidth}
                >
                  <Option value="80">100%</Option>
                  <Option value="40">50%</Option>
                </Select>
              </Col>
              <Col
                lg={{ span: 3 }}
                style={{ display: "flex", alignItems: "center" }}
              >
                <label style={{ fontWeight: 600 }}>Hiển thị: </label>
                <Switch
                  checkedChildren={"Giá tiền"}
                  unCheckedChildren={"Mã căn"}
                  defaultChecked={viewPrice}
                  onChange={() => {
                    setViewPrice(!viewPrice);
                  }}
                />
              </Col>
            </Row>
          </div>

          <div style={{ alignItems: "center", margin: "30px 0px" }}>
            <Row>
              <Col lg={1}>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px"
                  }}
                >
                  <i className="icon icon-company" />
                </p>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px",
                    overflow: "hidden",
                    whiteSpace: "nowrap"
                  }}
                >
                  <i className="icon icon-home" />
                </p>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px",
                    overflow: "hidden",
                    whiteSpace: "nowrap"
                  }}
                >
                  <i className=" icon icon-map-directions" />
                </p>
                <div style={{ height: "100%" }}>
                  {dataRow?.map((item, index) => (
                    <span
                      key={index}
                      style={{
                        height: `${height}px`,
                        margin: 0,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        handleChangeRowStatus(item);
                      }}
                    >
                      {item}
                    </span>
                  ))}
                </div>
              </Col>
              <Col lg={23}>
                <div style={{ overflowX: "scroll" }}>
                  <div style={{ width: (dataCollumn?.length || 0) * width }}>
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px"
                        }}
                      >
                        {item}
                      </p>
                    ))}
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px",
                          // overflow: "hidden",
                          whiteSpace: "nowrap"
                        }}
                      >
                        {data?.rowInfo[item]?.numberRooms || "-"}
                      </p>
                    ))}
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px",
                          overflow: "hidden",
                          whiteSpace: "nowrap"
                        }}
                      >
                        {data?.rowInfo[item]?.direction || "-"}
                      </p>
                    ))}

                    {dataRow?.map(row =>
                      dataCollumn?.map(column => {
                        let item = aptRow[row]?.find(ele => {
                          return (
                            ele.floorCode == row && ele.apartmentCode == column
                          );
                        });
                        if (item) {
                          return (
                            <div
                              key={"apartment" + item.id}
                              style={{
                                width: `${width}px`,
                                backgroundColor: colorEnum[item.statusSale],
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                cursor: "pointer",
                                position: "relative"
                              }}
                              onClick={() => hangdelSelectItem(item.id)}
                            >
                              <span
                                className="status__span"
                                style={{
                                  position: "absolute",
                                  width: "30px",
                                  height: "10px",
                                  backgroundColor: enumStatus[item.status],
                                  top: 0,
                                  right: "-15px",
                                  transform: "rotateY(0deg) rotate(45deg)"
                                }}
                              ></span>
                              {item.statusSale === "HIDE" ? (
                                <p style={{ margin: 0 }}>
                                  <LockOutlined />
                                </p>
                              ) : (
                                <span
                                  style={{
                                    fontWeight: "bolder",
                                    color: "black",
                                    margin: 0,
                                    whiteSpace: "nowrap"
                                  }}
                                >
                                  <Badge
                                    count={
                                      selectedItem.includes(item.id) ? (
                                        <CheckCircleTwoTone
                                          style={{ color: "#f5222d" }}
                                        />
                                      ) : (
                                        0
                                      )
                                    }
                                  >
                                    {width > 40 ? (
                                      viewPrice ? (
                                        formatMoney(item.totalPrice)
                                      ) : (
                                        item.shortCode
                                      )
                                    ) : (
                                      <i className="icon icon-home" />
                                    )}
                                  </Badge>
                                </span>
                              )}
                            </div>
                          );
                        } else {
                          return (
                            <div
                              key={uuid()}
                              style={{
                                width: `${width}px`,
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                backgroundImage:
                                  "repeating-linear-gradient( 45deg, black, black 2px, transparent 2px, transparent 5px)"
                              }}
                            >
                              <span style={{ marginBottom: "2px" }}>Trống</span>
                            </div>
                          );
                        }
                      })
                    )}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </>
      ) : (
        <>
          <div>
            <Row>
              <Col lg={{ span: 5, offset: 1 }}>
                <label>Chọn dãy:</label>
                <Select
                  defaultValue="all"
                  style={{ width: 200, marginLeft: 10 }}
                  onChange={handleChange}
                >
                  <Option key={"all"} value="all">
                    Tất cả
                  </Option>
                  {data?.row?.map((item, index) => (
                    <Option key={index} value={item}>
                      Dãy {item}
                    </Option>
                  ))}
                </Select>
              </Col>
              <Col lg={{ span: 4, offset: 11 }}>
                <label style={{ fontWeight: 600 }}>Tỉ lệ:</label>
                <Select
                  defaultValue={"80"}
                  style={{ width: 100, marginLeft: 10 }}
                  onChange={handleChangeWidth}
                >
                  <Option value="80">100%</Option>
                  <Option value="40">50%</Option>
                </Select>
              </Col>
              <Col
                lg={{ span: 3 }}
                style={{ display: "flex", alignItems: "center" }}
              >
                <label style={{ fontWeight: 600 }}>Hiển thị: </label>
                <Switch
                  checkedChildren={"Giá tiền"}
                  unCheckedChildren={"Mã căn"}
                  defaultChecked={viewPrice}
                  onChange={() => {
                    setViewPrice(!viewPrice);
                  }}
                />
              </Col>
            </Row>
          </div>

          <div style={{ alignItems: "center", marginTop: 30 }}>
            <Row>
              <Col lg={1}>
                <p
                  style={{
                    textAlign: "center",
                    border: "solid 1px",
                    overflow: "hidden",
                    whiteSpace: "nowrap"
                  }}
                >
                  <i className="icon icon-home" />
                </p>
                <div style={{ height: "100%" }}>
                  {dataRow?.map((item, index) => (
                    <span
                      key={index}
                      style={{
                        height: `${height}px`,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        cursor: "pointer"
                      }}
                      onClick={() => {
                        handleChangeRowStatus(item);
                      }}
                    >
                      {item}
                    </span>
                  ))}
                </div>
              </Col>
              <Col lg={23}>
                <div style={{ overflowX: "scroll" }}>
                  <div
                    style={{ width: (dataCollumn?.length || 0) * width + 10 }}
                  >
                    {dataCollumn?.map((item, index) => (
                      <p
                        key={index}
                        style={{
                          display: "inline-block",
                          width: `${width}px`,
                          textAlign: "center",
                          border: "solid 1px"
                        }}
                      >
                        {item}
                      </p>
                    ))}
                  </div>
                  <div
                    style={{ width: (dataCollumn?.length || 0) * width + 10 }}
                  >
                    {dataRow?.map(row =>
                      dataCollumn?.map(column => {
                        let item = aptRow[row]?.find(ele => {
                          return ele.row == row && ele.apartmentCode == column;
                        });
                        if (item) {
                          return (
                            <div
                              key={"apartment" + item.id}
                              style={{
                                width: `${width}px`,
                                backgroundColor: colorEnum[item.statusSale],
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                cursor: "pointer",
                                position: "relative"
                              }}
                              onClick={() => hangdelSelectItem(item.id)}
                            >
                              <span
                                className="status__span"
                                style={{
                                  position: "absolute",
                                  width: "30px",
                                  height: "10px",
                                  backgroundColor: enumStatus[item.status],
                                  top: 0,
                                  right: "-15px",
                                  transform: "rotateY(0deg) rotate(45deg)"
                                }}
                              ></span>
                              {item.statusSale === "HIDE" ? (
                                <p style={{ margin: 0 }}>
                                  <LockOutlined />
                                </p>
                              ) : (
                                <span
                                  style={{
                                    fontWeight: "bolder",
                                    color: "black",
                                    margin: 0,
                                    whiteSpace: "nowrap"
                                  }}
                                >
                                  <Badge
                                    count={
                                      selectedItem.includes(item.id) ? (
                                        <CheckCircleTwoTone
                                          style={{ color: "#f5222d" }}
                                        />
                                      ) : (
                                        0
                                      )
                                    }
                                  >
                                    {width > 40 ? (
                                      viewPrice ? (
                                        formatMoney(item.totalPrice)
                                      ) : (
                                        item.shortCode
                                      )
                                    ) : (
                                      <i className="icon icon-home" />
                                    )}
                                  </Badge>
                                </span>
                              )}
                            </div>
                          );
                        } else {
                          return (
                            <div
                              key={uuid()}
                              style={{
                                width: `${width}px`,
                                height: `${height}px`,
                                border: "solid 1px",
                                display: "inline-flex",
                                alignItems: "center",
                                justifyContent: "center",
                                overflow: "hidden",
                                backgroundImage:
                                  "repeating-linear-gradient( 45deg, black, black 2px, transparent 2px, transparent 5px)"
                              }}
                            >
                              <span style={{ marginBottom: "2px" }}>Trống</span>
                            </div>
                          );
                        }
                      })
                    )}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </>
      )}
    </>
  );
};

export default ApartmentList;
