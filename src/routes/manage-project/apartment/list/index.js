import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Input,
  Button,
  Space,
  Tooltip,
  Modal,
  Upload,
  Card,
  message
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  UploadOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { union, cloneDeep } from "lodash";
import ApartmentDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";

const UpLoadFile = Upload;
const tranformApartmentParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "apartment";
const ApartmentList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const [isShowModal, setIsShowModal] = useState(false);
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState({
    id: { fixed: "left" },
    status: { fixed: "left" },
    type: { fixed: "left" },
    buildingCode: { fixed: "left" },
    floorCode: { fixed: "left" },
    apartmentCode: { fixed: "left" },
    cornerApartment: { fixed: "left" },
    view: { fixed: "left" },
    "type ": { fixed: "left" },
    numberRooms: { fixed: "left" },
    areaHeartWall: { fixed: "left" },
    area: { fixed: "left" },
    pricePerNotVAT: { fixed: "left" },
    totalPriceNotVAT: { fixed: "left" },
    row: { fixed: "left" },
    subdivision: { fixed: "left" },
    areaLand: { fixed: "left" },
    areaBuilding: { fixed: "left" }
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;

  const fetchData = () => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformApartmentParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: {
          ...initParams.filters,
          projectId: props.projectId,
          type: props.projectType === "house" ? "high" : "low"
        }
      }
    });
  };
  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformApartmentParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", 'false');
          values = {};
        }
        const initParams = tranformApartmentParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  };

  const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  };

  const highColumns = [
    {
      title: "Mã căn",
      dataIndex: "fullCode",
      width: 120,
      sorter: false,
      ...getColumnSearchProps("fullCode")
    },
    {
      title: "Mã căn rút gọn",
      dataIndex: "shortCode",
      width: 60,
      sorter: false,
      ...getColumnSearchProps("shortCode")
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      width: 60,
      filters: true,
      valueEnum: {
        "Mở bán": { "text": "Mở bán", "status": "Processing", "color": "#ec3b3b", "isText": true },
        "Chưa mở bán": { "text": "Chưa mở bán", "status": "Default", "color": "#ec3b3b", "isText": true },
        "Lock": { "text": "Lock", "status": "Error", "color": "#ec3b3b", "isText": true },
      },
    },
    {
      title: "Mã tòa nhà",
      dataIndex: "buildingCode",
      width: 60,

      sorter: true,
      hideInSearch: true,
      ...getColumnSearchProps("buildingCode")
    },
    {
      title: "Mã tầng",
      dataIndex: "floorCode",
      width: 50,
      sorter: true,
      hideInSearch: true,
      ...getColumnSearchProps("floorCode")
    },
    {
      title: "Số nhà",
      dataIndex: "apartmentCode",
      width: 50,
      sorter: true,
      hideInSearch: false,
      ...getColumnSearchProps("apartmentCode")
    },
    {
      title: "Vị trí",
      dataIndex: "cornerApartment",
      width: 50,
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: "View",
      dataIndex: "view",
      width: 80,
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: "Trạng thái bán hàng",
      dataIndex: "statusSale",
      width: 120,
      filters: true,
      valueEnum: {
        "CHƯA BÁN 1_Chưa cọc": { "text": "CHƯA BÁN 1_Chưa cọc", "status": "Processing", "color": "#ec3b3b", "isText": true },
        "CHƯA BÁN 2_Chưa đủ cọc": { "text": "CHƯA BÁN 2_Chưa đủ cọc", "status": "Default", "color": "#ec3b3b", "isText": true },
        "CHƯA BÁN 3_HủyHDV/HĐMB": { "text": "CHƯA BÁN 3_HủyHDV/HĐMB", "status": "Error", "color": "#ec3b3b", "isText": true },
        "CHƯA BÁN 4_LOCK": { "text": "CHƯA BÁN 3_HủyHDV/HĐMB", "status": "Error", "color": "#ec3b3b", "isText": true },
        "BÁN 1_Đủ cọc và thủ tục": { "text": "CHƯA BÁN 3_HủyHDV/HĐMB", "status": "Error", "color": "#ec3b3b", "isText": true },
        "BÁN 2_Đủ cọc": { "text": "BÁN 2_Đủ cọc", "status": "Error", "color": "#ec3b3b", "isText": true },
        "BÁN 4_Đã ký HĐMB": { "text": "BÁN 4_Đã ký HĐMB", "status": "Error", "color": "#ec3b3b", "isText": true },

      },
    },
    // {
    //   title: "Số phòng ngủ",
    //   dataIndex: "numberRooms",
    //   width: 60,

    //   sorter: true,
    //   hideInSearch: true
    // },
    // {
    //   title: "Diện tích tim tường",
    //   dataIndex: "areaHeartWall",
    //   width: 60,

    //   sorter: true,
    //   hideInSearch: true
    // },
    // {
    //   title: "Diện tích",
    //   dataIndex: "area",
    //   width: 60,

    //   sorter: true,
    //   hideInSearch: true
    // },
    // {
    //   title: "Giá(chưa VAT)",
    //   dataIndex: "pricePerNotVAT",
    //   width: 60,
    //   sorter: true,
    //   hideInSearch: true
    // },
    // {
    //   title: "Tổng giá(chưa VAT)",
    //   dataIndex: "totalPriceNotVAT",
    //   width: 60,

    //   sorter: true,
    //   hideInSearch: true
    // },
    {
      title: "Thao tác",
      width: 60,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      )
    }
  ];

  const lowColumns = [
    {
      title: "Mã căn",
      dataIndex: "fullCode",
      width: 60,
      sorter: false,
      hideInSearch: false,
      ...getColumnSearchProps("fullCode")
    },
    {
      title: "Mã căn rút gọn",
      dataIndex: "shortCode",
      width: 60,
      sorter: false,
      hideInSearch: false,
      ...getColumnSearchProps("shortCode")
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      width: 60,
      sorter: false,
      hideInSearch: false
    },
    {
      title: "Tầng",
      dataIndex: "row",
      width: 60,
      sorter: true,
      hideInSearch: true,
      ...getColumnSearchProps("row")
    },
    {
      title: "Phân khu",
      dataIndex: "subdivision",
      width: 60,

      sorter: true,
      hideInSearch: true,
      ...getColumnSearchProps("subdivision")
    },
    {
      title: "Diện tích căn",
      dataIndex: "areaLand",
      width: 60,

      sorter: true,
      hideInSearch: true
    },
    {
      title: "Diện tích xây dựng",
      dataIndex: "areaBuilding",
      width: 60,

      sorter: true,
      hideInSearch: true
    },
    {
      title: "Thao tác",
      width: 100,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      )
    }
  ];
  const [fileList, setFileList] = useState([]);
  const onSubmitImport = value => {
    const data = cloneDeep(value);
    console.log("🚀 ~ file: index.js ~ line 401 ~ onSubmitImport ~ data", data);

    if (props.projectId) {
      data.projectId = props.projectId;
    }
    if (props.projectType) {
      data.type = props.projectType === "house" ? "high" : "low";
    }
    if (fileList.length > 0) {
      data.fileExcel = fileList;
    }

    dispatch({
      type: `${RESOURCE}/importExcel`,
      payload: data,
      callback: response => {
        if (response.status === 200) {
          fetchData();
        }
      }
    });
  };
  const uploadProps = {
    maxCount: 1,
    onRemove: file => {
      setFileList(fileList => {
        const index = fileList.indexOf(file);
        const newFileList = fileList.slice();
        newFileList.splice(index, 1);
        return newFileList;
      });
    },
    beforeUpload: file => {
      if (fileList.length > 0) {
        message.warn("Tối đa một file");
        return;
      }
      setFileList(fileList => [...fileList, file]);
      return false;
    },
    fileList
  };

  return (
    <>
      <Form form={form} onFinish={onSubmitImport}>
        <Modal
          visible={isShowModal}
          onOk={() => form.submit()}
          onCancel={() => setIsShowModal(false)}
        >
          <Card
            title="Import file Excel danh sách bảng hàng"
          // className={styles.cardCheckCodeResult}
          >
            <div style={{ textAlign: "center" }}>
              <Form.Item name="fileExcel">
                <UpLoadFile {...uploadProps}>
                  <Button icon={<UploadOutlined />}>Upload</Button>
                </UpLoadFile>
              </Form.Item>
              {props.projectType === "house" ? (
                <div>
                  <p>
                    Tải file mẫu{" "}
                    <a
                      href={`/static/form-import-high-apartment.xlsx`}
                      download
                    >
                      tại đây
                    </a>
                  </p>
                </div>
              ) : (
                <div>
                  <p>
                    Tải file mẫu{" "}
                    <a href={`/static/low-apartment.xlsx`} download>
                      tại đây
                    </a>
                  </p>
                </div>
              )}
            </div>
          </Card>
        </Modal>
      </Form>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={false}
        headerTitle="Căn hộ"
        actionRef={actionRef}
        formRef={form}
        form={{
          initialValues: formValues
        }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={props.projectType === "house" ? highColumns : lowColumns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
          >
            {`Thêm mới`}
          </Button>,
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => setIsShowModal(true)}
          >
            {`Import`}
          </Button>
          // <Select defaultValue="high" style={{ width: 120 }} onChange={(val) => setType(val)}>
          //   <Option value="high">Cao cấp</Option>
          //   <Option value="low">Thường</Option>
          // </Select>
        ]}
      />
      <ApartmentDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ apartment, loading }) => ({
  apartment,
  loading: loading.models.apartment
}))(ApartmentList);
