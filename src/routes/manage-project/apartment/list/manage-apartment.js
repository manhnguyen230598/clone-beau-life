import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Input,
  Button,
  Space,
  Select,
  Tooltip,
  Avatar,
  Row,
  Col,
  Upload,
  Card,
  message,
  Divider,
  Modal
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  UploadOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { get, union, cloneDeep } from "lodash";
import ApartmentDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import ApartmentListTable from "./apartmentList";
import Loading from "src/components/Loading";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
const UpLoadFile = Upload;
const enumStatus = {
  "Chưa mở bán": "yellow",
  "Mở bán": "green",
  Lock: "red"
};
const { Option } = Select;

const RESOURCE = "apartment";
const ApartmentList = props => {
  console.log(props)
  const intl = useIntl();
  const [form] = Form.useForm();
  const [isShowModal, setIsShowModal] = useState(false);
  const [fileList, setFileList] = useState([]);
  const [projectType, setProjectType] = useState(props.projectType);
  const onSubmitImport = value => {
    setLoadingModal(true);
    const data = cloneDeep(value);
    if (props.projectId) {
      data.projectId = props.projectId;
    }
    setProjectType(data.type)
    data.type = data.type === "house" ? "high" : "low";

    if (fileList.length > 0) {
      data.fileExcel = fileList;
    } else {
      setLoadingModal(false);
      message.error("Vui lòng chọn file");
    }
    dispatch({
      type: `${RESOURCE}/importExcel`,
      payload: data,
      callback: rs => {
        if (rs.status === 200) {
          fetchData();
          setLoadingModal(false);
          setIsShowModal(false);
          Modal.success({
            title: "Kết quả update mở bán căn",
            content: (
              <div>
                <p style={{ color: "green" }}>
                  Thành công: {rs?.data?.success.length}
                </p>
                <p style={{ color: "red" }}>
                  Thất bại: {rs?.data?.fail.length}
                </p>
                {rs?.data?.fail.length > 0 ? (
                  <span>Mã căn thất bại: {rs?.data?.fail?.join()}</span>
                ) : (
                  ""
                )}
                <p style={{ color: "red" }}>
                  Cập nhật: {rs?.data?.updated.length}
                </p>
                {rs?.data?.updated.length > 0 ? (
                  <span>Mã căn cập nhật: {rs?.data?.updated?.join()}</span>
                ) : (
                  ""
                )}
                {/* <p style={{fontWeight : 600}}>Vui lòng reload lại trang để thấy sự thay đổi!</p> */}
              </div>
            ),
            width: "50vw"
          });
        }
      }
    });
  };
  const uploadProps = {
    maxCount: 1,
    onRemove: file => {
      setFileList(fileList => {
        const index = fileList.indexOf(file);
        const newFileList = fileList.slice();
        newFileList.splice(index, 1);
        return newFileList;
      });
    },
    beforeUpload: file => {
      if (fileList.length > 0) {
        message.warn("Tối đa một file");
        return;
      }
      setFileList(fileList => [...fileList, file]);
      return false;
    },
    fileList
  };
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [colorStatus, setColorStatus] = useState([]);
  const [colorEnum, setColorEnum] = useState({});
  const [countStatus, setCountStatus] = useState({});
  const [mapStatusCount, setMapStatusCount] = useState({});
  const [list, setList] = useState([]);
  const [loadingModal, setLoadingModal] = useState(false);
  const {
    dispatch,
    [RESOURCE]: { data }
    // loading,
    // history
  } = props;

  const fetchData = () => {
    dispatch({
      type: `${RESOURCE}/fetchManage`,
      payload: {
        projectId: props.projectId
      }
    });
  };
  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    setColorStatus(data.colorStatusSale);
    setCountStatus(data.countStatusSale);
    setList(data.list);
    form.setFieldsValue({ type: props.projectType });
  }, [data]);
  const countStatusData = arr => {
    let temp = {
      "Chưa mở bán": 0,
      "Mở bán": 0,
      Lock: 0
    };
    if (Array.isArray(list)) {
      list.map(e => {
        temp[e.status]++;
      });
      setMapStatusCount(temp);
    }
  };
  useEffect(() => {
    let temp = {};
    if (Array.isArray(colorStatus)) {
      colorStatus.map(e => {
        temp[e.status] = e.color;
      });
      setColorEnum(temp);
    }
  }, [colorStatus]);
  useEffect(() => {
    countStatusData(list);
  }, [list]);
  const renderDowload = () => {
    const openUrl = () => {
      if (form.getFieldValue("type") === "house") {
        return window.open("/static/form-import-high-apartment.xlsx");
      }
      if (form.getFieldValue("type") === "resort") {
        return window.open("/static/low-apartment.xlsx");
      }
      message.warn("Vui lòng chọn loại dự án.");
    };
    return (
      <div>
        <p>
          Tải file mẫu{" "}
          <a onClick={() => openUrl()}>
            {/* <a href={`/static/form-import-high-apartment.xlsx`} download> */}
            tại đây
          </a>
        </p>
      </div>
    );
  };
  if (!colorStatus) return <Loading />;
  return (
    <>
      <Card style={{ backgroundColor: "#e1f5fe", border: "solid 1px" }}>
        <Row>
          <Col key={`status_total_can`} xl={6} lg={6} md={6} sm={6} xs={12}>
            {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
             */}
            <span
              style={{
                width: "40px",
                height: "15px",
                backgroundColor: "transparent",
                marginRight: "8px",
                display: "inline-block",
                border: "solid 1px"
              }}
            ></span>
            <span style={{ fontWeight: 600 }}>
              TỔNG SỐ CĂN ({data?.list?.length || 0} căn)
            </span>
          </Col>
          {colorStatus?.map((item, index) => (
            <Col key={index} xl={6} lg={6} md={6} sm={6} xs={12}>
              {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
               */}
              <span
                style={{
                  width: "40px",
                  height: "15px",
                  backgroundColor: `${item.color}`,
                  marginRight: "8px",
                  display: "inline-block"
                }}
              ></span>
              <span style={{ fontWeight: 600 }}>
                {item.status} ({countStatus[item.status] || 0} căn)
              </span>
            </Col>
          ))}
        </Row>
        <Divider />
        <Row>
          {Object.keys(enumStatus)?.map((item, index) => (
            <Col key={index} xl={6} lg={6} md={6} sm={6} xs={12}>
              {/* <Badge className="nv-badge-all" key={index} color={item.color} text= />
               */}
              <span
                style={{
                  width: "40px",
                  height: "15px",
                  marginRight: "8px",
                  display: "inline-block",
                  position: "relative",
                  overflow: "hidden",
                  backgroundColor: "white"
                }}
              >
                <span
                  className="status__span"
                  style={{
                    position: "absolute",
                    width: "30px",
                    height: "10px",
                    backgroundColor: enumStatus[item],
                    top: 0,
                    right: "-15px",
                    transform: "rotateY(0deg) rotate(45deg)"
                  }}
                ></span>
              </span>

              <span style={{ fontWeight: 600 }}>
                {item} ({mapStatusCount[item]} căn)
              </span>
            </Col>
          ))}
        </Row>
      </Card>
      <Form form={form} onFinish={onSubmitImport}>
        <Modal
          visible={isShowModal}
          onOk={() => form.submit()}
          onCancel={() => setIsShowModal(false)}
          confirmLoading={loadingModal}
        >
          <Card
            title="Import file Excel danh sách bảng hàng"
            // className={styles.cardCheckCodeResult}
          >
            <div style={{ textAlign: "center" }}>
              <Form.Item
                label={"Loại căn"}
                name="type"
                rules={[{ required: true, message: "Loại không được trống" }]}
                initialValue={props.projectType}
              >
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      resort: { text: "Dự án thấp tầng" },
                      house: { text: "Dự án cao tầng" }
                    },
                    hideInTable: false,
                    hideInSearch: false
                  }}
                  disabled={props.projectType ? true : false}
                  intl={intl}
                />
              </Form.Item>
              <Form.Item name="fileExcel">
                <UpLoadFile {...uploadProps}>
                  <Button icon={<UploadOutlined />}>Upload</Button>
                </UpLoadFile>
              </Form.Item>
              {renderDowload()}
            </div>
          </Card>
        </Modal>
      </Form>
      <ApartmentListTable
        type={projectType}
        data={data}
        projectId={props.projectId}
        colorEnum={colorEnum}
        history={props.history}
        match={props.match}
        setIsShowModal={setIsShowModal}
        fetchData={fetchData}
      />
    </>
  );
};

export default connect(({ apartment, loading }) => ({
  apartment,
  loading: loading.models.apartment
}))(ApartmentList);
