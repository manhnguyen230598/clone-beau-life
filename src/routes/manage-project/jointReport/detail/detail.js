import React, { useEffect, useState } from "react";
import { Card, Tabs } from "antd";
import { router } from 'dva';
import get from 'lodash/get';
import asyncComponent from "util/asyncComponent";
import IntlMessages from "util/IntlMessages";
import * as projectServices from 'services/project';

const TabPane = Tabs.TabPane;
const { Switch, Route } = router;
const ProjectDetailRoute = ({ component: Component, projectId, templateCustomerFile, ...rest }) =>
  <Route
    {...rest}
    render={props => <Component {...props} templateCustomerFile={templateCustomerFile} projectId={Number(projectId)} />}
  />;

const JointReport = ({ projectId, match, location, history, ...rest }) => {
  const [projectInfo, setProjectInfo] = useState(null);
  const [user, setUser] = useState();
  useEffect(() => {
    projectServices.get(Number(match.params.id), {}, false).then(res => {
      setProjectInfo(get(res, "data.data[0]", {}));
    });
    const currentUser = JSON.parse(localStorage.getItem("user_id"));
    setUser(currentUser);
  }, [match.params.id]);

  if (!projectInfo) {
    return null;
  }

  function callback(key) {
    switch (key) {
      case 'customProject':
        history.push({ pathname: `${match.url}/customProject` });
        break;
      case 'chartProject':
        history.push({ pathname: `${match.url}/chartProject` });
        break;
      case 'mediaCustomer':
        history.push({ pathname: `${match.url}/mediaCustomer` });
        break;
      case 'VPBHCustomer':
        history.push({ pathname: `${match.url}/VPBHCustomer` });
        break;
      case 'customerBought':
        history.push({ pathname: `${match.url}/customerBought` });
        break;
      case 'reportFund':
        history.push({ pathname: `${match.url}/reportFund` });
        break;
      case 'resultSaleAgency':
        history.push({ pathname: `${match.url}/resultSaleAgency` });
        break;
      case 'salesReport':
        history.push({ pathname: `${match.url}/salesReport` });
        break;
      case 'completedReport':
        history.push({ pathname: `${match.url}/completedReport` });
        break;
      default:
        break;
    }
  }
  const getTablist = tabList => {
    if (user?.roleId === 21) {
      let temp = [];
      let includes = ["salesReport", "resultSaleAgency", "completedReport"];
      tabList.map(e => {
        if (includes.includes(e.key)) {
          temp.push(e);
        }
      });
      return temp;
    }
    return tabList;
  };
  const tabList = [
    {
      key: 'customProject',
      tab: <IntlMessages id="customProject.customProject" />,
    },
    {
      key: 'chartProject',
      tab: <IntlMessages id="customProject.chartProject" />,
    },
    {
      key: 'mediaCustomer',
      tab: <IntlMessages id="customProject.mediaCustomer" />,
    },
    {
      key: 'VPBHCustomer',
      tab: <IntlMessages id="customProject.VPBHCustomer" />,
    },
    {
      key: 'customerBought',
      tab: <IntlMessages id="customProject.customerBought" />,
    },
    {
      key: 'reportFund',
      tab: <IntlMessages id="customProject.reportFund" />,
    },
    {
      key: 'resultSaleAgency',
      tab: <IntlMessages id="customProject.resultSaleAgency" />,
    },
    {
      key: 'salesReport',
      tab: <IntlMessages id="salesReport.title" />
    },
    {
      key: 'completedReport',
      tab: <IntlMessages id="completedReport.title" />
    },
  ];

  const getTabKey = () => {
    const url = match.url === '/' ? '' : match.url;
    const tabKey = location.pathname.replace(`${url}`, '');
    if (tabKey && tabKey !== '') {
      return tabKey.replace(/\//g, "");
    }
    if (user?.roleId === 21){
      return "resultSaleAgency"
    }
    return 'customProject';
  };

  return (
    <Card className="gx-card">
      <Tabs
        // defaultActiveKey="1"
        onChange={callback}
        activeKey={getTabKey()}
      >
        {getTablist(tabList).map((item, index) => (
          <TabPane {...item} tab={item.tab} key={item.key || index} />
        ))}
      </Tabs>
      <Switch>
        {/* <Redirect exact from={`${match.url}/`} to={`${match.url}/${getTabKey()}`} /> */}
        <ProjectDetailRoute path={`${match.url}/customProject`} projectId={projectId} component={asyncComponent(() => import("../../customProject/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/chartProject`} projectId={projectId} component={asyncComponent(() => import("../../chartProject/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/mediaCustomer`} projectId={projectId} component={asyncComponent(() => import("../../mediaCustomer/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/VPBHCustomer`} projectId={projectId} component={asyncComponent(() => import("../../VPBHCustomer/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/customerBought`} projectId={projectId} component={asyncComponent(() => import("../../customerBought/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/reportFund`} projectId={projectId} component={asyncComponent(() => import("../../reportFund/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/resultSaleAgency`} projectId={projectId} component={asyncComponent(() => import("../../resultSaleAgency/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/salesReport`} projectId={projectId} component={asyncComponent(() => import("../../salesReport/list"))} exact={true} />
        <ProjectDetailRoute path={`${match.url}/completedReport`} projectId={projectId} component={asyncComponent(() => import("../../completedReport/list"))} exact={true} />
        {/* <ProjectDetailRoute path={`${match.url}/chartproject/:id`} projectId={match.params.id} component={asyncComponent(() => import("../../chartProject/list"))} exact={true} /> */}
      </Switch>
    </Card>
  );
};

export default JointReport;
