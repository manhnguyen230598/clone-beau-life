import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  InputNumber,
  Button,
  Space,
  Tooltip,
  Avatar,
  Divider,
  Popconfirm,
  Modal,
  message,
  Input
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  MenuUnfoldOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { get, isNumber, union } from "lodash";
import TargetsDealerDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import ColumnSetting from "src/packages/pro-table/component/columnSetting";
import AgencySelect from "../../../../components/Select/Agency/ListAgency";
import * as agencyTargetServices from "src/services/targetsDealer";
const tranformTargetsDealerParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "targetsDealer";
const TargetsDealerList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();
  const [isViewModal, setIsViewModal] = useState(false);
  const [modalFix, setModalFix] = useState(false);
  const [selectRecord, setSelectRecord] = useState({});
  const [listAgency, setListAgency] = useState([]);
  const [agencySelect, setAgencySelect] = useState({});
  const [target, setTarget] = useState();
  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState();

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformTargetsDealerParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: { projectId: props.projectId }
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);
  useEffect(() => {
    if (selectRecord.id) {
      agencyTargetServices.getListAgency(selectRecord.id).then(rs => {
        if (rs && rs.data?.code === 0) {
          setListAgency(rs.data?.data);
        }
      });
    }
  }, [selectRecord]);
  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformTargetsDealerParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });
        if (typeof values.agencyId == "object") {
          values.agencyId = values.agencyId?.id;
        }
        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", "false");
          values = {};
        }
        const initParams = tranformTargetsDealerParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters
            },
            populate: "agencyId",
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({
      pathname: `${props.match.url}/add`,
      state: { projectId: props.projectId }
    });
  };

  const handleEditClick = item => {
    history.push({
      pathname: `${props.match.url}/${item.id}`,
      state: { projectId: props.projectId }
    });
  };
  const handleListClick = item => {
    setSelectRecord(item);
    setIsViewModal(true);
  };
  const handleDeleteClick = item => {
    dispatch({
      type: `${RESOURCE}/delete`,
      payload: item,
      callback: () => {
        let initParams = {};
        let pagi = Object.assign(pagination, tableChange?.pagination || {});
        const defaultCurrent = pagi.current || 1;
        initParams = tranformTargetsDealerParams(
          tableChange?.filtersArg,
          tableChange?.formValues,
          tableChange?.sort
        );

        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            page: tableChange?.pagination?.current || current,
            current: tableChange?.pagination?.current || current,
            skip: (defaultCurrent - 1) * pagi.pageSize,
            limit: pagi.pageSize || limit,
            pageSize: pagi.pageSize || limit,
            sort: initParams.sort
              ? initParams.sort.length <= 0
                ? [{ createdAt: "desc" }]
                : initParams.sort
              : [{ createdAt: "desc" }],
            queryInput: { projectId: props.projectId }
          }
        });
      }
    });
  };
  const updateAmount = async () => {
    let temp = target;
    temp = Number(temp);
    console.log(temp);
    if (!isNumber(target)) {
      return message.error("Vui lòng nhập số");
    }
    let rs = await agencyTargetServices.updateAmount(agencySelect.id, target);
    message.success({content : "Cập nhật thành công!"})
    agencyTargetServices.getListAgency(selectRecord.id).then(rs => {
      if (rs && rs.data?.code === 0) {
        setListAgency(rs.data?.data);
      }
    });
    setModalFix(false);
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Giai đoạn",
      dataIndex: "name",
      width: 70
    },
    {
      title: "Ngày bắt đầu",
      dataIndex: "startDate",
      width: 70,
      valueType: "dateTime",
      hideInSearch: true
    },
    {
      title: "Ngày kết thúc",
      dataIndex: "endDate",
      width: 70,
      valueType: "dateTime",
      hideInSearch: true
    },
    {
      title: "Thao tác",
      width: 50,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
          <Divider type="vertical" />

          <Tooltip title={`Danh sách đại lý`}>
            <MenuUnfoldOutlined
              onClick={() => {
                handleListClick(record);
              }}
            />
          </Tooltip>
        </>
      )
    }
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Chỉ tiêu"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
          >
            {`Thêm mới`}
          </Button>
        ]}
      />
      <TargetsDealerDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
      <Modal
        visible={isViewModal}
        onCancel={() => setIsViewModal(false)}
        onOk={() => setIsViewModal(false)}
        title={selectRecord.name}
        width={"35%"}
      >
        <ProTable
          tableClassName="gx-table-responsive"
          type="table"
          rowKey="id"
          search={false}
          headerTitle={"Chỉ tiêu đại lý"}
          dataSource={listAgency}
          pagination={false}
          columns={[
            {
              title: "Đại lý",
              dataIndex: ["agencyId", "name"],
              width: 60,
              fixed: "left"
            },
            {
              title: "Số lượng",
              dataIndex: "amount",
              width: 60,
              fixed: "left",
              hideInSearch: true
            },
            {
              title: "Thao tác",
              width: 50,
              fixed: "right",
              render: (text, record) => (
                <>
                  <Tooltip title={`Sửa`}>
                    <EditOutlined
                      onClick={() => {
                        setTarget(record?.amount || 0);
                        setAgencySelect(record);
                        setModalFix(true);
                      }}
                    />
                  </Tooltip>
                </>
              )
            }
          ]}
        />
      </Modal>
      <Modal
        visible={modalFix}
        onCancel={() => {
          setModalFix(false);
        }}
        onOk={() => updateAmount()}
        title={`Sửa chỉ tiêu đại lý ${agencySelect?.agencyId?.name}`}
      >
        {" "}
        <span>Chỉ tiêu: </span>
        <InputNumber
          defaultValue={target}
          onChange={e => {
            setTarget(e);
          }}
        />
      </Modal>
    </>
  );
};

export default connect(({ targetsDealer, loading }) => ({
  targetsDealer,
  loading: loading.models.targetsDealer
}))(TargetsDealerList);
