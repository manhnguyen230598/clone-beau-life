import React, { useEffect, useState } from 'react';
import { Card, Button, Form, Col, Row, Popover } from 'antd';
import _ from 'lodash';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect/* , routerRedux */ } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import ProjectSelect from 'components/Select/Project/ListProject';
// import * as enums from 'util/enums';
// import { camelCaseToDash } from 'util/helpers';

const RESOURCE = "saleTarget";
const fieldLabels = { "name": "Chỉ tiêu", "year": "Năm", "month": "Tháng", "week": "Tuần", "amount": "Số lượng", "projectId": "Dự án", "group": "Nhóm", "isActive": "Trạng thái" };
const SaleTargetForm = ({ saleTarget: { formTitle, formData }, dispatch, submitting, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');
  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };
  

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: (res) => {
        if (res && !res.error) {
          history.goBack();
        }
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  });

  if (params.id === 'add' || (formData && formData.id && formData.id !== 'add')) {
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin cơ bản" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['name']}
                name="name"
              >
                <FormInputRender
                  item={{
                    "title": "Chỉ tiêu", "dataIndex": "name", "width": 200,
                    formItemProps: {
                      disabled: true
                    }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['year']}
                name="year"
              >
                <FormInputRender
                  item={{
                    "title": "Năm", "dataIndex": "year", "width": 100,
                    formItemProps: {
                      disabled: true
                    }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['month']}
                name="month"
              >
                <FormInputRender
                  item={{
                    "title": "Tháng", "dataIndex": "month", "width": 120,
                    "valueEnum": {
                      "1": { "text": "Tháng 1", "status": "Processing", "color": "#ec3b3b", "isText": true },
                      "2": { "text": "Tháng 2", "status": "Default", "color": "#ec3b3b", "isText": true },
                      "3": { "text": "Tháng 3", "status": "Error", "color": "#ec3b3b", "isText": true },
                      "4": { "text": "Tháng 4", "status": "Success", "color": "#ec3b3b", "isText": true },
                      "5": { "text": "Tháng 5", "status": "Success", "color": "#ec3b3b", "isText": true },
                      "6": { "text": "Tháng 6", "status": "Processing", "color": "#ec3b3b", "isText": true },
                      "7": { "text": "Tháng 7", "status": "Default", "color": "#ec3b3b", "isText": true },
                      "8": { "text": "Tháng 8", "status": "Error", "color": "#ec3b3b", "isText": true },
                      "9": { "text": "Tháng 9", "status": "Success", "color": "#ec3b3b", "isText": true },
                      "10": { "text": "Tháng 10", "status": "Success", "color": "#ec3b3b", "isText": true },
                      "11": { "text": "Tháng 11", "status": "Success", "color": "#ec3b3b", "isText": true },
                      "12": { "text": "Tháng 12", "status": "Success", "color": "#ec3b3b", "isText": true }
                    },
                    formItemProps: {
                      disabled: true
                    }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['week']}
                name="week"
              >
                <FormInputRender
                  item={{
                    "title": "Tuần", "dataIndex": "week", "width": 120,
                    "valueEnum": {
                      "Tuần 1": { "text": "Tuần 1", "status": "Processing", "color": "#ec3b3b", "isText": true },
                      "Tuần 2": { "text": "Tuần 2", "status": "Default", "color": "#ec3b3b", "isText": true },
                      "Tuần 3": { "text": "Tuần 3", "status": "Error", "color": "#ec3b3b", "isText": true },
                      "Tuần 4": { "text": "Tuần 4", "status": "Success", "color": "#ec3b3b", "isText": true },
                      "Tuần 5": { "text": "Tuần 5", "status": "Success", "color": "#ec3b3b", "isText": true }
                    },
                    formItemProps: {
                      disabled: true
                    }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['amount']}
                name="amount"
              >
                <FormInputRender
                  item={{ "title": "Số lượng", "dataIndex": "amount", "width": 200 }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['projectId']}
                name="projectId"
              >
                <ProjectSelect disabled={true} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['group']}
                name="group"
              >
                <FormInputRender
                  item={{
                    "title": "Nhóm", "dataIndex": "group", "width": 120, formItemProps: {
                      disabled: true
                    }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            {/* <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['isActive']}
                name="isActive"
              >
                <FormInputRender
                  item={{ "title": "Trạng thái", "dataIndex": "isActive", "width": 120 }}
                  intl={intl}
                />
              </Form.Item>
            </Col> */}
          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack(); }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ saleTarget, loading, router }) => ({
  submitting: loading.effects['saleTarget/submit'],
  saleTarget,
  router
}))(SaleTargetForm);
