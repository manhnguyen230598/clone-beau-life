import React, { FC, useCallback, memo } from 'react';
import { Modal, Form } from 'antd';
import { connect } from 'dva';
import dayjs from 'dayjs';
import { DatePicker } from '../../../../packages/pro-component/index';
// import { FormInputRender } from '../../../../packages/pro-table/form';
// import { useIntl } from '../../../../packages/pro-table/component/intlContext';

interface SaleTargetCreateMultiProp {
  projectId: number
  createMultiVisible: boolean
  setCreateMultiVisible: (visible: boolean) => void
  dispatch: any
}

const SaleTargetCreateMultiComp: FC<SaleTargetCreateMultiProp> = (props: SaleTargetCreateMultiProp) => {
  const {
    dispatch,
    projectId,
    createMultiVisible,
    setCreateMultiVisible
  } = props;
  const [form] = Form.useForm();
  // const intl = useIntl();

  const handleCancel = useCallback(() => {
    setCreateMultiVisible(false);
  }, [setCreateMultiVisible]);

  const onFinish = useCallback((values) => {
    const params = {
      year: dayjs(values.yearmonth).year(),
      month: dayjs(values.yearmonth).month() + 1,
      projectId
    };

    console.log(`🚀 ~ file: SaleTargetCreateMulti.tsx ~ line 32 ~ onFinish ~ params`, params);
    dispatch({
      type: "saleTarget/createMulti",
      payload: params,
      callback: (res: any) => {
        console.log(`🚀 ~ file: SaleTargetCreateMulti.tsx ~ line 39 ~ onFinish ~ res`, res);
      }
    });
  }, [projectId]);

  const onFinishFailed = useCallback((errors) => {
    console.log(`🚀 ~ file: SaleTargetCreateMulti.tsx ~ line 33 ~ onFinishFailed ~ errors`, errors);
  }, []);

  return (
    <React.Fragment>
      <Modal
        title="Thêm mới chỉ tiêu"
        visible={createMultiVisible}
        onCancel={handleCancel}
        onOk={() => form.submit()}
      >
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label={"Tháng và năm"}
            name="yearmonth"
            rules={[
              { required: true, message: 'Vui lòng chọn tháng và năm' }
            ]}
          >
            <DatePicker picker="month" />
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};

const SaleTargetCreateMulti: FC<SaleTargetCreateMultiProp> = memo<any>(connect(null)(SaleTargetCreateMultiComp));

export default SaleTargetCreateMulti;
