import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Input,
  Button,
  Space,
  Tooltip,
  Modal,
  Card /* , Avatar */,
  message
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  RadarChartOutlined /* , EyeOutlined */
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { /* get,  */ cloneDeep, union } from "lodash";
import SaleTargetDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import SaleTargetCreateMulti from "../crud/SaleTargetCreateMulti";
import ReportSaleTarget from "../report/ReportSaleTarget";
import dayjs from "dayjs";
import { DatePicker } from "../../../../packages/pro-component/index";
import * as saleTargetServices from "../../../../services/saleTarget";
import moment from "moment";

var d = new Date();
var currentMonth = d.getMonth() + 1;
var currentYear = d.getFullYear();

const tranformSaleTargetParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "saleTarget";
const SaleTargetList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState({
    id: { show: false },
    createdAt: { show: true },
    updatedAt: { show: false }
  });
  const [createMultiVisible, setCreateMultiVisible] = useState(false);
  const [reportVisible, setReportVisible] = useState(false);

  const {
    dispatch,
    [RESOURCE]: { data, report },
    loading,
    history,
    projectId,
    projectName
  } = props;

  const [dateMonth, setDateMonth] = useState(currentMonth);
  const [dateYear, setDateYear] = useState(currentYear);

  const fetchData = (month, year) => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformSaleTargetParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/report3Months`,
      payload: {
        // page: tableChange?.pagination?.current || current,
        // current: tableChange?.pagination?.current || current,
        // skip: (defaultCurrent - 1) * pagi.pageSize,
        // limit: 100,
        // pageSize: pagi.pageSize || limit,
        // sort: initParams.sort ? (initParams.sort.length <= 0 ? [{ "createdAt": "desc" }] : initParams.sort) : [{ "createdAt": "desc" }],
        // queryInput: {
        projectId,
        month: month,
        year: year
        // }
      }
    });
  };
  useEffect(() => {
    fetchData(dateMonth, dateYear);
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformSaleTargetParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        projectId
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", false);
          values = {};
        }
        const initParams = tranformSaleTargetParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(fieldsValue);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters,
              projectId
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    // history.push({ pathname: `${props.match.url}/add` });
    setCreateMultiVisible(true);
  };

  const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  };

  const handleReportClick = () => {
    setReportVisible(true);
  };

  const initCols = [
    {
      title: "Thời gian",
      dataIndex: "group",
      key: "group",
      width: 100,
      // hideInSearch: false,
      // hideInTable: false,
      // fixed: 'left',
      render: (value, row, index) => {
        const obj = {
          children: value,
          props: {}
        };
        if (index == 0 || index == 2) {
          obj.props.rowSpan = 2;
        }
        if (index == 1 || index == 3) {
          obj.props.rowSpan = 0;
        }
        if (index == 4) {
          obj.props.rowSpan = 3;
        }
        if (index == 5 || index == 6) {
          obj.props.rowSpan = 0;
        }
        if (index == 7) {
          obj.props.rowSpan = 4;
        }
        if (index == 8 || index == 9 || index == 10) {
          obj.props.rowSpan = 0;
        }
        return obj;
      }
    },
    {
      title: "Tháng",
      children: [
        {
          title: "Tuần",
          dataIndex: "name",
          key: "name",
          width: 200
        }
      ]
    }

    // {
    //   title: 'test',
    //   children: [
    //     {
    //       title: 'Tuần 1',
    //       dataIndex: `2021_7_Tuần 1`,
    //       key: `2021_7_Tuần 1`,
    //       width: 200,
    //       render: (value, idx) => {
    //         console.log("$$value", value)
    //         return(
    //           <>
    //           <span style={{color:"red"}}>{value}</span>
    //           </>
    //         );
    //       }
    //     },
    //   ]
    // },
  ];

  const [isShowModal, setIsShowModal] = useState(false);
  const [objValueTarGet, setObjValueTaget] = useState({});
  const renderColumn = (value, record) => {
    // console.log("$$ value",value)
    // console.log("$$ record",record)
    return (
      <>
        <a
          onClick={() => {
            console.log("$$ record", record);
            console.log("$$ value", value);
            setObjValueTaget({
              name: record.key,
              id: value.id,
              week: value.week,
              month: value.month,
              year: value.year,
              amount: value.amount
              // week= record.w
            });
            setIsShowModal(true);
          }}
        >
          {value.amount !== undefined ? value.amount : "-"}
        </a>
      </>
    );
  };

  //bảng updete column
  const [columns, setColumns] = useState(initCols);

  useEffect(() => {
    if (report && report.columns && report.columns.length) {
      let tempColumns = [];
      report.columns.map(item => {
        let arr = [];
        item.children.map(child => {
          child = { ...child, render: renderColumn };
          arr.push(child);
        });
        tempColumns.push({ ...item, ["children"]: [...arr] });
      });
      setColumns([...initCols, ...tempColumns]);
    }
  }, [report]);

  const seeReport = () => {
    form
      .validateFields()
      .then(values => {
        const params = {
          queryInput: {
            projectId,
            year: dayjs(values.yearmonth).year(),
            month: dayjs(values.yearmonth).month() + 1
          }
        };
        fetchData(params.queryInput.month, params.queryInput.year);
        setDateMonth(params.queryInput.month);
        setDateYear(params.queryInput.year);
      })
      .catch(err => {
        console.log(
          `🚀 ~ file: ReportSaleTarget.tsx ~ line 45 ~ seeReport ~ err`,
          err
        );
      });
  };

  const submitModal = () => {
    saleTargetServices
      .update(objValueTarGet.id, {
        amount: objValueTarGet.amount
      })
      .then(response => {
        if (response.status === 200) {
          message.success("Cập nhật thành công");
          fetchData(dateMonth, dateYear);
          setIsShowModal(false);

          return;
        }
        message.error("Cập nhật thất bại");
      });
  };
  const monthFormat = "YYYY/MM";
  return (
    <>
      <Modal
        visible={isShowModal}
        closable={true}
        onOk={submitModal}
        onCancel={() => setIsShowModal(false)}
      >
        <Card
          title={`Điều chỉnh ${objValueTarGet.name ||
            ""} ${objValueTarGet.week || ""} tháng ${objValueTarGet.month ||
            ""} năm ${objValueTarGet.year || ""}`}
        >
          <Input
            value={objValueTarGet.amount}
            addonBefore={<span>Giá trị</span>}
            onChange={e => {
              setObjValueTaget(origin => {
                return { ...origin, ["amount"]: +e.target.value };
              });
            }}
          />
        </Card>
      </Modal>
      <Form form={form}>
        <div style={{ display: "flex", alignItems: "center" }}>
          <Form.Item
            label={"Tháng và năm"}
            name="yearmonth"
            rules={[{ required: true, message: "Vui lòng chọn tháng và năm" }]}
          >
            {/* <DatePicker picker="month" value={moment(`${currentYear}/${currentMonth}`, monthFormat)} format={monthFormat}/> */}
            <DatePicker
              value={moment(`${currentYear}/${currentMonth}`, monthFormat)}
              format={monthFormat}
              picker="month"
            />
          </Form.Item>
          <Form.Item>
            <Tooltip title="Bảng chỉ tiêu sẽ lọc theo 3 tháng gần nhất trở lại từ tháng được lọc">
              <Button
                style={{ marginLeft: "10px" }}
                type="primary"
                onClick={seeReport}
              >
                Lọc
              </Button>
            </Tooltip>
          </Form.Item>
        </div>
      </Form>
      <div className="nv-saletarget-report-table">
        <ProTable
          tableClassName="gx-table-responsive"
          type="table"
          // rowKey="id"
          bordered
          search={false}
          headerTitle={`Chỉ tiêu dự án ${projectName}`}
          actionRef={actionRef}
          // formRef={form}
          // form={{ initialValues: sessionStorage.getItem('isReset') == 'true' ? {} : formValues, preserve: false }}
          dataSource={report.data}
          pagination={false}
          columns={columns}
          // columnsStateMap={columnsStateMap}
          // onColumnsStateChange={(mapCols) => {
          //   setColumnsStateMap(mapCols);
          // }}
          loading={loading}
          // scroll={{ x: 1500, y: 300 }}
          onChange={handleTableChange}
          onSubmit={handleTableSearch}
          beforeSearchSubmit={searchParams => {
            return {};
          }}
          onReset={() => {
            sessionStorage.setItem("isReset", "true");
            setTableChange(RESOURCE, {
              pagination: {
                current: 1,
                skip: 0,
                limit
              },
              filtersArg: {},
              sorter: {},
              formValues: {}
            });
          }}
          toolBarRender={(action, { selectedRows }) => [
            <Button
              icon={<PlusOutlined />}
              type="primary"
              onClick={() => handleAddClick()}
            >
              {`Thêm mới`}
            </Button>,
            <Button icon={<RadarChartOutlined />} type="text" onClick={() => handleReportClick()}>
              {`Báo cáo`}
            </Button>
          ]}
        />
      </div>

      <SaleTargetDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
      <SaleTargetCreateMulti
        projectId={projectId}
        createMultiVisible={createMultiVisible}
        setCreateMultiVisible={setCreateMultiVisible}
      />
      <ReportSaleTarget
        projectId={projectId}
        reportVisible={reportVisible}
        setReportVisible={setReportVisible}
        projectName={projectName}
      />
    </>
  );
};

export default connect(({ saleTarget, loading }) => ({
  saleTarget,
  loading: loading.models.saleTarget
}))(SaleTargetList);
