import React, { FC, useCallback, memo, useState } from 'react';
import { Modal, Form, Button } from 'antd';
import { connect } from 'dva';
import dayjs from 'dayjs';
import { DatePicker } from '../../../../packages/pro-component/index';
import ProTable from '../../../../packages/pro-table/Table';
// import type { ProColumnType } from '../../../../packages/pro-table/Table';
import type { ProColumns } from '../../../../packages/pro-table/Table';
import get from 'lodash/get';
import pick from 'lodash/pick';
import groupBy from 'lodash/groupBy';
import forIn from 'lodash/forIn';
import * as saleTargetServices from 'src/services/saleTarget';
interface SaleTarget {
  id: number
  createdAt: number
  updatedAt: number
  month: number | string
  year: number | string
  week: string
  amount: number
  group: string
  name: string
  projectId: number
};

interface ISaleTargetReport {
  data: SaleTarget[],
  columns: ProColumns<any>[]
};

interface ReportSaleTargetProp {
  projectId: number
  reportVisible: boolean
  setReportVisible: (visible: boolean) => void
  dispatch: any
  report: ISaleTargetReport
  projectName : any
}

const ReportSaleTargetComp: FC<ReportSaleTargetProp> = (props: ReportSaleTargetProp) => {
  
  const {
    projectId,
    reportVisible,
    setReportVisible,
    projectName
  } = props;
  const [form] = Form.useForm();
  const [data , setData] = useState([]);

  const transformReport = (data: any) => {
    const dataGroup = groupBy(data, function (item) {
      return item.name;
    });
    let datafinal : any = [];
    forIn(dataGroup, function (value, key) {
      const chitieuobj = value.reduce((acc, cur) => {
        const dataobj = pick(cur, ["group", "name","week","month","year"]);
        return {
          ...acc,
          ...dataobj,
          [`${cur.year}_${cur.month}_${cur.week}`]:{week:cur.week,month:cur.month,year:cur.year,id:cur.id,amount:cur.amount}
        };
      }, {});
      datafinal.push({
        key: key,
        ...chitieuobj
      });
    });
  
    // const dataYm = groupBy(data, function (item) {
    //   return {yearmonth :`${item.month}/${item.year}`, time : dayjs(`01/${item.month}/${item.year}`,"DD/MM/YYYY").valueOf()};
    // });
    const dataYm = groupBy(data, function (item) {
        return `${item.month}/${item.year}`;
      });
     let keysSorted = Object.keys(dataYm).sort((a:String,b:String)=>{
       let temp1 = dayjs(`01/${a}`,"DD/MM/YYYY")
       let temp2 = dayjs(`01/${b}`,"DD/MM/YYYY")
      return temp1.isAfter(temp2) ? 1 : -1
      }
       )
    let temp = {};
    keysSorted.map((e:any)=>{
      temp[e] = dataYm[e]
    })
    let dataCol : any = [];
    forIn(temp, function (value, key) {
      console.log(key)
      const colobj = groupBy(value, (item: any) => {
        return `${item.year}_${item.month}_${item.week}`;
      });
      let colChild : any = [];
      forIn(colobj, function (value, key) {
        colChild.push({
          title: key.split('_')[key.split('_').length - 1],
          key: key,
          dataIndex: key
        });
      });
      dataCol.push({
        title: `Tháng ${key}`,
        children: colChild
      });
    });
    console.log(datafinal, dataCol)
    return {
      data: datafinal,
      columns: dataCol
    };
  };
  const initCols: ProColumns<any>[] = [
    {
      title: 'Thời gian',
      dataIndex: 'group',
      key: 'group',
      width: 100,
      // fixed: 'left',
      render: (value, row, index) => {
        const obj: any = {
          children: value,
          props: {},
        };
//1 3 4 6 8 9 10
        if(index == 1 || index == 3 || index == 4 || index == 6 || index == 7 || index == 9 || index == 10 || index == 11){
          obj.props.rowSpan = 0;
        };
        if(index == 0){
          obj.props.rowSpan = 2;
        }

        if(index == 2) {
          obj.props.rowSpan = 3;
        }
        
        if(index == 5){
          obj.props.rowSpan = 3;
        }
        if(index == 8){
          obj.props.rowSpan = 4;
        }
        

        
        // if(index == 4) {
        //   obj.props.rowSpan = 3;
        // }
        // if(index == 5 || index == 6) {
        //   obj.props.rowSpan = 0;
        // }
        // if(index == 7) {
        //   obj.props.rowSpan = 4;
        // }
        // if(index == 8 || index == 9 || index == 10) {
        //   obj.props.rowSpan = 0;
        // }
        return obj;
      }
    },
    {
      title: 'Tháng',
      children: [
        {
          title: 'Tuần',
          dataIndex: 'name',
          key: 'name',
          width: 200,
        },
      ]
    },
  ];
  const [columns, setColumns] = useState<ProColumns<any>[]>(initCols);


  const handleCancel = useCallback(() => {
    setReportVisible(false);
  }, [setReportVisible]);
  const renderColumn = (value:any) => {
    // console.log("$$ value",value)
    // console.log("$$ record",record)
    return (
      <>
        <span

        >
          {value.amount !== undefined ? value.amount : "-"}
        </span>
      </>
    );
  };
  const seeReport = () => {
    form.validateFields()
      .then( async (values: any) => {
        console.log(values)
        const params = {
          startDate : dayjs(values.yearmonth[0]).startOf("day").valueOf(),
          endDate :dayjs(values.yearmonth[1]).endOf("day").valueOf(),
          projectId
        };
        let rs = await saleTargetServices.getReportRangeData(params);
        if(rs && rs.data.code === 0){
          let list = get(rs,"data.data",[]);
          let listData = transformReport(list);
          setData(listData.data);
          let tempColumns: any = [];
          listData.columns.map((item:any) => {
        let arr: any = [];
        item.children.map((child:any) => {
          child = { ...child, render: renderColumn };
          arr.push(child);
        });
        tempColumns.push({ ...item, ["children"]: [...arr] });
      });
      setColumns([...initCols, ...tempColumns]);
        }
      })
      .catch(err => {
        console.log(`🚀 ~ file: ReportSaleTarget.tsx ~ line 45 ~ seeReport ~ err`, err);

      });
  };
  const exportData = ()=>{
    form.validateFields()
    .then( async (values: any)=>{
      window.open(
        `${process.env.REACT_APP_URL}/api/admin/saletarget/export?projectId=${projectId}&startDate=${dayjs(
          values.yearmonth[0]
        ).valueOf()}&endDate=${dayjs(
          values.yearmonth[1]
        ).valueOf()}&accesstoken=${localStorage.getItem("token")}`
      );
    })
    .catch(err => {
      console.log(`🚀 ~ file: ReportSaleTarget.tsx ~ line 45 ~ seeReport ~ err`, err);

    });
  }
  return (
    <React.Fragment>
      <Modal
        title={"Báo cáo chỉ tiêu dự án " + projectName}
        visible={reportVisible}
        onCancel={handleCancel}
        footer={null}
        width={`70vw`}
      // destroyOnClose={true}
      >
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          <Form.Item
            label={"Khoảng thời gian"}
            name="yearmonth"
            rules={[
              { required: true, message: 'Vui lòng chọn khoảng thời gian' }
            ]}
          >
            <DatePicker.RangePicker />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" onClick={seeReport}>Xem báo cáo</Button>
            <Button type="default" onClick={exportData} disabled={data?.length === 0}>Xuất báo cáo</Button>
          </Form.Item>
        </Form>
        {data?.length > 0 ?   <div className="nv-saletarget-report-table">
          <ProTable
            tableClassName="gx-table-responsive"
            params={{}}
            search={false}
            headerTitle={`Báo cáo chỉ tiêu`}
            rowKey="id"
            toolBarRender={false}
            pagination={false}
            dataSource={data}
            columns={columns}
            dateFormatter="string"
            type="table"
            bordered
          // components={{
          //   id: 'saleTargetReport'
          // } as any}
          />
        </div> : null}
      
      </Modal>
    </React.Fragment>
  );
};

const mapToProps = (state: any) => {
  const { saleTarget: { report } } = state;
  return { report };
};

const ReportSaleTarget: FC<ReportSaleTargetProp> = memo<any>(connect(mapToProps)(ReportSaleTargetComp));

export default ReportSaleTarget;
