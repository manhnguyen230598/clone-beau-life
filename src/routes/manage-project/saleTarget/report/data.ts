import type { ProColumns } from '../../../../packages/pro-table/Table';

export const columns: ProColumns<any>[] = [
  {
    title: 'Thời gian',
    dataIndex: 'group',
    key: 'group',
    width: 100,
    fixed: 'left',
    render: (value, row, index) => {
      const obj: any = {
        children: value,
        props: {},
      };
      if(index == 0 || index == 2) {
        obj.props.rowSpan = 2;
      }
      if(index === 1 || index == 3) {
        obj.props.rowSpan = 0;
      }
      return obj;
    }
  },
  {
    title: 'Tháng',
    children: [
      {
        title: 'Tuần',
        dataIndex: 'name',
        key: 'name',
        width: 150,
      },
    ]
  },
  {
    title: 'Tháng 3/2012',
    children: [
      {
        title: '1',
        dataIndex: 'week1_month3_year2021',
        key: 'week1_month3_year2021',
        width: 50,
      },
      {
        title: '2',
        dataIndex: 'week2_month3_year2021',
        key: 'week2_month3_year2021',
        width: 50,
      },
      {
        title: '3',
        dataIndex: 'week3_month3_year2021',
        key: 'week3_month3_year2021',
        width: 50,
      },
      {
        title: '4',
        dataIndex: 'week4_month3_year2021',
        key: 'week4_month3_year2021',
        width: 50,
      },
    ]
  },
  {
    title: 'Tháng 4/2012',
    children: [
      {
        title: '1',
        dataIndex: 'week1_month4_year2021',
        key: 'week1_month4_year2021',
        width: 50,
      },
      {
        title: '2',
        dataIndex: 'week2_month4_year2021',
        key: 'week2_month4_year2021',
        width: 50,
      },
      {
        title: '3',
        dataIndex: 'week3_month4_year2021',
        key: 'week3_month4_year2021',
        width: 50,
      },
      {
        title: '4',
        dataIndex: 'week4_month4_year2021',
        key: 'week4_month4_year2021',
        width: 50,
      },
    ]
  },
  {
    title: 'Tháng 5/2012',
    children: [
      {
        title: '1',
        dataIndex: 'week1_month5_year2021',
        key: 'week1_month5_year2021',
        width: 50,
      },
      {
        title: '2',
        dataIndex: 'week2_month5_year2021',
        key: 'week2_month5_year2021',
        width: 50,
      },
      {
        title: '3',
        dataIndex: 'week3_month5_year2021',
        key: 'week3_month5_year2021',
        width: 50,
      },
      {
        title: '4',
        dataIndex: 'week4_month5_year2021',
        key: 'week4_month5_year2021',
        width: 50,
      },
    ]
  }
];
