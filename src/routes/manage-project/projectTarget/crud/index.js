import React, { useEffect, useState } from "react";
import { Card, Button, Form, Col, Row, Popover, Space, Input, Select } from "antd";
import _ from "lodash";
import {
  CloseCircleOutlined,
  MinusCircleOutlined,
  PlusOutlined
} from "@ant-design/icons";
import { connect, routerRedux } from "dva";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import FooterToolbar from "packages/FooterToolbar";
import UploadFile from "components/UploadFile";
import Upload from "components/Upload";
import * as enums from "util/enums";
import { camelCaseToDash } from "util/helpers";
import provinces from "./provinces.json"
const RESOURCE = "projectTarget";
const fieldLabels = {
  name: "Tên tài liệu",
  type: "Loại tài liệu",
  link: "Link",
  thumbnail: "Ảnh bìa",
  projectId: "Dự án",
  sequence: "Thứ tự",
  isActive: "Trạng thái",
  shortDescription: "Mô tả ngắn"
};
const DocumentProjectForm = ({
  projectTarget: { formTitle, formData },
  dispatch,
  submitting,
  match: { params },
  history,
  ...rest
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState("100%");
  const getErrorInfo = errors => {
    const errorCount = errors.filter(item => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = fieldKey => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map(err => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li
          key={key}
          className="nv-error-list-item"
          onClick={() => scrollToField(key)}
        >
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={trigger => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = values => {
    setError([]);
    const data = _.cloneDeep(values);
    
    console.log(data)
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: res => {
        if (res && !res.error) {
        //   history.goBack();
        }
      }
    });
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  }, []);

  useEffect(() => {
    console.log(formData);
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll(".ant-layout-sider")[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    }
    window.addEventListener("resize", resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener("resize", resizeFooterToolbar);
    };
  });
  const listAge = ["Từ 25-35", "Từ 35-45", "Từ 45-55", "Trên 55"];
  const switchAge = ()=>{
      return <Select disabled={!form.getFieldValue('area')} style={{ width: 130 }}>
      {listAge.map(item => (
        <Select.Option key={item} value={item}>
          {item}
        </Select.Option>
      ))}
    </Select>
  }
  const switchArea = ()=>{
    return <Select style={{ width: 130 }}>
    {provinces.map(item => (
      <Select.Option key={item} value={item.name}>
        {item.name}
      </Select.Option>
    ))}
  </Select>
}
  if (
    params.id === "add" ||
    (formData && formData.id && formData.id !== "add")
  ) {
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin cơ bản" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item label={"Định hướng"} name="type">
                <FormInputRender
                  disabled
                  item={{
                    title: "Trạng thái",
                    dataIndex: "isActive",
                    width: 200,
                    filters: true,
                    hasFilter: false,
                    hideInTable: false,
                    hideInSearch: false,
                    valueEnum: {
                      area: {
                        text: "Khu vực",
                        // status: "Processing",
                        // color: "#ec3b3b",
                        isText: true
                      },
                      age: {
                        text: "Độ tuổi",
                        // status: "Default",
                        // color: "#ec3b3b",
                        isText: true
                      }
                    },
                    formPattern: { card: "Thông tin khác", row: 2, col: 1 }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          {/* <Row> */}
          <p>Dữ liệu</p>
          <div style={{width : "40%"}}>
            <Form.List name="data">
              {(fields, { add, remove }) => (
                <>
                  {fields.map(({ key, name, fieldKey, ...restField }) => (
                    <Space
                      key={key}
                      style={{ display: "flex", marginBottom: 8 }}
                      align="baseline"
                    >
                      <Form.Item
                        {...restField}
                        name={[name, "name"]}
                        fieldKey={[fieldKey, "first"]}
                        rules={[
                          { required: true, message: "Thiếu tiêu đề" }
                        ]}
                      >
                        {formData?.type === "age" ? switchAge() : switchArea()}
                      </Form.Item>
                      <Form.Item
                        {...restField}
                        name={[name, "value"]}
                        fieldKey={[fieldKey, "last"]}
                        rules={[
                          { required: true, message: "Thiếu số lượng" }
                        ]}
                      >
                        <Input placeholder="Số lượng" />
                      </Form.Item>
                      <MinusCircleOutlined onClick={() => remove(name)} />
                    </Space>
                  ))}
                  <Form.Item>
                    <Button
                      type="dashed"
                      onClick={() => add()}
                      block
                      icon={<PlusOutlined />}
                      disabled={formData?.type === "age"}
                    >
                      Thêm dữ liệu
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </div>

          {/* </Row> */}
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={submitting}
          >
            {params.id === "add" ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button
            type="default"
            style={{ color: "#fa5656" }}
            onClick={() => {
              history.goBack();
            }}
          >
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ projectTarget, loading, router }) => ({
  submitting: loading.effects["projectTarget/submit"],
  projectTarget,
  router
}))(DocumentProjectForm);
