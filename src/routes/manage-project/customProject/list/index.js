/* eslint-disable array-callback-return */
import React, { useCallback, useEffect, useState } from 'react';
import { ComposedChart, Line, Bar, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { BarChart } from 'recharts';
import { PieChart, Pie, Sector, Cell } from 'recharts';
import { Row } from 'antd';
import { Col } from "antd";
import Widget from 'src/components/Widget';
import * as project from 'src/services/project';

var d = new Date();
var currentMonth = d.getMonth()+1;
var currentYear = d.getFullYear();

const ReportVisitingCustomer = (props) => {

  const [dataChart, setDataChart] = useState([]);
  const [month, setMonth] = useState(currentMonth)
  const [year, setYear] = useState(currentYear)

  const getData = async () => {
    let api = await project.customProject(props.projectId , month , year)
    setDataChart(api.data)
  }


  useEffect(() => {
    getData();
  }, [month, year])

  const renderTick = (props) => {
    const { x, y, index } = props;
    if (index === 8) {

      return (<><path d={`M${x - 40.5},${y - 38}v${-150}`} stroke="black" />
        <path d={`M${x + 40.5},${y - 38}v${-150}`} stroke="black" /></>);
    } else {
      return (<path d={`M${x - 40.5},${y - 38}v${-150}`} stroke="black" />);
    }
  };

  const renderCustomizedLabel = (props) => {

    const { x, y, stroke, value } = props;
    return (
      <text x={x} y={y} dy={-10} fill={stroke} fontSize={15} textAnchor="middle">
        {value}%
      </text>
    );
  };
  const style = {

    left: 60,
    bottom: 79,
    lineHeight: 2,
    height: 122,
    border: "2px solid #a6a6a6",
    paddingTop: 4,
    paddingLeft: 5,
    borderRight: "none"
  };
  //---------------------------
  return (
    <>
    <Widget styleName="gx-card-full is-build-chartclass">
      <div className="nv-group-calender">
        <div className="nv-select-month">
          <select name="cars" defaultValue={currentMonth} onChange={(e) => setMonth(e.target.value)}>
            <option value="1">Tháng 1</option>
            <option value="2">Tháng 2</option>
            <option value="3">Tháng 3</option>
            <option value="4">Tháng 4</option>
            <option value="5">Tháng 5</option>
            <option value="6">Tháng 6</option>
            <option value="7">Tháng 7</option>
            <option value="8">Tháng 8</option>
            <option value="9">Tháng 9</option>
            <option value="10">Tháng 10</option>
            <option value="11">Tháng 11</option>
            <option value="12">Tháng 12</option>
          </select>
        </div>
        <div className="nv-select-month">
          <select name="cars" defaultValue={currentYear} onChange={(e) => setYear(e.target.value)}>
            <option value="2018">Năm 2018</option>
            <option value="2019">Năm 2019</option>
            <option value="2020">Năm 2020</option>
            <option value="2021">Năm 2021</option>
          </select>
        </div>
      </div>
      <ResponsiveContainer width={980} height={500}>
        <ComposedChart
          width={900}
          height={600}
          //  data={data}
          data={dataChart? dataChart.data : null}
          margin={{
            top: 100,
            right: 20,
            bottom: 20,
            left: 0,
          }}
        >
          <YAxis ticks={[0, 10, 20, 30, 40, 50]} domain={[0, 50]} />
          <YAxis
            yAxisId="right"
            type="number"
            dataKey="PCV"
            name="pcv"
            unit="%"
            orientation="right"
            stroke="#82ca9d"
            ticks={[0, 2, 4, 6, 8, 10, 12, 14, 16]}
            domain={[0, 16]}
          />
          <CartesianGrid stroke="#f5f5f5" />
          <Tooltip />
          <Legend
            align="left"
            layout="vertical"
            wrapperStyle={style}
          />
          <XAxis
            dataKey="name"
            scale="auto"
            tickLine={false}
            interval={0}
          />
          <XAxis
            dataKey="VPBH"
            axisLine={true}
            tickLine={false}
            // tickFormatter={monthTickFormatter}
            height={30}
            scale="auto"
            xAxisId="quarter1"
          />
          <XAxis
            dataKey="TT"
            axisLine={true}
            tickLine={false}
            // tickFormatter={monthTickFormatter}
            height={30}
            scale="auto"
            xAxisId="quarter2"
          />
          <XAxis
            dataKey="BM"
            axisLine={true}
            tickLine={false}
            interval={0}
            height={30}
            scale="auto"
            xAxisId="quarter3"
          />
          <XAxis
            dataKey="PCV"
            axisLine={true}
            tickLine={false}
            interval={0}
            height={30}
            scale="auto"
            xAxisId="quarter4"
          />
          <XAxis
            dataKey=""
            height={30}
            xAxisId="x5"
            tickLine={false}
            tick={false} />
          <XAxis
            dataKey=""
            height={30}
            xAxisId="x6"
            tickLine={false}
            tick={renderTick}
            
            axisLine={false} />
          <Bar dataKey="VPBH" barSize={20} fill="#413ea0" name="KH đến VPBH" />
          <Bar dataKey="TT" barSize={20} fill="#e19129" name="KH truyền thông"/>
          <Bar dataKey="BM" barSize={20} fill="#808080" name="Căn bán mới"/>
          {/* <Line dataKey="PCV" yAxisId="right" stroke="#ff7300" dot={true} /> */}
          <Line dataKey="PCV" yAxisId="right" stroke="#ff7300" name="Tỷ lệ convert" dot={false} label={renderCustomizedLabel} />
        </ComposedChart>
      </ResponsiveContainer>
    </Widget>
  </>
  );
};

export default ReportVisitingCustomer;
