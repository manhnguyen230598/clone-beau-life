import React from "react";
import { router } from 'dva';
import asyncComponent from "util/asyncComponent";
/* PLOP_INJECT_IMPORT */

const { Route, Switch } = router;


const Project = ({ match }) => (
  <Switch>
    {/* PLOP_INJECT_EXPORT */}
<Route path={`${match.url}/dealerTarget/:id`} component={asyncComponent(() => import('./dealerTarget/crud'))} />
<Route path={`${match.url}/dealerTarget`} component={asyncComponent(() => import('./dealerTarget/list'))} exact={true} />
<Route path={`${match.url}/projectVisitingCustomer/:id`} component={asyncComponent(() => import('./projectVisitingCustomer/crud'))} />
<Route path={`${match.url}/projectMediaCustomer/:id`} component={asyncComponent(() => import('./projectMediaCustomer/crud'))} />


    <Route path={`${match.url}/reportFund`} component={asyncComponent(() => import('./reportFund/list'))} exact={true} />
    <Route path={`${match.url}/bookRequestPending`} component={asyncComponent(() => import('./bookRequestPending/list'))} exact={true} />
    <Route path={`${match.url}/customer`} component={asyncComponent(() => import('./customer/list'))} exact={true} />
    <Route path={`${match.url}/apartment`} component={asyncComponent(() => import('./apartment/list'))} exact={true} />
    {/* <Route path={`${match.url}/documentproject`} component={asyncComponent(() => import("./documentProject/list"))} exact={true} />
    <Route path={`${match.url}/documentproject/:id`} component={asyncComponent(() => import("./documentProject/crud"))} exact={true} /> */}
    <Route path={`${match.url}/project`} component={asyncComponent(() => import("./project/list"))} exact={true} />
    <Route path={`${match.url}/project/:id`} component={asyncComponent(() => import("./project/crud"))} />
    <Route path={`${match.url}/project-detail/:id`} component={asyncComponent(() => import("./project/detail/detail"))} />
    <Route path={`${match.url}/videoproject`} component={asyncComponent(() => import("./videoProject/list"))} exact={true} />
    <Route path={`${match.url}/videoproject/:id`} component={asyncComponent(() => import("./videoProject/crud"))} />
    <Route path={`${match.url}/building`} component={asyncComponent(() => import("./building/list"))} exact={true} />
    <Route path={`${match.url}/building/:id`} component={asyncComponent(() => import("./building/crud"))} />
  </Switch>

);

export default Project;
