import React, { useEffect, useState, useMemo } from "react";
import { Card, Button, Form, Col, Row, Popover, Input, Select } from "antd";
import _ from "lodash";
import { CloseCircleOutlined, CompassOutlined } from "@ant-design/icons";
import { connect, routerRedux } from "dva";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import FooterToolbar from "packages/FooterToolbar";
import * as enums from "util/enums";
import { camelCaseToDash } from "util/helpers";
import ListProjectSelect from "src/components/Select/Project/ListProject";
import ListAgencySelect from "src/components/Select/Agency/ListAgencyName";
import ProvincesSelectName from "../../../../components/Select/Provinces/SelectName";

const { TextArea } = Input;
const { Option } = Select;
const RESOURCE = "projectMediaCustomer";
const fieldLabels = {
  name: "Tên khách hàng",
  facebook: "Facebook",
  address: "Địa chỉ",
  phone: "Số điện thoại",
  email: "Địa chỉ email",
  comments: "Phản hồi KH trên kênh truyền thông",
  productTypeCare: "Sản phẩm quan tâm",
  state: "Trạng thái KH",
  groupType: "Phân nhóm phản hồi của KH",
  customerReply: "Chi tiết phản hồi",
  channelReply: "Kênh phản hồi",
  purpose: "Mục đích",
  consultantInCharge: "Chuyên viên tư vấn phụ trách",
  careAgent: "Đại lý chăm sóc",
  provinceName: "Khu vực sinh sống"
};
const ProjectMediaCustomerForm = ({
  projectMediaCustomer: { formTitle, formData },
  projectId,
  dispatch,
  submitting,
  match: { params },
  location,
  history,
  ...rest
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState("100%");
  const getErrorInfo = errors => {
    const errorCount = errors.filter(item => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = fieldKey => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map(err => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li
          key={key}
          className="nv-error-list-item"
          onClick={() => scrollToField(key)}
        >
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={trigger => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };
  const onFinish = values => {
    setError([]);
    const data = _.cloneDeep(values);
    data.incomeDate = Date.now();
    data.projectId = projectId;
    if (data.careAgent && typeof data.careAgent == "object") {
      data.careAgent = data.careAgent.name;
    }
    if (data.provinceName && typeof data.provinceName == "object") {
      data.provinceName = data.provinceName.label;
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: res => {
        if (res && !res.error) {
          // history.goBack();
        }
      }
    });
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll(".ant-layout-sider")[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    }
    window.addEventListener("resize", resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener("resize", resizeFooterToolbar);
    };
  });

  const layout = {
    labelCol: { span: 12 },
    wrapperCol: { span: 16 }
  };

  if (
    params.id === "add" ||
    (formData && formData.id && formData.id !== "add")
  ) {
    return (
      <Form
        {...layout}
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title={"Thông tin khách hàng"}>
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["channelReply"]}
                name="channelReply"
              >
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["name"]} name="name">
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["email"]} name="email">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["phone"]} name="phone">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>

            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["facebook"]} name="facebook">
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["comments"]} name="comments">
                <TextArea intl={intl} />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Đại lý">
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["consultantInCharge"]}
                name="consultantInCharge"
              >
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["careAgent"]} name="careAgent">
                <ListAgencySelect />
              </Form.Item>
            </Col>
          </Row>
        </Card>

        <Card title={"Nhu cầu khách hàng"}>
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["address"]} name="address">
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["provinceName"]}
                name="provinceName"
              >
                <ProvincesSelectName mode="simple" />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["customerReply"]}
                name="customerReply"
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["groupType"]} name="groupType">
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      "Pháp lý": { text: "Pháp lý" },
                      "Thiết kế sản phẩm": { text: "Thiết kế sản phẩm" },
                      "Tiến độ xây dựng": { text: "Tiến độ xây dựng" },
                      "Giá bán": { text: "Giá bán" },
                      "Tiến độ thanh toán": { text: "Tiến độ thanh toán" },
                      "Chính sách bán hàng": { text: "Chăm sóc bán hàng" },
                      "Vị trí": { text: "Vị trí" }
                    }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["purpose"]} name="purpose">
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      "Để ở": { text: "Để ở" },
                      "Để đầu tư": { text: "Để đầu tư" }
                    },
                    hideInTable: false,
                    hideInSearch: false
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["productTypeCare"]}
                name="productTypeCare"
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["state"]} name="state">
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "state",
                    width: 120,
                    filters: "true",
                    valueEnum: {
                      "Đã đặt cọc": { text: "Đã đặt cọc" },
                      "Quan tâm sâu": { text: "Quan tâm sâu" },
                      "Tìm hiểu ban đầu": { text: "Tìm hiểu ban đầu" },
                      "Không liên hệ được": { text: "Không liên hệ được" },
                      "Đã đặt hẹn": { text: "Đã đặt hẹn" }
                    },
                    hideInTable: false,
                    hideInSearch: false
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={submitting}
          >
            {params.id === "add" ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button
            type="default"
            style={{ color: "#fa5656" }}
            onClick={() => {
              history.goBack();
            }}
          >
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ projectMediaCustomer, loading, router }) => ({
  submitting: loading.effects["projectMediaCustomer/submit"],
  projectMediaCustomer,
  router
}))(ProjectMediaCustomerForm);
