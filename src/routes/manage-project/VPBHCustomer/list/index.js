import React, { useCallback, useEffect, useState } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { PieChart, Pie, Sector, Cell } from 'recharts';
import { Row } from 'antd';
import { Col } from "antd";
import Widget from 'src/components/Widget';
import * as project from 'src/services/project';


const ReportMediaCustomer = (props) => {

  const [pieVisit, setPieVisit] = useState([]);
  const [activeIndex, setActiveIndex] = useState(0);
  const onPieEnter = useCallback(
    (_, index) => {
      setActiveIndex(index);
    },
    [setActiveIndex]
  );
  const getDataVisit = async () => {
    let api = await project.reportVisit(props.projectId)
    setPieVisit(api.data.data)
    
  }
  useEffect(() => {
    getDataVisit();
  },[])
//-----------------------


const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];
  const RADIAN = Math.PI / 180;

  const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };
  const renderActiveShape = (props) => {

    const {
      cx,
      cy,
      midAngle,
      innerRadius,
      outerRadius,
      startAngle,
      endAngle,
      fill,
      percent,
      value
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? "start" : "end";

    return (
      <>
        <g>
          {/* <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
          {payload.name}
        </text> */}
          <Sector
            cx={cx}
            cy={cy}
            innerRadius={innerRadius}
            outerRadius={outerRadius}
            startAngle={startAngle}
            endAngle={endAngle}
            fill={fill}
          />
          <Sector
            cx={cx}
            cy={cy}
            startAngle={startAngle}
            endAngle={endAngle}
            innerRadius={outerRadius + 6}
            outerRadius={outerRadius + 10}
            fill={fill}
          />
          <path
            d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
            stroke={fill}
            fill="none"
          />

          <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
          <text
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            textAnchor={textAnchor}
            fill="#333"
          >{`PV ${value}`}</text>
          <text
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            dy={18}
            textAnchor={textAnchor}
            fill="#999"
          >
            {`(Rate ${(percent * 100).toFixed(2)}%)`}
          </text>

        </g>
      </>
    );
  };
  return (
    <>
    <Widget styleName="gx-card-full is-build-chartclass" title="Mục đích khách hàng quan tâm">
      <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <ResponsiveContainer width="100%" height={300}>
              <PieChart width={600} height={400}>
                <Pie
                  activeIndex={activeIndex}
                  activeShape={renderActiveShape}
                  data={pieVisit ? pieVisit.pieNeed : null}
                  cx={240}
                  cy={150}
                  labelLine={false}
                  label={renderCustomizedLabel}
                  outerRadius={80}
                  fill="#8884d8"
                  dataKey="value"
                  onMouseEnter={onPieEnter}
                >
                  {pieVisit ? (pieVisit.pieNeed || []).map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  )) : null}
                </Pie>
                <Legend/>
              </PieChart>
            </ResponsiveContainer>
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            {pieVisit ? (pieVisit.pieNeed || []).map((item, index) => {
              return (
                <>
                  <div className="nv-info-classroom">
                    <span key={index}>{item.name}: </span>
                    <span key={index}>{item.value} Lượt</span>
                  </div>
                </>
              );
            }) : null}
          </Col>
        </Row>
    </Widget>
    <Widget styleName="gx-card-full is-build-chartclass" title="Khách hàng đang lo lắng điều gì">
      <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <ResponsiveContainer width="100%" height={300}>
              <PieChart width={600} height={400}>
                <Pie
                  activeIndex={activeIndex}
                  activeShape={renderActiveShape}
                  data={pieVisit ? pieVisit.pieGroupType : null}
                  cx={240}
                  cy={150}
                  labelLine={false}
                  label={renderCustomizedLabel}
                  outerRadius={80}
                  fill="#8884d8"
                  dataKey="value"
                  onMouseEnter={onPieEnter}
                >
                  {pieVisit ? (pieVisit.pieGroupType || []).map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  )) : null}
                </Pie>
                <Legend/>
              </PieChart>
            </ResponsiveContainer>
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            {pieVisit ? (pieVisit.pieGroupType || []).map((item, index) => {
              return (
                <>
                  <div className="nv-info-classroom">
                    <span key={index}>{item.name}: </span>
                    <span key={index}>{item.value} Lượt</span>
                  </div>
                </>
              );
            }) : null}
          </Col>
        </Row>
    </Widget>
    <Widget styleName="gx-card-full is-build-chartclass" title="Sản phẩm khách hàng quan tâm">
      <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <ResponsiveContainer width="100%" height={300}>
              <PieChart width={600} height={400}>
                <Pie
                  activeIndex={activeIndex}
                  activeShape={renderActiveShape}
                  data={pieVisit ? pieVisit.pieProduct : null}
                  cx={240}
                  cy={150}
                  labelLine={false}
                  label={renderCustomizedLabel}
                  outerRadius={80}
                  fill="#8884d8"
                  dataKey="value"
                  onMouseEnter={onPieEnter}
                >
                  {pieVisit ? (pieVisit.pieProduct || []).map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  )) : null}
                </Pie>
                <Legend/>
              </PieChart>
            </ResponsiveContainer>
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            {pieVisit ? (pieVisit.pieProduct || []).map((item, index) => {
              return (
                <>
                  <div className="nv-info-classroom">
                    <span key={index}>{item.name}: </span>
                    <span key={index}>{item.value} Lượt</span>
                  </div>
                </>
              );
            }) : null}
          </Col>
        </Row>
    </Widget>
   </>
  );

};
export default ReportMediaCustomer;
