import React, { useState, useEffect, useRef } from "react";
import {
  Table,
  Card,
  DatePicker,
  Popconfirm,
  Select,
  Button,
  Modal,
  message
} from "antd";
import * as resultSaleAgencyServices from "../../../../services/resultSaleAgency";
import useFetchData from "src/packages/pro-table/useFetchData";
import dayjs from "dayjs";
import * as agencyTargetServices from "src/services/targetsDealer";
import { stringify } from "qs";
const { RangePicker } = DatePicker;
const ResultSaleAgency = ({ projectId }) => {
  const [data, setData] = useState([]);
  const [listProgress, setListProgress] = useState([]);
  const [timeStamp, setTimeStamp] = useState([
    dayjs().startOf("month"),
    dayjs().endOf("month")
  ]);
  const [isShowModal, setIsShowModal] = useState(false);
  const [selectProgress, setSelectProgress] = useState();
  const [selectProgressList, setSelectProgressList] = useState([]);
  useEffect(() => {
    (async () => {
      let response = await agencyTargetServices.getList({
        queryInput: {
          projectId
        }
      });
      if (response.status === 200) {
        setListProgress(response.data.data);
      }
    })();
  }, []);
  useEffect(() => {
    // (async () => {
    //   let response = await resultSaleAgencyServices.getList({
    //     projectId: projectId,
    //     startDate: dayjs(timeStamp[0]).valueOf(),
    //     endDate: dayjs(timeStamp[1]).valueOf()
    //   });
    //   if (response.status === 200) {
    //     setData(response.data.data);
    //   }
    // })();
  }, [timeStamp]);
  const fetchData = async () => {
    let response = await resultSaleAgencyServices.getList({
      projectId: projectId,
      agencyTargetId: selectProgress
    });
    if (response.status === 200) {
      setData(response.data.data);
    }
  };
  const columns = [
    {
      title: "Đại lý",
      dataIndex: "name",
      key: "name",
      width: 100,
      fixed: "left"
    },
    {
      title: "Phát sinh trong tuần",
      children: [
        {
          title: "Đủ cọc",
          dataIndex: "weekDuCoc",
          key: "age",
          width: 150
        },
        {
          title: "Ký TTĐC",
          dataIndex: "weekTTDC",
          key: "age",
          width: 150
        },
        {
          title: "Ký HĐMB",
          dataIndex: "weekHDMB",
          key: "age",
          width: 150
        }
      ]
    },
    {
      title: "Lũy kế tháng/kì chi tiêu",
      children: [
        {
          title: "Đủ cọc",
          dataIndex: "monthDucoc",
          key: "age",
          width: 150
        },
        {
          title: "Kí TTDC",
          dataIndex: "monthTTDC",
          key: "age",
          width: 150
        },
        {
          title: "Ký HĐMB",
          dataIndex: "monthHDMB",
          key: "age",
          width: 150
        },
        {
          title: "Chỉ tiêu",
          dataIndex: "monthTarget",
          key: "age",
          width: 150
        },
        {
          title: "% hoàn thành chỉ tiêu",
          dataIndex: "percentDone",
          key: "age",
          width: 150
        }
      ]
    }
  ];

  return (
    <>
      <Card title="Báo cáo bán hàng đại lý">
        {" "}
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <span>Giai đoạn:&nbsp;&nbsp;</span>
            {/* <RangePicker
            showTime
            defaultValue={timeStamp}
            onChange={e => {
              console.log(e);
              setTimeStamp(e);
            }}
          /> */}
            <Select
              style={{ width: 400 }}
              onChange={e => {
                setSelectProgress(e);
              }}
              placeholder="Chọn giai đoạn"
            >
              {listProgress?.map(e => {
                return (
                  <Select.Option key={e.id} value={e.id}>
                    {`${e.name}. Từ ${dayjs(e.startDate).format(
                      "DD/MM/YYYY"
                    )} đến ${dayjs(e.endDate).format("DD/MM/YYYY")}`}
                  </Select.Option>
                );
              })}
            </Select>
            <Button
              type="primary"
              style={{ marginLeft: 15 }}
              onClick={fetchData}
            >
              Báo cáo
            </Button>
          </div>

          <Button onClick={() => setIsShowModal(true)}>Xuất Excel</Button>
        </div>
        <div className="nv-sale-agency">
          {data.length > 0 ? (
            <Table
              className="nv-fix-table"
              columns={columns}
              dataSource={data}
              bordered
              size="middle"
              style={{ border: "2px", width: "100%" }}
            />
          ) : (
            ""
          )}
        </div>
      </Card>
      <Modal
        visible={isShowModal}
        onCancel={() => {
          setIsShowModal(false);
        }}
        onOk={()=>{
          if(selectProgressList.length === 0){
            return message.error("Vui lòng chọn giai đoạn")
          }
          let data = {
            agencyTargetIds : selectProgressList,
            projectId,
            accesstoken : localStorage.getItem("token")
          };
          window.open(
            `${process.env.REACT_APP_URL}/api/admin/apartment/dowload-report-agency-sale?${stringify(data)}`
          );
        }}
      >
        <span>Chọn giai đoạn cần xuất:</span>
        <Select
          style={{ width: 400 }}
          onChange={e => {
            setSelectProgressList(e);
          }}
          placeholder="Chọn giai đoạn"
          mode="multiple"
          allowClear
        >
          {listProgress?.map(e => {
            return (
              <Select.Option key={e.id} value={e.id}>
                {`${e.name}. Từ ${dayjs(e.startDate).format(
                  "DD/MM/YYYY"
                )} đến ${dayjs(e.endDate).format("DD/MM/YYYY")}`}
              </Select.Option>
            );
          })}
        </Select>
      </Modal>
    </>
  );
};

export default ResultSaleAgency;
