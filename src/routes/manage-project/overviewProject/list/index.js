import { PlusOutlined } from '@ant-design/icons';
import { Button, Col, Row } from 'antd';
import React, { useCallback, useEffect, useState } from 'react';
import Widget from 'src/components/Widget';
import ProTable from 'src/packages/pro-table';
import * as project from 'src/services/project';

const OverviewProject = (props) => {

  const [dataBooking, setDataBooking] = useState([]);
  const [dataYetBooking, setDataYetBooking] = useState([]);
  

  const getDataBooking = async () => {
    let api = await project.countBooking(props.projectId);
    setDataBooking(api.data.data);
  }

  const getNotYetBooking = async () => {
    let api = await project.notYetBooking(props.projectId, 1, 10);
    setDataYetBooking(api.data.data);
  }

  console.log(dataYetBooking, 2233)
 
  useEffect(() => {
    getDataBooking()
    getNotYetBooking()
  }, []);

  
  return (
    <>
    <Widget styleName="gx-card-widget is-build-chartclass">
      <Row>
        <Col xl={16} lg={16} md={16} sm={16} xs={24}>
          <div className="nv-info-booking">
            <span style={{marginRight:"50px"}}>Số lượt book căn hôm nay: {dataBooking ? dataBooking.countNow : "0"}</span>
            <span>Tỷ lệ tăng trưởng book căn: {dataBooking ? dataBooking.growthRate : "0"}%</span>
          </div>

          {/* <div className="nv-content-booking">
            <ul style={{listStyle:"none", padding:"0", margin:"0"}}>
              <li>
                <span>ID</span>
                <span>NAME</span>
                <span>PRICE</span>
              </li>
              {dataYetBooking ? dataYetBooking.map((item, index) => {
                return(
                  <>
                      <li key={index}>
                        <Row>
                          <Col xl={18} lg={18} md={18} sm={18} xs={18}>
                            <Row>
                              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                                <span>{item.id}</span>
                              </Col>
                              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                                <span>{item.fullCode}</span>
                              </Col>
                              <Col xl={8} lg={8} md={8} sm={8} xs={8}>
                                <span>{item.moneyCollect}</span>
                              </Col>
                            </Row>
                            
                            
                           
                          </Col>
                          <Col xl={6} lg={6} md={6} sm={6} xs={6}>
                            <Row>
                              <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                                <div><span>Huỷ</span></div>
                              </Col>
                              <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                                <div><span>Duyệt</span></div>
                              </Col>
                            </Row>
                            
                          </Col>
                        </Row>
                      </li>
                  </>
                )
              }) : null}
            </ul>
          </div> */}
        </Col>
        <Col xl={8} lg={8} md={8} sm={8} xs={24}>

        </Col>
      </Row>
      
    </Widget>

   </>
  );

};
export default OverviewProject;
