import React, { useCallback, useEffect, useState } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { PieChart, Pie, Sector, Cell } from 'recharts';
import { Row } from 'antd';
import { Col } from "antd";
import Widget from 'src/components/Widget';
import * as project from 'src/services/project';

var date = new Date();
var currentMonth1 = date.getMonth()+1;
var currentYear1 = date.getFullYear();


const ReportMediaCustomer = (props) => {

  const [dataChart, setDataChart] = useState([]);
  const [month, setMonth] = useState(currentMonth1);
  const [year, setYear] = useState(currentYear1);


  const getData = async () => {
    let api = await project.customerVisit(props.projectId , month , year)
    setDataChart(api.data)
  }

  useEffect(() => {
    getData();
  }, [month, year])

  

    const dataDefault = [
      {
        name: 'Vãng Lai',
        tuan1: 0,
        tuan2: 0,
        tuan3: 0,
        tuan4: 0,
        tuan5: 0,
      },
      {
        name: 'CĐT',
        tuan1: 0,
        tuan2: 0,
        tuan3: 0,
        tuan4: 0,
        tuan5: 0,
      },
    ];
  const style = {
    left: 80,
    bottom: 79,
    lineHeight: 2,
    border: "2px solid #a6a6a6",
    paddingTop: 5,
    paddingLeft: 5,
    borderRight: "none"
  };
  const CustomizedLabel = (props) => {

    const { x, y, stroke, value } = props;

    return (
      <text x={x + 10} y={y} dy={-5} fill={stroke} fontSize={15} textAnchor="middle">
        {value}
      </text>
    );

  };

  const renderTick = (props) => {
    const { x, y, index } = props;
    if (index === 2) {
      return (
        <>
          <path d={`M${x - 133},${y - 38}v${-180}`} stroke="black" />
          <path d={`M${x + 133},${y - 38}v${-180}`} stroke="black" />
        </>);
    } else {
      return (<path d={`M${x - 133},${y -38}v${-180}`} stroke="black" />);
    }

  };

  return (
    <>
    <Widget styleName="gx-card-full is-build-chartclass">
      <div className="nv-group-calender">
        <div className="nv-select-month">
          <select name="mdz" defaultValue={currentMonth1} onChange={(e) => setMonth(e.target.value)}>
            <option value="1">Tháng 1</option>
            <option value="2">Tháng 2</option>
            <option value="3">Tháng 3</option>
            <option value="4">Tháng 4</option>
            <option value="5">Tháng 5</option>
            <option value="6">Tháng 6</option>
            <option value="7">Tháng 7</option>
            <option value="8">Tháng 8</option>
            <option value="9">Tháng 9</option>
            <option value="10">Tháng 10</option>
            <option value="11">Tháng 11</option>
            <option value="12">Tháng 12</option>
          </select>
        </div>
        <div className="nv-select-month">
          <select name="mdz" defaultValue={currentYear1} onChange={(e) => setYear(e.target.value)}>
            <option value="2018">Năm 2018</option>
            <option value="2019">Năm 2019</option>
            <option value="2020">Năm 2020</option>
            <option value="2021">Năm 2021</option>
          </select>
        </div>
      </div>
      <ResponsiveContainer width={1000} height={700}>
        <BarChart
          width={500}
          height={600}
          data={dataChart ?  dataChart.data : dataDefault }
          // data={data}
          margin={{
            top: 100,
            right: 20,
            left: 20,
            bottom: 20,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" tickLine={false} stroke="black" />
          <YAxis ticks={[0, 10, 20, 30, 40, 50, 60]} domain={[0, 60]} stroke="black" interval={0}/>
          <Tooltip />
          <Legend
            align="left"
            layout="vertical"
            height={152}
            wrapperStyle={style} />
          <Bar dataKey="Tuần 1" barSize={20} fill="#413ea0" label={CustomizedLabel} />
          <Bar dataKey="Tuần 2" barSize={20} fill="#e19129" label={CustomizedLabel} />
          <Bar dataKey="Tuần 3" barSize={20} fill="#808080" label={CustomizedLabel} />
          <Bar dataKey="Tuần 4" barSize={20} fill="#ffee00" label={CustomizedLabel} />
          <Bar dataKey="Tuần 5" barSize={20} fill="#f31322" label={CustomizedLabel} />
          <XAxis
            dataKey="Tuần 1"
            height={30}
            xAxisId="x1"
            tickLine={false} />
          <XAxis
            dataKey="Tuần 2"
            height={30}
            xAxisId="x2"
            tickLine={false} />
          <XAxis
            dataKey="Tuần 3"
            height={30}
            xAxisId="x3"
            tickLine={false} />
          <XAxis
            dataKey="Tuần 4"
            height={30}
            xAxisId="x4"
            tickLine={false} />
          <XAxis
            dataKey="Tuần 5"
            height={30}
            xAxisId="x5"
            tickLine={false} />
          <XAxis
            dataKey=""
            height={30}
            xAxisId="x6"
            tickLine={false}
            tick={false} />
          <XAxis
            dataKey=""
            height={30}
            xAxisId="x7"
            tickLine={false}
            tick={renderTick}
            axisLine={false} />
        </BarChart>
      </ResponsiveContainer>
    </Widget>

   </>
  );

};
export default ReportMediaCustomer;
