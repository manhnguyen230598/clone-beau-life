import React, { useEffect, useState } from 'react';
import { Card, Button, Form, Col, Row, Popover ,DatePicker,Switch} from 'antd';
import _ from 'lodash';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import * as enums from 'util/enums';
import Upload from 'components/Upload';
import TextArea from 'rc-textarea';
import { camelCaseToDash } from 'util/helpers';
import dayjs from 'dayjs';

const RESOURCE = "popupApp";
const fieldLabels =
{
  "data": "Dữ liệu",
  "action": "Hành động",
  "image": "Hình ảnh",
  "isActive": "Trạng thái",
  "startDate": "Ngày bắt đầu",
  "endDate": "Ngày kết thúc"
};

const PopupAppForm = ({ popupApp: { formTitle, formData }, dispatch, submitting, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');
  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    console.log(data)
    if(data.startDate){
      data.startDate = dayjs(data.startDate).valueOf()
     }
      if(data.endDate){
       data.endDate = dayjs(data.endDate).valueOf()
      }
    if (data.image) {
      data.image =
        data.image && data.image.length > 0
          ? data.image
            .map(
              (i) =>
                i.url ||
                (i.response 
                  ? i.response.url
                  : ''),
            )
            .toString()
          : '';
    }
    console.log(data)
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
			callback: (res) => {
				if (res && !res.error) {
          // history.goBack();
        }
			}
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  },[])

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  },[formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  })

  if(params.id === 'add' || (formData && formData.id && formData.id !== 'add')){
    return (
      <Form
      className="ant-advanced-search-form"
      form={form}
      layout="vertical"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      hideRequiredMark
      initialValues={{ ...formData }}
    >
      <Card className="gx-card" title="Thông tin chung">
        <Row gutter={24}>
          <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
            <Form.Item
              label={fieldLabels['action']}
              name="action"
              rules={[{ required: true, message: `${fieldLabels['name']} không được trống` }]}
            >
              <FormInputRender
                item={{ "valueEnum":enums.actionBannerEnums,"title": "Tên lớp học", "dataIndex": "name", "width": 200, "hasFilter": true, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 1, "col": 1 } }}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
            <Form.Item
              label={fieldLabels['data']}
              name="data"
              rules={[{ required: true, message: `${fieldLabels['data']} không được trống` }]}
            >
               <TextArea placeholder="Id của lớp học/ Id của dự án/ Link web"/>
            </Form.Item>
          </Col>
          <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
            <Form.Item
              label={fieldLabels['image']}
              name="image"
              rules={[{ required: true, message: `${fieldLabels['image']} không được trống` }]}
            >
              <Upload />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
            <Form.Item
              label={fieldLabels['startDate']}
              name="startDate"
              rules={[{ required: true, message: `${fieldLabels['startDate']} không được trống` }]}
            >
              <DatePicker />
            </Form.Item>
          </Col>
          <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
            <Form.Item
              label={fieldLabels['endDate']}
              name="endDate"
              rules={[{ required: true, message: `${fieldLabels['endDate']} không được trống` }]}
            >
              <DatePicker />
            </Form.Item>
          </Col>
          <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
            <Form.Item
              label={fieldLabels['isActive']}
              name="isActive"
              rules={[{ required: true, message: `${fieldLabels['isActive']} không được trống` }]}
            >
              <Switch defaultChecked={formData.isActive}></Switch>
            </Form.Item>
          </Col>
        </Row>
      </Card>

      <FooterToolbar style={{ width }}>
        {getErrorInfo(error)}
        <Button type="primary" onClick={() => form.submit()} loading={submitting}>
          {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
        </Button>
        <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack() }}>
          {`Quay lại`}
        </Button>
      </FooterToolbar>
    </Form>
    );
  }
  return <></>;
};

export default connect(({ popupApp, loading, router }) => ({
  submitting: loading.effects['popupApp/submit'],
  popupApp,
	router
}))(PopupAppForm);
