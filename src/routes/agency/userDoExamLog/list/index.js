import React, { useState, useEffect, useRef } from "react";
import {
  Form /* , Input, Button, Space */,
  Button,
  Modal,
  Tooltip,
  message
} from "antd";
import { SearchOutlined, UnorderedListOutlined } from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { union } from "lodash";
import UserDoExamLogDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import ListUserSelect from "src/components/Select/User/ListUser";
import ListAgency from "src/components/Select/Agency/ListAgency";
const tranformUserDoExamLogParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "userDoExamLog";
const UserDoExamLogList = props => {
  const [form] = Form.useForm();
  // const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  // const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState({
    id: { fixed: "left" }
  });
  const [showModal, setShowModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});
  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformUserDoExamLogParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: {
          ...initParams.filters,
          ["classId"]: props.classId
        },
        populate: "userId,examinationId,agencyId"
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});
    if (filters.userId) {
      filters.userId = filters.userId?.id;
    }
    if (filters.agencyId) {
      filters.agencyId = filters.agencyId?.id;
    }
    const initParams = tranformUserDoExamLogParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        ["classId"]: props.classId
      },
      populate: "userId,examinationId, agencyId",
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });
        if (typeof values.userId == "object") {
          values.userId = values.userId?.id;
        }
        if (typeof values.agencyId == "object") {
          values.agencyId = values.agencyId?.id;
        }
        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", "false");
          values = {};
        }
        const initParams = tranformUserDoExamLogParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters,
              ["classId"]: props.classId
            },
            populate: "userId,examinationId,agencyId",
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  /* const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  }; */

  /* const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  }); */

  /* const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  }; */

  /* const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  }; */

  const handleEditClick = item => {
    setSelectedItem(item);
    setShowModal(true);
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Tên đề thi",
      dataIndex: "examinationId",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: false,
      render: val => <p>{val.name}</p>
    },
    {
      title: "Học viên thực hiện",
      dataIndex: "userId",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: false,
      render: val => <p>{val.name}</p>,
      renderFormItem: (_, { type, defaultRender, ...rest }, form) => {
        if (type === "form") {
          return null;
        }
        return <ListUserSelect mode="radio" />;
        // return defaultRender(_);
      }
    },
    {
      title: "Đại lý",
      dataIndex: "agencyId",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: false,
      render: val => <p>{val.name}</p>,
      renderFormItem: (_, { type, defaultRender, ...rest }, form) => {
        if (type === "form") {
          return null;
        }
        return <ListAgency mode="radio" />;
        // return defaultRender(_);
      }
    },
    {
      title: "Tổng số câu",
      dataIndex: "totalQuestion",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Số câu đúng",
      dataIndex: "correctAnswer",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Đạt",
      dataIndex: "isReached",
      width: 60,
      fixed: "left",
      filters: true,
      hideInSearch: false,
      valueEnum: {
        true: {
          text: "Đạt",
          status: "Processing",
          color: "#ec3b3b",
          isText: true
        },
        false: {
          text: "Không đạt",
          status: "Default",
          color: "#ec3b3b",
          isText: true
        }
      }
    },
    {
      title: "Lần thi",
      dataIndex: "countTest",
      width: 60,
      hideInSearch: true
    },
    {
      title: "Thời gian nộp bài",
      dataIndex: "timeSubmit",
      width: 60,
      hideInSearch: true,
      valueType: "dateTime"
    },
    {
      title: "Thời gian kết thúc bài",
      dataIndex: "timeEnd",
      width: 60,
      hideInSearch: true,
      valueType: "dateTime"
    },
    {
      title: "Thao tác",
      width: 30,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Xem kết quả`}>
            <UnorderedListOutlined  onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      )
    }
  ];
  const columnsResult = [
    {
      title: "Tên câu hỏi",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Người dùng trả lời",
      dataIndex: "userAnswer",
      key: "age"
    },
    {
      title: "Đáp án đúng",
      dataIndex: "correctAnswer",
      key: "address"
    },
    {
      title: "So sánh",
      dataIndex: "correct",
      key: "correct",
      valueEnum: {
        true: {
          text: "Đúng",
          status: "Processing",
          color: "#ec3b3b",
          isText: true
        },
        false: {
          text: "Sai",
          status: "Default",
          color: "#ec3b3b",
          isText: true
        }
      }
    }
  ];
  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Lịch sử làm bài thi"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            type="primary"
            onClick={() =>
              window.open(
                `${
                  process.env.REACT_APP_URL
                }/api/admin/examination/export-by-class?classId=${
                  props.classId
                }&accesstoken=${localStorage.getItem("token")}`
              )
            }
          >
            {`Xuất báo cáo`}
          </Button>
        ]}
      />
      <Modal
        visible={showModal}
        onCancel={() => {
          setShowModal(false);
        }}
        width={"60%"}
      >
        <ProTable
          dataSource={selectedItem?.resultJson || []}
          columns={columnsResult}
          search={false}
          headerTitle={
            "Kết quả thi " + (selectedItem.userId ? selectedItem?.userId?.name : "")
          }
        />
      </Modal>
    </>
  );
};

export default connect(({ userDoExamLog, loading }) => ({
  userDoExamLog,
  loading: loading.models.userDoExamLog
}))(UserDoExamLogList);
