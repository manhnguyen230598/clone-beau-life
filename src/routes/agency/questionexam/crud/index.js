import React, { useEffect, useState } from 'react';
import { Card, Button, Form, Col, Row, Popover,Input } from 'antd';
import _ from 'lodash';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import * as enums from 'util/enums';
import { camelCaseToDash } from 'util/helpers';
import CorrectAnswer from '../../classroom/crud/components/Step3/components/Collapse/CorrectAnswer'

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 17 },
    md: { span: 17 }
  }
};
const RESOURCE = "questionexam";
const fieldLabels = {
  "name":"Câu hỏi",
  "examinationId":"Bài thi",
  "answerA":"Câu hỏi A",
  "answerB":"Câu hỏi B",
  "answerC":"Câu hỏi C",
  "answerD":"Câu hỏi D",
  "correctAnswer":"Đáp án"};
const QuestionexamForm = ({ questionexam: { formTitle, formData,examinationId }, dispatch, submitting,location, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');
  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    if(examinationId){
      data.examinationId = examinationId
    }
 
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
			callback: (res) => {
				if (res && !res.error) {
          history.goBack();
        }
			}
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  },[])

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  },[formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  })

  if(params.id === 'add' || (formData && formData.id && formData.id !== 'add')){
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin cơ bản" className="gx-card" bordered={false}>

              <Form.Item
                label={fieldLabels["name"]}
                name={'name'}
                rules={[{ required: true, message: `Không được trống` }]}
                // initialValue={field["name"]}
                {...formItemLayout}
              >
                <Input name={'name'} />
              </Form.Item>
              <Form.Item
                label={fieldLabels["answerA"]}
                name={'answerA'}
                rules={[{ required: true, message: `Không được trống` }]}
                // initialValue={field["answerA"]}
                {...formItemLayout}
              >
                <Input name={'answerA'}/>
              </Form.Item>
              <Form.Item
                label={fieldLabels["answerB"]}
                name={ 'answerB'}
                rules={[{ required: true, message: `Không được trống` }]}
                // initialValue={field["answerB"]}
                {...formItemLayout}
              >
                <Input name={'answerB'} />
              </Form.Item>
              <Form.Item
                label={fieldLabels["answerC"]}
                name={'answerC'}
                rules={[{ required: true, message: `Không được trống` }]}
                // initialValue={field["answerC"]}
                {...formItemLayout}
              >
                <Input name={'answerC'} />
              </Form.Item>
              <Form.Item
                label={fieldLabels["answerD"]}
                name={'answerD'}
                rules={[{ required: true, message: `Không được trống` }]}
                // initialValue={field["answerD"]}
                {...formItemLayout}
              >
                <Input name={'answerD'} />
              </Form.Item>
              <Form.Item
                label={fieldLabels["correctAnswer"]}
                name={'correctAnswer'}
                rules={[{ required: true, message: `Không được trống` }]}
                // initialValue={field["correctAnswer"]}
                {...formItemLayout}
              >
                <CorrectAnswer name={'correctAnswer'}  />
              </Form.Item>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack() }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ questionexam, loading, router }) => ({
  submitting: loading.effects['questionexam/submit'],
  questionexam,
	router
}))(QuestionexamForm);
