import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Space, Tooltip } from 'antd';
import { SearchOutlined, PlusOutlined, EditOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import ProTable from 'packages/pro-table/Table';
import QuestionexamDetail from '../detail';
import { getValue, setTableChange, getTableChange } from 'util/helpers';

const tranformQuestionexamParams = (match,filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter) {
    initSort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "questionexam";
const QuestionexamList = (props) => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ?
    Number(process.env.REACT_APP_PAGESIZE) : 10);
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState((origin) => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, (tableChange?.pagination) || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformQuestionexamParams(tableChange?.filtersArg, tableChange?.formValues, tableChange?.sort);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort || [
          { "createdAt": "desc" }
        ],
        queryInput: {
          ...initParams.filters,
          ["examinationId"]:props.match.params.id
        }
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformQuestionexamParams(filters, formValues, sorter);

    if (sessionStorage.getItem('isReset') === 'true') {
      sessionStorage.setItem('isReset', false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        ["examinationId"]:props.match.params.id
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current.validateFields().then(fieldsValue => {
      console.log('handleSearch -> fieldsValue', fieldsValue);
      let values = {};
      Object.keys(fieldsValue).forEach(key => {
        if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
          values[key] = fieldsValue[key];
        }
      });

      if (sessionStorage.getItem('isReset') === 'true') {
        sessionStorage.setItem('isReset', 'false');
        values = {};
      }
      const initParams = tranformQuestionexamParams(tableChange?.filters, values, tableChange?.sort);

      setTableChange(RESOURCE, {
        ...tableChange,
        pagination: {
          ...tableChange.pagination,
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit
        },
        formValues: values
      });
      setFormValues(() => values);
      dispatch({
        type: `${RESOURCE}/fetch`,
        payload: {
          queryInput: {
            ...initParams.filters,
            ["examinationId"]:props.match.params.id
          },
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit
        }
      });
    }).catch(err => {
      if (err) return;
    });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0],
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: '',
    });
    confirm();
  };

  const handleAddClick = () => {
    dispatch({
      type:`${RESOURCE}/storeExaminationId`,
      payload:props.match.params.id
    });
    dispatch(routerRedux.push({ pathname: `${props.match.url}/add`}));
  };

  const handleEditClick = item => {
    dispatch({
      type:`${RESOURCE}/storeExaminationId`,
      payload:props.match.params.id
    });
    dispatch(routerRedux.push({ pathname: `${props.match.url}/${item.id}`}));
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 60,
      fixed: 'left',
      hideInSearch: true,
    },
    {
      title: 'Câu hỏi',
      dataIndex: 'name',
      width: 200,
      ...getColumnSearchProps('name'),
    },
    {
      title: 'Bài thi',
      dataIndex: 'examinationId',
      width: 200,
      hideInSearch: true,
      hideInTable : true,
    },
    {
      title: 'Câu hỏi A',
      dataIndex: 'answerA',
      width: 200,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Câu hỏi B',
      dataIndex: 'answerB',
      width: 200,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Câu hỏi C',
      dataIndex: 'answerC',
      width: 200,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Câu hỏi D',
      dataIndex: 'answerD',
      width: 200,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Đáp án',
      dataIndex: 'correctAnswer',
      width: 200,
      hideInSearch: true,
      return : val=>{
        return <span>{val?.join(",")}</span>;
      }
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
      width: 150,
      hideInSearch : true
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updatedAt',
      valueType: 'dateTime',
      width: 150,
      hideInSearch : true
    },
    {
      title: 'Thao tác',
      width: 150,
      fixed: 'right',
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      ),
    },
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Câu hỏi thi"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={(searchParams) => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem('isReset', 'true');
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit,
            },
            filtersArg: {}, sorter: {}, formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
            {`Thêm mới`}
          </Button>
        ]}
      />
      <QuestionexamDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={(v) => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ questionexam, loading }) => ({
  questionexam,
  loading: loading.models.questionexam
}))(QuestionexamList);
