import React, { useEffect, useState } from "react";
import { Card, Button, Form, Col, Row, Popover, Select } from "antd";
import _ from "lodash";
import {
  CloseCircleOutlined,
  ConsoleSqlOutlined,
  DownCircleOutlined
} from "@ant-design/icons";
import { connect, routerRedux } from "dva";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import FooterToolbar from "packages/FooterToolbar";
import * as enums from "util/enums";
import XLSX from "xlsx";
import { camelCaseToDash } from "util/helpers";
import UploadFile from "../../../../components/UploadFile";
import QuestionExam from "../Collapse";

import dayjs from "dayjs";
const { Option } = Select;

const RESOURCE = "examination";
const fieldLabels = {
  name: "Đề thi",
  startDate: "Ngày bắt đầu",
  endDate: "Ngày kết thúc",
  classId: "Lớp học",
  totalQuestion: "Số câu hỏi",
  timeTestText: "Thời gian thi",
  timeTest: "Thời gian đếm",
  minCorrectAnswer: "Số câu hỏi tối thiểu đạt",
  isActive: "Trạng thái",
  maxTest: "Số lần thi tối đa"
};
const ExaminationForm = ({
  examination: { formTitle, formData, classId },
  dispatch,
  location,
  submitting,
  match: { params },
  history,
  ...rest
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState("100%");
  const [unitTimeTest, setUnitTimeTest] = useState("minute");
  const onChangeAfter = val => {
    setUnitTimeTest(val);
  };
  const selectAfter = (
    <Select
      defaultValue={unitTimeTest}
      className="select-after"
      onChange={onChangeAfter}
    >
      <Option value="minute">{`phút`}</Option>
      <Option value="hour">{`giờ`}</Option>
    </Select>
  );
  const getErrorInfo = errors => {
    const errorCount = errors.filter(item => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = fieldKey => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map(err => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li
          key={key}
          className="nv-error-list-item"
          onClick={() => scrollToField(key)}
        >
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });

    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={trigger => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const handleFile = (file, callback) => {
    const reader = new FileReader();
    reader.onload = e => {
      try {
        const dataBit = new Uint8Array(e.target.result);
        const wb = XLSX.read(dataBit, { type: "array" });
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        const data = XLSX.utils.sheet_to_json(ws);
        callback(data);
      } catch (error) {
        console.log(
          `🚀 ~ file: index.js ~ line 236 ~ handleFile ~ error`,
          error
        );
      }
    };
    reader.readAsArrayBuffer(file);
  };

  const onFinish = values => {
    setError([]);
    const data = _.cloneDeep(values);

    if (data.startDate) {
      data.startDate = dayjs(data.startDate).$d.getTime();
    }
    if (data.endDate) {
      data.endDate = dayjs(data.endDate).$d.getTime();
    }
    if (classId) {
      data.classId = classId;
    }
    if (data.isActive) {
      data.isActive = data.isActive === "true" ? true : false;
    }
    if (data.xlsx) {
      handleFile(data.xlsx[0].originFileObj, questions => {
        data.questions = questions.map(item=>{
          return {...item,["correctAnswer"]:item.correctAnswer.split(",")}
        });
        dispatch({
          type: `${RESOURCE}/submitExam`,
          payload: data,
          callback: res => {
            if (res && !res.error) {
              // history.goBack();
            }
          }
        });
      });
      return
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: res => {
        if (res && !res.error) {
          // history.goBack();
        }
      }
    });
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll(".ant-layout-sider")[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    }
    window.addEventListener("resize", resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener("resize", resizeFooterToolbar);
    };
  });

  if (
    params.id === "add" ||
    (formData && formData.id && formData.id !== "add")
  ) {
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin cơ bản" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["name"]}
                name="name"
                labelCol={{
                  span: 4
                }}
                wrapperCol={{
                  span: 20
                }}
              >
                <FormInputRender
                  item={{
                    title: "Đề thi",
                    dataIndex: "name",
                    width: 200,
                    hasFilter: true,
                    hideInTable: false,
                    hideInSearch: false,
                    formPattern: { card: "Thông tin cơ bản", row: 1, col: 1 }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item label={fieldLabels["startDate"]} name="startDate">
                <FormInputRender
                  item={{
                    title: "Ngày bắt đầu",
                    dataIndex: "startDate",
                    width: 120,
                    valueType: "dateTime",
                    hasFilter: false,
                    hideInTable: false,
                    hideInSearch: false,
                    formPattern: { card: "Thông tin cơ bản", row: 2, col: 1 }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item label={fieldLabels["endDate"]} name="endDate">
                <FormInputRender
                  item={{
                    title: "Ngày kết thúc",
                    dataIndex: "endDate",
                    width: 120,
                    valueType: "dateTime",
                    hasFilter: false,
                    hideInTable: false,
                    hideInSearch: false,
                    formPattern: { card: "Thông tin cơ bản", row: 2, col: 2 }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["totalQuestion"]}
                name="totalQuestion"
              >
                <FormInputRender
                  item={{
                    title: "Số câu hỏi",
                    dataIndex: "totalQuestion",
                    width: 200,
                    hasFilter: false,
                    hideInTable: false,
                    hideInSearch: true,
                    valueType: "digit"
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["timeTestText"]}
                name="timeTestText"
                rules={[
                  {
                    required: true,
                    message: `${fieldLabels["timeTestText"]} không được trống`
                  },
                  {
                    pattern: /^\d{1,6}$/g,
                    message: `${fieldLabels["timeTestText"]} không hợp lệ`
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Thời gian thi",
                    dataIndex: "timeTestText",
                    width: 200,
                    formItemProps: { addonAfter: selectAfter }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["timeTest"]}
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.timeTestText !== curValues.timeTestText
                }
              >
                {({ getFieldValue, setFieldsValue }) => {
                  const timeTestText = getFieldValue("timeTestText");
                  if (parseInt(timeTestText)) {
                    if (unitTimeTest == "minute") {
                      setFieldsValue({
                        timeTest: Number(timeTestText) * 60 * 1000
                      });
                    } else if (unitTimeTest == "hour") {
                      setFieldsValue({
                        timeTest: Number(timeTestText) * 60 * 60 * 1000
                      });
                    }
                  }
                  return (
                    <Form.Item name="timeTest">
                      <FormInputRender
                        item={{
                          title: "Thời gian đếm",
                          dataIndex: "timeTest",
                          width: 200,
                          valueType: "number",
                          formItemProps: { disabled: true }
                        }}
                        intl={intl}
                      />
                    </Form.Item>
                  );
                }}
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["minCorrectAnswer"]}
                name="minCorrectAnswer"
              >
                <FormInputRender
                  item={{
                    title: "Số câu hỏi tối thiểu đạt",
                    placeholder:
                      "Số câu hỏi đúng tối thiểu để hoàn thành bài thi",
                    dataIndex: "minCorrectAnswer",
                    width: 200,
                    hasFilter: false,
                    hideInTable: false,
                    hideInSearch: true,
                    valueType: "digit"
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["isActive"]}
                name="isActive"
                rules={[
                  {
                    required: true,
                    message: `${fieldLabels["isActive"]} không được trống`
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Trạng thái",
                    dataIndex: "isActive",
                    width: 200,
                    filters: true,
                    hasFilter: false,
                    hideInTable: false,
                    hideInSearch: false,
                    valueEnum: {
                      true: {
                        text: "Hoạt động",
                        status: "Processing",
                        color: "#ec3b3b",
                        isText: true
                      },
                      false: {
                        text: "Dừng",
                        status: "Default",
                        color: "#ec3b3b",
                        isText: true
                      }
                    },
                    formPattern: { card: "Thông tin khác", row: 2, col: 1 }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["maxTest"]}
                name="maxTest"
                rules={[
                  {
                    required: true,
                    message: `${fieldLabels["maxTest"]} không được trống`
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Trạng thái",
                    dataIndex: "isActive",
                    width: 200,
                    filters: true,
                    hasFilter: false,
                    hideInTable: false,
                    hideInSearch: false,
                    valueEnum: {
                      1: {
                        text: "1",
                        status: "Processing",
                        color: "#ec3b3b",
                        isText: true
                      },
                      2: {
                        text: "2",
                        status: "Default",
                        color: "#ec3b3b",
                        isText: true
                      }
                    },
                    formPattern: { card: "Thông tin khác", row: 2, col: 1 }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          {params.id === "add" ? (
            <>
            <Row>
              <Col>
                <Form.Item
                  label={"Import câu hỏi"}
                // noStyle
                >
                  <div className="gx-d-flex gx-flex-nowrap gx-justify-content-start gx-align-items-center">
                    <Form.Item
                      name="xlsx"
                      noStyle
                    // label={"Nhập danh sách câu hỏi"}
                    >
                      <UploadFile autoUpload={false} />
                    </Form.Item>
                    <a className="gx-ml-3" href={`/static/question.xlsx`}>
                      <DownCircleOutlined />
                      &nbsp;Tải File mẫu
                    </a>
                  </div>
                </Form.Item>
              </Col>
            </Row>
             <Row gutter={24}>
             <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
               <Form.Item
                 label={fieldLabels["questions"]}
                 shouldUpdate={(prevValues, curValues) =>
                   prevValues.xlsx !== curValues.xlsx
                 }
                 labelCol={{
                   span: 4
                 }}
                 wrapperCol={{
                   span: 20
                 }}
               >
                 {({ getFieldValue }) => {
                   const xlsxFile = getFieldValue("xlsx") || {};
                   console.log(
                     `🚀 ~ file: index.js ~ line 251 ~ Step3 ~ xlsxFile`,
                     xlsxFile
                   );
                   return (
                     <Form.Item name="questions" noStyle>
                       <QuestionExam name="questions" data={xlsxFile} />
                     </Form.Item>
                   );
                 }}
               </Form.Item>
             </Col>
           </Row>

           </> 
          ) : (
            ""
          )}
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={submitting}
          >
            {params.id === "add" ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button
            type="default"
            style={{ color: "#fa5656" }}
            onClick={() => {
              history.goBack();
            }}
          >
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ examination, loading, router }) => ({
  submitting: loading.effects["examination/submit"],
  examination,
  router
}))(ExaminationForm);
