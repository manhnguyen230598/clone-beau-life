import React from "react";
import { router } from 'dva';
import asyncComponent from "util/asyncComponent";
/* PLOP_INJECT_IMPORT */


const { Route, Switch } = router;
const Agency = ({ match }) => {
  return (
    <Switch>
      {/* PLOP_INJECT_EXPORT */}
      <Route path={`${match.url}/eventapp/:id`} component={asyncComponent(() => import('./eventApp/crud'))} />
      <Route path={`${match.url}/eventapp`} component={asyncComponent(() => import('./eventApp/list'))} exact={true} />
      <Route path={`${match.url}/popupApp/:id`} component={asyncComponent(() => import('./popupApp/crud'))} />
      <Route path={`${match.url}/popupApp`} component={asyncComponent(() => import('./popupApp/list'))} exact={true} />
      <Route path={`${match.url}/bannerApp/:id`} component={asyncComponent(() => import('./bannerApp/crud'))} />
      <Route path={`${match.url}/bannerApp`} component={asyncComponent(() => import('./bannerApp/list'))} exact={true} />
      <Route path={`${match.url}/reportclass`} component={asyncComponent(() => import('./reportclass/list'))} exact={true} />
      <Route path={`${match.url}/userDoExamLog`} component={asyncComponent(() => import('./userDoExamLog/list'))} exact={true} />
      <Route path={`${match.url}/classroom`} component={asyncComponent(() => import("./classroom/list"))} exact={true} />
      <Route path={`${match.url}/classroom/:id`} component={asyncComponent(() => import("./classroom/crud"))} />
      <Route path={`${match.url}/classroom-create`} component={asyncComponent(() => import("./classroom/crud/create"))} />
      <Route path={`${match.url}/classroom-detail/:id`} component={asyncComponent(() => import("./classroom/detail/detail"))} />
    </Switch>
  );
};

export default Agency;
