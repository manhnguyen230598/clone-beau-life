import React, { useState, useEffect } from 'react';
import { Modal, Form } from 'antd';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import { connect } from 'dva';
import TransferUser, { leftTableColumns, rightTableColumns } from '../../../agency/classroom/crud/components/Step4/components/TransferUser';
import * as userClassServices from 'services/userclass';

const RESOURCE = "userclass";
const AddUserToClassModal = ({ isShowModal = false, setIsShowModal, classId, dispatch }) => {
  const [form] = Form.useForm();
  const [users, setUsers] = useState([]);
  const onFinish = (values) => {
    const data = cloneDeep(values);
    data.classId = classId;
    data.type = "users";
    dispatch({
      type: `${RESOURCE}/appendBroker`,
      payload: data,
    });
  };

  useEffect(() => {
    if (classId) {
      userClassServices.getUserNotInClass({ classId }).then(res => {
        if (res.status == 200) {
          setUsers(get(res, "data.data", []));
        }
      });
    }
  }, [classId]);

  return (
    <React.Fragment>
      <Modal
        width={1000}
        visible={isShowModal}
        okText="Thêm học viên"
        onCancel={() => setIsShowModal(false)}
        onOk={() => form.submit()}
        title={"Thêm học viên"}
      >
        <Form
          className="ant-advanced-search-form"
          form={form}
          layout="vertical"
          onFinish={onFinish}
          hideRequiredMark
        >
          <Form.Item
            // label="Học viên"
            name="userIds"
            required={false}
            shouldUpdate={(prevValues, curValues) => prevValues.type !== curValues.type}
          >
            <TransferUser
              dataSource={users}
              disabled={false}
              showSearch={true}
              filterOption={(inputValue, item) =>
                item.name.indexOf(inputValue) !== -1 || item.phone.indexOf(inputValue) !== -1
              }
              leftColumns={leftTableColumns}
              rightColumns={rightTableColumns}
            />
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};

export default connect(null)(AddUserToClassModal);
