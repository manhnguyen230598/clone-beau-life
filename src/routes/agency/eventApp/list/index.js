import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Space, Tooltip, Avatar, DatePicker, Modal } from 'antd';
import { SearchOutlined, PlusOutlined, EditOutlined, DatabaseOutlined, QrcodeOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import ProTable from 'packages/pro-table/Table';
import { get, union } from 'lodash';
import EventAppDetail from '../detail';
import moment from 'moment'
import QRCode from 'qrcode.react';
import { getValue, setTableChange, getTableChange } from 'util/helpers';

const tranformEventAppParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "eventApp";
const EventAppList = (props) => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ?
    Number(process.env.REACT_APP_PAGESIZE) : 10);
  const [current, setCurrent] = useState(1);
  const [paramsListCheckin, setParamsListCheckin] = useState();
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState((origin) => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [columnsStateMap, setColumnsStateMap] = useState({ "id": { "fixed": "left" } });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const {
    dispatch,
    [RESOURCE]: { data, dataCheckin },
    loading, history
  } = props;

  const fetchData = (action, params) => {
    let initParams = {};
    let pagi = Object.assign({}, (tableChange?.pagination) || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformEventAppParams(tableChange?.filtersArg, tableChange?.formValues, tableChange?.sort);

    dispatch({
      type: `${RESOURCE}/${action}`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * (pagi.pageSize || 10),
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort ? (initParams.sort.length <= 0 ? [{ "createdAt": "desc" }] : initParams.sort) : [{ "createdAt": "desc" }],
        queryInput: {
          ...initParams.filters,
          ...params
        }
      }
    });
  }
  useEffect(() => {
    fetchData("fetch")
  }, []);

  const handleTableListCheckinChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformEventAppParams(filters, formValues, sorter);

    if (sessionStorage.getItem('isReset') === 'true') {
      sessionStorage.setItem('isReset', false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        ...paramsListCheckin
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ "createdAt": "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformEventAppParams(filters, formValues, sorter);

    if (sessionStorage.getItem('isReset') === 'true') {
      sessionStorage.setItem('isReset', false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ "createdAt": "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current.validateFields().then(fieldsValue => {
      console.log('handleSearch -> fieldsValue', fieldsValue);
      let values = {};
      Object.keys(fieldsValue).forEach(key => {
        if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
          values[key] = fieldsValue[key];
        }
      });

      if (sessionStorage.getItem('isReset') === 'true') {
        sessionStorage.setItem('isReset', 'false');
        values = {};
      }
      const initParams = tranformEventAppParams(tableChange?.filters, values, tableChange?.sort);

      setTableChange(RESOURCE, {
        ...tableChange,
        pagination: {
          ...tableChange.pagination,
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit
        },
        formValues: values
      });
      setFormValues(() => values);
      dispatch({
        type: `${RESOURCE}/fetch`,
        payload: {
          queryInput: {
            ...initParams.filters
          },
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit,
          sort: union(initParams.sort || [], [{ "createdAt": "desc" }])
        }
      });
    }).catch(err => {
      if (err) return;
    });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0],
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: '',
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  };

  const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  };
  const handleShowListCheckinClick = item => {
    setIsShowModal({
      "state": true,
      "type": "checkin"
    })
    const params = {
      "eventId": item.id
    }
    fetchData("fetchPeopleCheckin", params)
    setParamsListCheckin(params)
  };
  const handleQrCode = item => {
    setIsShowModal({
      "state": true,
      "type": "qrcode",
      "qrString":item.id
    })
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
    },
    {
      title: 'Tên sự kiện',
      dataIndex: 'name',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
    },
    {
      title: 'Ngày bắt đầu',
      dataIndex: 'startDate',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
      render: val => (val ? moment(val).format("DD-MM-YYYY") : "-")
    },
    {
      title: 'Ngày kết thúc',
      dataIndex: 'endDate',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
      render: val => (val ? moment(val).format("DD-MM-YYYY") : "-")
    },
    {
      title: 'Thao tác',
      width: 50,
      fixed: 'right',
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
          <Tooltip title={`Danh sách checkin`}>
            <DatabaseOutlined style={{ marginLeft: 10 }} onClick={() => handleShowListCheckinClick(record)} />
          </Tooltip>
          <Tooltip title={`Lấy Qr code`}>
            <QrcodeOutlined style={{ marginLeft: 10 }} onClick={() => handleQrCode(record)}/>
          </Tooltip>
        </>
      ),
    },
  ];

  const columnsListCheckin = [
    {
      title: 'Tên khách hàng',
      dataIndex: 'name',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
    },
    {
      title: 'Checkin lúc',
      dataIndex: 'checkInAt',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
      render: val => (val ? moment(val).format("DD-MM-YYYY HH:mm:ss") : "-")
    },
  ]
  const [isShowModal, setIsShowModal] = useState({
    "state": false,
    "type": "",
    "qrString":""
  })

  return (
    <>
      <Modal
        visible={isShowModal.state}
        onCancel={() => setIsShowModal({
          "state": false,
          "type": ""
        })}
        width={900}
        height={500}
      >
        {isShowModal.type === "checkin" ?
          <ProTable
            tableClassName="gx-table-responsive"
            type="table"
            rowKey="id"
            search={false}
            headerTitle="Danh sách sự kiện cho app"
            actionRef={actionRef}
            formRef={form}
            form={{ initialValues: formValues }}
            dataSource={dataCheckin && dataCheckin?.list}
            pagination={dataCheckin?.pagination}
            columns={columnsListCheckin}
            columnsStateMap={columnsStateMap}
            onColumnsStateChange={(mapCols) => {
              setColumnsStateMap(mapCols);
            }}
            loading={loading}
            // scroll={{ x: 1500, y: 300 }}
            onChange={handleTableListCheckinChange}
          />
          :
          <div style={{textAlign:"center"}}>
          <QRCode
            id='qrcode'
            value={isShowModal.qrString}
            size={300}
            level={'H'}
          />
          </div>
        }
      </Modal>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Sự kiện"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={(mapCols) => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={(searchParams) => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem('isReset', 'true');
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit,
            },
            filtersArg: {}, sorter: {}, formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
            {`Thêm mới`}
          </Button>
        ]}
      />
      <EventAppDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={(v) => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
}

export default connect(({ eventApp, loading }) => ({
  eventApp,
  loading: loading.models.eventApp
}))(EventAppList);
