import React, { useState, useCallback } from 'react';
import { Row } from 'antd';
import { connect } from 'dva';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from 'recharts';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import Widget from 'components/Widget';
import { Col } from "antd";
import { useEffect } from 'react';


const RESOURCE = "reportclass";
const ReportclassList = (props) => {

  const {
    dispatch,
    // [RESOURCE]: { data },
  } = props;
  const [dataClass, setDataClass] = useState({});
  const [activeIndex, setActiveIndex] = useState(0);
  const onPieEnter = useCallback(
    (_, index) => {
      setActiveIndex(index);
    },
    [setActiveIndex]
  );

  const getDataClass = () => {
    dispatch({
      type: `${RESOURCE}/reportClass`,
      payload: {
        id: props.classId,
      },
      callback: (data) => {
        setDataClass(data);
      },
    });
    
  };

  useEffect(() => {
    getDataClass();
    
  }, []);
  
  // const dataNull = [
  //   { name: "Số lượng sinh viên", value: 0 },
  //   { name: "Sinh viên đã qua môn ", value: 0 },
  //   { name: "Sinh viên trượt môn", value: 0 },
  //   { name: "Sinh viên chưa tham gia thi", value: 0 }
  // ]; 
  const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];
  const RADIAN = Math.PI / 180;

  const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };
  const renderActiveShape = (props) => {

    const {
      cx,
      cy,
      midAngle,
      innerRadius,
      outerRadius,
      startAngle,
      endAngle,
      fill,
      percent,
      value
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? "start" : "end";

    return (
      <>
        <g>
          {/* <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
          {payload.name}
        </text> */}
          <Sector
            cx={cx}
            cy={cy}
            innerRadius={innerRadius}
            outerRadius={outerRadius}
            startAngle={startAngle}
            endAngle={endAngle}
            fill={fill}
          />
          <Sector
            cx={cx}
            cy={cy}
            startAngle={startAngle}
            endAngle={endAngle}
            innerRadius={outerRadius + 6}
            outerRadius={outerRadius + 10}
            fill={fill}
          />
          <path
            d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
            stroke={fill}
            fill="none"
          />

          <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
          <text
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            textAnchor={textAnchor}
            fill="#333"
          >{`PV ${value}`}</text>
          <text
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            dy={18}
            textAnchor={textAnchor}
            fill="#999"
          >
            {`(Rate ${(percent * 100).toFixed(2)}%)`}
          </text>

        </g>
      </>
    );
  };
  // chartInfo ----------------------------
  const data = [
    {
      name: 'Dai ly A',
      tuan1: 10,
      tuan2: 20,
      tuan3: 20,
      tuan4: 30,
    },
    {
      name: 'Dai ly B',
      tuan1: 30,
      tuan2: 10,
      tuan3: 20,
      tuan4: 30,
    },
    {
      name: 'Dai ly C',
      tuan1: 40,
      tuan2: 30,
      tuan3: 30,
      tuan4: 30,
    },
    {
      name: 'Dai ly D',
      tuan1: 20,
      tuan2: 30,
      tuan3: 10,
      tuan4: 15,
    },

  ];

  const style = {
    left: 80,
    bottom: 79,
    lineHeight: 2,
    border: "2px solid #a6a6a6",
    paddingTop: 5,
    paddingLeft: 5,
    borderRight: "none"
  };
  const CustomizedLabel = (props) => {

    const { x, y, stroke, value } = props;

    return (
      <text x={x + 10} y={y} dy={-5} fill={stroke} fontSize={15} textAnchor="middle">
        {value}
      </text>
    );

  };

  const renderTick = (props) => {
    const { x, y, index } = props;
    if (index === 3) {
      return (
        <>
          <path d={`M${x - 102},${y - 38}v${-150}`} stroke="black" />
          <path d={`M${x + 102},${y - 38}v${-150}`} stroke="black" />
        </>);
    } else {
      return (<path d={`M${x - 102},${y - 38}v${-150}`} stroke="black" />);
    }

  };

  return (
    <>
      <Widget styleName="gx-card-full is-build-chartclass" title={<span style={{marginLeft : "15px"}}>Báo cáo chung</span>}>
        <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <ResponsiveContainer width="100%" height={300}>
              <PieChart width={600} height={400}>
                <Pie
                  activeIndex={activeIndex}
                  activeShape={renderActiveShape}
                  data={ dataClass.data}
                  cx={240}
                  cy={150}
                  labelLine={false}
                  label={renderCustomizedLabel}
                  outerRadius={80}
                  fill="#8884d8"
                  dataKey="value"
                  onMouseEnter={onPieEnter}
                >
                
                  {dataClass ? (dataClass.data || []).map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                  )) : null}
                </Pie>
              </PieChart>
            </ResponsiveContainer>
            <div className="nv-note-chart">
              <Row>
                <Col xl={12} lg={12} md={12} sm={12} xs={24}>
                  <div className="nv-color-chart">
                    <div className="nv-color-chart__blue">

                    </div>
                    <span>: Sinh viên thi đạt</span>
                  </div>
                </Col>
                <Col xl={12} lg={12} md={12} sm={12} xs={24}>
                  <div className="nv-color-chart">
                    <div className="nv-color-chart__orange">

                    </div>
                    <span>: Sinh viên chưa thi</span>
                  </div>
                </Col>
                <Col xl={12} lg={12} md={12} sm={12} xs={24}>
                  <div className="nv-color-chart">
                    <div className="nv-color-chart__green">

                    </div>
                    <span>: Sinh viên chưa đạt</span>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <div className="nv-info-classroom">
              <span> Tổng số sinh viên: </span>
              <span> {dataClass.total} Sinh viên</span>
            </div>
            {dataClass ? (dataClass.data || []).map((item, index) => {
              return (
                <>
                  <div className="nv-info-classroom">
                    <span key={index}>{item.name}: </span>
                    <span key={index}>{item.value} Sinh viên</span>
                  </div>
                </>
              );
            }) : null}
          </Col>
        </Row>
      </Widget>
      <Widget styleName="gx-card-full is-build-chartclass" title={<span style={{marginLeft : "15px"}}>Phân tích theo đại lý</span>}>
        <ResponsiveContainer width={1000} height={450}>
          <BarChart
            width={500}
            height={450}
            data={dataClass.dataRaw}
            // data={data}
            margin={{
              top: 20,
              right: 20,
              left: 0,
              bottom: 20,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" tickLine={false} stroke="black" />
            <YAxis ticks={[0, 5, 10, 15, 20, 25, 30]} domain={[0, 30]} stroke="black" />
            <Tooltip />
            <Legend
              // align="left"
              // layout="vertical"
              height={40}
              // wrapperStyle={style} 
              />

            <Bar dataKey="notTest" barSize={20} fill="#413ea0" label={CustomizedLabel} />
            <Bar dataKey="finish" barSize={20} fill="#e19129" label={CustomizedLabel} />
            <Bar dataKey="fail" barSize={20} fill="#808080" label={CustomizedLabel} />
            <Bar dataKey="total" barSize={20} fill="#ffee00" label={CustomizedLabel} />
            {/* <XAxis
              dataKey="notTest"
              height={30}
              xAxisId="x1"
              tickLine={false} />
            <XAxis
              dataKey="finish"
              height={30}
              xAxisId="x2"
              tickLine={false} />
            <XAxis
              dataKey="fail"
              height={30}
              xAxisId="x3"
              tickLine={false} />
            <XAxis
              dataKey="total"
              height={30}
              xAxisId="x4"
              tickLine={false} />
            <XAxis
              dataKey=""
              height={30}
              xAxisId="x5"
              tickLine={false}
              tick={false} />
            <XAxis
              dataKey=""
              height={30}
              xAxisId="x6"
              tickLine={false}
              tick={renderTick}
              axisLine={false} /> */}
          </BarChart>
        </ResponsiveContainer>
      </Widget>
    </>
  );
};

export default connect(({ reportclass, loading }) => {
  const { data: dataReport } = reportclass;
  return ({
    dataReport,
    loading: loading.models.reportclass,
  });
})(ReportclassList);
