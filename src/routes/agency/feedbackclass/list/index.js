import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Space, Tooltip, Avatar, Modal, Comment, Switch } from 'antd';
import { SearchOutlined, EditOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import ProTable from 'packages/pro-table/Table';
import _ from 'lodash';
import FeedbackclassDetail from '../detail';
import { getValue, setTableChange, getTableChange } from 'util/helpers';


const { TextArea } = Input;
const tranformFeedbackclassParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter) {
    initSort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "feedbackclass";
const FeedbackclassList = (props) => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ?
    Number(process.env.REACT_APP_PAGESIZE) : 10);
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const [isShowModal, setIsShowModal] = useState(false);
  const tableChange = getTableChange(RESOURCE);
  const [record, setRecord] = useState();
  const [formValues, setFormValues] = useState((origin) => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, (tableChange?.pagination) || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformFeedbackclassParams(tableChange?.filtersArg, tableChange?.formValues, tableChange?.sort);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort ? (initParams.sort.length <= 0 ? [{ "createdAt": "desc" }] : initParams.sort) : [{ "createdAt": "desc" }],
        queryInput: {
          ...initParams.filters,
          ["classId"]: props.classId,
        },
        populate:"userId,replyId"
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const onFinish = (values) => {
    const data = _.cloneDeep(values);
    if(record.id){
      data.id = record.id;
    }
    dispatch({
      type: `${RESOURCE}/replyFeedback`,
      payload: data,
    });
  };

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformFeedbackclassParams(filters, formValues, sorter);

    if (sessionStorage.getItem('isReset') === 'true') {
      sessionStorage.setItem('isReset', false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current.validateFields().then(fieldsValue => {
      console.log('handleSearch -> fieldsValue', fieldsValue);
      let values = {};
      Object.keys(fieldsValue).forEach(key => {
        if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
          values[key] = fieldsValue[key];
        }
      });

      if (sessionStorage.getItem('isReset') === 'true') {
        sessionStorage.setItem('isReset', 'false');
        values = {};
      }
      const initParams = tranformFeedbackclassParams(tableChange?.filters, values, tableChange?.sort);

      setTableChange(RESOURCE, {
        ...tableChange,
        pagination: {
          ...tableChange.pagination,
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit
        },
        formValues: values
      });
      setFormValues(() => values);
      dispatch({
        type: `${RESOURCE}/fetch`,
        payload: {
          queryInput: {
            ...initParams.filters
          },
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit
        }
      });
    }).catch(err => {
      if (err) return;
    });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0],
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: '',
    });
    confirm();
  };

  // const handleAddClick = () => {
  //   // setIsShowModal(true)
  // };

  const handleEditClick = item => {
    form.resetFields();
    setRecord(item);
    setIsShowModal(true);
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 60,
      fixed: 'left',
      sorter: true,
      hideInSearch: true,
    },
    {
      title: 'Học viên hỏi',
      dataIndex: 'userId',
      width: 200,
      hideInSearch: true,
      render:(val,record)=> <p>{val.name}</p>
    },
    {
      title: 'Nội dung hỏi',
      dataIndex: 'content',
      width: 200,
      ...getColumnSearchProps('content'),
    },
    {
      title: 'Người trả lời',
      dataIndex: 'replyId',
      width: 200,
      hideInSearch: true,
      render:(val)=><p>{val.name}</p>
    },
    {
      title: 'Nội dung trả lời',
      dataIndex: 'answer',
      width: 200,
      ...getColumnSearchProps('answer'),
    },

    {
      title: 'Cho xem hết',
      dataIndex: 'isViewAll',
      width: 200,
      filters: true,
      onFilter:()=>true,
      valueEnum: {
        "true": { "text": "Có", "status": "Processing", "color": "#ec3b3b", "isText": "true", },
        "false": { "text": "Không", "status": "Default", "color": "#ec3b3b", "isText": "true", },
      },
      render: (value, record) => {
        return (
          <>
            <Switch defaultChecked={record.isViewAll}
              checkedChildren="Có"
              unCheckedChildren="Không"
              onChange={(val) => {
                let data = {
                  isViewAll: val,
                  id: record.id
                };
                dispatch({
                  type: `${RESOURCE}/updateStatus`,
                  payload: data,
                  callback: (res) => {
                    if (res && !res.error) {

                    }
                  }
                });
              }}
            />
          </>
        );
      },
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
      width: 150,
      sorter: true,
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updatedAt',
      valueType: 'dateTime',
      width: 150,
      sorter: true,
    },
    {
      title: 'Thao tác',
      width: 150,
      fixed: 'right',
      render: (text, record) => (
        <>
          <Tooltip title={`Phản hồi`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      ),
    },
  ];

  return (
    <>
      <Modal
        width={1000}
        visible={isShowModal}
        okText="Phản hồi"
        onCancel={() => setIsShowModal(false)}
        onOk={() => form.submit()}
        title={"Trả lời học viên"}
      >
        <Form
          className="ant-advanced-search-form"
          form={form}
          layout="vertical"
          onFinish={onFinish}
          hideRequiredMark

        >
          <Form.Item
            label="Nội dung câu hỏi"
            // name="question"
            required={false}
            shouldUpdate={(prevValues, curValues) => prevValues.type !== curValues.type}
          >
            <Comment
              author={record?.userId?.name}
              avatar={
                <Avatar
                  src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                  alt="Han Solo"
                />
              }
              content={"Câu hỏi :"+record?.content}
            />
          </Form.Item>


          <Form.Item
            label="Nội dung phản hồi"
            name="answer"
            required={false}
          >
            <TextArea
              autoSize={{ minRows: 2, maxRows: 6 }}
            />
          </Form.Item>
        </Form>
      </Modal>

      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Phản hồi người dùng"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={(searchParams) => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem('isReset', 'true');
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit,
            },
            filtersArg: {}, sorter: {}, formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          // <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
          //   {`Thêm mới`}
          // </Button>
        ]}
      />
      <FeedbackclassDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={(v) => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ feedbackclass, loading }) => ({
  feedbackclass,
  loading: loading.models.feedbackclass
}))(FeedbackclassList);
