import React, { } from 'react';
import { Form, Button } from 'antd';
import { connect } from 'dva';
// import { FormInputRender } from 'packages/pro-table/form';
// import { useIntl } from 'packages/pro-table/component/intlContext';
import Lessons from './components/Collapse';

const formItemLayout = {
  labelCol: {
    span: 5,
  },
  wrapperCol: {
    span: 19,
  },
};

// const fieldLabels = { "name": "Tên bài giảng", "thumbnail": "Hình ảnh", "classId": "Lớp học", "sequence": "Vị trí", "isActive": "Trạng thái" };
const Step2 = (props) => {
  const [form] = Form.useForm();

  const { data, dispatch, submitting } = props;
  console.log(`🚀 ~ file: index.js ~ line 23 ~ Step2 ~ data`, data);
  if (!data) {
    return null;
  }
  const { validateFields, getFieldsValue } = form;

  const onPrev = () => {
    if (dispatch) {
      const values = getFieldsValue();
      dispatch({
        type: 'classroom/saveStepFormData',
        payload: {
          ...data,
          classlesson: {
            ...data.classlesson,
            ...values
          }
        },
      });
      dispatch({
        type: 'classroom/saveCurrentStep',
        payload: 'classroom',
      });
    }
  };
  const onValidateForm = async () => {
    const values = await validateFields();
    console.log(`🚀 ~ file: index.js ~ line 50 ~ onValidateForm ~ values`, values);
    console.log(`🚀 ~ file: index.js ~ line 60 ~ onValidateForm ~ data.classlesson`, data.classlesson);
    if (dispatch) {
      dispatch({
        type: 'classroom/saveStepFormData',
        payload: {
          ...data,
          classlesson: {
            ...data.classlesson,
            ...values
          }
        },
      });
      dispatch({
        type: 'classroom/saveCurrentStep',
        payload: 'examination',
      });
    }
  };

  return (
    <Form
      // {...formItemLayout}
      form={form}
      layout="horizontal"
      className="ant-advanced-search-form"
      initialValues={{ ...data.classlesson }}
    >
      <Form.Item name="classlessons">
        <Lessons name="lessons" />
      </Form.Item>
      {/* <Divider style={{ margin: '24px 16px' }} /> */}
      <Form.Item
        style={{ marginBottom: 8 }}
        wrapperCol={{
          xs: { span: 24, offset: 0 },
          sm: {
            span: formItemLayout.wrapperCol.span,
            offset: formItemLayout.labelCol.span,
          },
        }}
      >
        <Button type="primary" onClick={onValidateForm} loading={submitting}>
          Tiếp theo
        </Button>
        <Button onClick={onPrev} style={{ marginLeft: 8 }}>
          Trở lại
        </Button>
      </Form.Item>
    </Form>
  );
};
export default connect(
  ({
    classroom,
    loading,
  }) => ({
    submitting: loading.effects['classroom/submitStepForm'],
    data: classroom.step,
  }),
)(Step2);
