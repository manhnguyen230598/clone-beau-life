import React, { useState, useEffect } from 'react';
import { Collapse, Form, Input, Upload, Button, message } from 'antd';
import { UploadOutlined, CaretRightOutlined, DragOutlined, StopOutlined, CloseOutlined, PlusOutlined, CheckCircleOutlined } from '@ant-design/icons';
import _ from 'lodash';

const { Panel } = Collapse;
const propsUpload = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 17 },
    md: { span: 17 }
  }
};
const schema = {
  "jsonSchema": {
    "title": "Khách hàng",
    "description": "",
    "type": "object",
    "required": [],
    "properties": {
      "name": {
        "type": "string",
        "title": "Name",
        "default": "",
        "maxLength": 100000
      },
      "type": {
        "type": "string",
        "title": "Loại tài liệu",
        "default": "",
        "maxLength": 100000
      },
      "link": {
        "type": "array",
        "title": "Đường dẫn",
        "default": [],
        "maxFileSize": 10,
        "maxFileNum": 10,
        "items": {
          "type": "string",
          "format": "data-url"
        },
        "uniqueItems": true
      }
    }
  },
  "uiSchema": {
    "name": {
      "ui:placeholder": "",
      "ui:help": "",
      "ui:disabled": false
    },
    "type": {
      "ui:widget": "textarea",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
    "link": {
      "ui:widget": "RcImage",
      "ui:options": {
        "exampleImageUrl": "",
        "label": "Pictures upload",
        "listType": "picture",
        "uploadType": "picture",
        "vertical": true,
        "accept": "image/*"
      },
      "ui:help": "",
      "ui:disabled": false
    },
  },
  "formData": {
    "name": "",
    "type": "",
    "link": []
  },
  "bizData": {},
  "sequence": [
    "name",
    "type",
    "link"
  ]
};

function callback(key) {
  console.log("Collapse -> callback -> key: ", key);
}

const genExtra = ({ field, index, addGoden, removeGoden, godenChange, godenOff }) => (
  <>
    <PlusOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        event.stopPropagation();
        addGoden();
      }}
    />
    <DragOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        event.stopPropagation();
      }}
    />
    {field.status ? <StopOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        event.stopPropagation();
        godenOff(index);
      }}
    /> : <CheckCircleOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        event.stopPropagation();
        godenOff(index);
      }}
    />}
    <CloseOutlined
      className="nv-icon-action"
      onClick={event => {
        event.stopPropagation();
        removeGoden(field, index);
      }}
    />
  </>
);

const WeddingItem = ({ value, onChange, name }) => {
  const [godens, setGodens] = useState(value || [{}]);

  useEffect(() => {
    setGodens(value || [{}]);
  }, [value]);

  const godenChange = (e, index) => {
    setGodens(origin => {
      let arr = _.cloneDeep(origin || []);
      schema.sequence.forEach(f => {
        if (e.target.name === f) {
          arr[index][f] = e.target.value;
        }
      });
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const godenOff = (index) => {
    setGodens(origin => {
      let arr = _.cloneDeep(origin || []);

      arr[index]['status'] = !arr[index]['status'];
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const addGoden = (e) => {
    // e.preventDefault();
    let arr = _.cloneDeep(godens || []);
    arr.push({
      // isNew: true,
      ...schema.formData
    });
    setGodens(arr);
  };

  const removeGoden = (id, index) => {
    setGodens(origin => {
      let arr = _.cloneDeep(godens || []);
      arr.splice(index, 1);
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  return (
    <>
      <Collapse
        onChange={callback}
        expandIconPosition={'left'}
        bordered={false}
        defaultActiveKey={['1']}
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        className="nv-custom-collapse"
      >
        {godens.map((field, index) => {
          return (
            <Panel header={field.name || (index + 1)} key={index} extra={genExtra({ field, index, addGoden, removeGoden, godenChange, godenOff })} className="nv-custom-panel">
              {/* <Form.Item
                label={schema.jsonSchema.properties["name"]["title"]}
                name={[`${name}-${index}`, 'name']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["name"]}
                {...formItemLayout}
              >
                <Input name={`name`} onChange={e => godenChange(e, index)} />
              </Form.Item> */}
              <Form.Item
                label={schema.jsonSchema.properties["type"]["title"]}
                name={[`${name}-${index}`, 'type']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["type"]}
                {...formItemLayout}
              >
                <Input onChange={e => godenChange(e, index)} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["link"]["title"]}
                name={[`${name}-${index}`, 'link']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["link"]}
                {...formItemLayout}
              >
                <Upload {...propsUpload}>
                  <Button icon={<UploadOutlined />}>Click to Upload</Button>
                </Upload>
              </Form.Item>
            </Panel>
          );
        })}
      </Collapse>
    </>
  );
};

export default WeddingItem;
