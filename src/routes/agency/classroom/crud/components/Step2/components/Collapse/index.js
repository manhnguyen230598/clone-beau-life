import React, { useState, useEffect } from 'react';
import { Collapse, Form, Input } from 'antd';
import { CaretRightOutlined, DragOutlined, StopOutlined, CloseOutlined, PlusOutlined, CheckCircleOutlined } from '@ant-design/icons';
import _ from 'lodash';
import Upload from 'components/Upload';
import Document from './DocumentCreate';

const { Panel } = Collapse;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 17 },
    md: { span: 17 }
  }
};
const schema = {
  "jsonSchema": {
    "title": "promotions",
    "description": "",
    "type": "object",
    "required": [],
    "properties": {
      "name": {
        "type": "string",
        "title": "Tiêu đề",
        "default": "",
        "maxLength": 100000
      },
      "sequence": {
        "type": "string",
        "title": "Thứ tự",
        "default": "",
        "maxLength": 100000
      },
      "thumbnail": {
        "type": "array",
        "title": "Ảnh đại diện",
        "default": [],
        "maxFileSize": 10,
        "maxFileNum": 10,
        "items": {
          "type": "string",
          "format": "data-url"
        },
        "uniqueItems": true
      },
      "documents": {
        "type": "string",
        "title": "Tài liệu",
        "default": "",
        "maxLength": 100000
      },
    }
  },
  "uiSchema": {
    "name": {
      "ui:placeholder": "",
      "ui:help": "",
      "ui:disabled": false
    },
    "sequence": {
      "ui:widget": "textarea",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
    "thumbnail": {
      "ui:widget": "RcImage",
      "ui:options": {
        "exampleImageUrl": "",
        "label": "Pictures upload",
        "listType": "picture",
        "uploadType": "picture",
        "vertical": true,
        "accept": "image/*"
      },
      "ui:help": "",
      "ui:disabled": false
    },
    "documents": {
      "ui:widget": "collapse",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
  },
  "formData": {
    "name": "",
    "sequence": "",
    "thumbnail": [],
    "documents": {},
    "status": true
  },
  "bizData": {},
  "sequence": [
    "name",
    "sequence",
    "thumbnail",
    "documents"
  ]
};

function callback(key) {
  console.log("Collapse -> callback -> key: ", key);
}

const genExtra = ({ field, index, addGoden, removeGoden, godenChange, godenOff }) => (
  <>
    <PlusOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        addGoden();
      }}
    />
    <DragOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
      }}
    />
    {field.status ? <StopOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        godenOff(index);
      }}
    /> : <CheckCircleOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        godenOff(index);
      }}
    />}
    <CloseOutlined
      className="nv-icon-action"
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        removeGoden(field, index);
      }}
    />
  </>
);

const CollapseItem = ({value, onChange, name = "collapse" }) => {
  const [godens, setGodens] = useState(value || [{}]);
  useEffect(() => {
    setGodens(value || [{}]);
  }, [value]);

  const godenChange = (e, index) => {
    console.log(`🚀 ~ file: index.js ~ line 169 ~ godenChange ~ e, index`, e, index);
    setGodens(origin => {
      let arr = _.cloneDeep(origin || []);
      console.log(`🚀 ~ file: index.js ~ line 171 ~ godenChange ~ arr`, arr);
      schema.sequence.forEach(f => {
        if (e.target.name === f) {
          arr[index][f] = e.target.value;
        }
      });
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const godenOff = (index) => {
    setGodens(origin => {
      let arr = _.cloneDeep(origin || []);

      arr[index]['status'] = !arr[index]['status'];
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const addGoden = (e) => {
    // e.preventDefault();
    let arr = _.cloneDeep(godens || []);
    arr.push({
      // isNew: true,
      ...schema.formData
    });
    setGodens(arr);
  };

  const removeGoden = (id, index) => {
    setGodens(origin => {
      let arr = _.cloneDeep(godens || []);
      arr.splice(index, 1);
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  return (
    <>
      <Collapse
        onChange={callback}
        expandIconPosition={'left'}
        bordered={false}
        defaultActiveKey={['0']}
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        className="nv-custom-collapse"
      >
        {godens.map((field, index) => {
          return (
            <Panel header={field.name || (index + 1)} key={index} extra={genExtra({ field, index, addGoden, removeGoden, godenChange, godenOff })} className="nv-custom-panel">
              <Form.Item
                label={schema.jsonSchema.properties["name"]["title"]}
                name={[`${name}-${index}`, 'name']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["name"]}
                {...formItemLayout}
              >
                <Input name={'name'} onChange={e => godenChange(e, index)} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["sequence"]["title"]}
                name={[`${name}-${index}`, 'sequence']}
                rules={[{ required: false, message: `Không được trống` }]}
                initialValue={field["sequence"]}
                {...formItemLayout}
              >
                <Input name={'sequence'} onChange={e => godenChange(e, index)} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["thumbnail"]["title"]}
                name={[`${name}-${index}`, 'thumbnail']}
                rules={[{ required: false, message: `Không được trống` }]}
                initialValue={field["thumbnail"]}
                {...formItemLayout}
              >
                <Upload mode="simple" onChange={val => {
                  godenChange({
                    target: {
                      name: "thumbnail",
                      value: val
                    }
                  }, index);
                }} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["documents"]["title"]}
                name={[`${name}-${index}`, 'documents']}
                // rules={[{ required: false, message: `Không được trống` }]}
                initialValue={field["documents"]}
                {...formItemLayout}
              >
                <Document name="documents"
                  onChange={val => {
                    godenChange({
                      target: {
                        name: "documents",
                        value: val
                      }
                    }, index);
                  }}
                />
              </Form.Item>
            </Panel>
          );
        })}
      </Collapse>
    </>
  );
};

export default CollapseItem;
