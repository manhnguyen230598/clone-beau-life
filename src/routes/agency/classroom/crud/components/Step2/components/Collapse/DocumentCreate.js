import React, { useState } from 'react';
import { Form, Input, Switch } from 'antd';
import { DownCircleOutlined } from '@ant-design/icons';
import UploadFile from 'components/UploadFile';

/* const formItemLayout = {
  labelCol: {
    span: 5,
  },
  wrapperCol: {
    span: 19,
  },
}; */
const schema = {
  "jsonSchema": {
    "title": "Khách hàng",
    "description": "",
    "type": "object",
    "required": [],
    "properties": {
      "name": {
        "type": "string",
        "title": "Tên tài liệu",
        "default": "",
        "maxLength": 100000
      },
      "type": {
        "type": "string",
        "title": "Loại tài liệu",
        "default": "",
        "maxLength": 100000
      },
      "link": {
        "type": "array",
        "title": "Đường dẫn",
        "default": [],
        "maxFileSize": 10,
        "maxFileNum": 10,
        "items": {
          "type": "string",
          "format": "data-url"
        },
        "uniqueItems": true
      }
    }
  },
  "uiSchema": {
    "name": {
      "ui:placeholder": "",
      "ui:help": "",
      "ui:disabled": false
    },
    "type": {
      "ui:widget": "textarea",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
    "link": {
      "ui:widget": "RcImage",
      "ui:options": {
        "exampleImageUrl": "",
        "label": "Pictures upload",
        "listType": "picture",
        "uploadType": "picture",
        "vertical": true,
        "accept": "image/*"
      },
      "ui:help": "",
      "ui:disabled": false
    },
  },
  "formData": {
    "name": "",
    "type": true,
    "link": []
  },
  "bizData": {},
  "sequence": [
    "name",
    "type",
    "link"
  ]
};

const DocumentCreate = ({ value: initValue, onChange, name }) => {
  const [value, setValue] = useState({
    ...(schema.formData || initValue)
  });

  const handleChange = (e) => {
    const curName = e.target.name;
    const curVal = e.target.value;
    setValue(origin => {
      if (onChange) {
        onChange({
          ...origin,
          [curName]: curVal
        });
      }
      return {
        ...origin,
        [curName]: curVal
      };
    });
  };

  const handleChangeFile = (file) => {
    const curFile = (file && file.length) ? file[0] : {};
    setValue(origin => {
      if (onChange) {
        onChange({
          ...origin,
          link: curFile
        });
      }
      return {
        ...origin,
        link: curFile
      };
    });
  };

  return (
    <React.Fragment>
      {/* <Form.Item
        label={schema.jsonSchema.properties["name"]["title"]}
        name={[`${name}`, 'name']}
        rules={[{ required: true, message: `Không được trống` }]}
      // initialValue={field["name"]}
      >
        <Input name={`name`} onChange={handleChange} />
      </Form.Item> */}
      <Form.Item
        label={schema.jsonSchema.properties["type"]["title"]}
        name={[`${name}`, 'type']}
        rules={[{ required: true, message: `Không được trống` }]}
        initialValue={value.type}
        valuePropName="checked"
      >
        <Switch checkedChildren="pdf" unCheckedChildren="video" onChange={val => {
          handleChange({
            target: {
              name: 'type',
              value: val
            }
          });
        }} />
      </Form.Item>
      <Form.Item
        label={schema.jsonSchema.properties["link"]["title"]}
        rules={[{ required: true, message: `Không được trống` }]}
        // initialValue={field["link"]}

        help={<>
          <small>(*) Tài liệu chỉ nhận file pdf hoặc link video</small>
          <div><a className="gx-ml-3" href={`/static/huong_dan_lay_link_you_tube.docx`} download><DownCircleOutlined />&nbsp;Hướng dẫn lấy link youtube</a></div>
        </>}
        shouldUpdate={(prevValues, curValues) => prevValues.type !== curValues.type}
      >
        {({ getFieldValue }) => {
          const type = getFieldValue([`${name}`, 'type']);
          if (type) {
            return (
              <Form.Item
                name={[`${name}`, 'link']}
              >
                <UploadFile autoUpload={true} accept=".pdf" onChange={handleChangeFile} />
              </Form.Item>
            );
          }
          return (
            <Form.Item
              name={[`${name}`, 'link']}
            >
              <Input name={`link`} onChange={(val) => {
                console.log(`🚀 ~ file: DocumentCreate.js ~ line 172 ~ DocumentCreate ~ val`, val);
                handleChange(val);
              }} />
            </Form.Item>
          );
        }}
      </Form.Item>
    </React.Fragment>
  );
};

export default DocumentCreate;
