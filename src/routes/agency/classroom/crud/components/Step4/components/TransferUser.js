import React, { useState } from 'react';
import { Transfer, Table, Tag } from 'antd';
import difference from 'lodash/difference';

export const leftTableColumns = [
  {
    dataIndex: 'id',
    title: 'ID',
  },
  {
    dataIndex: 'name',
    title: 'Tên',
  },
  {
    dataIndex: 'phone',
    title: 'SĐT',
    render: val => <>{val}</>,
  },
  {
    dataIndex: 'gender',
    title: 'Giới tính',
    render: val => {
      return val == 'male' ?
        (
          <Tag>Nam</Tag>
        ) : (
          <Tag>Nữ</Tag>
        );
    },
  },
];

export const rightTableColumns = [
  {
    dataIndex: 'name',
    title: 'Tên',
  },
];

const TransferUser = ({ value, onChange, leftColumns, rightColumns, ...restProps }) => {
  const [targetKeys, setTargetKeys] = useState(value || []);

  const handleTranfer = nextTargetKeys => {
    console.log(`🚀 ~ file: TransferUser.js ~ line 48 ~ TransferUser ~ nextTargetKeys`, nextTargetKeys);
    setTargetKeys(nextTargetKeys);
    if (onChange) {
      onChange(nextTargetKeys);
    }
  };

  return (
    <Transfer
      {...restProps}
      showSelectAll={false}
      rowKey={record => record.id}
      targetKeys={targetKeys}
      onChange={handleTranfer}
    >
      {({
        direction,
        filteredItems,
        onItemSelectAll,
        onItemSelect,
        selectedKeys: listSelectedKeys,
        disabled: listDisabled,
      }) => {
        const columns = direction === 'left' ? leftColumns : rightColumns;

        const rowSelection = {
          getCheckboxProps: item => ({ isActive: listDisabled || item.isActive }),
          onSelectAll(selected, selectedRows) {
            const treeSelectedKeys = selectedRows
              .filter(item => !item.isActive)
              .map(({ id: key }) => key);
            const diffKeys = selected
              ? difference(treeSelectedKeys, listSelectedKeys)
              : difference(listSelectedKeys, treeSelectedKeys);
            onItemSelectAll(diffKeys, selected);
          },
          onSelect({ id: key }, selected) {
            onItemSelect(key, selected);
          },
          selectedRowKeys: listSelectedKeys,
        };

        return (
          <Table
            rowKey="id"
            rowSelection={rowSelection}
            columns={columns}
            dataSource={filteredItems}
            size="small"
            style={{ pointerEvents: listDisabled ? 'none' : null }}
            onRow={({ id: key, disabled: itemDisabled, ...rest }) => ({
              onClick: () => {
                if (itemDisabled || listDisabled) return;
                onItemSelect(key, !listSelectedKeys.includes(key));
              },
            })}
          />
        );
      }}
    </Transfer>
  );
};

export default TransferUser;
