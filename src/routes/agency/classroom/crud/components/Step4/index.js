import React, { useEffect, useState } from 'react';
import { Form, Button, Switch } from 'antd';
import { connect } from 'dva';
import get from 'lodash/get';
import TransferUser, { leftTableColumns, rightTableColumns } from './components/TransferUser';
import * as userServices from 'services/user';

const formItemLayout = {
  labelCol: {
    span: 0,
  },
  wrapperCol: {
    span: 24,
  },
};

const Step4 = (props) => {
  const [form] = Form.useForm();
  const { data, dispatch, submitting } = props;
  console.log(`🚀 ~ file: index.js ~ line 20 ~ Step4 ~ data`, data);

  const [users, setUsers] = useState([]);

  useEffect(() => {
    userServices.getList({ queryInput: { agencyId: (data.classroom.agencyIds || []).map(i => i.id) } }).then(res => {
      if (res.status == 200) {
        setUsers(get(res, "data.data", []));
      }
    });
  }, [data.classroom]);

  if (!data) {
    return null;
  }

  const { validateFields, getFieldsValue } = form;
  const onPrev = () => {
    if (dispatch) {
      const values = getFieldsValue();
      dispatch({
        type: 'classroom/saveStepFormData',
        payload: {
          ...data,
          userclass: {
            ...data.userclass,
            ...values,
          }
        },
      });
      dispatch({
        type: 'classroom/saveCurrentStep',
        payload: 'examination',
      });
    }
  };
  const onValidateForm = async () => {
    const values = await validateFields();
    if (dispatch) {
      dispatch({
        type: 'classroom/submitStepForm',
        payload: {
          userclass: {
            ...data.userclass,
            ...values,
          }
        },
      });
    }
  };

  return (
    <Form
      {...formItemLayout}
      form={form}
      layout="horizontal"
      className="nv-step-form"
      initialValues={{ ...data.userclass }}
    >
      <Form.Item
        name="type"
        label="Theo đại lý hoặc tùy chọn"
        valuePropName="checked"
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 24
        }}
      >
        <Switch checkedChildren="Theo đại lý" unCheckedChildren="Tùy chọn" />
      </Form.Item>
      <Form.Item
        required={false}
        shouldUpdate={(prevValues, curValues) => prevValues.type !== curValues.type}
      >
        {({ getFieldValue }) => {
          const type = getFieldValue("type");
          console.log(`🚀 ~ file: index.js ~ line 99 ~ Step4 ~ type`, type);
          if (!type) {
            return (
              <Form.Item
                // label="Học viên"
                name="userIds"
                required={false}
                shouldUpdate={(prevValues, curValues) => prevValues.type !== curValues.type}
              >
                <TransferUser
                  dataSource={users}
                  disabled={false}
                  showSearch={true}
                  filterOption={(inputValue, item) =>
                    item.name.indexOf(inputValue) !== -1 || item.phone.indexOf(inputValue) !== -1
                  }
                  leftColumns={leftTableColumns}
                  rightColumns={rightTableColumns}
                />
              </Form.Item>
            );
          }
          return null;
        }}
      </Form.Item>
      <Form.Item
        style={{ marginBottom: 8 }}
        wrapperCol={{
          xs: { span: 24, offset: 0 },
          sm: {
            span: formItemLayout.wrapperCol.span,
            offset: formItemLayout.labelCol.span,
          },
        }}
      >
        <Button type="primary" onClick={onValidateForm} loading={submitting}>
          Tiếp theo
        </Button>
        <Button onClick={onPrev} style={{ marginLeft: 8 }}>
          Trở lại
        </Button>
      </Form.Item>
    </Form>
  );
};
export default connect(
  ({
    classroom,
    loading,
  }) => ({
    submitting: loading.effects['classroom/submitStepForm'],
    data: classroom.step,
  }),
)(Step4);
