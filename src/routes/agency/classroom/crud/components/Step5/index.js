import { Button, Result } from 'antd';
import React from 'react';
import { connect, routerRedux } from 'dva';
import dayjs from 'dayjs';

const Step5 = (props) => {
  const { data, dispatch, history } = props;
  if (!data) {
    return null;
  }

  const onFinish = () => {
    if (dispatch) {
      dispatch({
        type: 'classroom/saveCurrentStep',
        payload: 'info',
      });
      dispatch(routerRedux.push("/agency/classroom"));
    }
  };
  /* const information = (
    <div className="nv-information">
      <Descriptions column={1}>
        <Descriptions.Item label="付款账户"> {payAccount}</Descriptions.Item>
        <Descriptions.Item label="收款账户"> {receiverAccount}</Descriptions.Item>
        <Descriptions.Item label="收款人姓名"> {receiverName}</Descriptions.Item>
        <Descriptions.Item label="转账金额">
          <Statistic value={amount} suffix="元" />
        </Descriptions.Item>
      </Descriptions>
    </div>
  ); */
  const extra = (
    <>
      <Button type="primary" onClick={onFinish}>
        Đến danh sách
      </Button>
      {/* <Button>Đến danh sách</Button> */}
    </>
  );
  return (
    <Result
      status="success"
      title="Thêm lớp học thành công"
      subTitle={`Vào lúc: ${dayjs().format("MM-DD-YYYY hh:mm a")}`}
      extra={extra}
      className="nv-result"
    >
      {/* {information} */}
    </Result>
  );
};

export default connect(({ classroom }) => ({
  data: classroom.step,
}))(Step5);
