import React from 'react';
import { Form, Button, Divider, Row, Col } from 'antd';
import { connect } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import Upload from 'components/Upload';
import ProjectSelect from 'components/Select/Project/ListProject';
import AgencySelect from 'components/Select/Agency/ListAgency';
import UserSelect from 'components/Select/User/ListUser';

const formItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const form3ItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

const RESOURCE = "classroom";
const fieldLabels = { "name": "Tên lớp học", "thumbnail": "Ảnh đại diện", "startDate": "Ngày bắt đầu", "endDate": "Ngày kết thúc", "teacher": "Giáo viên", "projectId": "Dự án", "agencyId": "Đại lý", "description": "Mô tả", "isActive": "Trạng thái", "isViewAll": 'Xem hết', "type": "Loại bài giảng", "price": "Giá", "useFor": "Loại áp dụng", "hotSequence": "Thứ tự nổi bật" };
const Step1 = (props) => {
  const { dispatch, data } = props;
  console.log(`🚀 ~ file: index.js ~ line 23 ~ Step1 ~ data`, data);
  const intl = useIntl();
  const [form] = Form.useForm();

  if (!data) {
    return null;
  }
  const { validateFields } = form;
  const onValidateForm = async () => {
    const values = await validateFields();
    if (dispatch) {
      dispatch({
        type: `${RESOURCE}/saveStepFormData`,
        payload: {
          ...data,
          classroom: {
            ...data.classroom,
            ...values
          }
        },
      });
      dispatch({
        type: `${RESOURCE}/saveCurrentStep`,
        payload: 'classlesson',
      });
    }
  };
  return (
    <>
      <Form
        {...formItemLayout}
        form={form}
        layout="horizontal"
        className="ant-advanced-search-form"
        hideRequiredMark
        initialValues={data.classroom}
      >
        <Row gutter={24}>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['name']}
              name="name"
              rules={[{ required: true, message: `${fieldLabels['name']} không được trống` }]}
            >
              <FormInputRender
                item={{ "title": "Tên lớp học", "dataIndex": "name", "width": 200, "hasFilter": true, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 1, "col": 1 } }}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['thumbnail']}
              name="thumbnail"
              rules={[{ required: false, message: `${fieldLabels['thumbnail']} không được trống` }]}
            >
              <Upload />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['startDate']}
              name="startDate"
              rules={[{ required: true, message: `${fieldLabels['startDate']} không được trống` }]}
            >
              <FormInputRender
                item={{ "title": "Ngày bắt đầu", "dataIndex": "startDate", "width": 120, "valueType": "date", "hasFilter": false, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 2, "col": 1 } }}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['endDate']}
              name="endDate"
              rules={[{ required: true, message: `${fieldLabels['endDate']} không được trống` }]}
            >
              <FormInputRender
                item={{ "title": "Ngày kết thúc", "dataIndex": "endDate", "width": 120, "valueType": "date", "hasFilter": false, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 2, "col": 2 } }}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['teacher']}
              name="teacher"
              rules={[{ required: true, message: `${fieldLabels['teacher']} không được trống` }]}
            >
              <UserSelect type="radio" paramsList={{roleId : [1,14,15]}} />
            </Form.Item>
          </Col>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['projectId']}
              name="projectId"
              rules={[{ required: false, message: `${fieldLabels['projectId']} không được trống` }]}
            >
              <ProjectSelect type="radio" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['agencyId']}
              name="agencyIds"
              rules={[{ required: true, message: `${fieldLabels['agencyId']} không được trống` }]}
            >
              <AgencySelect modeChoose="checkbox" />
            </Form.Item>
          </Col>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['isActive']}
              name="isActive"
              rules={[{ required: true, message: `${fieldLabels['isActive']} không được trống` }]}
            >
              <FormInputRender
                item={{ "title": "Trạng thái", "dataIndex": "isActive", "width": 200, "filters": true, "hasFilter": false, "hideInTable": false, "hideInSearch": false, "valueEnum": { "true": { "text": "Hoạt động", "status": "Processing", "color": "#ec3b3b", "isText": true }, "false": { "text": "Dừng", "status": "Default", "color": "#ec3b3b", "isText": true } }, "formPattern": { "card": "Thông tin khác", "row": 2, "col": 1 } }}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['hotSequence']}
              name="hotSequence"
              rules={[{ required: true, message: `${fieldLabels['hotSequence']} không được trống` }]}
            >
              <FormInputRender
                item={{ "title": `${fieldLabels['hotSequence']}`, "dataIndex": "hotSequence", "width": 200, "valueType": "digit" }}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['useFor']}
              name="useFor"
              rules={[{ required: true, message: `${fieldLabels['useFor']} không được trống` }]}
            >
              <FormInputRender
                item={{ "title": `${fieldLabels['useFor']}`, "dataIndex": "type", "width": 200, "valueEnum": { "project": { "text": "Dự án", "status": "Processing", "color": "#ec3b3b", "isText": true }, "general": { "text": "Chung", "status": "Default", "color": "#ec3b3b", "isText": true } }, "valueType": "radioGroup" }}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              {...form3ItemLayout}
              label={fieldLabels['isViewAll']}
              name="isViewAll"
              rules={[{ required: true, message: `${fieldLabels['isViewAll']} không được trống` }]}
              valuePropName="checked"
            >
              <FormInputRender
                item={{ "title": `${fieldLabels['isViewAll']}`, "dataIndex": "isViewAll", "width": 120, "valueType": "switch", formItemProps: { checkedChildren: "Xem hết", unCheckedChildren: "Không" } }}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              {...form3ItemLayout}
              label={fieldLabels['type']}
              name="type"
              rules={[{ required: true, message: `${fieldLabels['type']} không được trống` }]}
            >
              <FormInputRender
                item={{ "title": `${fieldLabels['type']}`, "dataIndex": "type", "width": 200, "valueEnum": { "free": { "text": "Miễn phí", "status": "Processing", "color": "#ec3b3b", "isText": true }, "pay": { "text": "Trả phí", "status": "Default", "color": "#ec3b3b", "isText": true } }, "valueType": "radioGroup" }}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              label={fieldLabels['description']}
              name="description"
              rules={[{ required: false, message: `${fieldLabels['description']} không được trống` }]}
            // labelCol={{
            //   span: 4,
            //   offset: 0
            // }}
            // wrapperCol={{
            //   xs: { span: 24, offset: 0 },
            //   sm: {
            //     span: 21,
            //     offset: 2,
            //   },
            // }}
            >
              <FormInputRender
                item={{ "title": "Mô tả", "dataIndex": "description", valueType: 'textarea' }}
                type="form"
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item
              {...form3ItemLayout}
              shouldUpdate={(prevValues, curValues) => prevValues.type !== curValues.type}
              noStyle
            >
              {({ getFieldValue }) => {
                return getFieldValue("type") == 'pay' ?
                  (
                    <Form.Item
                      label={fieldLabels['price']}
                      name="price"
                      rules={[{ required: true, message: `${fieldLabels['price']} không được trống` }]}
                    >
                      <FormInputRender
                        item={{ "title": `${fieldLabels['price']}`, "dataIndex": "price", "width": 200, "valueType": "digit" }}
                        intl={intl}
                      />
                    </Form.Item>
                  ) : null;
              }}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item
          wrapperCol={{
            xs: { span: 24, offset: 0 },
            sm: {
              span: formItemLayout.wrapperCol.span,
              offset: formItemLayout.labelCol.span,
            },
          }}
        >
          <Button type="primary" onClick={onValidateForm}>
            Tiếp theo
          </Button>
        </Form.Item>
      </Form>
      <Divider style={{ margin: '40px 0 24px' }} />
      <div className="">
        <h3>Tạo thông tin lớp học</h3>
        <h4>Bao gồm 4 bước</h4>
        <p>
          Thông tin lớp học, bài giảng, đề thi, học viên
        </p>
      </div>
    </>
  );
};

export default connect(({ classroom }) => ({
  data: classroom.step,
}))(Step1);
