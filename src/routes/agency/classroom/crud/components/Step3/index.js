import React, { useState } from "react";
import { Form, Button, Row, Col, Select } from "antd";
import { connect } from "dva";
import { FormInputRender } from "packages/pro-table/form";
import { DownCircleOutlined } from "@ant-design/icons";
import { useIntl } from "packages/pro-table/component/intlContext";
import QuestionExam from "./components/Collapse";
import UploadFile from "components/UploadFile";

const { Option } = Select;
const formItemLayout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
};

// const RESOURCE = "examination";
const fieldLabels = {
  name: "Đề thi",
  startDate: "Ngày bắt đầu",
  endDate: "Ngày kết thúc",
  classId: "Lớp học",
  totalQuestion: "Số câu hỏi",
  timeTestText: "Thời gian thi",
  timeTest: "Thời gian đếm",
  minCorrectAnswer: "Số câu hỏi tối thiểu đạt",
  isActive: "Trạng thái",
  questionExams: "Câu hỏi",
  maxTest : "Số lần thi tối đa"
};
const Step3 = props => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [unitTimeTest, setUnitTimeTest] = useState("minute");
  const { data, dispatch, submitting } = props;
  console.log(`🚀 ~ file: index.js ~ line 24 ~ Step3 ~ data`, data);
  if (!data) {
    return null;
  }
  const { validateFields, getFieldsValue } = form;
  const onPrev = () => {
    if (dispatch) {
      const values = getFieldsValue();
      dispatch({
        type: "classroom/saveStepFormData",
        payload: {
          ...data,
          examination: {
            ...data.examination,
            ...values
          }
        }
      });
      dispatch({
        type: "classroom/saveCurrentStep",
        payload: "classlesson"
      });
    }
  };
  const onValidateForm = async () => {
    const values = await validateFields();
    if (dispatch) {
      dispatch({
        type: "classroom/saveStepFormData",
        payload: {
          ...data,
          examination: {
            ...data.examination,
            ...values
          }
        }
      });
      dispatch({
        type: "classroom/saveCurrentStep",
        payload: "userclass"
      });
    }
  };
  const onChangeAfter = val => {
    setUnitTimeTest(val);
  };
  const selectAfter = (
    <Select
      defaultValue={unitTimeTest}
      className="select-after"
      onChange={onChangeAfter}
    >
      <Option value="minute">{`phút`}</Option>
      <Option value="hour">{`giờ`}</Option>
    </Select>
  );

  return (
    <Form
      {...formItemLayout}
      form={form}
      layout="horizontal"
      className="nv-step-form"
      initialValues={{ ...data.examination }}
      preserve={false}
      // onFieldsChange={(fiedls => {
      //   console.log(`🚀 ~ file: index.js ~ line 90 ~ Step3 ~ fiedls`, fiedls);
      // })}
    >
      <Row gutter={24}>
        <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
          <Form.Item
            label={fieldLabels["name"]}
            rules={[
              {
                pattern: /[A-Za-z]\w+/g,
                message: `${fieldLabels["name"]} không hợp lệ`
              }
            ]}
            help={<small>(*) Không điền tên để bỏ qua tạo đề thi</small>}
            name="name"
            labelCol={{
              span: 4
            }}
            wrapperCol={{
              span: 20
            }}
          >
            <FormInputRender
              item={{
                title: "Đề thi",
                dataIndex: "name",
                width: 200,
                hasFilter: true,
                hideInTable: false,
                hideInSearch: false,
                formPattern: { card: "Thông tin cơ bản", row: 1, col: 1 }
              }}
              intl={intl}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item label={fieldLabels["startDate"]} name="startDate">
            <FormInputRender
              item={{
                title: "Ngày bắt đầu",
                dataIndex: "startDate",
                width: 120,
                valueType: "dateTime",
                hasFilter: false,
                hideInTable: false,
                hideInSearch: false,
                formPattern: { card: "Thông tin cơ bản", row: 2, col: 1 }
              }}
              intl={intl}
            />
          </Form.Item>
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item label={fieldLabels["endDate"]} name="endDate">
            <FormInputRender
              item={{
                title: "Ngày kết thúc",
                dataIndex: "endDate",
                width: 120,
                valueType: "dateTime",
                hasFilter: false,
                hideInTable: false,
                hideInSearch: false,
                formPattern: { card: "Thông tin cơ bản", row: 2, col: 2 }
              }}
              intl={intl}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item label={fieldLabels["totalQuestion"]} name="totalQuestion">
            <FormInputRender
              item={{
                title: "Số câu hỏi",
                dataIndex: "totalQuestion",
                width: 200,
                hasFilter: false,
                hideInTable: false,
                hideInSearch: true,
                valueType: "digit"
              }}
              intl={intl}
            />
          </Form.Item>
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item
            label={fieldLabels["timeTestText"]}
            name="timeTestText"
            rules={[
              {
                required: false,
                message: `${fieldLabels["timeTestText"]} không được trống`
              },
              {
                pattern: /^\d{1,6}$/g,
                message: `${fieldLabels["timeTestText"]} không hợp lệ`
              }
            ]}
          >
            <FormInputRender
              item={{
                title: "Thời gian thi",
                dataIndex: "timeTestText",
                width: 200,
                formItemProps: { addonAfter: selectAfter }
              }}
              intl={intl}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item
            label={fieldLabels["timeTest"]}
            shouldUpdate={(prevValues, curValues) =>
              prevValues.timeTestText !== curValues.timeTestText
            }
          >
            {({ getFieldValue, setFieldsValue }) => {
              const timeTestText = getFieldValue("timeTestText");
              if (parseInt(timeTestText)) {
                if (unitTimeTest == "minute") {
                  setFieldsValue({
                    timeTest: Number(timeTestText) * 60 * 1000
                  });
                } else if (unitTimeTest == "hour") {
                  setFieldsValue({
                    timeTest: Number(timeTestText) * 60 * 60 * 1000
                  });
                }
              }
              return (
                <Form.Item name="timeTest">
                  <FormInputRender
                    item={{
                      title: "Thời gian đếm",
                      dataIndex: "timeTest",
                      width: 200,
                      valueType: "number",
                      formItemProps: { disabled: true }
                    }}
                    intl={intl}
                  />
                </Form.Item>
              );
            }}
          </Form.Item>
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item
            label={fieldLabels["minCorrectAnswer"]}
            name="minCorrectAnswer"
          >
            <FormInputRender
              item={{
                title: "Số câu hỏi tối thiểu đạt",
                placeholder: "Số câu hỏi đúng tối thiểu để hoàn thành bài thi",
                dataIndex: "minCorrectAnswer",
                width: 200,
                hasFilter: false,
                hideInTable: false,
                hideInSearch: true,
                valueType: "digit"
              }}
              intl={intl}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item
            label={fieldLabels["isActive"]}
            name="isActive"
            rules={[
              {
                required: false,
                message: `${fieldLabels["isActive"]} không được trống`
              }
            ]}
          >
            <FormInputRender
              item={{
                title: "Trạng thái",
                dataIndex: "isActive",
                width: 200,
                filters: true,
                hasFilter: false,
                hideInTable: false,
                hideInSearch: false,
                valueEnum: {
                  true: {
                    text: "Hoạt động",
                    status: "Processing",
                    color: "#ec3b3b",
                    isText: true
                  },
                  false: {
                    text: "Dừng",
                    status: "Default",
                    color: "#ec3b3b",
                    isText: true
                  }
                },
                formPattern: { card: "Thông tin khác", row: 2, col: 1 }
              }}
              intl={intl}
            />
          </Form.Item>
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item
            label={fieldLabels["maxTest"]}
            name="maxTest"
            rules={[
              {
                required: true,
                message: `${fieldLabels["maxTest"]} không được trống`
              }
            ]}
          >
            <FormInputRender
              item={{
                title: "Trạng thái",
                dataIndex: "isActive",
                width: 200,
                filters: true,
                hasFilter: false,
                hideInTable: false,
                hideInSearch: false,
                valueEnum: {
                  1: {
                    text: "1",
                    status: "Processing",
                    color: "#ec3b3b",
                    isText: true
                  },
                  2: {
                    text: "2",
                    status: "Default",
                    color: "#ec3b3b",
                    isText: true
                  }
                },
                formPattern: { card: "Thông tin khác", row: 2, col: 1 }
              }}
              intl={intl}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <Form.Item
            label={"Import câu hỏi"}
            // noStyle
          >
            <div className="gx-d-flex gx-flex-nowrap gx-justify-content-start gx-align-items-center">
              <Form.Item
                name="xlsx"
                noStyle
                // label={"Nhập danh sách câu hỏi"}
              >
                <UploadFile autoUpload={false} />
              </Form.Item>
              <a className="gx-ml-3" href={`/static/question.xlsx`} download>
                <DownCircleOutlined />
                &nbsp;Tải File mẫu
              </a>
            </div>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
          <Form.Item
            label={fieldLabels["questionExams"]}
            shouldUpdate={(prevValues, curValues) =>
              prevValues.xlsx !== curValues.xlsx
            }
            labelCol={{
              span: 4
            }}
            wrapperCol={{
              span: 20
            }}
          >
            {({ getFieldValue }) => {
              const xlsxFile = getFieldValue("xlsx") || {};
              console.log(
                `🚀 ~ file: index.js ~ line 251 ~ Step3 ~ xlsxFile`,
                xlsxFile
              );
              return (
                <Form.Item name="questions" noStyle>
                  <QuestionExam name="questionExam" data={xlsxFile} />
                </Form.Item>
              );
            }}
          </Form.Item>
        </Col>
      </Row>
      <Form.Item
        style={{ marginBottom: 8 }}
        wrapperCol={{
          xs: { span: 24, offset: 0 },
          sm: {
            span: formItemLayout.wrapperCol.span,
            offset: formItemLayout.labelCol.span
          }
        }}
      >
        <Button type="primary" onClick={onValidateForm} loading={submitting}>
          Tiếp theo
        </Button>
        <Button onClick={onPrev} style={{ marginLeft: 8 }}>
          Trở lại
        </Button>
      </Form.Item>
    </Form>
  );
};
export default connect(({ classroom, loading }) => ({
  submitting: loading.effects["classroom/submitStepForm"],
  data: classroom.step
}))(Step3);
