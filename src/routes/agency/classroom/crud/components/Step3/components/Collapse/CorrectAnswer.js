import React from 'react';
import { Checkbox } from 'antd';
import { useState } from 'react';

const CheckboxGroup = Checkbox.Group;
const plainOptions = ['A', 'B', 'C', 'D'];
const CorrectAnswer = ({ value: initValue, onChange }) => {
  console.log(`🚀 ~ file: CorrectAnswer.js ~ line 8 ~ CorrectAnswer ~ initValue`, initValue);
  const [value, ] = useState(initValue);

  const onCheckChange = (val) => {
    console.log(`🚀 ~ file: CorrectAnswer.js ~ line 12 ~ onCheckChange ~ val`, val);
    // setValue(val);
    if (onChange) {
      onChange(val);
    }
  };

  return (
    <React.Fragment>
      <CheckboxGroup options={plainOptions} defaultValue={value} onChange={onCheckChange} />
    </React.Fragment>
  );
};

export default CorrectAnswer;
