import React, { useState, useEffect } from 'react';
import { Collapse, Form, Input, Button } from 'antd';
import { CaretRightOutlined, DragOutlined, StopOutlined, CloseOutlined, PlusOutlined, CheckCircleOutlined } from '@ant-design/icons';
import _ from 'lodash';
import XLSX from 'xlsx';
import CorrectAnswer from './CorrectAnswer';
// import { make_cols } from 'util/helpers';

const { Panel } = Collapse;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 17 },
    md: { span: 17 }
  }
};
const schema = {
  "jsonSchema": {
    "title": "promotions",
    "description": "",
    "type": "object",
    "required": [],
    "properties": {
      "name": {
        "type": "string",
        "title": "Câu hỏi",
        "default": "",
        "maxLength": 100000
      },
      "answerA": {
        "type": "string",
        "title": "Câu A",
        "default": "",
        "maxLength": 100000
      },
      "answerB": {
        "type": "string",
        "title": "Câu B",
        "default": "",
        "maxLength": 100000
      },
      "answerC": {
        "type": "string",
        "title": "Câu C",
        "default": "",
        "maxLength": 100000
      },
      "answerD": {
        "type": "string",
        "title": "Câu D",
        "default": "",
        "maxLength": 100000
      },
      "correctAnswer": {
        "type": "string",
        "title": "Đáp án",
        "default": "",
        "maxLength": 100000
      },
    }
  },
  "uiSchema": {
    "name": {
      "ui:placeholder": "",
      "ui:help": "",
      "ui:disabled": false
    },
    "answerA": {
      "ui:widget": "textarea",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
    "answerB": {
      "ui:widget": "textarea",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
    "answerC": {
      "ui:widget": "textarea",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
    "answerD": {
      "ui:widget": "textarea",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
    "correctAnswer": {
      "ui:widget": "collapse",
      "ui:placeholder": "Please input",
      "ui:help": "",
      "ui:disabled": false
    },
  },
  "formData": {
    "name": "",
    "answerA": "",
    "answerB": "",
    "answerC": "",
    "answerD": "",
    "correctAnswer": [],
    "status": true
  },
  "bizData": {},
  "sequence": [
    "name",
    "answerA",
    "answerB",
    "answerC",
    "answerD",
    "correctAnswer"
  ]
};

function callback(key) {
  console.log("Collapse -> callback -> key: ", key);
}

const genExtra = ({ field, index, addGoden, removeGoden, godenChange, godenOff }) => (
  <>
    <PlusOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        addGoden();
      }}
    />
    <DragOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
      }}
    />
    {field.status ? <StopOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        godenOff(index);
      }}
    /> : <CheckCircleOutlined
      className="nv-icon-action"
      style={{ marginRight: "5px" }}
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        godenOff(index);
      }}
    />}
    <CloseOutlined
      className="nv-icon-action"
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        removeGoden(field, index);
      }}
    />
  </>
);

const CollapseItem = ({ value, onChange, name = "collapse", data: dataExcel }) => {
  const [godens, setGodens] = useState(value || []);

  useEffect(() => {

    setGodens(value || []);
  }, [value]);

  useEffect(() => {

    if (dataExcel[0]) {
      handleFile(dataExcel[0].originFileObj);
    }
  }, [dataExcel]);

  const godenChange = (e, index) => {
    console.log(`🚀 ~ file: index.js ~ line 169 ~ godenChange ~ e, index`, e, index);
    setGodens(origin => {
      let arr = _.cloneDeep(origin || []);
      console.log(`🚀 ~ file: index.js ~ line 171 ~ godenChange ~ arr`, arr);
      schema.sequence.forEach(f => {
        if (e.target.name === f) {
          arr[index][f] = e.target.value;
        }
      });
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const godenOff = (index) => {
    setGodens(origin => {
      let arr = _.cloneDeep(origin || []);

      arr[index]['status'] = !arr[index]['status'];
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const addGoden = (e) => {
    // e.preventDefault();
    let arr = _.cloneDeep(godens || []);
    arr.push({
      // isNew: true,
      ...schema.formData
    });
    setGodens(arr);
  };

  const removeGoden = (id, index) => {
    setGodens(origin => {
      let arr = _.cloneDeep(godens || []);
      arr.splice(index, 1);
      if (onChange) {
        onChange(arr);
      }
      return arr;
    });
  };

  const handleFile = (file) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      try {
        const dataBit = new Uint8Array(e.target.result);
        const wb = XLSX.read(dataBit, { type: 'array' });
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        const data = XLSX.utils.sheet_to_json(ws);
        // const cols = make_cols(ws['!ref']);
        // setGodens(origin => data);
        if (onChange) {
          onChange(data);
        }
      } catch (error) {
        console.log(`🚀 ~ file: index.js ~ line 236 ~ handleFile ~ error`, error);
      }
    };
    reader.readAsArrayBuffer(file);
  };

  return (
    <>
      <Collapse
        onChange={callback}
        expandIconPosition={'left'}
        bordered={false}
        defaultActiveKey={['0']}
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        className="nv-custom-collapse"
      >
        {godens.map((field, index) => {
          const idx = index + 1;
          return (
            <Panel header={field.name || idx} key={index} extra={genExtra({ field, index, addGoden, removeGoden, godenChange, godenOff })} className="nv-custom-panel">
              <Form.Item
                label={schema.jsonSchema.properties["name"]["title"]}
                name={[`${name}-${idx}-${Date.now()}`, 'name']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["name"]}
                {...formItemLayout}
              >
                <Input name={'name'} onChange={e => godenChange(e, index)} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["answerA"]["title"]}
                name={[`${name}-${idx}-${Date.now()}`, 'answerA']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["answerA"]}
                {...formItemLayout}
              >
                <Input name={'answerA'} onChange={e => godenChange(e, index)} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["answerB"]["title"]}
                name={[`${name}-${idx}-${Date.now()}`, 'answerB']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["answerB"]}
                {...formItemLayout}
              >
                <Input name={'answerB'} onChange={e => godenChange(e, index)} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["answerC"]["title"]}
                name={[`${name}-${idx}-${Date.now()}`, 'answerC']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["answerC"]}
                {...formItemLayout}
              >
                <Input name={'answerC'} onChange={e => godenChange(e, index)} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["answerD"]["title"]}
                name={[`${name}-${idx}-${Date.now()}`, 'answerD']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["answerD"]}
                {...formItemLayout}
              >
                <Input name={'answerD'} onChange={e => godenChange(e, index)} />
              </Form.Item>
              <Form.Item
                label={schema.jsonSchema.properties["correctAnswer"]["title"]}
                name={[`${name}-${idx}-${Date.now()}`, 'correctAnswer']}
                rules={[{ required: true, message: `Không được trống` }]}
                initialValue={field["correctAnswer"]}
                {...formItemLayout}
              >
                <CorrectAnswer name={'correctAnswer'} onChange={val => {
                  godenChange({
                    target: {
                      name: 'correctAnswer',
                      value: val
                    }
                  }, index);
                }} />
              </Form.Item>
            </Panel>
          );
        })}
      </Collapse>
      {godens.length <= 0 && <Button type="dashed" onClick={addGoden}>Thêm câu hỏi</Button>}
    </>
  );
};

export default CollapseItem;
