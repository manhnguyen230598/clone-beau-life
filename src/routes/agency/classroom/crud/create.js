import React, { useState, useEffect } from 'react';
import { Card, Steps } from 'antd';
import { connect } from 'dva';
import IntlMessages from "util/IntlMessages";
import Step1 from './components/Step1';
import Step2 from './components/Step2';
import Step3 from './components/Step3';
import Step4 from './components/Step4';
import Step5 from './components/Step5';

const { Step } = Steps;
const getCurrentStepAndComponent = (current) => {
  switch (current) {
    case 'classlesson':
      return { step: 1, component: <Step2 /> };
    case 'examination':
      return { step: 2, component: <Step3 /> };
    case 'userclass':
      return { step: 3, component: <Step4 /> };
    case 'result':
      return { step: 4, component: <Step5 /> };
    case 'info':
      return { step: 0, component: null };
    case 'classroom':
    default:
      return { step: 0, component: <Step1 /> };
  }
};

const ClassRoomCreate = ({ current, intl }) => {
  const [stepComponent, setStepComponent] = useState(<Step1 />);
  const [currentStep, setCurrentStep] = useState(0);

  useEffect(() => {
    const { step, component } = getCurrentStepAndComponent(current);
    setCurrentStep(step);
    setStepComponent(component);
  }, [current]);

  return (
    <React.Fragment>
      <Card bordered={false}>
        <>
          <Steps current={currentStep} className="">
            <Step title={<IntlMessages id="classroom.info" />} />
            <Step title={<IntlMessages id="classroom.classlesson" />} />
            <Step title={<IntlMessages id="classroom.examination" />} />
            <Step title={<IntlMessages id="classroom.classuser" />} />
            <Step title={<IntlMessages id="classroom.createresult" />} />
          </Steps>
        </>
      </Card>
      <Card bordered={false}>
        {stepComponent}
      </Card>
    </React.Fragment>
  );
};

export default connect(({ classroom }) => ({
  current: classroom.current,
}))(ClassRoomCreate);
