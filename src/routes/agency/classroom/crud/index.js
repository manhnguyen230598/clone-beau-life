import React, { useEffect, useState } from 'react';
import { Card, Button, Form, Col, Row, Popover } from 'antd';
import _ from 'lodash';
import dayjs from 'dayjs';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import Upload from 'components/Upload';
import ProjectSelect from 'components/Select/Project/ListProject';
import UserSelect from 'components/Select/User/ListUser';
// import * as enums from 'util/enums';
import AgencySelect from 'components/Select/Agency/ListAgency';


const formItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

const form3ItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};



const RESOURCE = "classroom";
const fieldLabels = { "name": "Tên lớp học", "thumbnail": "Ảnh đại diện", "startDate": "Ngày bắt đầu", "endDate": "Ngày kết thúc", "teacherName": "Giáo viên", "projectId": "Dự án", "agencyId": "Đại lý", "description": "Mô tả", "isActive": "Trạng thái", "isViewAll": 'Xem hết', "type": "Loại bài giảng", "price": "Giá", "useFor": "Loại áp dụng", "hotSequence": "Thứ tự nổi bật" };
const ClassroomForm = ({ classroom: { formTitle, formData }, dispatch, submitting, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');
  // const { validateFields } = form;
  // const onValidateForm = async () => {
  //   const values = await validateFields();
  //   if (dispatch) {
  //     dispatch({
  //       type: `${RESOURCE}/saveStepFormData`,
  //       payload: {
  //         ...data,
  //         classroom: {
  //           ...data.classroom,
  //           ...values
  //         }
  //       },
  //     });
  //     dispatch({
  //       type: `${RESOURCE}/saveCurrentStep`,
  //       payload: 'classlesson',
  //     });
  //   }
  // };

  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);

    if (data.startDate) {
      data.startDate = dayjs(data.startDate).valueOf();
    }
    if (data.endDate) {
      data.endDate = dayjs(data.endDate).valueOf();
    }
    if (data.isActive == "true") {
      data.isActive = true;
    } else if (data.isActive == "false") {
      data.isActive = false;
    }
    if (data.projectId && typeof data.projectId == 'object') {
      data.projectId = data.projectId.id;
    }
    if (data.agencyId && typeof data.agencyId == 'object') {
      data.agencyId = data.agencyId.id;
    }
  
    if (data.agencyIds && Array.isArray(data.agencyIds)) {
      data.agencyIds = data.agencyIds.map(i => i.id ? i.id : i);
    }

    if (data.teacher && typeof data.teacher == 'object') {
      data.teacher = data.teacher.id;
    }
    if (data.thumbnail) {
      data.thumbnail =
        data.thumbnail && data.thumbnail.length > 0
          ? data.thumbnail
            .map(
              (i) =>
                i.url ||
                (i.response 
                  ? i.response.url
                  : ''),
            )
            .toString()
          : '';
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: (res) => {
        if (res && !res.error) {
          // history.goBack();
        }
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  });

  if (params.id === 'add' || (formData && formData.id && formData.id !== 'add')) {
    return (
      <Form
        {...formItemLayout}
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="horizontal"
        className="ant-advanced-search-form"
        hideRequiredMark
      // initialValues={data.classroom}
      >
        <Card className="gx-card">
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['name']}
                name="name"
                rules={[{ required: true, message: `${fieldLabels['name']} không được trống` }]}
              >
                <FormInputRender
                  item={{ "title": "Tên lớp học", "dataIndex": "name", "width": 200, "hasFilter": true, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 1, "col": 1 } }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['thumbnail']}
                name="thumbnail"
                rules={[{ required: true, message: `${fieldLabels['thumbnail']} không được trống` }]}
              >
                <Upload />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['startDate']}
                name="startDate"
                rules={[{ required: true, message: `${fieldLabels['startDate']} không được trống` }]}
              >
                <FormInputRender
                  item={{ "title": "Ngày bắt đầu", "dataIndex": "startDate", "width": 120, "valueType": "date", "hasFilter": false, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 2, "col": 1 } }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['endDate']}
                name="endDate"
                rules={[{ required: true, message: `${fieldLabels['endDate']} không được trống` }]}
              >
                <FormInputRender
                  item={{ "title": "Ngày kết thúc", "dataIndex": "endDate", "width": 120, "valueType": "date", "hasFilter": false, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 2, "col": 2 } }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['teacherName']}
                name="teacher"
                rules={[{ required: true, message: `${fieldLabels['teacher']} không được trống` }]}
              >
                <UserSelect type="radio" paramsList={{roleId : [1,14,15]}}/>
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['projectId']}
                name="projectId"
                rules={[{ required: false, message: `${fieldLabels['projectId']} không được trống` }]}
              >
                <ProjectSelect type="radio" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['agencyId']}
                name="agencyIds"
                rules={[{ required: true, message: `${fieldLabels['agencyId']} không được trống` }]}
              >
                <AgencySelect modeChoose="checkbox" />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['isActive']}
                name="isActive"
                rules={[{ required: true, message: `${fieldLabels['isActive']} không được trống` }]}
              >
                <FormInputRender
                  item={{ "title": "Trạng thái", "dataIndex": "isActive", "width": 200, "filters": true, "hasFilter": false, "hideInTable": false, "hideInSearch": false, "valueEnum": { "true": { "text": "Hoạt động", "status": "Processing", "color": "#ec3b3b", "isText": true }, "false": { "text": "Dừng", "status": "Default", "color": "#ec3b3b", "isText": true } }, "formPattern": { "card": "Thông tin khác", "row": 2, "col": 1 } }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['hotSequence']}
                name="hotSequence"
                rules={[{ required: true, message: `${fieldLabels['hotSequence']} không được trống` }]}
              >
                <FormInputRender
                  item={{ "title": `${fieldLabels['hotSequence']}`, "dataIndex": "hotSequence", "width": 200, "hasFilter": true, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 1, "col": 1 } }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['useFor']}
                name="useFor"
                rules={[{ required: true, message: `${fieldLabels['useFor']} không được trống` }]}
              >
                <FormInputRender
                  item={{ "title": `${fieldLabels['useFor']}`, "dataIndex": "type", "width": 200, "valueEnum": { "project": { "text": "Dự án", "status": "Processing", "color": "#ec3b3b", "isText": true }, "general": { "text": "Chung", "status": "Default", "color": "#ec3b3b", "isText": true } }, "valueType": "radioGroup" }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                {...form3ItemLayout}
                label={fieldLabels['isViewAll']}
                name="isViewAll"
                rules={[{ required: true, message: `${fieldLabels['isViewAll']} không được trống` }]}
                valuePropName="checked"
              >
                <FormInputRender
                  item={{ "title": `${fieldLabels['isViewAll']}`, "dataIndex": "isViewAll", "width": 120, "valueType": "switch", formItemProps: { checkedChildren: "Xem hết", unCheckedChildren: "Không" } }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                {...form3ItemLayout}
                label={fieldLabels['type']}
                name="type"
                rules={[{ required: true, message: `${fieldLabels['type']} không được trống` }]}
              >
                <FormInputRender
                  item={{ "title": `${fieldLabels['type']}`, "dataIndex": "type", "width": 200, "valueEnum": { "free": { "text": "Miễn phí", "status": "Processing", "color": "#ec3b3b", "isText": true }, "pay": { "text": "Trả phí", "status": "Default", "color": "#ec3b3b", "isText": true } }, "valueType": "radioGroup" }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['description']}
                name="description"
                rules={[{ required: false, message: `${fieldLabels['description']} không được trống` }]}
              // labelCol={{
              //   span: 4,
              //   offset: 0
              // }}
              // wrapperCol={{
              //   xs: { span: 24, offset: 0 },
              //   sm: {
              //     span: 21,
              //     offset: 2,
              //   },
              // }}
              >
                <FormInputRender
                  item={{ "title": "Mô tả", "dataIndex": "description", valueType: 'textarea' }}
                  type="form"
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                {...form3ItemLayout}
                shouldUpdate={(prevValues, curValues) => prevValues.type !== curValues.type}
                noStyle
              >
                {({ getFieldValue }) => {
                  return getFieldValue("type") == 'pay' ?
                    (
                      <Form.Item
                        label={fieldLabels['price']}
                        name="price"
                        rules={[{ required: true, message: `${fieldLabels['price']} không được trống` }]}
                      >
                        <FormInputRender
                          item={{ "title": `${fieldLabels['price']}`, "dataIndex": "price", "width": 200, "hasFilter": true, "hideInTable": false, "hideInSearch": false, "formPattern": { "card": "Thông tin cơ bản", "row": 1, "col": 1 } }}
                          intl={intl}
                        />
                      </Form.Item>
                    ) : null;
                }}
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            wrapperCol={{
              xs: { span: 24, offset: 0 },
              sm: {
                span: formItemLayout.wrapperCol.span,
                offset: formItemLayout.labelCol.span,
              },
            }}
          >
          </Form.Item>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack(); }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <>
    <span>Không lấy được thông tin bản ghi</span>
  </>;
};

export default connect(({ classroom, loading, router }) => ({
  submitting: loading.effects['classroom/submit'],
  classroom,
  router
}))(ClassroomForm);
