import { useState, useEffect } from 'react';
import get from 'lodash/get';
import { usePrevious } from 'util/helpers';
import * as classServices from 'services/classroom';

const useClassInfo = ({
  id
}) => {
  const [classId, setClassId] = useState(id);
  const preClassId = usePrevious(classId);
  const [classInfo, setClassInfo] = useState(null);

  useEffect(() => {
    if (typeof preClassId == 'undefined' && preClassId != classId) {
      classServices.get(Number(classId), {}, false).then(res => {
        setClassInfo(get(res, "data.data[0]", {}));
      });
    }
  }, [classId]);

  return {
    classInfo,
    setClassId
  };
};

export default useClassInfo;
