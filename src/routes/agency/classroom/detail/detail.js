import React, { } from "react";
import { Card, Tabs } from "antd";
import { router } from 'dva';
import asyncComponent from "util/asyncComponent";
import IntlMessages from "util/IntlMessages";
import useClassInfo from './hook/useClassInfo';

const TabPane = Tabs.TabPane;
const { Redirect, Switch, Route, Link } = router;
const ClassDetailRoute = ({ component: Component, classId, ...rest }) =>
  <Route
    {...rest}
    render={props => <Component {...props} classId={Number(classId)} />}
  />;

const ClassInSide = ({ match, location, history, ...rest }) => {
  const { classInfo } = useClassInfo({ id: match.params.id });

  /* useEffect(() => {
    classServices.get(Number(match.params.id), {}, false).then(res => {
      setClassInfo(get(res, "data.data[0]", {}));
    });
  }, [match.params.id]); */

  /* if (!classInfo){
    return null;
  } */

  function callback(key) {
    switch (key) {
      case 'userdoexamlog':
        history.push({ pathname: `${match.url}/userdoexamlog` });
        break;
      case 'classlesson':
        history.push({ pathname: `${match.url}/classlesson` });
        break;
      case 'examination':
        history.push({ pathname: `${match.url}/examination` });
        break;
      case 'userclass':
        history.push({ pathname: `${match.url}/userclass` });
        break;
      case 'feedbackclass':
        history.push({ pathname: `${match.url}/feedbackclass` });
        break;
      case 'reportclass':
        history.push({ pathname: `${match.url}/reportclass` });
        break;
      default:
        break;
    }
  }

  const tabList = [
    {
      key: 'classlesson',
      tab: <IntlMessages id="classroom.classlesson" />,
    },
    {
      key: 'examination',
      tab: <IntlMessages id="classroom.examination" />,
    },
    {
      key: 'userclass',
      tab: <IntlMessages id="classroom.classuser" />,
    },
    {
      key: 'feedbackclass',
      tab: <IntlMessages id="classroom.feedbackclass" />,
    },
    {
      key: 'userdoexamlog',
      tab: <IntlMessages id="classroom.userdoexamlog" />,
    },
    {
      key: 'reportclass',
      tab: <IntlMessages id="classroom.reportclass" />,
    },
  ];

  const getTabKey = () => {
    const url = match.url === '/' ? '' : match.url;
    const tabKey = location.pathname.replace(`${url}`, '');
    if (tabKey && tabKey !== '') {
      return tabKey.replace(/\//g, "");
    }
    return 'classlesson';
  };

  return (
    <Card className="gx-card" title={<Link to="/agency/classroom">{`Mã lớp ${match.params.id} - ${(classInfo || {}).name || ""} `}</Link>}>
      <Tabs
        // defaultActiveKey="1"
        onChange={callback}
        activeKey={getTabKey()}
      >
        {tabList.map((item, index) => (
          <TabPane {...item} tab={item.tab} key={item.key || index} />
        ))}
      </Tabs>
      <Switch>
        {location?.state ? <Redirect exact from={`${match.url}/`} to={{
          pathname: `${match.url}/${getTabKey()}`,
          state: { name: location?.state }
        }} />
          : <Redirect exact from={`${match.url}/`} to={`${match.url}/${getTabKey()}`} />}

        <Route path={`${match.url}/documents/:id`} component={asyncComponent(() => import("../../documents/crud"))} />
        <Route path={`${match.url}/documents`} component={asyncComponent(() => import("../../documents/list"))} exact={true} />
        {/* <Route path={`${match.url}/questionexam`} component={asyncComponent(() => import("../../questionexam/list"))} exact={true} /> */}
        <Route path={`${match.url}/classlesson/:id`} component={asyncComponent(() => import("../../classlesson/crud"))} />
        <Route path={`${match.url}/userclass/:id`} component={asyncComponent(() => import('../../userclass/crud'))} />
        <Route path={`${match.url}/examination/:id`} component={asyncComponent(() => import('../../examination/crud'))} />
        <Route path={`${match.url}/examinations/:id/questionexam/:id`} component={asyncComponent(() => import('../../questionexam/crud'))} />
        <ClassDetailRoute path={`${match.url}/examinations/:id/questionexam`} classId={match.params.id} component={asyncComponent(() => import("../../questionexam/list"))} exact={true} />
        <ClassDetailRoute path={`${match.url}/classlesson`} classId={match.params.id} component={asyncComponent(() => import("../../classlesson/list"))} exact={true} />
        <ClassDetailRoute path={`${match.url}/userclass`} classId={match.params.id} component={asyncComponent(() => import("../../userclass/list"))} exact={true} />
        <ClassDetailRoute path={`${match.url}/feedbackclass`} classId={match.params.id} component={asyncComponent(() => import("../../feedbackclass/list"))} exact={true} />
        <ClassDetailRoute path={`${match.url}/examination`} classId={match.params.id} component={asyncComponent(() => import("../../examination/list"))} exact={true} />
        {/* <ClassDetailRoute path={`${match.url}/reportclass`} classId={match.params.id} component={asyncComponent(() => import("../../reportclass/info"))} exact={true} /> */}
        <ClassDetailRoute path={`${match.url}/reportclass`} classId={match.params.id} component={asyncComponent(() => import("../../reportclass/list"))} exact={true} />
        <ClassDetailRoute path={`${match.url}/userdoexamlog`} classId={match.params.id} component={asyncComponent(() => import("../../userDoExamLog/list"))} exact={true} />
      </Switch>
    </Card>
  );
};

export default ClassInSide;
