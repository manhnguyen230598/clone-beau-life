
import React from 'react';
import { Image, Drawer, Row, Col } from 'antd';
// import TableStatus from 'packages/pro-table/component/status';
import get from 'lodash/get';
// import * as enums from 'util/enums';
import { formatDate } from 'util/helpers';

const DescriptionItem = ({ title, content }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const ImagesItem = ({ title, images }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <div>
      {(images || []).map(item => (
        <Image width={80} src={item.url} />
      ))}
    </div>
  </div>
);

const ClassroomDetail = ({ record, drawerVisible, onChange }) => {
  const triggerChange = (v) => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };
  /*   const genderStatus = get(enums.genderEnum[get(record, "gender", "-")], "status", null);
    const Status = TableStatus[genderStatus || 'Init'];
    const genderText = get(enums.genderEnum[get(record, "gender", "-")], "text", "-"); */

  return (
    <Drawer
      width={640}
      style={{ width: "640px !important" }}
      className="nv-drawer-detail"
      title={<span style={{ color: "#f09b1b" }}>{`Chi tiết ${get(record, "name", "-").toUpperCase()}`}</span>}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="ID" content={get(record, "id" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tên lớp học" content={get(record, "name" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
          <ImagesItem title="Ảnh đại diện" images={get(record, "thumbnail", [])} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Ngày bắt đầu" content={formatDate(get(record, "startDate" , "-" ))} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Ngày kết thúc" content={formatDate(get(record, "endDate" , "-" ))} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Giáo viên" content={get(record, "teacherName" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Dự án" content={get(record, "projectId.name" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Môi giới" content={get(record, "agencyId.name", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Xem hết hoặc không" content={get(record, "isViewAll", "-") ? "Có" : "Không"} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Loại bài giảng" content={get(record, "type", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Giá" content={get(record, "price", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Thứ tự nổi bật" content={get(record, "hotSequence", "-")} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Mô tả" content={get(record, "description", "-")} />
        </Col>
      </Row>
    </Drawer>
  );
};

export default ClassroomDetail;
