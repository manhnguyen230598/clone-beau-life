import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Space, Tooltip, Avatar } from 'antd';
import { SearchOutlined, PlusOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons';
import { connect, routerRedux, router } from 'dva';
import ProTable from 'packages/pro-table/Table';
import ClassroomDetail from '../detail';
import { getValue, setTableChange, getTableChange } from 'util/helpers';
import { get, union } from 'lodash';

const { Link } = router;
const tranformClassParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "classroom";
const ClassroomList = (props) => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ? Number(process.env.REACT_APP_PAGESIZE) : 10);
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState((origin) => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  /* const [pagination, setPagination] = useState({
    pageSize: limit
  }); */
  const [columnsStateMap, setColumnsStateMap] = useState({
    // id: { show: false }
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign({}, (tableChange?.pagination) || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformClassParams(tableChange?.filtersArg, tableChange?.formValues, tableChange?.sort);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * (pagi.pageSize || 10),
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: union(initParams.sort || [], [{ "createdAt": "desc" }]),
        queryInput: {
          ...initParams.filters,
        },
        populate: "projectId,agencyId"
      }
    });
  }, []);

  /* useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]); */

  const handleTableChange = (pagi, filtersArg, sorter) => {
    console.log(`🚀 ~ file: index.js ~ line 100 ~ handleTableChange ~ pagi`, pagi);
    console.log(`🚀 ~ file: index.js ~ line 99 ~ handleTableChange ~ sorter`, sorter);
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformClassParams(filters, formValues, sorter);
    console.log(`🚀 ~ file: index.js ~ line 106 ~ handleTableChange ~ initParams`, initParams);

    if (sessionStorage.getItem('isReset') === 'true') {
      sessionStorage.setItem('isReset', false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      console.log("initParams.sort", initParams.sort);
      console.log("(initParams.sort || []).filter(i => !i.createdAt)", union(initParams.sort || [], [{ "createdAt": "desc" }]));
      params.sort = union(initParams.sort || [], [{ "createdAt": "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagi.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        ...params,
        populate: "projectId,agencyId"
      }
    });
  };

  const handleTableSearch = () => {
    form.current.validateFields().then(fieldsValue => {
      let values = {};
      Object.keys(fieldsValue).forEach(key => {
        if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
          values[key] = fieldsValue[key];
        }
      });

      if (sessionStorage.getItem('isReset') === 'true') {
        sessionStorage.setItem('isReset', 'false');
        values = {};
      }
      const initParams = tranformClassParams(tableChange?.filters, values, tableChange?.sort);
      props.history.push({
        search: "?" + new URLSearchParams(values).toString()
      });
      setTableChange(RESOURCE, {
        ...tableChange,
        pagination: {
          ...tableChange.pagination,
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit,
          sort: union(initParams.sort || [], [{ "createdAt": "desc" }])
        },
        formValues: values
      });
      setFormValues(() => values);
      dispatch({
        type: `${RESOURCE}/fetch`,
        payload: {
          queryInput: {
            ...initParams.filters
          },
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit,
          populate: "projectId,agencyId"
        }
      });
    }).catch(err => {
      if (err) return;
    });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0],
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: '',
    });
    confirm();
  };

  const handleAddClick = () => {
    dispatch(routerRedux.push({ pathname: `/agency/classroom-create` }));
  };

  const handleEditClick = item => {
    dispatch(routerRedux.push({ pathname: `${props.match.url}/${item.id}` }));
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 60,
      fixed: 'left',
      hideInSearch: true,
      sorter: true,
    },
    {
      title: 'Tên lớp học',
      dataIndex: 'name',
      width: 200,
      sorter: true,
      render: (val, record) => {
        return (
          <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
            <div style={{ textTransform: 'uppercase' }}>
              <Avatar src={get(record, "thumbnail[0].url", "")} />
              <Link to={{
                pathname: `/agency/classroom-detail/${record.id}`,
                state: record.name
              }}>
                <span style={{ marginLeft: '5px' }}>{val}</span>
              </Link>
            </div>
            <a style={{ fontSize: '12px', marginLeft: '5px' }}
              onClick={() => {
                setDrawerDetail({
                  visible: true,
                  record
                });
              }}>
              <Tooltip title="Chi tiết">
                <EyeOutlined />
              </Tooltip>
            </a>
          </div>
        );
      },
      ...getColumnSearchProps('name'),
    },
    {
      title: 'Ngày bắt đầu',
      dataIndex: 'startDate',
      valueType: 'date',
      width: 120,
      hideInSearch : true
    },
    {
      title: 'Ngày kết thúc',
      dataIndex: 'endDate',
      valueType: 'date',
      width: 120,
      hideInSearch : true
    },
    {
      title: 'Giáo viên',
      dataIndex: 'teacherName',
      width: 200,
      hideInSearch: true,
    },
    {
      title: 'Dự án',
      dataIndex: ["projectId", "name"],
      width: 200,
      hideInSearch: true,
      hideInTable: false,
    },
    {
      title: 'Đại lý',
      dataIndex: ["agencyId", "name"],
      width: 200,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Thứ tự',
      dataIndex: 'hotSequence',
      width: 200,
      hideInSearch: true,
      hideInTable: false,
      sorter : true
    },
    {
      title: 'Mô tả',
      dataIndex: 'description',
      width: 200,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Trạng thái',
      dataIndex: 'isActive',
      width: 200,
      filters: true,
      valueEnum: {
        "true": { "text": "Hoạt động", "status": "Processing", "color": "#ec3b3b", "isText": "true", },
        "false": { "text": "Dừng", "status": "Default", "color": "#ec3b3b", "isText": "true", },
      }
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
      width: 150,
      hideInSearch : true
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updatedAt',
      valueType: 'dateTime',
      width: 150,
      hideInSearch : true
    },
    {
      title: 'Thao tác',
      width: 150,
      fixed: 'right',
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      ),
    },
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Lớp học"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={(mapCols) => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={(searchParams) => {
          return {};
        }}
        onReset={() => {
          console.log("reset");
          // setIsReset(true);
          sessionStorage.setItem('isReset', 'true');
          // form.resetFields();
          // setFormValues({})
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit,
            },
            filtersArg: {}, sorter: {}, formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
            {`Thêm mới`}
          </Button>
        ]}
      />
      <ClassroomDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={(v) => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ classroom, loading }) => ({
  classroom,
  loading: loading.models.classroom
}))(ClassroomList);
