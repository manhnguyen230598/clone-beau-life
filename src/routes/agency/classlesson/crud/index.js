import React, { useEffect, useState } from 'react';
import { Button, Form, Popover, Input, Switch } from 'antd';
import _ from 'lodash';
import { CloseCircleOutlined, DownCircleOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import FooterToolbar from 'packages/FooterToolbar';
import Upload from 'components/Upload';
import UploadFile from 'components/UploadFile';


const formItemLayout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 24,
  },
};

const formItem3Layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 17 },
    md: { span: 17 }
  }
};


const RESOURCE = "classlesson";
const fieldLabels =
{
  "name": "Tên bài giảng",
  "thumbnail": "Hình ảnh",
  "classId": "Lớp học",
  "sequence": "Thứ tự",
  "isActive": "Trạng thái",
  "documents": "Tài liệu"
};



const ClasslessonForm = ({ classlesson: { formTitle, formData,classId }, dispatch, submitting, match: { params }, location, history, ...rest }) => {
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');

  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }

    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    if(data.sequence){
      data.sequence = +data.sequence;
    }
    if (data.thumbnail) {
      data.thumbnail =
        data.thumbnail && data.thumbnail.length > 0
          ? data.thumbnail
            .map(
              (i) =>
                i.url ||
                (i.response
                  ? i.response.url
                  : ''),
            )
            .toString()
          : '';
    }

    if (data.linkPdf && data.type) {
      if (data.linkPdf) {
        data.linkPdf =
          data.linkPdf && data.linkPdf.length > 0
            ? data.linkPdf
              .map(
                (i) =>
                  i.url ||
                  (i.response && i.response.length > 0
                    ? i.response[0].url
                    : ''),
              )
              .toString()
            : '';
      }
    }
    if (classId) {
      data.classId = classId;
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: (res) => {
        if (res && !res.error) {
          dispatch({
            type: `documents/submit`,
            payload: {
              classLessonId: res.data.id,
              formType: params.id !== 'add' ? 'E' : 'A',
              type: data.type ? "pdf" : "video",
              name: data.nameDocument,
              link: data.type ? data.linkPdf: data.linkVideo,
              id: data.documents.id,
            },
          });
        }
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };


  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });


  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  });

  if (params.id === 'add' || (formData && formData.id && formData.id !== 'add')) {
    return (
      <Form
        {...formItemLayout}
        form={form}
        layout="horizontal"
        className="ant-advanced-search-form"
        onFinishFailed={onFinishFailed}
        onFinish={onFinish}
      >

        <Form.Item
          label={fieldLabels["name"]}
          name={'name'}
          rules={[{ required: true, message: `Không được trống` }]}
          {...formItemLayout}
        >
          <Input name={'name'} />
        </Form.Item>
        <Form.Item
          label={fieldLabels["sequence"]}
          name={'sequence'}
          rules={[{ required: false, message: `Không được trống` }]}
          {...formItemLayout}
        >
          <Input name={'sequence'} />
        </Form.Item>
        <Form.Item
          label={fieldLabels["thumbnail"]}
          name={'thumbnail'}
          rules={[{ required: false, message: `Không được trống` }]}
          {...formItemLayout}
        >
          <Upload mode="simple" />
        </Form.Item>
        <Form.Item
          label={fieldLabels["documents"]}
          name={'documents'}
          // rules={[{ required: false, message: `Không được trống` }]}
          {...formItemLayout}
        >
          <Document documents={formData.documents} name="documents"
          />
        </Form.Item>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack(); }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

const Document = ({ value: initValue, onChange,type,documents}) => {
  const [value, setValue] = useState({ ...initValue });
  const handleChange = (e) => {
    const curName = e.target.name;
    const curVal = e.target.value;
    setValue(origin => {
      if (onChange) {
        onChange({
          ...origin,
          [curName]: curVal
        });
      }
      return {
        ...origin,
        [curName]: curVal
      };
    });
  };

  const handleChangeFile = (file) => {
    const curFile = file;
    setValue(origin => {
      if (onChange) {
        onChange({
          ...origin,
          link: curFile[0] || {}
        });
      }
      return {
        ...origin,
        link: curFile[0] || {}
      };
    });
  };

  return (
    <React.Fragment>
      {/* <Form.Item
        label={"Tên tài liệu"}
        name={'nameDocument'}
        initialValue={documents?.name || ""}
        rules={[{ required: true, message: `Không được trống` }]}
        {...formItem3Layout}
      >
        <Input onChange={handleChange} />
      </Form.Item> */}
      <Form.Item
        label={"Kiểu tài liệu"}
        name={'type'}
        rules={[{ required: true, message: `Không được trống` }]}
        initialValue={!documents?.type ? false:(documents?.type)}
        valuePropName="checked"
        {...formItem3Layout}
      >
        <Switch checkedChildren="pdf" unCheckedChildren="video" onChange={val => {
          handleChange({
            target: {
              name: 'type',
              value: val
            }
          });
        }} />
      </Form.Item>
      <Form.Item
        label={"Đường dẫn"}
        rules={[{ required: true, message: `Không được trống` }]}
        help={<>
          <small>(*) Tài liệu chỉ nhận file pdf hoặc link video</small>
          <div><a className="gx-ml-3" href={`/static/huong_dan_lay_link_you_tube.docx`} download><DownCircleOutlined />&nbsp;Hướng dẫn lấy link youtube</a></div>
        </>}
        shouldUpdate={(prevValues, curValues) => prevValues.type !== curValues.type}
        {...formItem3Layout}
      >
        {({ getFieldValue }) => {
          const type = getFieldValue('type');
          if (type) {
            return (
              <Form.Item
                name={'linkPdf'}
                initialValue={documents?.linkPdf || ""}
              >
                <UploadFile autoUpload={true} accept=".pdf" onChange={handleChangeFile} />
              </Form.Item>
            );
          }
          return (
            <Form.Item
              name='linkVideo'
              initialValue={documents?.linkVideo || ""}
            >
              <Input onChange={handleChange} />
            </Form.Item>
          );
        }}
      </Form.Item>
    </React.Fragment>
  );
};

export default connect(({ classlesson, loading, router }) => ({
  submitting: loading.effects['classlesson/submit'],
  classlesson,
  router
}))(ClasslessonForm);
