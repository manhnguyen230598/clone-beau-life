import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Space, Tooltip, Switch } from 'antd';
import { SearchOutlined, PlusOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import ProTable from 'packages/pro-table/Table';
// import { get } from 'lodash';
import ClasslessonDetail from '../detail';
import { getValue, setTableChange, getTableChange } from 'util/helpers';


const tranformClasslessonParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter) {
    initSort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "classlesson";
const ClasslessonList = (props) => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ?
    Number(process.env.REACT_APP_PAGESIZE) : 10);
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState((origin) => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, (tableChange?.pagination) || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformClasslessonParams(tableChange?.filtersArg, tableChange?.formValues, tableChange?.sort);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort || [
          { "createdAt": "desc" }
        ],
        queryInput: {
          ...initParams.filters,
          ["classId"]: props.classId
        }
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformClasslessonParams(filters, formValues, sorter);

    if (sessionStorage.getItem('isReset') === 'true') {
      sessionStorage.setItem('isReset', false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        ["classId"]: props.classId
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [{ [sorter.field]: sorter.order === 'ascend' ? "asc" : "desc" }];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current.validateFields().then(fieldsValue => {
      console.log('handleSearch -> fieldsValue', fieldsValue);
      let values = {};
      Object.keys(fieldsValue).forEach(key => {
        if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
          values[key] = fieldsValue[key];
        }
      });

      if (sessionStorage.getItem('isReset') === 'true') {
        sessionStorage.setItem('isReset', 'false');
        values = {};
      }
      const initParams = tranformClasslessonParams(tableChange?.filters, values, tableChange?.sort);

      setTableChange(RESOURCE, {
        ...tableChange,
        pagination: {
          ...tableChange.pagination,
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit,
        },
        formValues: values
      });
      setFormValues(() => values);
      dispatch({
        type: `${RESOURCE}/fetch`,
        payload: {
          queryInput: {
            ...initParams.filters
          },
          page: 1,
          current: 1,
          skip: 0,
          limit,
          pageSize: limit,
          queryInput: {
            ...initParams.filters,
            ["classId"]: props.classId
          },
        }
      });
    }).catch(err => {
      if (err) return;
    });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0],
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: '',
    });
    confirm();
  };

  const handleAddClick = () => {
    dispatch({
      type: `${RESOURCE}/storeClassId`,
      payload: props.classId
    });
    dispatch(routerRedux.push({ pathname: `${props.match.url}/add` }));
  };

  const handleEditClick = item => {
    dispatch({
      type: `${RESOURCE}/storeClassId`,
      payload: props.classId
    });
    dispatch(routerRedux.push({ pathname: `${props.match.url}/${item.id}` }));
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 60,
      fixed: 'left',
      hideInSearch: true,
    },
    {
      title: 'Tên bài giảng',
      dataIndex: 'name',
      width: 200,
      render: (value, record) => {
        return (<>
          {value}
          <a style={{ fontSize: '12px', marginLeft: '5px' }}
            onClick={() => {
              setDrawerDetail({
                visible: true,
                record
              });
            }}>
            <Tooltip title="Chi tiết">
              <EyeOutlined />
            </Tooltip>
          </a>
        </>
        );
      },
      ...getColumnSearchProps('name'),
    },
    {
      title: 'Hình ảnh',
      dataIndex: 'thumbnail',
      width: 200,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Lớp học',
      dataIndex: 'classId',
      width: 200,
      hideInSearch: true,
    },
    {
      title: 'Vị trí',
      dataIndex: 'sequence',
      width: 200,
      hideInSearch: true,
    },
    {
      title: 'Trạng thái',
      dataIndex: 'isActive',
      width: 200,
      filters: true,
      valueEnum: {
        "true": { "text": "Hoạt động", "status": "Processing", "color": "#ec3b3b", "isText": "true", },
        "false": { "text": "Dừng", "status": "Default", "color": "#ec3b3b", "isText": "true", },
      },
      render: (value, record) => {
        return (
          <>
            <Switch defaultChecked={record.isActive}
              checkedChildren="Hoạt động"
              unCheckedChildren="Dừng"
              onChange={(val) => {
                let data = {
                  isActive: val,
                  id: record.id
                };
                dispatch({
                  type: `${RESOURCE}/updateStatus`,
                  payload: data,
                  callback: (res) => {
                    if (res && !res.error) {

                    }
                  }
                });
              }}
            />
          </>
        );
      },
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
      width: 150,
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updatedAt',
      valueType: 'dateTime',
      width: 150,
    },
    {
      title: 'Thao tác',
      width: 150,
      fixed: 'right',
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      ),
    },
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Bài giảng"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        loading={loading}
        scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={(searchParams) => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem('isReset', 'true');
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit,
            },
            filtersArg: {}, sorter: {}, formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
            {`Thêm mới`}
          </Button>
        ]}
      />
      <ClasslessonDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={(v) => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ classlesson, loading }) => ({
  classlesson,
  loading: loading.models.classlesson
}))(ClasslessonList);
