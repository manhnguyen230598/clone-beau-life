import React from "react";
import { router } from "dva";
import asyncComponent from "util/asyncComponent";
import Main from "./main/index";
/* PLOP_INJECT_IMPORT */
// import Main from './main';
import Agency from './agency';




// import ManageProject from './manageProject';

import Dashboard from './dashboard/index';
import Project from './manage-project/index';
import StateProject from './state-project/index';
import Account from './account/index';

const { Route, Switch } = router;
const App = ({ match }) => {
  return (
    <div className="gx-main-content-wrapper">
      <Switch>
        <Route path={`/home`} component={asyncComponent(() => import("./home"))} />
        <Route path={`${match.url}dashboard`} component={Dashboard} />
        <Route path={`${match.url}main`} component={Main} />
        <Route path={`${match.url}account`} component={Account} />
        {/* PLOP_INJECT_EXPORT */}
<Route path={`${match.url}main`} component={ Main } />

        {/* <Route path={`${match.url}manage-project`} component={ ManageProject } /> */}
        <Route path={`${match.url}agency`} component={Agency} />
        <Route path={`${match.url}manage-project`} component={Project} />
        <Route path={`${match.url}state-project`} component={StateProject} />
        {/* <Route path={`${match.url}booking-apartment`} component={Booking} /> */}
        <Route path={`${match.url}booking-apartment`} component={asyncComponent(() => import("./booking/listProject/index"))} />
        <Route path={`${match.url}apartment-site`} component={asyncComponent(() => import("./booking//listApartment"))} />
        <Route path={`${match.url}customer-media`}  exact={true} component={asyncComponent(() => import("./customer/projectMediaCustomer/list"))}  />
        <Route path={`${match.url}customer-media/:id`}  component={asyncComponent(() => import("./customer/projectMediaCustomer/crud"))}  />
        <Route path={`${match.url}customer-vpbh`}  exact={true} component={asyncComponent(() => import("./customer/projectVisitingCustomer/list"))}  />
        <Route path={`${match.url}customer-vpbh/:id`}  component={asyncComponent(() => import("./customer/projectVisitingCustomer/crud"))}  />
      </Switch>
    </div>
  );
};

export default App;
