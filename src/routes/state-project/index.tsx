import React from "react";
import { router } from 'dva';
import type { RouteComponentProps } from 'react-router-dom';
import asyncComponent from "../../util/asyncComponent";

interface StateProjectProps extends RouteComponentProps {

}

const { Route, Switch } = router;
const StateProject: React.FC<StateProjectProps> = ({ match }: StateProjectProps) => {
  return (
    <Switch>
      {/* PLOP_INJECT_EXPORT */}
      <Route path={`${match.url}/checklist`} component={asyncComponent(() => import("./checklist/listProject"))} exact={true} />
      <Route path={`${match.url}/checklist/:type/:projectId`} component={asyncComponent(() => import("./checklist/jobs"))} />
    </Switch>
  );
};

export default StateProject;
