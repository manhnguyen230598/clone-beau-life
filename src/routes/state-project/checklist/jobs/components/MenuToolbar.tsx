import React, { FC } from 'react';
import { Menu, Divider, Calendar } from 'antd';
import { CalendarOutlined, UserOutlined, MehOutlined, TagOutlined, ArrowUpOutlined, AppstoreAddOutlined, DeleteOutlined } from '@ant-design/icons';
import IntlMessages from "../../../../../util/IntlMessages";

interface MenuToolbarProps{
  node: any
}

const { SubMenu } = Menu;
const MenuToolbar: FC<MenuToolbarProps> = ({ node }: MenuToolbarProps) => {

  function handleClick(e: any) {
    console.log('click', e);
  }

  return (
    <React.Fragment>
      <Menu onClick={handleClick} style={{ width: 256 }} mode="vertical">
        <SubMenu key="sub1" icon={<CalendarOutlined />} title={<IntlMessages id="checklist.menu.due" />}>
          <div style={{ width: '300px' }}>
            <Calendar fullscreen={false} />
          </div>
        </SubMenu>
        <SubMenu key="sub2" icon={<UserOutlined />} title={<IntlMessages id="checklist.menu.assgin" />}>
          <Menu.ItemGroup title="Item 1">
            <Menu.Item key="5">Option 1</Menu.Item>
            <Menu.Item key="6">Option 2</Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup title="Iteom 2">
            <Menu.Item key="7">Option 3</Menu.Item>
            <Menu.Item key="8">Option 4</Menu.Item>
          </Menu.ItemGroup>
        </SubMenu>
        <SubMenu key="sub3" icon={<MehOutlined />} title={<IntlMessages id="checklist.menu.status" />}>
          <Menu.Item key="9">Option 5</Menu.Item>
          <Menu.Item key="10">Option 6</Menu.Item>
          <SubMenu key="sub3" title="Submenu">
            <Menu.Item key="11">Option 7</Menu.Item>
            <Menu.Item key="12">Option 8</Menu.Item>
          </SubMenu>
        </SubMenu>
        <SubMenu key="sub4" icon={<TagOutlined />} title={<IntlMessages id="checklist.menu.tags" />}>
          <Menu.Item key="13">Option 9</Menu.Item>
          <Menu.Item key="14">Option 10</Menu.Item>
          <Menu.Item key="15">Option 11</Menu.Item>
          <Menu.Item key="16">Option 12</Menu.Item>
        </SubMenu>
        <SubMenu key="sub5" icon={<ArrowUpOutlined />} title={<IntlMessages id="checklist.menu.priority" />}>
          <Menu.Item key="17">Option 9</Menu.Item>
          <Menu.Item key="18">Option 10</Menu.Item>
          <Menu.Item key="19">Option 11</Menu.Item>
          <Menu.Item key="20">Option 12</Menu.Item>
        </SubMenu>
        <Divider className="nv-divider" />
        <Menu.Item key="21" icon={<AppstoreAddOutlined />} ><IntlMessages id="checklist.menu.addSubtask" /></Menu.Item>
        <Menu.Item key="22" icon={<DeleteOutlined />} ><IntlMessages id="checklist.menu.delete" /></Menu.Item>
      </Menu>
    </React.Fragment>
  );
};

export default React.memo(MenuToolbar);
