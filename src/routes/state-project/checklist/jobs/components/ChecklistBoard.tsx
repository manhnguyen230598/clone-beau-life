import React, { FC, memo } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import Container from './Board/index';

interface ChecklistBoardProps {
  data: any
};

const ChecklistBoard: FC<ChecklistBoardProps> = (props: ChecklistBoardProps) => {
  return (
    <div className="nv-checklist-board">
      <DndProvider backend={HTML5Backend}>
        <Container />
      </DndProvider>
    </div>
  );
};

export default memo(ChecklistBoard);
