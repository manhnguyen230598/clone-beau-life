import React, { FC, useState, useCallback, useEffect } from "react";
import { Tree, Input, Button, message } from "antd";
import {} from "@ant-design/icons";
import dayjs from "dayjs";
import update from "immutability-helper";
import {
  GroupJobProps,
  JobProps,
  TagProps,
  RoleUserProjectJobProps,
  ShowJobDetailType
} from "../interface";
// import MenuToolbar from './MenuToolbar';
import TitleRender from "./Content/TitleRender";
import GroupJobDetail from "./GroupJobDetail";
import JobDetail from "./JobDetail";
import Container from "../container";
import { findNode, findNodeJobId, tranformDateTime } from "./utils";
import StatusIcon from "./Content/StatusIcon";
import ModalCreateGroupTask from "./Content/ModalCreateGroupTask";
import ModalCreateTask from "./Content/ModalCreateTask";

interface ChecklistContentProps {
  data?: Array<GroupJobProps>;
  tags?: Array<TagProps> | any;
  createByTemplate?: boolean | null;
  employees?: Array<RoleUserProjectJobProps>;
  showEditJobDetail?: React.Dispatch<React.SetStateAction<ShowJobDetailType>>;
  expandAll?: boolean;
}

type TaskType = GroupJobProps | JobProps;

/* interface RightNode extends EventDataNode {
  id?: number | string;
  level?: number | string;
  props?: any
}

 interface TreeRightMenuClickProps {
  event: React.MouseEvent;
  node: RightNode
}; */

const ChecklistContent: FC<ChecklistContentProps> = (
  props: ChecklistContentProps
) => {
  const {
    // showEditJobDetail
  } = props;
  const counter = Container.useContainer();
  const [gData, setGData] = useState(counter.data);
  const expand = props.expandAll;
  const [expandKeys, setExpandKeys] = useState<any>([]);
  const showJobDetail = counter.showJobDetail;
  const setShowJobDetail = counter.setShowJobDetail;

  /* const menuRef = useRef(null);
  const [rightClickNodeTreeItem, setRightClickNodeTreeItem] = useState<any>(null);
  const [display, setDisplay] = useState('none'); */
  const [, setSelectedKeys] = useState([]);
  const [, setSelectedInfo] = useState({});
  const [isCreateFirstGroupTab, setIsCreateFirstGroupTab] = useState(false);
  const [firstGroupTabName, setFirstGroupTabName] = useState("");
  const [modalGroupTaskVisible, setModalGroupTaskVisible] = useState<{
    visible: boolean;
    nodeData: GroupJobProps | {};
  }>({
    visible: false,
    nodeData: {}
  });
  const [modalTaskVisible, setModalTaskVisible] = useState<{
    visible: boolean;
    nodeData: GroupJobProps | {};
  }>({
    visible: false,
    nodeData: {}
  });
  // const [node, setNode] = useState(null);
  useEffect(() => {
    if (expand) {
      let keys = gData.map(e => e.key);
      setExpandKeys(keys);
      console.log(keys);
    } else {
      setExpandKeys([]);
    }
  }, [expand]);
  const onSelect = (keys: any, info: any) => {
    setSelectedKeys(keys);
    setSelectedInfo(info);
    /* showEditJobDetail?.((origin: ShowJobDetailType) => ({
      ...origin,
      record: info.node,
      type: info.node.isLeaf ? 'job' : 'groupjob'
    })); */
  };

  const onExpand = (expandedKeys: any, { expanded }: { expanded: boolean }) => {
    setExpandKeys(expandedKeys)
    console.log("Trigger Expand", expandedKeys, expanded);
  };

  const onDragEnter = (info: any) => {
    // expandedKeys 需要受控时设置
    // this.setState({
    //   expandedKeys: info.expandedKeys,
    // });
  };

  const onDrop = (info: any) => {
    console.log(info);
    const dropKey = info.node.key;
    const dragKey = info.dragNode.key;
    const dropPos = info.node.pos.split("-");
    const dropPosition =
      info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const loop = (data: any, key: string | number, callback: any) => {
      for (let i = 0; i < data.length; i++) {
        if (data[i].key === key) {
          return callback(data[i], i, data);
        }
        if (data[i].children) {
          loop(data[i].children, key, callback);
        }
      }
    };
    const data = [...gData];

    // Find dragObject
    let dragObj: any;
    loop(data, dragKey, (item: any, index: number, arr: any) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    console.log(`🚀 ~ file: ChecklistContent.tsx ~ dragObj`, dragObj);
    if (!info.dropToGap) {
      // Drop on the content
      loop(data, dropKey, (item: any) => {
        console.log("case 1");
        item.children = item.children || [];
        // where to insert
        item.children.unshift(dragObj);
        if (dragObj.id) {
          // console.log("parent Id", dragObj.id, item.id)
          // TODO: update dragNode with new group
          let itemId: Number;
          if (item.type === "grouptask") {
            itemId = 0;
          } else {
            itemId = item.id;
          }
          changeNode({
            ...dragObj,
            parentId: itemId
          } as JobProps);
          /* counter
            .crudTask({
              ...dragObj,
              parentId: itemId
            } as JobProps)
            .then(res => {
              Object.assign(item, res?.data || {});
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            }); */
        }
      });
    } else if (
      (info.node.props.children || []).length > 0 && // Has children
      info.node.props.expanded && // Is expanded
      dropPosition === 1 // On the bottom gap
    ) {
      console.log("case 2");
      /* loop(data, dropKey, (item: any) => {
        item.children = item.children || [];
        // where to insert
        item.children.unshift(dragObj);
        // in previous version, we use item.children.push(dragObj) to insert the
        // item to the tail of the children
        // TODO: update dragNode with new group
        counter.crudTask({
          ...dragObj,
          groupJobId: item.id
        } as JobProps).then(res => {
          Object.assign(item, res?.data || {});
        }).catch(err => {
          if(err.type === 'ERROR_SERVER') {
            message.error(err?.messasge || 'Đã có lỗi xảy ra', 5);
          }
        });
        console.log(`🚀 ~ 2. ~ loop ~ item.children`, item.children);
      }); */
    } else {
      console.log("case 3");
      let ar: any;
      let i: number;
      loop(data, dropKey, (item: any, index: number, arr: any) => {
        ar = arr;
        i = index;
      });
      if (dropPosition === -1) {
        // @ts-ignore
        ar.splice(i, 0, dragObj);
        let arr: any = [];
        ar.map((e: any, index: Number) => {
          let temp = {
            id: e.id,
            index,
            name: e.name
          };
          arr.push(temp);
        });
        counter
          .updateSequence(arr)
          .then(res => {})
          .catch(err => {
            if (err.type === "ERROR_SERVER") {
              message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
            }
          });
      } else {
        // @ts-ignore
        ar.splice(i + 1, 0, dragObj);
        let arr: any = [];
        ar.map((e: any, index: Number) => {
          let temp = {
            id: e.id,
            index,
            name: e.name
          };
          arr.push(temp);
        });
        counter
          .updateSequence(arr)
          .then(res => {})
          .catch(err => {
            if (err.type === "ERROR_SERVER") {
              message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
            }
          });
      }
      if (info.node.parentId !== dragObj.parentId) {
        changeNode({
          ...dragObj,
          parentId: info.node.parentId,
          level: info.node.level
        } as JobProps);
        /* counter
          .crudTask({
            ...dragObj,
            parentId: info.node.parentId,
            level: info.node.level
          } as JobProps)
          .then(res => {
            // Object.assign(item, res?.data || {});
          })
          .catch(err => {
            if (err.type === "ERROR_SERVER") {
              message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
            }
          }); */
      }
    }

    setGData(data);
  };

  /* const hideMenu = (e: any) => {
    setDisplay('none');
  }; */

  /* const handleBodyClick = (event: any) => {
    console.log('body click');
    event.stopPropagation();
  }; */

  /* const getNodeTreeRightClickMenu = () => {
    // @ts-ignore
    const { relativeX, relativeY, ...rest } = { ...rightClickNodeTreeItem };
    const tmpStyle: React.CSSProperties = {
      position: "absolute",
      // left: `${pageX + 50}px`,
      // top: `${pageY - 50}px`,
      left: relativeX,
      top: relativeY,
      // background: 'white',
      // color: 'black',
      // width: '120px',
      // textAlign: 'left',
      // border: '1px solid #D7D7D7',
      display: display,
      zIndex: 999,
      // flexDirection: 'column',
      // padding: '5px'
    };
    const menu = (
      <div style={tmpStyle} className="self-right-menu" ref={menuRef} onClick={handleBodyClick}>
        <MenuToolbar node={rest} />
      </div>
    );
    return rightClickNodeTreeItem == null ? "" : menu;
  }; */

  const tagClick = useCallback(
    (e: any, node: TaskType, tag: TagProps) => {
      const d = gData;
      findNode(d, node.key, (item: JobProps, index: number, arr: any) => {
        if (Array.isArray(item.tagIds) && item.tagIds?.indexOf(tag.id) == -1) {
          item.tagIds?.push(tag.id);
        } else {
          item.tagIds = [tag.id];
        }
        console.log(
          `🚀 ~ file: ChecklistContent.tsx ~ line 189 ~ findNode ~ node`,
          node
        );
        if (node.type == "task" && node.id) {
          counter
            .crudTask(node as JobProps)
            .then(res => {
              Object.assign(item, res?.data || {});
            })
            .catch(err => {
              console.log(
                `🚀 ~ file: ChecklistContent.tsx ~ line 194 ~ counter.crudTask ~ err.type`,
                err.type
              );
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  );

  const tagRemove = useCallback(
    (e: any, node: TaskType, tag: TagProps) => {
      const d = gData;
      findNode(d, node.key, (item: JobProps, index: number, arr: any) => {
        if (Array.isArray(item.tagIds)) {
          item.tagIds?.splice(item.tagIds?.indexOf(tag.id), 1);
          if (node.type == "task" && node.id) {
            counter
              .crudTask(node as JobProps)
              .then(res => {
                Object.assign(item, res?.data || {});
              })
              .catch(err => {});
          } else if (node.type == "grouptask" && node.id) {
            counter
              .crudGroup(node as GroupJobProps)
              .then(res => {
                Object.assign(item, res?.data || {});
              })
              .catch(err => {
                if (err.type === "ERROR_SERVER") {
                  message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
                }
              });
          }
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  );

  const calClick = useCallback(
    (e: any, node: TaskType, cal: dayjs.Dayjs) => {
      const d = gData;
      findNode(d, node.key, (item: JobProps, index: number, arr: any) => {
        item.outDateTime = cal;
        if (node.type == "task" && node.id) {
          counter
            .crudTask(node as JobProps)
            .then(res => {
              Object.assign(item, res?.data || {});
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  );

  const calRemove = useCallback(
    (e: any, node: TaskType) => {
      const d = gData;
      findNode(d, node.key, (item: JobProps, index: number, arr: any) => {
        item.outDateTime = null;
        if (node.type == "task" && node.id) {
          counter
            .crudTask(node as JobProps)
            .then(res => {
              Object.assign(item, res?.data || {});
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  );

  const assginClick = useCallback(
    (e: any, node: TaskType, u: number) => {
      const d = gData;
      findNode(d, node.key, (item: JobProps, index: number, arr: any) => {
        item.assignUserId = Number(u);
        if (node.type == "task" && node.id) {
          counter
            .crudTask(node as JobProps)
            .then(res => {
              Object.assign(item, res?.data || {});
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  );

  const assginRemove = useCallback(
    (e: any, node: TaskType) => {
      const d = gData;
      findNode(d, node.key, (item: JobProps, index: number, arr: any) => {
        item.assignUserId = null;
        if (node.type == "task" && node.id) {
          counter
            .crudTask(node as JobProps)
            .then(res => {
              Object.assign(item, res?.data || {});
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  );

  const nodeRemove = useCallback(
    (e: any, node: TaskType) => {
      const d = gData;
      findNode(d, node.key, (item: TaskType, index: number, arr: any) => {
        arr.splice(index, 1);
        if (node.type == "task" && node.id) {
          const itemU: JobProps = { ...(item as JobProps), isDelete: true };
          counter
            .crudTask(itemU)
            .then(res => {
              Object.assign(item, res?.data || {});
              message.info(res?.data?.message);
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        } else if (node.type == "grouptask" && node.id) {
          counter
            .crudGroup({
              ...(item as GroupJobProps),
              ...node
            })
            .then(res => {
              Object.assign(item, res?.data || {});
              message.info("Cập nhật thành công");
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  );

  const changeNode = useCallback(
    (node: TaskType) => {
      const d = gData;
      findNode(d, node.key, (item: TaskType, index: number, arr: any) => {
        Object.assign(item, node);
        if (node.type == "task" && typeof node?.status != "undefined") {
          item.icon = <StatusIcon status={node.status} />;
        }
        if (node.type == "task" && node.id) {
          console.log(
            `🚀 ~ file: ChecklistContent.tsx ~ line 311 ~ findNode ~ node.type == 'task'`,
            node.type == "task"
          );
          if ((item as JobProps).outDateTime) {
            (item as JobProps).outDateTime = tranformDateTime(item);
          }
          counter
            .crudTask(item as JobProps)
            .then(res => {
              message.info(res?.data?.message);
              Object.assign(item, res?.data?.data);
              if (res?.data?.dataChange?.length > 0) {
                res?.data?.dataChange?.map((e: any) => {
                  findNodeJobId(
                    d,
                    `job` + e.id,
                    (item: TaskType, index: number, arr: any) => {
                      Object.assign(item, e);
                    }
                  );
                });
              }
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        } else if (node.type == "grouptask" && node.id) {
          counter
            .crudGroup(item as GroupJobProps)
            .then(res => {
              Object.assign(item, res?.data || {});
              message.info("Cập nhật thành công");

              d.forEach((e: any) => {
                if (e.id === item.id) {
                  e = Object.assign(e, { title: item.name });
                }
              });
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  );

  /* const updateNodeAfterDrag = useCallback(
    (node: TaskType) => {
      const d = gData;
      findNode(d, node.key, (item: TaskType, index: number, arr: any) => {
        Object.assign(item, node);
        if (node.type == "task" && typeof node?.status != "undefined") {
          item.icon = <StatusIcon status={node.status} />;
        }
        if (node.type == "task" && node.id) {
          console.log(
            `🚀 ~ file: ChecklistContent.tsx ~ line 311 ~ findNode ~ node.type == 'task'`,
            node.type == "task"
          );
          if ((item as JobProps).outDateTime) {
            (item as JobProps).outDateTime = tranformDateTime(item);
          }
          counter
            .crudTask(item as JobProps)
            .then(res => {
              message.info(res?.data?.message);
              Object.assign(item, res?.data?.data);
              if (res?.data?.dataChange?.length > 0) {
                res?.data?.dataChange?.map((e: any) => {
                  findNodeJobId(
                    d,
                    `job` + e.id,
                    (item: TaskType, index: number, arr: any) => {
                      Object.assign(item, e);
                    }
                  );
                });
              }
            })
            .catch(err => {
              if (err.type === "ERROR_SERVER") {
                message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
              }
            });
        }
      });
      setGData(origin => [...d]);
      counter.setData(origin => [...d]);
    },
    [gData]
  ); */

  const createTask = useCallback(
    (node: any, task: JobProps) => {
      const d = gData;
      findNode(d, node.key, (item: JobProps, index: number, arr: any) => {
        counter
          .crudTask(task)
          .then(res => {
            setGData(
              update(gData, {
                [index]: {
                  children: {
                    $push: [{ ...task, ...(res?.data || {}) }]
                  }
                }
              })
            );
            counter.setData(
              update(gData, {
                [index]: {
                  children: {
                    $push: [{ ...task, ...(res?.data || {}) }]
                  }
                }
              })
            );
          })
          .catch(err => {
            console.log(
              `🚀 ~ file: ChecklistContent.tsx ~ line 354 ~ counter.crudTask ~ err`,
              err
            );
            if (err.type === "ERROR_SERVER") {
              message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
            }
          });
      });
    },
    [gData]
  );

  const createGroupTask = useCallback(
    (node: any, gTask: GroupJobProps) => {
      counter
        .crudGroup(gTask)
        .then(res => {
          setGData(
            update(gData, {
              $push: [{ ...gTask, ...(res?.data || {}) }]
            })
          );
          counter.setData(
            update(gData, {
              $push: [{ ...gTask, ...(res?.data || {}) }]
            })
          );
        })
        .catch(err => {
          if (err.type === "ERROR_SERVER") {
            message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
          }
        });
    },
    [gData]
  );

  const createGroupTabWhenEmpty = () => {
    setIsCreateFirstGroupTab(true);
  };

  const doCreateFirstGroupTab = (e: any) => {
    const gTab: GroupJobProps = {
      type: "grouptask",
      title: firstGroupTabName,
      name: firstGroupTabName,
      key: `0-0`,
      children: []
    };
    setGData([gTab]);
    counter.setData([gTab]);
    setIsCreateFirstGroupTab(false);
  };

  // useOutsideMenu(menuRef, hideMenu);
  React.useEffect(() => {
    setGData(counter.data);
    return () => undefined;
  }, [counter.data]);

  // console.log(`🚀 ~ file: ChecklistContent.tsx ~ line 38 ~ gData`, gData);
  return (
    <React.Fragment>
      {!gData.length && !isCreateFirstGroupTab && (
        <span style={{ cursor: "pointer" }} onClick={createGroupTabWhenEmpty}>
          Ấn tạo nhiệm vụ ở đây
        </span>
      )}
      {isCreateFirstGroupTab && (
        <div className="gx-d-flex gx-align-items-center gx-justify-content-start">
          <Input
            style={{ width: "80%", minWidth: "150px", marginRight: "5px" }}
            onChange={(e: any) => {
              setFirstGroupTabName(e.target.value);
            }}
          />
          <Button type="primary" onClick={doCreateFirstGroupTab}>
            Ok
          </Button>
        </div>
      )}
      <Tree
        showIcon
        className="nv-checklist-tree"
        // defaultExpandedKeys={this.state.expandedKeys}
        draggable={(node: any) => {
          if (node.type == "grouptask") {
            return false;
          }
          return true;
        }}
        // selectedKeys={selectedKeys}
        autoExpandParent
        expandedKeys={expandKeys}
        defaultExpandAll={false}
        blockNode
        onDragEnter={onDragEnter}
        onDrop={onDrop}
        onSelect={onSelect}
        onExpand={onExpand}
        titleRender={node => {
          return (
            <TitleRender
              nodeData={node}
              tags={counter.tags}
              employees={counter.employees}
              tagClick={tagClick}
              tagRemove={tagRemove}
              calClick={calClick}
              calRemove={calRemove}
              assginClick={assginClick}
              assginRemove={assginRemove}
              createTask={createTask}
              createGroupTask={createGroupTask}
              modalGroupTaskVisible={modalGroupTaskVisible}
              setModalGroupTaskVisible={setModalGroupTaskVisible}
              modalTaskVisible={modalTaskVisible}
              setModalTaskVisible={setModalTaskVisible}
              nodeRemove={nodeRemove}
            />
          );
        }}
        treeData={gData}
        /* onRightClick={({ event, node }: TreeRightMenuClickProps) => {
        const containerTree = document.getElementsByClassName("gx-card-for-tree");
        const recTree = containerTree[0].getBoundingClientRect();
        // @ts-ignore
        const recTarget = event.target.getBoundingClientRect();
        setDisplay('flex');
        setRightClickNodeTreeItem({
          ...node,
          pageX: event.pageX,
          pageY: event.pageY,
          relativeX: event.pageX - recTarget.left + 80,
          relativeY: recTarget.top - recTree.top - 20,
          level: node.level,
          id: node.id,
          game_id: node.level === "game" ? node.props["data"]["id"] : node.props["data"]["game_id"],
          title: node.props["data"]["title"],
        });
        window.addEventListener('click', hideMenu);
      }} */
      ></Tree>
      {/* {getNodeTreeRightClickMenu()} */}
      {showJobDetail.visible && showJobDetail.type == "job" && (
        <JobDetail
          onChange={setShowJobDetail}
          record={showJobDetail.record as JobProps}
          drawerVisible={showJobDetail.visible}
          changeNode={changeNode}
        />
      )}
      {showJobDetail.visible && showJobDetail.type == "groupjob" && (
        <GroupJobDetail
          onChange={setShowJobDetail}
          record={showJobDetail.record as GroupJobProps}
          drawerVisible={showJobDetail.visible}
          changeNode={changeNode}
        />
      )}
      <ModalCreateGroupTask
        nodeData={modalGroupTaskVisible.nodeData as GroupJobProps}
        modalGroupTaskVisible={modalGroupTaskVisible.visible}
        setModalGroupTaskVisible={setModalGroupTaskVisible}
        createGroupTask={createGroupTask}
      />
      <ModalCreateTask
        nodeData={modalTaskVisible.nodeData as GroupJobProps}
        modalTaskVisible={modalTaskVisible.visible}
        setModalTaskVisible={setModalTaskVisible}
        createTask={createTask}
      />
    </React.Fragment>
  );
};

export default React.memo(ChecklistContent);
