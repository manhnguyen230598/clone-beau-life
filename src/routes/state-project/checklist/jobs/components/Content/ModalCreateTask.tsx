import React, { FC } from 'react';
import { Modal, Form, Input, Button } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';
import { v4 as uuid } from 'uuid';
import { GroupJobProps, JobProps } from '../../interface';
import StatusIcon from './StatusIcon';
interface ModalCreateTaskProps {
  modalTaskVisible: boolean;
  setModalTaskVisible: any;
  nodeData: GroupJobProps;
  createTask: (n: any, task: JobProps) => void;
}

const ModalCreateTask: FC<ModalCreateTaskProps> = (props: ModalCreateTaskProps) => {
  const {
    nodeData,
    modalTaskVisible,
    setModalTaskVisible,
    createTask
  } = props;
  const [frmTask] = Form.useForm();

  const onFinishTask = (values: any) => {
    createTask?.(nodeData, {
      type: 'task',
      title: values.name,
      name: values.name,
      key: `${uuid()}`,
      children: [],
      isLeaf: true,
      groupJobId: nodeData.id,
      status: 'Backlog',
      icon: <StatusIcon status={'Backlog'} />,
    });
    frmTask.resetFields();
    setModalTaskVisible(false);
  };

  const onFinishFailedTask = (err: any) => {
    console.log(`🚀 ~ file: TitleRender.tsx ~ onFinishFailedTask ~ err`, err);
  };

  return (
    <Modal
      visible={modalTaskVisible}
      onCancel={() => {
        setModalTaskVisible(false);
      }}
      footer={null}
      destroyOnClose
      title="Nhiệm vụ"
    >
      <Form
        form={frmTask}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinishTask}
        onFinishFailed={onFinishFailedTask}
      >
        <Form.Item
          label={`Tên nhiệm vụ`}
          name="name"
          rules={[
            { required: true, message: 'Vui lòng nhập tên nhiệm vụ' }
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button
            type="primary"
            icon={<PlusCircleOutlined />}
            onClick={(e) => { frmTask.submit(); }}
            htmlType="submit"
          >{`Thêm mới`}</Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalCreateTask;
