import React, { FC, memo } from 'react';
// import { } from 'antd';
import { JobProps } from '../../interface';

interface StatusMenuProps {
  node: JobProps;
  statusClick?: (e: any, n: any, s: string) => void;
  statusRemove?: (e: any, n: any) => void;
  setStatusMenuVisible?: React.Dispatch<React.SetStateAction<boolean>>;
}

// const arrStatus = ["Backlog", "Inprocessing", "Test", "Done"];

const StatusMenu: FC<StatusMenuProps> = ({
  node,
  statusClick,
  statusRemove,
  setStatusMenuVisible
}: StatusMenuProps) => {

  const onClick = (e: any, key: string) => {
    e.preventDefault();
    statusClick?.(null, node, key);
    // setStatusMenuVisible?.(false);
  };

  return (
    <>
      <ul className="nv-dropdown-status-menu">
        <li key={"Backlog"} className="u-item">
          <a onClick={(e) => onClick(e, "Backlog")}>
            <i className="gx-float-left ic-quire-state ic-quire-to-do lb iconc-51"></i>
            <div className="name">{"Backlog"}</div>
          </a>
        </li>
        <li key={"Inprocessing"} className="u-item">
          <a onClick={(e) => onClick(e, "Processing")}>
            <i className="gx-float-left ic-quire-state status-icon b-1">
              <i className="x14 pie iconc-02">
                <i className="pie-mask-wrap" style={{ transform: "rotate(36deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
                  <i className="pie-mask" style={{ transform: "rotate(180deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
                </i>
                <i className="pie-mask-wrap" style={{ transform: "rotate(-180deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
                  <i className="pie-mask" style={{ transform: "rotate(180deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
                </i>
              </i>
            </i>
            <div className="name">{"Processing"}</div>
          </a>
        </li>
        <li key={"Overdue"} className="u-item">
          <a onClick={(e) => onClick(e, "Overdue")}>
            <i className="gx-float-left ic-quire-state status-icon b-1">
              <i className="x14 pie iconc-12">
                <i className="pie-mask-wrap" style={{ transform: "rotate(-162deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
                  <i className="pie-mask" style={{ transform: "rotate(162deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
                </i>
              </i>
            </i>
            <div className="name">{"Overdue"}</div>
          </a>
        </li>
        <li key={"Done"} className="u-item">
          <a onClick={(e) => onClick(e, "Done")}>
            <i className="gx-float-left ic-quire-state ic-quire-completed lb iconc-43"></i>
            <div className="name">{"Done"}</div>
          </a>
        </li>
      </ul>
    </>
  );
};

export default memo(StatusMenu);
