import React, { FC } from 'react';
import { Menu } from 'antd';
import { TagProps } from '../../interface';

interface TagMenuProps {
  data: Array<TagProps>;
  node: any;
  tagClick?: (e: any, n: any, t: TagProps) => void;
  tagRemove?: (e: any, n: any, t: TagProps) => void;
}

const TagMenu: FC<TagMenuProps> = ({
  data,
  node,
  tagClick = (e: any, n: any, t: TagProps) => { },
  tagRemove = (e: any, n: any, t: TagProps) => { }
}: TagMenuProps) => {
  return (
    <Menu>
      {(data ?? []).map((item, idx) => {
        return (
          <Menu.Item key={idx}>
            <a href="#" style={{ color: item?.color ?? '#000' }} onClick={(e) => tagClick(e, node, item)}>
              {item.name}
            </a>
          </Menu.Item>
        );
      })}
    </Menu>
  );
};

export default React.memo(TagMenu);
