import React, { FC, useState } from "react";
import { Dropdown, Tag, Tooltip, Avatar, Popconfirm } from "antd";
import {
  PlusOutlined,
  AppstoreAddOutlined,
  UserOutlined,
  TagOutlined,
  EditOutlined,
  EditTwoTone,
  DeleteOutlined,
  DeleteRowOutlined
} from "@ant-design/icons";
import dayjs from "dayjs";
import {
  GroupJobProps,
  TagProps,
  JobProps,
  RoleUserProjectJobProps
} from "../../interface";
import TagMenu from "./TagMenu";
// import CalendarMenu from './CalendarMenu';
import EmployeeMenu from "./EmployeeMenu";
/* import ModalCreateGroupTask from './ModalCreateGroupTask';
import ModalCreateTask from './ModalCreateTask'; */
import Container from "../../container";

type TaskType = GroupJobProps | JobProps;
interface TitleRenderProps {
  nodeData?: JobProps | GroupJobProps | any;
  tags: Array<TagProps>;
  employees?: Array<RoleUserProjectJobProps>;
  tagClick?: (e: any, n: any, t: TagProps) => void;
  tagRemove?: (e: any, n: any, t: TagProps) => void;
  calClick?: (e: any, n: any, c: dayjs.Dayjs) => void;
  calRemove?: (e: any, n: any) => void;
  assginClick?: (e: any, n: any, u: number) => void;
  assginRemove?: (e: any, n: any) => void;
  nodeRemove?: (e: any, n: TaskType) => void;
  createTask?: (n: any, task: JobProps) => void;
  createGroupTask?: (n: any, gTask: GroupJobProps) => void;
  modalGroupTaskVisible: {
    visible: boolean;
    nodeData: GroupJobProps | {};
  };
  modalTaskVisible: {
    visible: boolean;
    nodeData: JobProps | {};
  };
  setModalGroupTaskVisible: React.Dispatch<
    React.SetStateAction<{
      visible: boolean;
      nodeData: GroupJobProps | {};
    }>
  >;
  setModalTaskVisible: React.Dispatch<
    React.SetStateAction<{
      visible: boolean;
      nodeData: JobProps | {};
    }>
  >;
}

const TitleRender: FC<TitleRenderProps> = ({
  nodeData,
  tags,
  employees = [],
  tagClick = (e: any, n: any, t: TagProps) => {},
  tagRemove = (e: any, n: any, t: TagProps) => {},
  calClick = (e: any, n: any, c: dayjs.Dayjs) => {},
  calRemove = (e: any, n: any) => {},
  assginClick = (e: any, n: any, u: number) => {},
  assginRemove = (e: any, n: any) => {},
  createTask = (n: any, task: JobProps) => {},
  createGroupTask = (n: any, gTask: GroupJobProps) => {},
  nodeRemove = (e: any, n: any) => {},
  modalGroupTaskVisible,
  modalTaskVisible,
  setModalGroupTaskVisible,
  setModalTaskVisible
}: TitleRenderProps) => {
  const counter = Container.useContainer();
  // const [, setCalMenuVisible] = useState<boolean>(false);
  const [employeeMenuVisible, setEmployeeMenuVisible] = useState<boolean>(
    false
  );
  /* const [modalGroupTaskVisible, setModalGroupTaskVisible] = useState<boolean>(false);
  const [modalTaskVisible, setModalTaskVisible] = useState<boolean>(false); */

  // const handleVisibleCalChange = (flag: boolean) => {
  //   setCalMenuVisible(flag);
  // };

  const handleVisibleEmployeeChange = (flag: boolean) => {
    setEmployeeMenuVisible(flag);
  };

  if (nodeData.type == 'grouptask') {
    return (
      <>
        <div className="nv-tree-node gx-d-flex gx-align-items-center gx-justify-content-between gx-p-1">
          <div className="nv-tree-node-title">{nodeData.title || ""}</div>
          <div className="nv-tree-node-toolbar">
            <Tooltip title="Thêm nhóm nhiệm vụ">
              <AppstoreAddOutlined
                className="nv-tree-node-toolbar-icon nv-tree-node-toolbar-icon-add gx-ml-2"
                onClick={() => {
                  setModalGroupTaskVisible(
                    (origin: {
                      visible: boolean;
                      nodeData: GroupJobProps | {};
                    }) => ({
                      ...origin,
                      visible: true,
                      nodeData
                    })
                  );
                }}
              />
            </Tooltip>
            <Tooltip title="Thêm nhiệm vụ">
              <PlusOutlined
                className="nv-tree-node-toolbar-icon nv-tree-node-toolbar-icon-add gx-ml-2"
                onClick={() => {
                  setModalTaskVisible(
                    (origin: {
                      visible: boolean;
                      nodeData: GroupJobProps | {};
                    }) => ({
                      ...origin,
                      visible: true,
                      nodeData
                    })
                  );
                }}
              />
            </Tooltip>
            <Tooltip title="Sửa nhóm nhiệm vụ">
              <EditTwoTone
                className="nv-tree-node-toolbar-icon nv-tree-node-toolbar-icon-edit-group gx-ml-2"
                onClick={() => {
                  counter.setShowJobDetail(origin => ({
                    type: "groupjob",
                    visible: true,
                    record: nodeData
                  }));
                }}
              />
            </Tooltip>
            <Tooltip title="Xoá nhóm nhiệm vụ">
              <Popconfirm
                placement="top"
                title={"Bạn chắc chắn muốn xóa?"}
                onConfirm={e => {
                  nodeRemove(e, {
                    ...nodeData,
                    isActive: false
                  });
                }}
                okText="Yes"
                cancelText="No"
              >
                <DeleteRowOutlined style={{ color: '#fc9c4e' }} className="nv-tree-node-toolbar-icon nv-tree-node-toolbar-icon-edit-task gx-ml-2 gx-d-flex gx-justify-content-end gx-align-items-center" />
              </Popconfirm>
            </Tooltip>
          </div>
        </div>
        {/* <ModalCreateGroupTask
          nodeData={nodeData}
          modalGroupTaskVisible={modalGroupTaskVisible}
          setModalGroupTaskVisible={setModalGroupTaskVisible}
          createGroupTask={createGroupTask}
        />
        <ModalCreateTask
          nodeData={nodeData}
          modalTaskVisible={modalTaskVisible}
          setModalTaskVisible={setModalTaskVisible}
          createTask={createTask}
        /> */}
      </>
    );
  }

  let tag;
  if (Array.isArray(nodeData.tagIds) && nodeData.tagIds.length > 0) {
    tag = nodeData.tagIds.map((i: number) => {
      const t = tags.find(j => j.id == i) as TagProps;
      if (t)
        return (
          <Dropdown
            overlay={
              <TagMenu data={tags} node={nodeData} tagClick={tagClick} />
            }
            trigger={["click"]}
          >
            <Tag
              color={t?.color ?? "#000"}
              closable
              onClose={e => tagRemove(e, nodeData, t)}
            >
              {t?.name}
            </Tag>
          </Dropdown>
        );
      return null;
    });
  }
  let cal;
  if (
    nodeData.outDateTime &&
    typeof nodeData.outDateTime != "undefined" &&
    (typeof nodeData.outDateTime == "number" ||
      nodeData.outDateTime instanceof dayjs)
  ) {
    cal = (
      // <Dropdown
      //   visible={calMenuVisible}
      //   onVisibleChange={handleVisibleCalChange}
      //   overlay={<CalendarMenu
      //     node={nodeData}
      //     calClick={calClick}
      //     setCalMenuVisible={setCalMenuVisible}
      //   />}
      //   trigger={["click"]}
      //   className="gx-ml-5"
      // >
      <Tooltip
        title={
          dayjs(nodeData?.outDateTime)
            .format("DD/MM/YYYY")
            .toString() ?? ""
        }
      >
        <Tag
        color={nodeData?.outDateTime < new Date().getTime() && nodeData?.status !== "Done" ? "#f53f3f" : ""}
        // closable
        // onClose={(e) => calRemove(e, nodeData)}
        >
          {dayjs(nodeData?.outDateTime)
            .format("DD/MM/YY")
            .toString() ?? ""}
        </Tag>
      </Tooltip>
      // </Dropdown>
    );
  }
  let employee;
  if (nodeData.assignUserId && typeof nodeData.assignUserId == "number") {
    const u = employees.find(i => i?.userId?.id == nodeData.assignUserId);
    employee = (
      <Dropdown
        visible={employeeMenuVisible}
        onVisibleChange={handleVisibleEmployeeChange}
        overlay={
          <EmployeeMenu
            node={nodeData}
            employees={employees}
            assginClick={assginClick}
            setEmployeeMenuVisible={setEmployeeMenuVisible}
          />
        }
        trigger={["click"]}
      >
        <Tooltip title={"Phân cho"}>
          {/* <Tag
            color={'#3c3a3d'}
            closable
            onClose={(e) => assginRemove(e, nodeData)}
          >
            <span style={{}}>{u?.userId?.name ?? ''}</span>
          </Tag> */}
          <div>
            <Avatar size="small" src={u?.userId?.avatar ?? ""} />
            <span style={{ marginLeft: "3px" }}>{u?.userId?.name ?? ""}</span>
          </div>
        </Tooltip>
      </Dropdown>
    );
  } else if (
    nodeData.assignUserId &&
    typeof nodeData.assignUserId == "object"
  ) {
    const u = employees.find(i => i?.userId?.id == nodeData?.assignUserId?.id);
    employee = (
      <Dropdown
        visible={employeeMenuVisible}
        onVisibleChange={handleVisibleEmployeeChange}
        overlay={
          <EmployeeMenu
            node={nodeData}
            employees={employees}
            assginClick={assginClick}
            setEmployeeMenuVisible={setEmployeeMenuVisible}
          />
        }
        trigger={["click"]}
      >
        <Tooltip title={"Phân cho"}>
          {/* <Tag
            color={'#3c3a3d'}
            closable
            onClose={(e) => assginRemove(e, nodeData)}
          >
            <span style={{}}>{u?.userId?.name ?? ''}</span>
          </Tag> */}
          <div>
            <Avatar size="small" src={u?.userId?.avatar ?? ""} />
            <span style={{ marginLeft: "3px", whiteSpace: "nowrap" }}>
              {u?.userId?.name ?? ""}
            </span>
          </div>
        </Tooltip>
      </Dropdown>
    );
  }
  return (
    <div className="nv-tree-node gx-d-flex gx-align-items-center gx-justify-content-between gx-p-1">
      <div className="nv-tree-node-title">{nodeData.title || ""}</div>
      <div className="nv-tree-node-toolbar">
        {counter.needCrudDb && (
          <>
            <div
              style={{}}
              className="gx-d-flex gx-align-items-center gx-justify-content-end"
            >
              {tag ?? (
                <Dropdown
                  overlay={
                    <TagMenu data={tags} node={nodeData} tagClick={tagClick} />
                  }
                  trigger={["click"]}
                >
                  <Tooltip title="Gắn thẻ">
                    <TagOutlined className="nv-tree-node-toolbar-icon nv-tree-node-toolbar-icon-tag" />
                  </Tooltip>
                </Dropdown>
              )}
            </div>
            <div
              style={{ width: "60px" }}
              className="gx-d-flex gx-align-items-center gx-justify-content-end gx-ml-5"
            >
              {cal ? cal : null}
            </div>
            <div
              style={{ width: "200px" }}
              className="gx-d-flex gx-align-items-center gx-justify-content-start gx-ml-5"
            >
              {employee ? (
                employee
              ) : (
                <Dropdown
                  visible={employeeMenuVisible}
                  onVisibleChange={handleVisibleEmployeeChange}
                  overlay={
                    <EmployeeMenu
                      node={nodeData}
                      employees={employees}
                      assginClick={assginClick}
                      setEmployeeMenuVisible={setEmployeeMenuVisible}
                    />
                  }
                  trigger={["click"]}
                >
                  <Tooltip title="Phân cho">
                    <UserOutlined className="nv-tree-node-toolbar-icon nv-tree-node-toolbar-icon-assign" />
                  </Tooltip>
                </Dropdown>
              )}
            </div>
            <Tooltip title="Sửa nhiệm vụ">
              <EditOutlined
                className="nv-tree-node-toolbar-icon nv-tree-node-toolbar-icon-edit-task gx-ml-2 gx-d-flex gx-justify-content-end gx-align-items-center"
                onClick={() => {
                  counter.setShowJobDetail(origin => ({
                    type: "job",
                    visible: true,
                    record: nodeData
                  }));
                }}
              />
            </Tooltip>
          </>
        )}
        <Tooltip title="Xoá nhiệm vụ">
          <Popconfirm
            placement="top"
            title={"Bạn chắc chắn muốn xóa nhiệm vụ này?"}
            onConfirm={e => {
              nodeRemove(e, nodeData);
            }}
            okText="Yes"
            cancelText="No"
          >
            <DeleteOutlined className="nv-tree-node-toolbar-icon nv-tree-node-toolbar-icon-edit-task gx-ml-2 gx-d-flex gx-justify-content-end gx-align-items-center" />
          </Popconfirm>
        </Tooltip>
      </div>
    </div>
  );
};

export default React.memo(TitleRender);
