import React, { FC, memo } from 'react';

interface StatusIconProps {
  status: string;
}

const StatusIcon: FC<StatusIconProps> = ({
  status
}: StatusIconProps) => {

  if(status == 'Backlog' || status == 'backlog') {
    return (
      <i className="gx-float-left ic-quire-state ic-quire-to-do lb iconc-51" style={{ lineHeight: '22px' }}></i>
    );
  } else if(status == 'Processing' || status == 'processing') {
    return (
      <i className="gx-float-left ic-quire-state status-icon b-1" style={{ lineHeight: '22px' }}>
        <i className="x14 pie iconc-02">
          <i className="pie-mask-wrap" style={{ transform: "rotate(36deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
            <i className="pie-mask" style={{ transform: "rotate(180deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
          </i>
          <i className="pie-mask-wrap" style={{ transform: "rotate(-180deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
            <i className="pie-mask" style={{ transform: "rotate(180deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
          </i>
        </i>
      </i>
    );
  } else if(status == 'Overdue' || status == 'Overdue') {
    return (
      <i className="gx-float-left ic-quire-state status-icon b-1" style={{ lineHeight: '22px' }}>
        <i className="x14 pie iconc-12">
          <i className="pie-mask-wrap" style={{ transform: "rotate(-162deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
            <i className="pie-mask" style={{ transform: "rotate(162deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
          </i>
        </i>
      </i>
    );
  } else if(status == 'Done' || status == 'Done') {
    return (
      <i className="gx-float-left ic-quire-state ic-quire-completed lb iconc-43" style={{ lineHeight: '22px' }}></i>
    );
  }
  return null;
};

export default memo(StatusIcon);
