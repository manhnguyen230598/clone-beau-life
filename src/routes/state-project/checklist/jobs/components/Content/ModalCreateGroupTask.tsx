import React, { FC } from 'react';
import { Modal, Form, Input, Button } from 'antd';
import { PlusCircleOutlined, FolderOutlined } from '@ant-design/icons';
import dayjs from 'dayjs';
import { GroupJobProps } from '../../interface';

interface ModalCreateGroupTaskProps {
  modalGroupTaskVisible: boolean;
  setModalGroupTaskVisible: any;
  nodeData: GroupJobProps;
  createGroupTask: (n: any, gTask: GroupJobProps) => void;
}

const ModalCreateGroupTask: FC<ModalCreateGroupTaskProps> = (props: ModalCreateGroupTaskProps) => {
  const {
    nodeData,
    modalGroupTaskVisible,
    setModalGroupTaskVisible,
    createGroupTask
  } = props;
  const [frmGroup] = Form.useForm();

  const onFinishGroup = (values: any) => {
    createGroupTask?.(nodeData, {
      type: 'grouptask',
      title: values.name,
      name: values.name,
      key: `${dayjs().valueOf()}`,
      children: [],
      icon: <FolderOutlined />,
    });
    frmGroup.resetFields();
    setModalGroupTaskVisible(false);
  };

  const onFinishFailedGroup = (err: any) => {
    console.log(`🚀 ~ file: TitleRender.tsx ~ onFinishFailedGroup ~ err`, err);
  };

  return (
    <Modal
      visible={modalGroupTaskVisible}
      onCancel={() => {
        setModalGroupTaskVisible(false);
      }}
      footer={null}
      destroyOnClose
      title="Nhóm nhiệm vụ"
    >
      <Form
        form={frmGroup}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinishGroup}
        onFinishFailed={onFinishFailedGroup}
      >
        <Form.Item
          label={`Tên`}
          name="name"
          rules={[
            { required: true, message: 'Vui lòng nhập tên nhóm' }
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button
            type="primary"
            icon={<PlusCircleOutlined />}
            htmlType="submit"
            onClick={(e) => { frmGroup.submit(); }}
          >{`Thêm mới`}</Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalCreateGroupTask;
