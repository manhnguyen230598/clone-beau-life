import React, { useEffect, useState } from "react";
import { Modal, Timeline } from "antd";
import * as jobServices from "src/services/job";
import moment from "moment";
const ModalHistoryChange = props => {
  const { isView, data, onCancelModal } = props;
  const [list, setList] = useState([]);
  useEffect(() => {
    fetchData();
  }, [data]);
  const fetchData = async () => {
    if (data && data.id) {
      let rs = await jobServices.getHistoryChange({
        jobId: data.id
      });

      setList(rs.data?.data);
    }
  };
  return (
    <Modal
      visible={isView}
      title={`Lịch sử thay đổi công việc ${data?.name || "-"}`}
      onCancel={() => onCancelModal(false)}
      width={"50%"}
      onOk={() => onCancelModal(false)}
    >
      {list.length === 0 ? (
        <span>Không có thay đổi</span>
      ) : (
        <Timeline>
          {list.map(e => {
            return <Timeline.Item key={`timeline ${e.id}`}>
            <span>{e.description} </span>
            <span
              style={{
                color: "red"
              }}
            >
              {moment(e.createdAt).format("DD/MM/YYY hh:mm")}
            </span>
          </Timeline.Item>;
            
          })}
        </Timeline>
      )}
    </Modal>
  );
};
export default ModalHistoryChange;
