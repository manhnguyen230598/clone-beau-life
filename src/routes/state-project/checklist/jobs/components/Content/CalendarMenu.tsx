import React, { FC, memo, useState } from 'react';
import { /* Menu,  */ Input, Button } from 'antd';
import dayjs from 'dayjs';
import { Calendar } from '../../../../../../packages/pro-component';

interface CalendarMenuProps {
  node: any;
  calClick?: (e: any, n: any, t: dayjs.Dayjs) => void;
  calRemove?: (e: any, n: any) => void;
  setCalMenuVisible?: React.Dispatch<React.SetStateAction<boolean>>;
}

const CalendarMenu: FC<CalendarMenuProps> = ({
  node,
  calClick,
  calRemove,
  setCalMenuVisible
}: CalendarMenuProps) => {
  const [value, setValue] = useState<dayjs.Dayjs>(node.outDateTime);

  const onSelect = (date: dayjs.Dayjs) => {
    setValue(date);
  };

  const onOk = () => {
    calClick?.(null, node, value);
    setCalMenuVisible?.(false);
  };

  return (
    <>
      <div style={{ width: '300px', minHeight: '300px', padding: '10px', borderRadius: '6px', background: '#fff' }}>
        <Calendar
          value={value}
          fullscreen={false}
          onSelect={onSelect}
        />
        <div className="gx-d-flex gx-justify-content-around gx-align-items-center gx-mt-1">
          <Input style={{ }} value={value?.format("DD-MM-YYYY").toString() ?? ''} />
          <Button className="gx-ml-2" type="primary" onClick={onOk}>OK</Button>
        </div>
      </div>
    </>
  );
};

export default memo(CalendarMenu);
