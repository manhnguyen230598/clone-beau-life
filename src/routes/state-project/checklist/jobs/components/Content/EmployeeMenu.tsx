import React, { FC, memo } from 'react';
import { Menu, Avatar } from 'antd';
import { RoleUserProjectJobProps, JobProps } from '../../interface';

interface EmployeeMenuProps {
  node: JobProps;
  employees: RoleUserProjectJobProps[];
  assginClick?: (e: any, n: any, u: number) => void;
  assginRemove?: (e: any, n: any) => void;
  setEmployeeMenuVisible?: React.Dispatch<React.SetStateAction<boolean>>;
}

const EmployeeMenu: FC<EmployeeMenuProps> = ({
  node,
  employees = [],
  assginClick,
  assginRemove,
  setEmployeeMenuVisible
}: EmployeeMenuProps) => {

  const onClick = ({ item, key, keyPath }: any) => {
    assginClick?.(null, node, key);
    setEmployeeMenuVisible?.(false);
  };

  return (
    <>
      <Menu onClick={onClick}>
        {employees.map(item => {
          return (
            <Menu.Item key={item?.userId?.id}>
              <a href="#">
                <Avatar size="small" src={item?.userId?.avatar || ""}/>
                <span style={{marginLeft : "2px"}}>{item?.userId?.name}</span>
              </a>
            </Menu.Item>
          );
        })}
      </Menu>
    </>
  );
};

export default memo(EmployeeMenu);
