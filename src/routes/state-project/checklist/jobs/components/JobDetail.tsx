import React, { FC, useState, useEffect, useCallback } from "react";
import { Drawer, Row, Col, Dropdown, Form, Button, Select, Avatar } from "antd";
import {
  DownOutlined,
  // WindowsOutlined,
  HistoryOutlined
} from "@ant-design/icons";
import get from "lodash/get";
import cloneDeep from "lodash/cloneDeep";
import dayjs from "dayjs";
import { JobProps, ShowJobDetailType } from "../interface";
import StatusMenu from "./Content/StatusMenu";
import { FormInputRender } from "../../../../../packages/pro-table/form";
import { useIntl } from "../../../../../packages/pro-table/component/intlContext";
import * as jobServices from "../../../../../services/job";
import Container from "../container";
// import GroupTimeLine from "./GroupTimeLine";
import HistoryChangeModal from "./Content/HistoryChange";
import { tranformDateTime } from "./utils";
interface JobDetailProps {
  record: JobProps;
  drawerVisible: boolean;
  onChange?: React.Dispatch<React.SetStateAction<ShowJobDetailType>>;
  changeNode?: (node: JobProps) => void;
}

const DescriptionItem = ({
  title,
  content
}: {
  title?: string | React.ReactElement;
  content: number | string | React.ReactElement;
}) => (
  <div className="nv-detail-item-wrapper">
    {title ? (
      <p className="nv-detail-item-label">{title ? `${title}:` : ""}</p>
    ) : null}
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const JobDetail: FC<JobDetailProps> = (props: JobDetailProps) => {
  const { record, drawerVisible, onChange, changeNode } = props;
  const counter = Container.useContainer();
  const [form] = Form.useForm();
  const intl = useIntl();
  const [loading, setLoading] = useState(false);
  const [dependJob, setDependJob] = useState(false);
  // const [itemGroupTimeLine, setItemGroupTimeLine] = useState();
  // const [isViewModal, setIsViewModal] = useState(false);
  const [isViewHistory, setIsViewHistory] = useState(false);
  const [recordView, setRecordView] = useState<any>();
  const [messageChangeDeadline, setMessageChangeDeadline] = useState();
  const FooterDrawer = ({ onOk }: { onOk: (e: any) => void }) => {
    return (
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Button onClick={onOk} type="primary">
          Lưu thay đổi
        </Button>
        <Button
          onClick={() => {
            console.log(data);
            setRecordView(data);
            setIsViewHistory(true);
          }}
        >
          <HistoryOutlined /> Xem lịch sử chỉnh sửa
        </Button>
      </div>
    );
  };
  const getJobs = () => {
    let datas: any[] = [];
    const getDataChild = (list: any, child: any) => {
      if (child.children.length > 0) {
        child.children.map((ele: any) => {
          if (ele.id !== data.id) {
            list.push(ele);
          }
          getDataChild(list, ele);
        });
      }
    };
    counter.data.forEach(e => {
      getDataChild(datas, e);
    });
    return datas;
  };
  const [data, setData] = useState<any>((origin: any) => {
    if (typeof record.assignUserId == "object") {
      return {
        ...data,
        assignUserId: record.assignUserId?.id,
        outDateTime: tranformDateTime(record)
      };
    }
    return {
      ...record,
      outDateTime: tranformDateTime(record)
    };
  });
  const status = data?.status?.toLowerCase() ?? "backlog";
  // @ts-ignore
  // const [statusChange, setStatusChange] = useState(status);
  // const status = "inprocessing";
  // const status = "test";
  // const status = "done";

  useEffect(() => {
    if (record?.id) {
      setLoading(true);
      jobServices
        .getById(record.id)
        .then(res => {
          if (res && res.status == 200) {
            const resData = get(res, "data.data", {});
            const message = get(res, "data.message", "");
            setMessageChangeDeadline(message);
            if (typeof resData.outDateTime == "number") {
              setData({
                ...resData,
                outDateTime: dayjs(resData.outDateTime)
              });
            } else {
              setData(resData);
            }
            setDependJob(resData.dependAnotherJob);
          }
          setLoading(false);
        })
        .catch(err => {
          setLoading(false);
        });
    }
  }, []);

  const onOk = (e: any) => {
    form
      .validateFields()
      .then(values => {
        const params = cloneDeep(values);
        changeNode?.({
          ...record,
          ...params
        });
      })
      .catch(err => {
        console.log(
          `🚀 ~ file: JobDetail.tsx ~ line 181 ~ form.validateFields ~ err`,
          err
        );
      });
  };

  const triggerChange = (v: boolean) => {
    onChange?.((origin: ShowJobDetailType) => ({
      ...origin,
      visible: v
    }));
  };

  const onClose = () => {
    triggerChange(false);
  };

  const statusClick = useCallback(
    (e: any, n: any, s: string) => {
      form.setFieldsValue({
        status: s
      });
      setData((origin: any) => {
        return {
          ...origin,
          status: s
        };
      });
    },
    [data]
  );

  // const groupJobClick = (item: any) => {
  //   setItemGroupTimeLine(item);
  //   setIsViewModal(true);
  // };

  const completeJob = (e: any) => {
    e.preventDefault();
    form.setFieldsValue({
      status: "Done"
    });
    setData((origin: any) => ({
      ...origin,
      status: "Done"
    }));
  };

  const onChangeJobDepend = () => {
    let jobDepend = form.getFieldValue("jobDependId");
    let countDay = form.getFieldValue("countDayNext");
    let currentJobOutDateTime;
    if (jobDepend) {
      let jobs = getJobs();
      let info = jobs.find(e => e.id === jobDepend);
      currentJobOutDateTime = info.outDateTime;
    }
    if (countDay && currentJobOutDateTime) {
      let day = dayjs(currentJobOutDateTime).add(countDay, "day");
      form.setFieldsValue({ outDateTime: day });
    }
  };

  useEffect(() => {
    form.setFieldsValue({
      ...data,
      outDateTime: tranformDateTime(data)
    });
  }, [data]);

  return (
    <>
      <Drawer
        width={640}
        style={{ width: "640px !important" }}
        className={`nv-drawer-detail nv-checklist-task-detail ${status}`} //backlog, completed,
        title={
          <span style={{ color: "#f09b1b" }}>{`#${get(
            record,
            "id",
            "-"
          )}`}</span>
        }
        placement="right"
        closable={true}
        onClose={onClose}
        visible={drawerVisible}
        footer={record && record.id ? <FooterDrawer onOk={onOk} /> : null}
      >
        <>
          {loading === true ? (
            <div className="loader loading">
              <img src="/loading.gif" alt="loader" />
            </div>
          ) : (
            <Form
              form={form}
              labelCol={{
                span: 8
              }}
              wrapperCol={{
                span: 16
              }}
              initialValues={{
                ...data
              }}
            >
              <Row gutter={16}>
                <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
                  <DescriptionItem
                    content={
                      <div className="gx-d-flex gx-align-items-center gx-justify-content-start">
                        <div className="nv-complete-btn-wrap">
                          <Form.Item label={""} name="status" noStyle>
                            <a
                              href="#"
                              onClick={e => {
                                completeJob(e);
                              }}
                              className="nv-complete-combobtn"
                            >
                              <i className="complete-btn lb iconc-51">
                                {/* @ts-ignore */}
                                {status == "processing" && (
                                  <i className="status-icon">
                                    <i className="x26 pie iconc-02">
                                      <i
                                        className="pie-mask-wrap"
                                        style={{
                                          transform: "rotate(36deg)",
                                          clip: "rect(0, 22px, 22px, 11px)"
                                        }}
                                      >
                                        <i
                                          className="pie-mask"
                                          style={{
                                            transform: "rotate(180deg)",
                                            clip: "rect(0, 11px, 22px, 0)"
                                          }}
                                        ></i>
                                      </i>
                                      <i
                                        className="pie-mask-wrap"
                                        style={{
                                          transform: "rotate(-180deg)",
                                          clip: "rect(0, 22px, 22px, 11px)"
                                        }}
                                      >
                                        <i
                                          className="pie-mask"
                                          style={{
                                            transform: "rotate(180deg)",
                                            clip: "rect(0, 11px, 22px, 0)"
                                          }}
                                        ></i>
                                      </i>
                                    </i>
                                  </i>
                                )}
                                {status == "overdue" && (
                                  <i className="status-icon">
                                    <i className="x26 pie iconc-12">
                                      <i
                                        className="pie-mask-wrap"
                                        style={{
                                          transform: "rotate(-162deg)",
                                          clip: "rect(0, 22px, 22px, 11px) "
                                        }}
                                      >
                                        <i
                                          className="pie-mask"
                                          style={{
                                            transform: "rotate(162deg)",
                                            clip: "rect(0, 11px, 22px, 0)"
                                          }}
                                        ></i>
                                      </i>
                                    </i>
                                  </i>
                                )}
                              </i>
                            </a>
                            <Dropdown
                              overlay={
                                <StatusMenu
                                  statusClick={statusClick}
                                  node={data}
                                />
                              }
                              trigger={["click"]}
                            >
                              <DownOutlined />
                            </Dropdown>
                          </Form.Item>
                        </div>
                        <div style={{ width: "90%" }}>
                          <Form.Item
                            label={""}
                            name="name"
                            rules={[
                              {
                                required: true,
                                message: "Vui lòng nhập tên nhiệm vụ"
                              }
                            ]}
                            noStyle
                          >
                            <FormInputRender
                              item={{ dataIndex: "name", valueType: "text" }}
                              intl={intl}
                              type="form"
                            />
                          </Form.Item>
                        </div>
                      </div>
                    }
                  />
                </Col>
              </Row>
              <Form.Item
                label={"Nhóm nhiệm vụ"}
                name="groupJobId"
                rules={[{ required: false }]}
              >
                <Select>
                  {counter.groupJobs.map(i => (
                    <Select.Option
                      key={`groupJobs-${i.id as number}`}
                      value={i.id as number}
                    >
                      {i.name}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              {/* <Form.Item
                label={"Nhóm nhiệm vụ liên quan"}
                name="groupTimelineId"
                rules={[{ required: false }]}
              >
                <Select>
                  {counter.groupTimelines.map(i => (
                    <Select.Option key={`groupTimelines-${i.id}`} value={i.id}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          alignContent: "center"
                        }}
                      >
                        <span>{i.name}</span>
                        <Button
                          onClick={() => {
                            groupJobClick({ id: i.id, name: i.name });
                          }}
                          type="primary"
                        >
                          <WindowsOutlined /> Xem DS NV
                        </Button>
                      </div>
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item> */}
              <Form.Item
                label={"Phụ thuộc vào nhiệm vụ khác"}
                name="dependAnotherJob"
                rules={[{ required: false }]}
                valuePropName="checked"
              >
                <FormInputRender
                  item={{
                    title: "dependAnotherJob",
                    dataIndex: "dependAnotherJob",
                    valueType: "switch"
                  }}
                  intl={intl}
                  onChange={value => setDependJob(value)}
                  type="form"
                />
              </Form.Item>
              {dependJob ? (
                <Form.Item
                  label={"Nhiệm vụ trước đó"}
                  name="jobDependId"
                  rules={[
                    { required: true, message: "Vui lòng chọn nhiệm vụ" }
                  ]}
                  // valuePropName="checked"
                >
                  <Select
                    showSearch
                    placeholder={"Chọn nhiệm vụ"}
                    optionFilterProp="children"
                    filterOption={(input, option: any) => {
                      return (
                        option.children.props.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      );
                    }}
                    onChange={() => {
                      onChangeJobDepend();
                    }}
                  >
                    {getJobs().map(i => (
                      <Select.Option key={`jobs-${i.id}`} value={i.id}>
                        <span>{i.name}</span>
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              ) : null}
              {dependJob ? (
                <Form.Item
                  label={"Số ngày tăng thêm"}
                  name="countDayNext"
                  rules={[
                    { required: true, message: "Vui lòng điền số ngày  " }
                  ]}
                  // valuePropName="checked"
                >
                  <FormInputRender
                    onChange={() => {
                      onChangeJobDepend();
                    }}
                    item={{
                      title: "dependAnotherJob",
                      dataIndex: "dependAnotherJob"
                      // valueType: "switch"
                    }}
                    intl={intl}
                    type="form"
                  />
                </Form.Item>
              ) : null}
              <Form.Item
                label={"Nhiệm vụ cha"}
                name="parentId"
                rules={[
                  { required: false, message: "Vui lòng chọn nhiệm vụ cha" }
                ]}
                extra={
                  <span style={{ fontSize: "12px", color: "#fa8c16" }}>
                    (*) Vui lòng reload lại trang để cập nhật dữ liệu nếu thay
                    đổi trường này
                  </span>
                }
                // valuePropName="checked"
              >
                <Select
                  showSearch
                  placeholder={"Chọn nhiệm vụ cha"}
                  optionFilterProp="children"
                  filterOption={(input, option: any) => {
                    return (
                      option.children.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    );
                  }}
                  onChange={() => {
                    onChangeJobDepend();
                  }}
                >
                  <Select.Option value={0}>
                    <span>{`---CHỌN NHIỆM VỤ CHA---`}</span>
                  </Select.Option>
                  {getJobs().map(i => (
                    <Select.Option key={`jobs-${i.id}`} value={i.id}>
                      <span>{i.name}</span>
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                label={"Ngày hết hạn"}
                name="outDateTime"
                rules={[{ required: false }]}
                extra={
                  <span style={{ color: "red" }}>{messageChangeDeadline}</span>
                }
              >
                <FormInputRender
                  item={{ dataIndex: "outDateTime", valueType: "date" }}
                  intl={intl}
                  type="form"
                  disabled={dependJob}
                />
              </Form.Item>

              <Form.Item
                label={"Phụ trách"}
                name="assignUserId"
                rules={[{ required: false }]}
              >
                <Select
                  showSearch
                  filterOption={(input: any, option: any) =>
                    option.key.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {counter.employees.map(i => (
                    <Select.Option
                      value={i.userId?.id}
                      key={i.userId?.id + i.userId?.name}
                    >
                      <div>
                        {" "}
                        <Avatar size="small" src={i?.userId?.avatar || ""} />
                        <span style={{ marginLeft: "2px" }}>
                          {i?.userId?.name}
                        </span>
                      </div>
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                label={"Tags"}
                name="tagIds"
                rules={[{ required: false }]}
              >
                <Select mode="tags">
                  {counter.tags.map(i => (
                    <Select.Option key={`tags-${i.id}`} value={i.id}>
                      <span style={{ color: i?.color ?? "#000" }}>
                        {i.name}
                      </span>
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                label={"Độ ưu tiên"}
                name="priority"
                rules={[{ required: false }]}
              >
                <FormInputRender
                  item={{
                    dataIndex: "priority",
                    valueEnum: {
                      Urgent: {
                        text: "Cấp bách",
                        status: "Default",
                        color: "#ec3b3b",
                        isText: true
                      },
                      High: {
                        text: "Cao",
                        status: "Default",
                        color: "#ec3b3b",
                        isText: true
                      },
                      Medium: {
                        text: "Trung bình",
                        status: "Default",
                        color: "#ec3b3b",
                        isText: true
                      },
                      Low: {
                        text: "Thấp",
                        status: "Processing",
                        color: "#ec3b3b",
                        isText: true
                      }
                    }
                  }}
                  intl={intl}
                  type="form"
                />
              </Form.Item>
              <Form.Item
                label={"Ghi chú"}
                name="note"
                rules={[{ required: false }]}
              >
                <FormInputRender
                  item={{ dataIndex: "note", valueType: "textarea" }}
                  intl={intl}
                  type="form"
                />
              </Form.Item>
            </Form>
          )}
        </>
      </Drawer>
      {/* <GroupTimeLine
        data={itemGroupTimeLine}
        isView={isViewModal}
        onCancelModal={setIsViewModal}
      /> */}
      <HistoryChangeModal
        isView={isViewHistory}
        onCancelModal={setIsViewHistory}
        data={recordView}
      />
    </>
  );
};

export default JobDetail;
