import React from "react";
import { FolderOutlined } from "@ant-design/icons";
import { v4 as uuid } from "uuid";
import dayjs from "dayjs";
import { GroupJobProps, JobProps } from "../interface";
import StatusIcon from "./Content/StatusIcon";

export const findNode = (
  data: (GroupJobProps | JobProps)[],
  key: string | number,
  callback: any
) => {
  for (let i = 0; i < data.length; i++) {
    if (data[i].key === key) {
      return callback(data[i], i, data);
    }
    if (data[i].children) {
      findNode(data[i].children, key, callback);
    }
  }
};
export const findNodeJobId = (
  data: (GroupJobProps | JobProps)[],
  id: string | number,
  callback: any
) => {
  for (let i = 0; i < data.length; i++) {
    if (`job` + data[i].id === id) {
      return callback(data[i], i, data);
    }
    if (data[i].children) {
      findNodeJobId(data[i].children, id, callback);
    }
  }
};

export function generateTemplate(data: any) {
  let arr;
  arr = data.map((item: any, idx: number) => {
    const temp: GroupJobProps = {
      ...item,
      id: item.groupId,
      type: "grouptask",
      title: item.name,
      name: item.name,
      key: `${uuid()}`,
      icon: <FolderOutlined />,
      children: (item?.jobs ?? []).map((itemc: any, idxc: number) => {
        let children: any = itemc?.children || [];
        const findChildren = (children: any) => {
          for (let i = 0; i < children.length; i++) {
            children[i] = Object.assign(children[i], {
              type: "task",
              title: children[i].name,
              name: children[i].name,
              key: `${uuid()}`,
              // isLeaf: false,
              icon: <StatusIcon status={children[i].status} />
            });
            if (Array.isArray(children[i].children)  && children[i].children.length > 0) {
              findChildren(children[i].children);
            }else {
              children[i] = Object.assign(children[i], {
                // isLeaf: true,
              });
            }
          }
          return children
        };
        children = findChildren(children)
        // children = children.map((itemchild: any, idxc: number) => {
        //   return {
        //     ...itemchild,
        //     type: "task",
        //     title: itemchild.name,
        //     name: itemchild.name,
        //     key: `${uuid()}`,
        //     isLeaf: true,
        //     icon: <StatusIcon status={itemchild.status} />
        //   };
        // });
        console.log(itemc.name,"Aaa",children);
        return {
          ...itemc,
          type: "task",
          title: itemc.name,
          name: itemc.name,
          key: `${uuid()}`,
          // isLeaf: (children && children.length > 0) ?  false : true,
          icon: <StatusIcon status={itemc.status} />,
          children
        };
      })
    };
    return temp;
  });
  return arr;
}

export const tranformDateTime = (data: any) => {
  return data.outDateTime && typeof data.outDateTime === "number"
    ? dayjs(data.outDateTime)
    : data.outDateTime;
};
