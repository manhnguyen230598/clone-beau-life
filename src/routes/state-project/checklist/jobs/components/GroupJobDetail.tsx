import React, { FC, useState, useEffect } from 'react';
import { Drawer, Form, Button } from 'antd';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';
import { FormInputRender } from '../../../../../packages/pro-table/form';
import { useIntl } from '../../../../../packages/pro-table/component/intlContext';
import { GroupJobProps, ShowJobDetailType } from '../interface';
import * as groupJobServices from '../../../../../services/groupJob';

interface GroupJobDetailProps {
  record: GroupJobProps;
  drawerVisible: boolean;
  onChange?: React.Dispatch<React.SetStateAction<ShowJobDetailType>>;
  changeNode?: (node: GroupJobProps) => void;
}

const FooterDrawer = ({ onOk }: {
  onOk: (e: any) => void;
}) => {
  return (
    <Button onClick={onOk} type="primary">Ok</Button>
  );
};

/* const DescriptionItem = ({ title, content }: {
  title?: string | React.Component;
  content: number | string | React.Component;
}) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title ? `${title}:` : ""}</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
); */

const GroupJobDetail: FC<GroupJobDetailProps> = (props: GroupJobDetailProps) => {
  const {
    record,
    drawerVisible,
    onChange,
    changeNode
  } = props;
  const [form] = Form.useForm();
  const intl = useIntl();
  const [data, setData] = useState(record);
  const triggerChange = (v: boolean) => {
    onChange?.((origin: ShowJobDetailType) => ({
      ...origin,
      visible: v
    }));
  };

  const onClose = () => {
    triggerChange(false);
  };

  const onOk = (e: any) => {
    form.validateFields()
      .then(values => {
        console.log(`🚀 ~ file: JobDetail.tsx ~ line 60 ~ form.validateFields ~ values`, values);
        const params = cloneDeep(values);
        if(record.id) {
          changeNode?.({
            ...record,
            ...params
          })

          /* groupJobServices.update(record.id, params).then(res => {
            if(res && res.status == 200) {
              // console.log(res.data)
              
              message.success("Cập nhật thành công!")
            }
          }); */
        }
      })
      .catch(err => {

        console.log(`🚀 ~ file: JobDetail.tsx ~ line 181 ~ form.validateFields ~ err`, err);
      });
  };

  useEffect(() => {
    if(record?.id) {
      groupJobServices.get(record.id, {}).then(res => {
        if(res && res.status == 200) {
          const resData = get(res, "data.data[0]", {});
          setData(resData);
        }
      });
    }
  }, []);

  return (
    <Drawer
      width={640}
      style={{ width: "640px !important" }}
      className="nv-drawer-detail"
      title={<span style={{ color: "#f09b1b" }}>{`Chi tiết ${get(record, "name", "-").toUpperCase()}`}</span>}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
      footer={(record && record.id) ? <FooterDrawer onOk={onOk} /> : null}
    >
      <Form
        form={form}
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          ...data
        }}
      >
        <Form.Item
          label={""}
          name="name"
          rules={[
            { required: true, message: 'Vui lòng nhập thông tin nhóm' }
          ]}
        >
          <FormInputRender
            item={{ dataIndex: "name", valueType: "text" }}
            intl={intl}
            type="form"
          />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default GroupJobDetail;
