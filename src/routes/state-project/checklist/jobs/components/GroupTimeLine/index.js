import React, { useState, useEffect } from "react";
import { Modal, Tree, message } from "antd";
import * as jobService from "src/services/job";
import moment from "dayjs";
import StatusIcon from "../Content/StatusIcon";
const ModalJob = props => {
  let { data, isView, onCancelModal } = props;
  const [jobs, setJobs] = useState([]);

  useEffect(() => {
    if (data && data.id) {
      jobService.getList({
        queryInput: { groupTimelineId: data.id },
        sort: [{ indexTimeline: "asc" }],
        populate: "assignUserId"
      }).then(rs => {
        let temp = rs?.data?.data?.map((e, index) => {
          return {
            key: e.id,
            title: index + 1 + ". " + e.name,
            data: e,
            index: index + 1,
            icon: <StatusIcon status={e.status} />
          };
        });
        setJobs(temp);
      }).catch(err => {

      });
    }
    return () => undefined;
  }, [data]);

  const onDrop = info => {
    // console.log(info);
    let { dropPosition, dragNode } = info;
    let arrayData = [...jobs];
    let splitArray = arrayData.filter(e => e.index !== dragNode.index);
    if (dropPosition === -1) {
      splitArray.splice(0, 0, dragNode);
    } else {
      splitArray.splice(dropPosition, 0, dragNode);
    }
    let tempArray = [];
    splitArray.map((e, index) => {
      tempArray.push({
        id: e.key,
        index: index
      });
    });
    let temp = splitArray.map((e, index) => {
      return {
        key: e.key,
        title: index + 1 + ". " + e.data.name,
        data: e.data,
        index: index + 1,
        icon: <StatusIcon status={e.data.status} />
      };
    });
    setJobs(temp);
    jobService
      .updateSequenceTimeline({
        array: tempArray
      })
      .then(res => {
        message.success("Thay đổi thứ tự thành công");
        // fetchData();
      })
      .catch(err => {
        if (err.type === "ERROR_SERVER") {
          message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
        }
        // fetchData();
      });
    // if (dropPosition === -1) {
    //   console.log(`🚀 ~ 3.a ~ onDrop ~ ar`, dropPosition);
    //   jobService
    //     .updateSequenceTimeline({
    //       id: dragNode.key,
    //       // @ts-ignore
    //       index: dropPosition
    //     })
    //     .then(res => {
    //       message.success("Thay đổi thứ tự thành công");
    //     })
    //     .catch(err => {
    //       if (err.type === "ERROR_SERVER") {
    //         message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
    //       }
    //     });
    // } else {
    //   // @ts-ignore
    //   console.log(`🚀 ~ 3.b ~ onDrop ~ ar`, dropPosition);
    //   jobService
    //     .updateSequenceTimeline({
    //       id: dragNode.key,
    //       // @ts-ignore
    //       index: dropPosition + 1
    //     })
    //     .then(res => {
    //       message.success("Thay đổi thứ tự thành công");
    //     })
    //     .catch(err => {
    //       if (err.type === "ERROR_SERVER") {
    //         message.error(err?.messasge || "Đã có lỗi xảy ra", 5);
    //       }
    //     });
    // }
    // // if (dragNode.index - 1 === dropPosition) {
    // //   console.log("not update");
    // // } else {
    // //   if (dropPosition !== -1) {
    // //     if (dragNode.index > node.index) {
    // //       console.log(`update node`, dragNode.index);
    // //       console.log(`update dragnode`, node.index + 1);
    // //     } else {
    // //       console.log(`update node`, dragNode.index);
    // //       console.log(`update dragnode`, node.index);
    // //     }
    // //   } else {
    // //     console.log(`update node`, dragNode.index);
    // //     console.log(`update dragnode`, 1);
    // //   }
    // // }
    // fetchData();
  };
  return (
    <Modal
      visible={isView}
      title={`Danh sách công việc nhóm ${data?.name || "-"}`}
      onCancel={() => onCancelModal(false)}
      width={"50%"}
      onOk={() => onCancelModal(false)}
    >
      {jobs.length === 0 ? (
        <span>Không có công việc nào</span>
      ) : (
        <Tree
          showIcon
          treeData={jobs}
          draggable={true}
          onDrop={onDrop}
          blockNode
          className="nv-checklist-tree"
          titleRender={node => {
            return (
              <div
                style={{
                  borderBottom: "1px solid #f8f8f8",
                  display: "flex",
                  justifyContent: "space-between",
                  padding: "0.25rem"
                }}
              >
                <span>{node.title}</span>
                <div
                  style={{
                    display: "flex",
                    width: "50%"
                    //   justifyContent: "space-evenly"
                  }}
                >
                  <span style={{ width: "33%" }}>
                    {node.data.assignUserId?.name || "_"}
                  </span>
                  <span style={{ width: "33%" }}>
                    {node.data.status || "_"}
                  </span>
                  <span style={{ width: "33%" }}>
                    {node.data.outDateTime
                      ? moment(node.data.outDateTime).format("DD/MM/YYYY")
                      : ""}
                    {/* <CalendarOutlined /> */}
                  </span>
                </div>
              </div>
            );
          }}
        />
      )}
    </Modal>
  );
};
export default ModalJob;
