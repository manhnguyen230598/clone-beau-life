import React, { FC, useEffect } from 'react';
import { Drawer, Form, Button, Select, Checkbox, Avatar } from 'antd';
/* import get from 'lodash/get'; */
import cloneDeep from 'lodash/cloneDeep';
import { FormInputRender } from '../../../../../packages/pro-table/form';
import { useIntl } from '../../../../../packages/pro-table/component/intlContext';
import Container from '../container';
import { IFitlerProps } from '../interface';
import dayjs from "dayjs";

interface FilterDrawerProps {
  drawerVisible: {
    visible: boolean;
    options: IFitlerProps | {};
  };
  setDrawerVisible?: React.Dispatch<React.SetStateAction<{
    visible: boolean;
    options: IFitlerProps | {};
  }>>;
  filterData?: (opts: IFitlerProps) => void;
}

const statusOptions = [
  {
    label: <span>
      <i className="ic-quire-state ic-quire-to-do lb iconc-51"></i>&nbsp;Backlog
    </span>,
    value: 'Backlog'
  },
  {
    label: <span>
      <i className="ic-quire-state status-icon b-1">
        <i className="x14 pie iconc-02">
          <i className="pie-mask-wrap" style={{ transform: "rotate(36deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
            <i className="pie-mask" style={{ transform: "rotate(180deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
          </i>
          <i className="pie-mask-wrap" style={{ transform: "rotate(-180deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
            <i className="pie-mask" style={{ transform: "rotate(180deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
          </i>
        </i>
      </i>&nbsp;Processing
    </span>,
    value: 'Processing'
  },
  {
    label: <span>
      <i className="ic-quire-state status-icon b-1">
        <i className="x14 pie iconc-12">
          <i className="pie-mask-wrap" style={{ transform: "rotate(-162deg)", clip: "rect(0, 12px, 12px, 6px)" }}>
            <i className="pie-mask" style={{ transform: "rotate(162deg)", clip: "rect(0, 6px, 12px, 0)" }}></i>
          </i>
        </i>
      </i>&nbsp;Overdue
    </span>,
    value: 'Overdue'
  },
  {
    label: <span>
      <i className="ic-quire-state ic-quire-completed lb iconc-43"></i>&nbsp;Done
    </span>,
    value: 'Done'
  },
];

const transformOptions = (otps: IFitlerProps) => {
  if(otps.outDateTime) {
    if(Array.isArray(otps.outDateTime)) {
      const gte = dayjs(otps.outDateTime[0]).valueOf();
      const lte = dayjs(otps.outDateTime[1]).valueOf();
      otps.outDateTime = {
        '>=': gte, '<=': lte
      };
    }
  }
  if(otps.status && Array.isArray(otps.status)) {
    const arrStatus = otps.status;
    otps.status = {
      in: arrStatus
    };
  }
  return otps;
};

const FooterDrawer = ({ onOk, onReset }: {
  onOk: (e: any) => void;
  onReset: (e: any) => void;
}) => {
  return (
    <>
      <Button onClick={onOk} type="primary">Ok</Button>
      <Button onClick={onReset} type="default">Reset</Button>
    </>
  );
};

const FilterDrawer: FC<FilterDrawerProps> = (props: FilterDrawerProps) => {
  const {
    drawerVisible,
    setDrawerVisible,
    filterData
  } = props;
  const counter = Container.useContainer();
  const [form] = Form.useForm();
  const intl = useIntl();

  const triggerChange = (v: boolean, opts?: IFitlerProps) => {
    if(opts) {
      setDrawerVisible?.((origin: {
        visible: boolean;
        options: IFitlerProps | {};
      }) => ({
        ...origin,
        visible: v,
        options: opts
      }));
    } else {
      setDrawerVisible?.((origin: {
        visible: boolean;
        options: IFitlerProps | {};
      }) => ({
        ...origin,
        visible: v
      }));
    }
  };

  const onClose = () => {
    triggerChange(false);
  };

  const onOk = (e: any) => {
    form.validateFields()
      .then(values => {
        console.log(`🚀 ~ file: FilterDrawer.tsx ~ form.validateFields ~ values`, values);
        const opts = transformOptions(cloneDeep(values));
        filterData?.(opts);
        setDrawerVisible?.((origin: {
          visible: boolean;
          options: IFitlerProps | {};
        }) => ({
          ...origin,
          visible: false,
          options: values
        }));
      })
      .catch(err => {
        console.log(`🚀 ~ file: FilterDrawer.tsx ~ form.validateFields ~ err`, err);
      });
  };

  const onReset = (e: any) => {
    form.resetFields();
    setDrawerVisible?.((origin: {
      visible: boolean;
      options: IFitlerProps | {};
    }) => ({
      ...origin,
      visible: false,
      options: {}
    }));
    filterData?.({});
  };

  useEffect(() => {
    /* if(record?.id) {
      groupJobServices.get(record.id, {}).then(res => {
        if(res && res.status == 200) {
          const resData = get(res, "data.data[0]", {});
          setData(resData);
        }
      });
    } */
  }, []);

  useEffect(() => {
    form.setFieldsValue({
      ...drawerVisible.options
    });
  }, [drawerVisible.options]);

  return (
    <Drawer
      width={640}
      style={{ width: "46vw !important" }}
      className="nv-drawer-detail"
      title={<span style={{ color: "#f09b1b" }}>{`Bộ lọc`}</span>}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible.visible}
      footer={<FooterDrawer onOk={onOk} onReset={onReset} />}
    >
      <Form
        form={form}
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          ...drawerVisible.options
        }}
      >
        <Form.Item
          label={"Người phụ trách"}
          name="assignUserId"
          rules={[
            { required: false, message: 'Vui lòng nhập thông tin nhóm' }
          ]}
        >
          <Select
            showSearch
            filterOption={(input: any, option: any) => option.key.toLowerCase().indexOf(input.toLowerCase()) >= 0

            }
          >
            {counter.employees.map(i => (
              <Select.Option value={i.userId?.id} key={i.userId?.id + i.userId?.name}>
                <div>
                  {" "}
                  <Avatar size="small" src={i?.userId?.avatar || ""} />
                  <span style={{ marginLeft: "2px" }}>
                    {i?.userId?.name}
                  </span>
                </div>
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label={"Hạn chót"}
          name="outDateTime"
          rules={[
            { required: false, message: 'Vui lòng nhập thông tin nhóm' }
          ]}
        >
          <FormInputRender
            item={{ dataIndex: "outDateTime", valueType: "dateRange" }}
            intl={intl}
            type="form"
          />
        </Form.Item>
        <Form.Item
          label={"Trạng thái"}
          name="status"
          rules={[
            { required: false, message: 'Vui lòng nhập thông tin nhóm' }
          ]}
        >
          <Checkbox.Group
            options={statusOptions}
          // disabled
          // defaultValue={['Apple']}
          />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default FilterDrawer;
