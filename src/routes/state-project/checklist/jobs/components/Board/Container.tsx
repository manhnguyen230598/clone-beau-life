import React, { FC, useState, memo, useCallback } from 'react';
// import { useDrop } from 'react-dnd';
import update from 'immutability-helper';
import { columns as dataColumns } from "./constants";
import { tasks } from "./tasks";
import { Column } from './Column';
import { MovableItem } from './Box';
import type { TaskProp } from './tasks';
import type { ColumnProp } from './constants';

export const Container: FC = memo(function Container() {
  const [items, setItems] = useState<TaskProp[]>(tasks);
  const [columns, setColumns] = useState<ColumnProp[]>(dataColumns);

  const moveCardHandler = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      console.log(`🚀 ~ file: Container.tsx ~ line 35 ~ Container ~ hoverIndex`, hoverIndex);
      console.log(`🚀 ~ file: Container.tsx ~ line 35 ~ Container ~ dragIndex`, dragIndex);
      const dragItem = items[dragIndex];

      if(dragItem) {
        setItems(
          update(items, {
            $splice: [
              [dragIndex, 1],
              [hoverIndex, 0, dragItem],
            ],
          }),
        );
      }
    }, [items]);

  const moveColumnHandler = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragItem = columns[dragIndex];

      if(dragItem) {
        setColumns(
          update(columns, {
            $splice: [
              [dragIndex, 1],
              [hoverIndex, 0, dragItem],
            ],
          }),
        );
      }
    },
    [columns]
  );

  const returnItemsForColumn = (columnName: string) => {
    return items
      .filter((item) => item.column === columnName)
      .map((item, index) => (
        <MovableItem
          key={item.id}
          name={item.name}
          currentColumnName={item.column}
          setItems={setItems}
          index={index}
          moveCardHandler={moveCardHandler}
        />
      ));
  };

  return (
    <div className="nv-checklist-board-container">
      {columns.map((item, index) => (
        <Column
          index={index}
          id={item.id}
          title={item.title}
          className="nv-checklist-board-column do-it-column"
          moveColumnHandler={moveColumnHandler}
        >
          {returnItemsForColumn(item.title)}
        </Column>
      ))}
    </div>
  );
});
