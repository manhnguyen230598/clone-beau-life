import React, { CSSProperties, FC, memo, useRef } from 'react';
import { useDrop, /* useDrag, DropTargetMonitor */ } from 'react-dnd';
// import { XYCoord } from 'dnd-core';
import { COLUMN_NAMES } from './constants';
import { ItemTypes } from './ItemTypes';

const style: CSSProperties = {

};

export interface ColumnProps {
  children: any,
  className: string
  title: string
  name?: string
  id: number,
  index: number,
  moveColumnHandler: (dragIndex: number | any, hoverIndex: number | any) => void
}

export const Column: FC<ColumnProps> = memo(
  ({ children, className, title, id, index, moveColumnHandler }) => {
    const ref = useRef<HTMLDivElement>(null);

    const [{ isOver, canDrop, draggingColor }, drop] = useDrop({
      accept: [ItemTypes.BOX],
      drop: () => ({ name: title }),
      collect: (monitor) => ({
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
        draggingColor: monitor.getItemType()
      }),
      // Override monitor.canDrop() function
      canDrop: (item: any) => {
        console.log(`🚀 ~ file: Column.tsx ~ line 34 ~ item`, item);
        const { DO_IT, IN_PROGRESS, AWAITING_REVIEW, DONE } = COLUMN_NAMES;
        const { currentColumnName } = item;
        return (
          currentColumnName === title ||
          (currentColumnName === DO_IT && title === IN_PROGRESS) ||
          (currentColumnName === IN_PROGRESS &&
            (title === DO_IT || title === AWAITING_REVIEW)) ||
          (currentColumnName === AWAITING_REVIEW &&
            (title === IN_PROGRESS || title === DONE)) ||
          (currentColumnName === DONE && title === AWAITING_REVIEW)
        );
      },
      /* hover(item: any, monitor: DropTargetMonitor) {
        console.log(`🚀 ~ file: Column.tsx ~ line 48 ~ $hover ~ monitor`, monitor);
        if(!ref.current) {
          return;
        }
        const dragIndex = item.index;
        const hoverIndex = index;

        console.log(`🚀 ~ file: Column.tsx ~ line 50 ~ hover ~ ref.current`, ref.current);
        // Don't replace items with themselves
        if(dragIndex === hoverIndex) {
          return;
        }

        // Determine rectangle on screen
        const hoverBoundingRect = ref.current?.getBoundingClientRect();

        // Get vertical middle
        const hoverMiddleY =
          (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

        // Determine mouse position
        const clientOffset = monitor.getClientOffset();

        // Get pixels to the top
        const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%

        // Dragging downwards
        if(dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
          return;
        }

        // Dragging upwards
        if(dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
          return;
        }

        // Time to actually perform the action
        moveColumnHandler(dragIndex, hoverIndex);

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        item.index = hoverIndex;
      }, */
    });

    /* const [{ isDragging }, drag] = useDrag<any, any, any>({
      type: ItemTypes.COLUMN,
      item: { index, id, title },
      end: (item, monitor) => {
        console.log(`🚀 ~ file: Dustbin.tsx ~ line 49 ~ constColumn:FC<ColumnProps>=memo ~ monitor`, monitor);
        console.log(`🚀 ~ file: Dustbin.tsx ~ line 49 ~ constColumn:FC<ColumnProps>=memo ~ item`, item);
      },
      collect: (monitor) => ({
        isDragging: monitor.isDragging()
      })
    }); */

    const getBackgroundColor = () => {
      if(isOver) {
        if(canDrop) {
          return "rgb(188,251,255)";
        } else if(!canDrop) {
          return "rgb(255,188,188)";
        }
      }
      return "";
    };

    drop(ref);

    console.log(`🚀 ~ file: Column.tsx ~ line 26 ~ draggingColor`, draggingColor);

    return (
      <div
        ref={ref}
        className={className}
        style={{
          ...style, backgroundColor: getBackgroundColor(),
          // opacity: isDragging ? 0.5 : 1,
        }}
      >
        <p>{title}</p>
        {children}
      </div>
    );
  });

/* export const Dustbin: FC<DustbinProps> = memo(function Dustbin({
  name,
  accept,
  lastDroppedItem,
  onDrop,
}) {
  const [{ isOver, canDrop }, drop] = useDrop({
    accept,
    drop: onDrop,
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  const isActive = isOver && canDrop;
  let backgroundColor = '#222';
  if(isActive) {
    backgroundColor = 'darkgreen';
  } else if(canDrop) {
    backgroundColor = 'darkkhaki';
  }

  const renderBox = () => {
    if(lastDroppedItem && lastDroppedItem.dustbinName == name && lastDroppedItem.isDropped){
      console.log(`🚀 ~ file: Dustbin.tsx ~ line 50 ~ renderBox ~ lastDroppedItem`, lastDroppedItem);
      console.log(`🚀 ~ file: Dustbin.tsx ~ line 51 ~ renderBox ~ name`, name);
      return <Box name={lastDroppedItem.name} type={lastDroppedItem.type} isDropped={lastDroppedItem.isDropped} />;
    }
    return null;
  };

  return (
    <div ref={drop} role="Dustbin" style={{ ...style, backgroundColor }}>
      {isActive
        ? 'Release to drop'
        : `This dustbin accepts: ${accept.join(', ')}`}


    { renderBox() }
    </div >
  );
}); */
