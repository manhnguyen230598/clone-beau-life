export const COLUMN_NAMES = {
  DO_IT: 'Do it',
  IN_PROGRESS: 'In Progress',
  AWAITING_REVIEW: 'Awaiting review',
  DONE: 'Done',
};
export interface ColumnProp {
  id: number
  name: string
  title: string
}

export const columns: ColumnProp[] = [
  { id: 1, name: 'DO_IT', title: 'Do it' },
  { id: 2, name: 'IN_PROGRESS', title: 'In Progress' },
  { id: 3, name: 'AWAITING_REVIEW', title: 'Awaiting review' },
  { id: 4, name: 'DONE', title: 'Done' },
];
