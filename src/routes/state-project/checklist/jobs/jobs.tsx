import React, { FC, useState, useEffect } from 'react';
import axios from 'axios';
import { Card, Tooltip, Button, Divider, message } from 'antd';
import { /* AlignRightOutlined, PicCenterOutlined, */ ProjectOutlined, FileExcelOutlined, FilterOutlined } from '@ant-design/icons';
// import type { DispatchProp } from 'react-redux';
import { saveAs } from "file-saver";
import ChecklistContent from './components/ChecklistContent';
import ChecklistBoard from './components/ChecklistBoard';
import { template as dataTemplate } from './templates';
import type { GroupJobProps, TagProps, RoleUserProjectJobProps, ProjectProps, ShowJobDetailType, IChecklistInfo, IFitlerProps } from './interface';
import * as checklistServices from '../../../../services/checklist';
import * as tagJobServices from '../../../../services/tagJob';
import * as roleUserProjectJobServices from '../../../../services/roleUserProjectJob';
import * as groupJobServices from '../../../../services/groupJob';
import * as relatedTaskServices from '../../../../services/relatedTasks';
import Container from './container';
import { generateTemplate } from './components/utils';
import FilterDrawer from './components/FilterDrawer';
import { Switch } from 'antd';
export interface CheckListProjectProps {
  // data: GroupJobProps[];
  projectId: number | string;
  projectInfo: ProjectProps | null;
  checklistInfo?: IChecklistInfo | null;
}

// const RESOURCE = "checklist";
/* const colorChecklistLayoutNormal = '#000';
const colorChecklistLayoutActive = '#3296e7';

const iconLayoutChecklistColor = (curLayout: string, typeLayout: string) => {
  return {
    color: curLayout == typeLayout ? colorChecklistLayoutActive : colorChecklistLayoutNormal
  };
}; */

const renderLayout = ({ data, layout, tags, createByTemplate = null, employees, showEditJobDetail, fetchData, expand }: {
  data?: GroupJobProps[]
  layout?: string;
  tags?: TagProps[];
  createByTemplate?: boolean | null;
  employees?: RoleUserProjectJobProps[];
  showEditJobDetail: React.Dispatch<React.SetStateAction<ShowJobDetailType>>;
  fetchData? : any,
  expand? : boolean

}) => {
  if(layout == 'board') {
    return <ChecklistBoard data={data} />;
  }
  return <ChecklistContent
    tags={tags}
    createByTemplate={createByTemplate}
    employees={employees}
    showEditJobDetail={showEditJobDetail}
    // fetchData ={fetchData}
    expandAll={expand}
  />;
};

const CheckListProject: FC<CheckListProjectProps> = (props: CheckListProjectProps) => {
  const {
    projectId,
    projectInfo,
    checklistInfo
  } = props;
  const counter = Container.useContainer();
  const [layout,] = useState('tree');
  const [initData, setInitData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [expand, setExpand] = useState(false);
  const [showFilter, setShowFilter] = useState<{
    visible: boolean;
    options: IFitlerProps | {};
  }>({
    visible: false,
    options: {}
  });
  const [isClickCreateByTemplate, setIsClickCreateByTemplate] = useState<{
    isClicK: boolean;
    defaultTemplate: string;
    okTemplate: boolean;
    startChecklistOk: string;
  }>({
    isClicK: false,
    defaultTemplate: '',
    okTemplate: false,
    startChecklistOk: ''
  });

  const dataFromDb = React.useMemo(() => {
    return initData.length;
  }, [initData]);

  const showStartChecklist = React.useMemo(() => {
    if(isClickCreateByTemplate.startChecklistOk === '') {
      if(projectInfo?.isStartChecklist === false) {
        if(isClickCreateByTemplate.okTemplate === false && !dataFromDb) {
          return false;
        }
        return true;
      }
      return false;
    } else {
      // @ts-ignore
      return isClickCreateByTemplate.startChecklistOk !== 'true';
    }
  }, [projectInfo, isClickCreateByTemplate, dataFromDb]);

  const createChecklistClient = (e: any, byTemp: boolean) => {
    if(byTemp) {
      counter.setData(dataTemplate);
    } else {
      // TODO: call api tạo template rỗng
      // createCheckListbyTempOrNot(projectId, false, []);
    }
    setIsClickCreateByTemplate(origin => ({
      ...origin,
      isClicK: true,
      defaultTemplate: byTemp ? 'true' : 'false'
    }));
  };

  const okTemplate = (e: any) => {
    /* setIsClickCreateByTemplate(origin => ({
      ...origin,
      okTemplate: true
    })); */
    const dataG = counter.data;
    const params = {
      projectId,
      defaultTemplate: isClickCreateByTemplate.defaultTemplate == "true",
      groupJobs: dataG
    };
    checklistServices.createChecklist(params)
      .then(res => {
        if(res?.status == 200) {
          message.info("Tạo checklist dự án theo template thành công", 10);
          setIsClickCreateByTemplate(origin => ({
            ...origin,
            okTemplate: true
          }));
        }
      })
      .catch(err => {
        message.error(err.message || 'Tạo checklist dự án thất bại', 10);
      });
  };

  const startChecklist = (e: any) => {
    checklistServices.startChecklist({ projectId })
      .then(res => {
        if(res?.status == 200) {
          setIsClickCreateByTemplate((origin: any) => ({
            ...origin,
            startChecklistOk: 'true'
          }));
          message.info("Khởi checklist dự án thành công", 10);
        }
      })
      .catch(err => {
        message.error(err.message || 'Khởi tạo checklist dự án thất bại', 10);
        setIsClickCreateByTemplate((origin: any) => ({
          ...origin,
          startChecklistOk: 'false'
        }));
      });
  };

  const exportExcel = (e: any) => {
    const token = localStorage.getItem("token");
    checklistServices.exportExcel(projectId, token)
      .then(res => {
        if(res && res.data) {
          const blob = new Blob([res.data], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          });
          try {
            saveAs(blob, "thong_ke.xlsx");
          } catch(error) {
            console.log("error: ", error);
            message.error("Không xuất được báo cáo!");
          }
        } else {
          message.error("Không xuất được báo cáo!");
        }
      })
      .catch(error => {
        console.log("error: ", error.message);
        message.error("Không xuất được báo cáo!");
      });
  };

  const filterData = React.useCallback((opts: IFitlerProps) => {
    setLoading(true);
    checklistServices.getByProject({
      projectId,
      options: opts
    }).then(res => {
      if(res && res.status == 200) {
        setInitData(res?.data?.data);
        counter.setData(generateTemplate(res?.data?.data));
        setLoading(false);
      }
    }).catch(err => {
      console.log(`🚀 ~ file: jobs.tsx ~ filterData ~ err`, err);
    });
  }, [projectId, counter]);

  useEffect(() => {
    setLoading(true);
    // const ac = new AbortController();
    const cancelToken = axios.CancelToken;
    const source = cancelToken.source();
    Promise.all([
      checklistServices.getByProject({ projectId }, { cancelToken: source.token, }),
      tagJobServices.getListWithCancel({
        limit: 1000
      }, { cancelToken: source.token, }),
      roleUserProjectJobServices.getListWithCancel({
        limit: 1000,
        populate: "userId",
        queryInput: {
          projectId
        }
      }, { cancelToken: source.token, }),
      groupJobServices.getListWithCancel({
        limit: 1000,
        queryInput: {
          projectId
        }
      }, { cancelToken: source.token, }),
      relatedTaskServices.getListWithCancel({
        limit: 1000,
        queryInput: {
          projectId
        }
      }, { cancelToken: source.token, })
    ]).then(res => {
      if(res && res[0] && res[0].status == 200) {
        setInitData(res[0]?.data?.data);
        counter.setData(generateTemplate(res[0]?.data?.data));
      }
      if(res && res[1] && res[1].status == 200) {
        counter.setTags(res[1]?.data?.data);
      }
      if(res && res[2] && res[2].status == 200) {
        counter.setEmployees(res[2]?.data?.data);
      }
      if(res && res[3] && res[3].status == 200) {
        counter.setGroupJobs(res[3]?.data?.data);
      }
      if(res && res[4] && res[4].status == 200) {
        counter.setGroupTimelines(res[4]?.data?.data);
      }
      setLoading(false);
    }).catch(ex => {
      setLoading(false);
      // console.error(`🚀 ~ file: jobs.tsx ~ useEffect ~ error: `, ex);
    });
    return () => {
      source.cancel("unmount component cancel axios request!");
    };
  }, []);
  const fetchData =  ()=>{
    setLoading(true);
    const cancelToken = axios.CancelToken;
    const source = cancelToken.source();
    checklistServices.getByProject({ projectId }, { cancelToken: source.token, }).then((res)=>{
      if(res && res.status == 200) {
        setInitData(res?.data?.data);
        counter.setData(generateTemplate(res?.data?.data));
      }
      setLoading(false);
    });
  };
  /* useEffect(() => {
    setLoading(true);
    try {
      checklistServices.getByProject({
        projectId
      }).then(res => {
        if(res && res.status == 200) {
          setInitData(res?.data?.data);
          counter.setData(generateTemplate(res?.data?.data));
          setLoading(false);
        }
      });
      tagJobServices.getList({
        limit: 1000
      }).then(res => {
        if(res && res.status == 200) {
          counter.setTags(res?.data?.data);
        }
      });
      roleUserProjectJobServices.getList({
        limit: 1000,
        populate: "userId",
        queryInput: {
          projectId
        }
      }).then(res => {
        if(res && res.status == 200) {
          counter.setEmployees(res?.data?.data);
        }
      });
      groupJobServices.getList({
        limit: 1000,
        queryInput: {
          projectId
        }
      }).then(res => {
        if(res && res.status == 200) {
          counter.setGroupJobs(res?.data?.data);
        }
      });
      relatedTaskServices.getList({
        limit: 1000,
        queryInput: {
          projectId
        }
      }).then(res => {
        if(res && res.status == 200) {
          counter.setGroupTimelines(res?.data?.data);
        }
      });
    } catch(error) {
      setLoading(false);
    }
    return () => undefined;
  }, []); */

  useEffect(() => {
    let needCrudDb = false;
    if(projectInfo?.isStartChecklist === false) {
      if(isClickCreateByTemplate.startChecklistOk != '') {
        if(isClickCreateByTemplate.okTemplate === true || initData.length || isClickCreateByTemplate.startChecklistOk === 'true') {
          needCrudDb = true;
        } else {
          needCrudDb = false;
        }
      } else {
        if(isClickCreateByTemplate.okTemplate == true || initData.length) {
          needCrudDb = true;
        } else {
          needCrudDb = false;
        }
      }
    } else {
      needCrudDb = true;
    }
    counter.setNeedCrudDb(needCrudDb);
    return () => undefined;
  }, [projectInfo, isClickCreateByTemplate, initData]);

  console.log(`🚀 ~ file: jobs.tsx ~ line 255 ~ useEffect ~ counter`, counter);

  if(loading) {
    return (
      <div className="loader loading">
        <img src="/loading.gif" alt="loader" />
      </div>
    );
  }

  return (
    <React.Fragment>
      <Card className="gx-card gx-card-for-tree">
        {/* <div className="nv-checklist-tab">
        </div> */}
        <div className="nv-checklist-content">
          <div className="nv-checklist-toolbar">
            <div>
              {(checklistInfo?.checkCanCreate == true && !isClickCreateByTemplate.isClicK && !dataFromDb) && <>
                <Button
                  icon={<i className="icon icon-add-circle" style={{ fontSize: '12px', cursor: 'pointer' }} />}
                  onClick={(e) => createChecklistClient(e, false)}
                >
                  {`Tạo checklist rỗng`}
                </Button>&nbsp;&nbsp;
                <Button
                  icon={<i className="icon icon-anchor" style={{ fontSize: '12px', cursor: 'pointer' }} />}
                  onClick={(e) => createChecklistClient(e, true)}
                >
                  {`Tạo theo mẫu`}
                </Button>
              </>}
              {(
                (isClickCreateByTemplate.isClicK == true && !isClickCreateByTemplate.okTemplate && !dataFromDb)
              ) && <Button
                icon={<ProjectOutlined />}
                type="primary"
                onClick={okTemplate}
              >
                  {`Lưu lại`}
                </Button>}
              {showStartChecklist && <Button
                icon={<ProjectOutlined />}
                type="primary"
                onClick={startChecklist}
              >
                {`Khởi động dự án`}
              </Button>}
            </div>
            <div className="gx-d-flex gx-align-items-center gx-justify-content-center">
              <div>Mở rộng tất cả: <Switch defaultChecked={expand} onChange={e=>setExpand(e)}/>&nbsp;&nbsp;&nbsp;</div>
              <Tooltip title="Báo cáo excel">
                <FileExcelOutlined
                  onClick={(e) => {
                    exportExcel(e);
                  }}
                  style={{}}
                />
              </Tooltip>
              {counter.needCrudDb && <>
                &nbsp;&nbsp;<Tooltip title="Bộ lọc">
                  <FilterOutlined
                    onClick={(e) => {
                      setShowFilter?.((origin: {
                        visible: boolean;
                        options: IFitlerProps | {};
                      }) => ({
                        ...origin,
                        visible: true
                      }));
                    }}
                    style={{ color: (showFilter.options && Object.keys(showFilter.options).length > 0) ? '#3296e7' : '' }}
                  />
                </Tooltip>
              </>}
              {/* <Tooltip title="Tree"><AlignRightOutlined onClick={() => setLayout('tree')} style={{ ...iconLayoutChecklistColor(layout, 'tree') }} /></Tooltip>&nbsp;&nbsp; */}
              {/* <Tooltip title="Board"><PicCenterOutlined onClick={() => setLayout('board')} style={{ ...iconLayoutChecklistColor(layout, 'board') }} /></Tooltip>&nbsp;&nbsp;
              <Tooltip title="Chi tiết">
                <span
                  onClick={() => {
                    counter.setShowJobDetail(origin => ({
                      ...origin,
                      visible: true
                    }));
                  }}
                  style={{ fontSize: '14px', cursor: 'pointer' }}
                >{`>>`}</span>
              </Tooltip> */}
            </div>
          </div>
          <Divider />
          {renderLayout({
            layout,
            createByTemplate: isClickCreateByTemplate.isClicK,
            showEditJobDetail: counter.setShowJobDetail,
            fetchData,
            expand
          })}
        </div>
      </Card>
      <FilterDrawer
        drawerVisible={showFilter}
        setDrawerVisible={setShowFilter}
        filterData={filterData}
      />
    </React.Fragment>
  );
};

/* const mapStateToProps = (({ checklist }: any) => {
  const { data } = checklist;
  return { data };
}); */

// const CheckListProjectRedux = connect(mapStateToProps)(CheckListProject);

const CheckListWrap = (props: CheckListProjectProps) => (
  <Container.Provider initialState={props}>
    <CheckListProject {...props} />
  </Container.Provider>
);

export default CheckListWrap;
