import dayjs from 'dayjs';
export interface JobProps {
  type: 'task';
  id?: number;
  key: number | string;
  name: string;
  title: string;
  createdAt?: number | dayjs.Dayjs | null;
  updatedAt?: number | dayjs.Dayjs | null;
  projectId?: number;
  status?: string;
  groupJobId?: number;
  outDateTime?: number | dayjs.Dayjs | null;
  outDateTimeChange?: number | dayjs.Dayjs | null;
  note?: string | null;
  priority?: string | null;
  isApproved?: boolean;
  doneTime?: number | null;
  groupTimelineId?: number | null;
  indexTimeline?: number | null;
  tagIds?: number[] | string | null;
  numberDayOutDate?: number | dayjs.Dayjs | null;
  parentId?: number | null;
  assignUserId?: number | UserProps | null;
  creatorId?: number | null;
  children?: any;
  isLeaf?: boolean;
  icon?: ((nodeProps: any) => React.ReactNode) | React.ReactNode;
  isDelete?: boolean;
}

export interface GroupTimelineProps {
  id: number;
  name: string;
}
export interface GroupJobProps {
  type: 'grouptask';
  id?: number;
  key: number | string;
  name: string;
  title: string;
  children: Array<JobProps>
  icon?: ((nodeProps: any) => React.ReactNode) | React.ReactNode;
  isActive?: boolean;
}

export interface TagProps {
  id: number;
  name: string;
  color?: string;
}

export interface UserProps{
  id: number;
  name: number | any;
  email?: string;
  birth?: number | string;
  address?: string;
  avatar?: string | string[];
  cmnd_number?: string;
  cmnd_issue_date?: string;
  cmnd_province?: number;
  provinceId?: number;
  districtId?: number;
  wardId?: number;
  phone?: string;
  projectJoin?: number | string;
  experienceYear?: string;
  soldSuccessCount?: number;
  isApprove?: boolean;
  anotherAgency?: string;
  lastLogin?: number;
  position?: string;
  roleId?: number;
  agencyId?: number | any;
}

export interface RoleProjectJobProps {
  id: number;
  name: number | any;
  description?: string;
  canApproveJob?: boolean;
  maxDay?: number;
  projectId?: number;
  isNormal?: boolean | null;
}

export interface RoleUserProjectJobProps {
  id: number;
  projectId: number | any;
  roleProjectJobId: number | RoleProjectJobProps;
  userId: UserProps;
  isActive?: boolean;
}

export interface ProjectProps {
  id: number;
  name: string;
  thumbnail?: string;
  shortDescription?: string;
  projectAreaIds?: number;
  isHot?: string;
  isActive?: boolean;
  isViewAll?: boolean;
  sequence?: string;
  images?: string | string[];
  logoLinkWeb?: string;
  refLinkWeb?: string;
  refLinkFacebook?: string;
  type?: string;
  address?: string;
  provinceId?: number;
  districtId?: number;
  wardId?: number;
  investor?: string;
  minPriceText?: number;
  description?: string;
  templateCustomerFile?: string;
  isStartChecklist?: boolean;
  adminId?: number;
  kskdUserId?: number | null;
  createdAt?: number;
  updatedAt?: number;
}

export interface IChecklistInfo {
  role: object;
  countRequest: number;
  checkCanCreate: boolean;
}

export type ShowJobDetailType = {
  visible: boolean;
  type: string;
  record?: JobProps | GroupJobProps | {} | null;
}

export type IFitlerProps = {
  assignUserId?: number;
  outDateTime?: dayjs.Dayjs[] | object;
  status?: string[] | object;
}
