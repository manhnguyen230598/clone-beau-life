import React, { useState, useEffect, useRef } from "react";
import { Form, Input, Button, Space, Tooltip, Avatar, Popconfirm } from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  CloseOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import dayjs from "dayjs";
import ProTable from "packages/pro-table/Table";
import { get, union } from "lodash";
import RequireChangeDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import Modal from "antd/lib/modal/Modal";
import * as dataRequireChange from "src/services/requireChange";

const tranformRequireChangeParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "requireChange";
const RequireChangeList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();
  const [showModal, setShowModal] = useState(false);

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState();

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;

  const fetchData = () => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformRequireChangeParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: {
          ...initParams.filters,
          projectId: props.projectId
        },
        populate: "jobId, creatorId, approveUserId"
      }
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformRequireChangeParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        projectId: props.projectId
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", 'false');
          values = {};
        }
        const initParams = tranformRequireChangeParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters,
              projectId: props.projectId
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  };

  const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  };

  const [dataChange, setDataChange] = useState([]);
  const [temp, setTemp] = useState(0);

  const changeDeadline = async item => {
    let api = await dataRequireChange.getChangeDeadline(item.id);
    setDataChange(api.data);
    setShowModal(true);
    setTemp(item.id);
  };
  const cancelDeadline = async item => {
    approveChange(item.id, "cancel");
  };
  const approveChange = async (id, status = "approved") => {

    let data1 = {
      id: id || temp,
      status
    };
    dispatch({
      type: `${RESOURCE}/updateStatus`,
      payload: data1,
      callback: res => {
        if (res && !res.error) {
          fetchData();
        }
      }
    });
    setShowModal(false);
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 30,
      fixed: "left",
      hideInSearch: true
    },
    {
      title: "Công việc",
      dataIndex: "jobId",
      width: 200,
      fixed: "left",
      render: value => <span>{value.name}</span>
    },
    {
      title: "Người tạo",
      dataIndex: "creatorId",
      width: 100,
      hideInSearch: true,
      render: value => <span>{value.name}</span>
    },
    {
      title: "Người duyệt",
      dataIndex: "approveUserId",
      width: 120,
      hideInSearch: true,
      render: value => <span>{value ? value.name : "-"}</span>
    },
    {
      title: "Ngày duyệt",
      dataIndex: "approveDate",
      valueType: "date",
      width: 150,
      hideInSearch: true
    },
    {
      title: "Mô tả",
      dataIndex: "description",
      width: 150,
      hideInTable: true,
      hideInSearch: true
    },
    {
      title: "DeadLine thay đổi",
      dataIndex: "deadlineChange",
      valueType: "date",
      width: 150,
      hideInSearch: true
    },
    {
      title: "DeadLine hiện tại",
      dataIndex: "deadlineNow",
      valueType: "date",
      width: 150,
      hideInSearch: true
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      width: 200,
      filters: true,
      valueEnum: {
        approved: {
          text: "Đã duyệt",
          status: "approved",
          color: "#ec3b3b",
          isText: "true"
        },
        pending: {
          text: "Đang đợi",
          status: "pending",
          color: "#ec3b3b",
          isText: "true"
        },
        cancel: {
          text: "Đã hủy",
          status: "pending",
          color: "#ec3b3b",
          isText: "true"
        }
      }
    },
    {
      title: "Thao tác",
      width: 200,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Duyệt`}>
            <Button
              className="nv-btn-duyet"
              style={{
                marginRight: "0",

              }}
              onClick={() => changeDeadline(record)}
              disabled={
                record.status === "approved" || record.status === "cancel"
              }
            >
              <img
                style={{ width: "15px" }}
                src={require("assets/images/img-success.jpg")}
                alt="framed"
              />
              Duyệt
            </Button>
          </Tooltip>
          <Popconfirm
            title={`Bạn chắc chắn muốn hủy yêu cầu này?`}
            placement="top"
            okText="Yes"
            cancelText="No"
            onConfirm={() => { cancelDeadline(record); }}
          >
            <Button
              className="nv-btn-duyet"
              // style={{
              //   width: "20px",
              //   display: "inline-block",
              //   marginRight: "5px",
              //   cursor: "pointer"
              // }}
              // onClick={() => changeDeadline(record)}
              danger
              disabled={
                record.status === "approved" || record.status === "cancel"
              }
            >
              <CloseOutlined />
              Hủy
            </Button>
          </Popconfirm>
        </>
      )
    }
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Người dùng"
        actionRef={actionRef}
        formRef={form}
        form={{
          initialValues: formValues
        }}
        dataSource={data && data.list}
        pagination={pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
          >
            {`Thêm mới`}
          </Button>
        ]}
      />
      <RequireChangeDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
      {/* -------Modal------- */}
      <Modal
        visible={showModal}
        footer={null}
        onCancel={() => setShowModal(false)}
      >
        {dataChange.data
          ? dataChange.data.map((item, idx) => {
            return (
              <>
                <div
                  key={idx}
                  style={{
                    borderBottom: "1px solid gray",
                    paddingBottom: "10px",
                    marginBottom: "10px"
                  }}
                >
                  <div
                    style={{
                      display: "block",
                      fontWeight: "700",
                      fontSize: "1.1em"
                    }}
                  >
                    {item.name}
                  </div>
                  <div>
                    <span>Deadline cũ: </span>
                    {item.oldDate
                      ? dayjs(item.oldDate).format("DD-MM-YYYY")
                      : "-"}
                  </div>
                  <div>
                    <span>Deadline mới: </span>
                    {item.newDate > 0
                      ? dayjs(item.newDate).format("DD-MM-YYYY")
                      : "-"}
                  </div>
                </div>
              </>
            );
          })
          : null}
        <div style={{ textAlign: "center", marginTop: "20px" }}>
          <Button type="primary" onClick={() => approveChange()}>
            Đồng ý
          </Button>
        </div>
      </Modal>
    </>
  );
};

export default connect(({ requireChange, loading }) => ({
  requireChange,
  loading: loading.models.requireChange
}))(RequireChangeList);
