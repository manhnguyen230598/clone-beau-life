import { createContainer } from "unstated-next";
import React, { useState, useRef } from "react";
import dayjs from "dayjs";
import {
  GroupJobProps,
  ShowJobDetailType,
  TagProps,
  RoleUserProjectJobProps,
  JobProps,
  ProjectProps,
  IChecklistInfo,
  GroupTimelineProps
} from "./interface";
import {
  create as createGroup,
  update as updateGroup
} from "../../../../services/groupJob";
import {
  create as createTask,
  updateJob as updateTask
} from "../../../../services/job";
import * as checklistServices from "../../../../services/checklist";
import HttpError from "../../../../util/Error";
// import { usePrevious } from '../../../../util/helpers';
export interface UseCounterProps {
  projectId?: number | string;
  projectInfo?: ProjectProps | null;
  checklistInfo?: IChecklistInfo | null;
}

function useCounter(props: UseCounterProps = {}) {
  const actionRef = useRef<any>();
  const projectId = React.useMemo(() => {
    return props?.projectId;
  }, [props?.projectId]);
  const [needCrudDb, setNeedCrudDb] = useState<boolean>(false);
  // const preProjectStart = usePrevious(projectStart);
  const [data, setData] = useState<GroupJobProps[]>([]);
  const [showJobDetail, setShowJobDetail] = useState<ShowJobDetailType>({
    visible: false,
    type: "groupjob"
  });
  const [tags, setTags] = useState<TagProps[]>([]);
  const [employees, setEmployees] = useState<RoleUserProjectJobProps[]>([]);
  const [groupJobs, setGroupJobs] = useState<GroupJobProps[]>([]);
  const [groupTimelines, setGroupTimelines] = useState<GroupTimelineProps[]>(
    []
  );
  const test = false;

  const crudGroup = async (g: GroupJobProps) => {
    // TODO: check project isStartChecklist
    // return checklistServices.checkInfoProjectChecklist({ projectId }).then(resCheck => {
    //   console.log(`🚀 ~ file: container.tsx ~ line 27 ~ returnchecklistServices.checkInfoProjectChecklist ~ resCheck`, resCheck);
    //   const { data: { projectInfo, checkCanCreate } } = resCheck;
    //   if(
    //     !checkCanCreate
    //     && projectInfo.isStartChecklist
    //   ) {
    if (needCrudDb) {
      // TODO: duoc create or update
      if (
        typeof g.id != "undefined" &&
        (typeof g.id == "number" || typeof g.id == "string")
      ) {
        // TODO: update
        if (test) {
          console.log(`🚀 ~ file: container.tsx ~ crudGroup ~ update`, g);
          return {
            status: 200,
            data: g
          };
        } else {
          return updateGroup(g.id, g)
            .then(res => {
              console.log(`🚀 ~ file: container.tsx ~ updateGroup ~ res`, res);
              return res;
            })
            .catch(err => {
              console.log(`🚀 ~ file: container.tsx ~ updateGroup ~ err`, err);
              throw new HttpError(err.message, "ERROR_SERVER");
            });
        }
      } else {
        // TODO: create
        if (test) {
          console.log(`🚀 ~ file: container.tsx ~ crudGroup ~ create`, g);
          return {
            status: 200,
            data: g
          };
        } else {
          return createGroup({
            ...g,
            projectId
          })
            .then(res => {
              console.log(`🚀 ~ file: container.tsx ~ createGroup ~ res`, res);
              return res;
            })
            .catch(err => {
              console.log(`🚀 ~ file: container.tsx ~ createGroup ~ err`, err);
              throw new HttpError(err.message, "ERROR_SERVER");
            });
        }
      }
    } else {
      throw new HttpError("Not need crud", "ERROR_CLIENT");
    }
    // });
  };

  const crudTask = async (t: JobProps) => {
    if (t.outDateTime) {
      t.outDateTime = dayjs(t.outDateTime).valueOf();
    }
    // return checklistServices.checkInfoProjectChecklist({ projectId }).then(resCheck => {
    //   console.log(`🚀 ~ file: container.tsx ~ line 79 ~ returnchecklistServices.checkInfoProjectChecklist ~ resCheck`, resCheck);
    //   const { data: { projectInfo, checkCanCreate } } = resCheck;
    //   if(
    //     !checkCanCreate
    //     && projectInfo.isStartChecklist
    //   ) {
    if (needCrudDb) {
      // TODO: duoc create or update
      if (
        typeof t.id != "undefined" &&
        (typeof t.id == "number" || typeof t.id == "string")
      ) {
        // TODO: update
        if (test) {
          console.log(`🚀 ~ file: container.tsx ~ crudTask ~ update`, t);
          return {
            status: 200,
            data: t
          };
        } else {
          return updateTask(t.id, t)
            .then(res => {
              console.log(`🚀 ~ file: container.tsx ~ updateTask ~ res`, res);
              return res;
            })
            .catch(err => {
              console.log(
                `🚀 ~ file: container.tsx ~ returnupdateTask ~ err`,
                err
              );
              throw new HttpError(err.message, "ERROR_SERVER");
            });
        }
      } else {
        // TODO: create
        if (test) {
          console.log(`🚀 ~ file: container.tsx ~ crudTask ~ create`, t);
          return {
            status: 200,
            data: t
          };
        } else {
          return createTask({
            ...t,
            projectId
          })
            .then(res => {
              console.log(`🚀 ~ file: container.tsx ~ createTask ~ res`, res);
              return res;
            })
            .catch(err => {
              console.log(`🚀 ~ file: container.tsx ~ createTask ~ err`, err);
              throw new HttpError(err.message, "ERROR_SERVER");
            });
        }
      }
    } else {
      throw new HttpError("Not need crud", "ERROR_CLIENT");
    }
    // });
  };

  const updateSequence = async (t: any) => {
    if (needCrudDb) {
      // TODO: duoc create or update
      if (Array.isArray(t)) {
        // TODO: update
        if (test) {
          console.log(`🚀 ~ file: container.tsx ~ crudTask ~ update`, t);
          return {
            status: 200,
            data: t
          };
        } else {
          return checklistServices
            .updateSequence({ array: t })
            .then(res => {
              return res;
            })
            .catch(err => {
              console.log(
                `🚀 ~ file: container.tsx ~ updateSequence ~ err`,
                err
              );
              throw new HttpError(err.message, "ERROR_SERVER");
            });
        }
      } else {
        throw new HttpError("Not id", "ERROR_CLIENT");
      }
    } else {
      throw new HttpError("Not need crud", "ERROR_CLIENT");
    }
  };

  return {
    action: actionRef,
    setAction: (newAction: any) => {
      actionRef.current = newAction;
    },
    data,
    setData,
    showJobDetail,
    setShowJobDetail,
    tags,
    setTags,
    employees,
    setEmployees,
    crudGroup,
    crudTask,
    needCrudDb,
    setNeedCrudDb,
    updateSequence,
    projectId,
    groupJobs,
    setGroupJobs,
    groupTimelines,
    setGroupTimelines
  };
}

const Counter = createContainer<ReturnType<typeof useCounter>, UseCounterProps>(
  useCounter
);

export { useCounter };

export default Counter;
