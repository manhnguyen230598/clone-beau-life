import type { GroupJobProps } from '../interface';

export const dataTemplate = [
  {
    "name": "CHUNG",
    "jobs": [
      {
        "name": "Trình bày đề xuất bán hàng tổng thể"
      },
      {
        "name": "Phân quyền giữa WL & CĐT"
      },
      {
        "name": "Phê duyệt danh sách đại lý"
      },
      {
        "name": "Hoàn thiện Văn phòng bán hàng"
      },
      {
        "name": "Hoàn thành Nhà Mẫu CĐT"
      },
      {
        "name": "Sự kiện khai trương nhà mẫu"
      }
    ]
  },
  {
    "name": "PHÁP LÝ",
    "jobs": [
      {
        "name": "Ngày đủ điều kiện pháp lý để ra mắt dự án"
      },
      {
        "name": "Ngày đủ điều kiện pháp lý để kí HĐMB"
      },
      {
        "name": "Mẫu biểu thủ tục cho KH ký"
      }
    ]
  },
  {
    "name": "SẢN PHẨM",
    "jobs": [
      {
        "name": "Đề bài/ đề xuất điều chỉnh sản phẩm"
      },
      {
        "name": "Tổng mặt bằng dự án"
      },
      {
        "name": "Mặt bằng tầng điển hình"
      },
      {
        "name": "Mặt bằng từng căn"
      },
      {
        "name": "Concept mặt ngoài ( facade) "
      },
      {
        "name": "Concept tiện ích - cảnh quan"
      },
      {
        "name": "Concept nội thất : Nhà thầu CC"
      },
      {
        "name": "Bảng thống kê chi tiết căn"
      },
      {
        "name": "Tiêu chuẩn bàn giao và Nguyên vật liệu hoàn thiện"
      }
    ]
  },
  {
    "name": "CHÍNH SÁCH BÁN HÀNG - GIÁ",
    "jobs": [
      {
        "name": "PD bảng giá chi tiết để test thị trường + khung CSBH"
      },
      {
        "name": "PD bảng giá + CSBH chính thức ( điều chỉnh sau khi test thị trường )"
      },
      {
        "name": "Phương án làm việc với Ngân hàng/ các đối tác liên quan đến CSBH"
      }
    ]
  },
  {
    "name": "SALES",
    "jobs": [
      {
        "name": "Chuẩn bị đại lý"
      },
      {
        "name": "Đào tạo đại lý (QLĐL, GDĐL)"
      },
      {
        "name": "Các quy trình Sales"
      }
    ]
  },
  {
    "name": "MARKETING",
    "jobs": [
      {
        "name": "CÁC MỐC MARTKETING",
        "jobChildren": [
          {
            "name": "Sự kiện truyền lửa môi giới (Ra quân)"
          },
          {
            "name": "On air Truyền thông Gián tiếp (Rumor)"
          },
          {
            "name": "On air Truyền thông trực tiếp (Branding)"
          }
        ]
      }
    ]
  }
];

let levelC = 0;
function generateTemplate<T>(data: any): T[] {
  let arr: T[];
  arr = data.map((item: any, idx: number) => {
    const temp = {
      type: 'grouptask',
      title: item.name,
      name: item.name,
      key: `${levelC}-${idx}`,
      children: (item?.jobs ?? []).map((itemc: any, idxc: number) => ({
        type: 'task',
        title: itemc.name,
        name: itemc.name,
        key: `${levelC}-${idx}-${idxc}`,
        isLeaf: true
      }))
    };
    levelC += 1;
    return temp;
  });
  return arr;
};

export const template = generateTemplate<GroupJobProps>(dataTemplate);
