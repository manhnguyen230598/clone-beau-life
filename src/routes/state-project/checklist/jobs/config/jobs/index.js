import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Input,
  Button,
  Space,
  Tooltip,
  Avatar,
  Modal,
  Select,
  message
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { get, union } from "lodash";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import * as roleProjectService from "src/services/roleProjectJob";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import * as jobServices from "services/job";
const tranformjobParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "job";
const { Option } = Select;
const JobList = props => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();
  const [formRole] = Form.useForm();
  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const [columnsStateMap, setColumnsStateMap] = useState({
    id: { fixed: "left" }
  });
  const [projectRoleMap, setProjectRoleMap] = useState({});
  const [showModal, setShowModal] = useState(false);
  const [jobSelect, setJobSelect] = useState({});
  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history,
    projectId
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformjobParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: {
          ...initParams.filters,
          projectId
        }
      }
    });
    let roleProject = roleProjectService
      .getList({ queryInput: { projectId } })
      .then(rs => {
        if (rs.data) {
          let temp = { ...projectRoleMap };
          temp[0] = { text: "Tất cả" };
          let roles = rs.data?.data;
          let map = (roles || [])?.map(e => {
            temp[e.id] = { text: e.name };
            return;
          });
          setProjectRoleMap(temp);
        }
      });
  }, []);
  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformjobParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters,
        projectId
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", "false");
          values = {};
        }
        const initParams = tranformjobParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters,
              projectId
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  };

  const handleEditClick = item => {
    item.roleApproveId = `${item.roleApproveId}`
    setJobSelect(item);
    formRole.resetFields();
    formRole.setFieldsValue({
      ...item
    });
    setShowModal(true);
  };
  const onFinish = value => {
    let data = { ...value };
    jobServices.update(jobSelect.id, data).then(rs => {
      if (rs) {
        message.success("Cập nhật thành công.");
      }
      let initParams = {};
      let pagi = Object.assign(pagination, tableChange?.pagination || {});
      const defaultCurrent = pagi.current || 1;
      initParams = tranformjobParams(
        tableChange?.filtersArg,
        tableChange?.formValues,
        tableChange?.sort
      );
      dispatch({
        type: `${RESOURCE}/fetch`,
        payload: {
          page: tableChange?.pagination?.current || current,
          current: tableChange?.pagination?.current || current,
          skip: (defaultCurrent - 1) * pagi.pageSize,
          limit: pagi.pageSize || limit,
          pageSize: pagi.pageSize || limit,
          sort: initParams.sort
            ? initParams.sort.length <= 0
              ? [{ createdAt: "desc" }]
              : initParams.sort
            : [{ createdAt: "desc" }],
          queryInput: {
            ...initParams.filters,
            projectId
          }
        }
      });
    });
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Tên nhiệm vụ",
      dataIndex: "name",
      width: 80,
      fixed: "left",
      sorter: true,
      hideInSearch: false
    },
    {
      title: "Người được duyệt deadline",
      dataIndex: "roleApproveId",
      width: 80,
      fixed: "left",
      sorter: true,
      hideInSearch: false,
      valueEnum: projectRoleMap
    },
    {
      title: "Thao tác",
      width: 40,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Chỉ định quyền duyệt`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      )
    }
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Nhiệm vụ"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          //   <Button
          //     icon={<PlusOutlined />}
          //     type="primary"
          //     onClick={() => handleAddClick()}
          //   >
          //     {`Thêm mới`}
          //   </Button>
        ]}
      />
      <Modal
        visible={showModal}
        title={"Chỉ định người duyệt"}
        onCancel={() => setShowModal(false)}
        onOk={() => formRole.submit()}
      >
        <Form
          form={formRole}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          onFinishFailed={() => {}}
        >
          <Form.Item
            label="Nhiệm vụ"
            name="name"
            rules={[{ required: true, message: "Bạn phải chọn đại lý" }]}
          >
            <FormInputRender
              item={{ title: "Tên chính sách", dataIndex: "name", width: 200 }}
              intl={intl}
              disabled
            />
          </Form.Item>
          <Form.Item
            label="Quyền duyệt"
            name="roleApproveId"
            rules={[{ required: true, message: "Bạn chưa chọn quyền" }]}
          >
            <FormInputRender
              item={{
                valueEnum: projectRoleMap,
              }}
              intl={intl}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default connect(({ job, loading }) => ({
  job,
  loading: loading.models.job
}))(JobList);
