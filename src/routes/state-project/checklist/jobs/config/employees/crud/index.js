import React, { useEffect, useState } from "react";
import { Card, Button, Form, Col, Row, Popover } from "antd";
import _ from "lodash";
import { CloseCircleOutlined } from "@ant-design/icons";
import { connect } from "dva";
// import { FormInputRender } from "packages/pro-table/form";
// import { useIntl } from "packages/pro-table/component/intlContext";
import FooterToolbar from "packages/FooterToolbar";

import UserSelect from "../../../../../../../components/Select/User/ListUser";
import RoleProjectJobSelect from "../../../../../../../components/Select/RoleProjectJob/ListRoleProjectJob";

const RESOURCE = "employees";
const fieldLabels = {
  userId: "Tên nhân viên",
  roleProjectJobId: "Quyền"
};
const EmployeesForm = ({
  employees: { formTitle, formData },
  dispatch,
  submitting,
  match: { params },
  history,
  projectId,
  ...rest
}) => {
  // const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState("100%");
  const getErrorInfo = errors => {
    const errorCount = errors.filter(item => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = fieldKey => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map(err => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li
          key={key}
          className="nv-error-list-item"
          onClick={() => scrollToField(key)}
        >
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={trigger => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = values => {
    setError([]);
    const data = _.cloneDeep(values);
    if (data.roleProjectJobId) {
      data.roleProjectJobId = data.roleProjectJobId.id;
    }
    if (data.userId) {
      data.userId = data.userId.id;
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: res => {
        if (res && !res.error) {
          history.goBack();
        }
      }
    });
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        populate: "userId,roleProjectJobId",
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll(".ant-layout-sider")[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    }
    window.addEventListener("resize", resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener("resize", resizeFooterToolbar);
    };
  });

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 }
  };

  if (
    params.id === "add" ||
    (formData && formData.id && formData.id !== "add")
  ) {
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        {...layout}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin chung">
          <Row>
            <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
              <Form.Item label={fieldLabels["userId"]} name="userId">
                <UserSelect type="radio" disabled={true} />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
              <Form.Item
                label={fieldLabels["roleProjectJobId"]}
                name="roleProjectJobId"
              >
                <RoleProjectJobSelect type="radio" paramsList={{ projectId }} />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={submitting}
          >
            {params.id === "add" ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button
            type="default"
            style={{ color: "#fa5656" }}
            onClick={() => {
              history.goBack();
            }}
          >
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ employees, loading, router }) => ({
  submitting: loading.effects["employees/submit"],
  employees,
  router
}))(EmployeesForm);
