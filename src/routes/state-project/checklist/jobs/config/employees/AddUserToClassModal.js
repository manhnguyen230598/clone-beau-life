import React, { useState, useEffect } from "react";
import { Modal, Form, Tag, message } from "antd";
import cloneDeep from "lodash/cloneDeep";
import get from "lodash/get";
import { connect } from "dva";
import TransferUser, {
  rightTableColumns
} from "../../../../../agency/classroom/crud/components/Step4/components/TransferUser";
import * as employeesServices from "services/employees";
import { fnKhongDau } from "src/util/helpers";

const RESOURCE = "userclass";
const leftTableColumns = [
  {
    dataIndex: "id",
    title: "ID"
  },
  {
    dataIndex: "name",
    title: "Tên"
  },
  {
    dataIndex: "phone",
    title: "SĐT",
    render: val => <>{val}</>
  },
  {
    dataIndex: "agencyId",
    title: "Đại lý",
    render: val => <>{val?.name ? val.name : "-"}</>
  }
  // {
  //   dataIndex: "gender",
  //   title: "Giới tính",
  //   render: val => {
  //     return <Tag>{val}</Tag>
  //   }
  // }
];
const AddUserToClassModal = ({
  isShowModal = false,
  setIsShowModal,
  projectId,
  dispatch
}) => {
  const [form] = Form.useForm();
  const [users, setUsers] = useState([]);
  const onFinish = values => {
    const data = cloneDeep(values);
    data.projectId = projectId;
    employeesServices.approveStaff(data).then(res => {
      if (res.status == 200) {
        message.success(res.data.message);
      }
      employeesServices.getUserNotInClass({ projectId }).then(res => {
        if (res.status == 200) {
          setUsers(get(res, "data.data", []));
        }
      });
    });
  };

  useEffect(() => {
    let isMounted = true;
    if (projectId) {
      employeesServices.getUserNotInClass({ projectId }).then(res => {
        if (res.status == 200 && isMounted) {
          setUsers(get(res, "data.data", []));
        }
      });
    }
    return () => {
      isMounted = false;
    };
  }, [projectId]);

  return (
    <React.Fragment>
      <Modal
        width={1200}
        visible={isShowModal}
        okText="Thêm nhân viên"
        onCancel={() => setIsShowModal(false)}
        onOk={() => form.submit()}
        title={"Thêm nhân viên"}
      >
        <Form
          className="ant-advanced-search-form"
          form={form}
          layout="vertical"
          onFinish={onFinish}
          hideRequiredMark
        >
          <Form.Item
            // label="nhân viên"
            name="userIds"
            required={false}
            shouldUpdate={(prevValues, curValues) =>
              prevValues.type !== curValues.type
            }
          >
            <TransferUser
              dataSource={users}
              disabled={false}
              showSearch={true}
              filterOption={(inputValue, item) => {
                console.log(
                  item.name,
                  inputValue,
                  fnKhongDau(item.name)
                    ?.toLowerCase()
                    .indexOf(fnKhongDau(inputValue)?.toLowerCase())
                );
                if (
                  fnKhongDau(item.name)
                    ?.toLowerCase()
                    .indexOf(fnKhongDau(inputValue)?.toLowerCase()) >=0
                ) {
                  return true;
                }
                if (
                  item.phone
                    ?.toLowerCase()
                    .indexOf(fnKhongDau(inputValue)?.toLowerCase()) >=0
                ) {
                  return true;
                }
                if (
                  item.agencyId &&  item.agencyId?.name
                    ?.toLowerCase()
                    .indexOf(inputValue?.toLowerCase()) >=0
                ) {
                  return true;
                }
              }}
              leftColumns={leftTableColumns}
              rightColumns={rightTableColumns}
            />
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};

export default connect(null)(AddUserToClassModal);
