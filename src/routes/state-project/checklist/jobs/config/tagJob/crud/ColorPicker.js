import React from 'react';
import { SketchPicker } from 'react-color';

class ColorPicker extends React.Component {

  state = {
    background: this.props.color,
  };

  handleChange = (color) => {
    this.props.setColor(color.hex)
    this.setState({ background: color.hex });
  };

  handleChangeComplete = (color) => {
    this.props.setColor(color.hex)
    this.setState({ background: color.hex });
  }

  render() {
    console.log("this.props.color",this.props.color)
    return <SketchPicker
      color={ this.state.background }
      onChange={ this.handleChange }
      onChangeComplete={ this.handleChangeComplete }
    />;
  }
}

export default ColorPicker