import React, { useState, useEffect } from "react";
import { Menu, Dropdown, Badge } from "antd";
import { GroupOutlined, TagOutlined, UserOutlined,UserSwitchOutlined, ReadOutlined, PicRightOutlined } from '@ant-design/icons';
import { router } from 'dva';
import get from 'lodash/get';
import { usePrevious } from '../../../../../util/helpers';
import * as checklistServices from '../../../../../services/checklist';
import IntlMessages from "../../../../../util/IntlMessages";
interface UseProjectInfoProps {
  id: string | number
  match: any
}

const { Link } = router;

const configMenu = (match: any) => (
  <Menu>
    <Menu.Item>
      <Link to={`${match.url}/groupJobs`}><GroupOutlined />&nbsp;&nbsp;{`Nhóm nhiệm vụ`}</Link>
    </Menu.Item>
    {/* <Menu.Item>
      <Link to={`${match.url}/relationGroupJob`}><GlobalOutlined />&nbsp;&nbsp;{`Nhóm nhiệm vụ liên quan`}</Link>
    </Menu.Item> */}
    <Menu.Item>
      <Link to={`${match.url}/tags`}><TagOutlined />&nbsp;&nbsp;{`Từ khóa`}</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to={`${match.url}/historyChangeJob`}><UserSwitchOutlined />&nbsp;&nbsp;{`Lịch sử thay đổi công việc`}</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to={`${match.url}/employees`}><UserOutlined />&nbsp;&nbsp;{`Nhân viên dự án`}</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to={`${match.url}/checklistRole`}><ReadOutlined />&nbsp;&nbsp;{`Thiết lập quyền`}</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to={`${match.url}/jobsRole`}><PicRightOutlined />&nbsp;&nbsp;{`Quyền duyệt nhiệm vụ`}</Link>
    </Menu.Item>
  </Menu>
);

const getTablist = (match: any, totalDeadline?: number) => ([
  {
    key: 'jobs',
    tab: <IntlMessages id="checklist.tab.jobs" />,
  },
  {
    key: 'deadline',
    tab: <Badge count={totalDeadline} offset={[0, -6]} overflowCount={99} showZero size="small"><IntlMessages id="checklist.tab.deadline" /></Badge>,
  },
  {
    key: 'config',
    tab: <Dropdown overlay={configMenu(match)} placement="bottomLeft">
      <span><IntlMessages id="checklist.tab.config" /></span>
    </Dropdown>,
  },
]);

export interface UseProjectInfoAction {
  projectInfo: {
    id: string | number
    name: string
  } | any
  setProjectId: (prodId: number) => void;
}

const useProjectInfo = ({
  id,
  match
}: UseProjectInfoProps): UseProjectInfoAction => {
  const [projectId, setProjectId] = useState(id);
  const preProjectId = usePrevious(projectId);
  const [projectInfo, setProjectInfo] = useState({});

  useEffect(() => {
    if(typeof preProjectId == 'undefined' && preProjectId != projectId) {
      checklistServices.checkInfoProjectChecklist({ projectId: Number(projectId) }, false).then(res => {
        // setProjectInfo(get(res, "data", {}));
        const resData = get(res, "data", {});
        const tabList = getTablist(match, resData?.countRequest);
        const newTablist = tabList.filter((item: any) => {
          if(
            (item.key == 'jobs' && resData?.role?.checklist)
            || (item.key == 'deadline' && resData?.role?.aprrove)
            || (item.key == 'config' && resData?.role?.setting)
          ) {
            return true;
          }
          return false;
        });
        setProjectInfo({ ...resData, tabList: newTablist });
      }).catch(err => {
        console.log(`🚀 ~ file: useProjectInfo.tsx ~ checklistServices.checkInfoProjectChecklist ~ err`, err);
      });
    }
  }, [projectId]);

  return {
    projectInfo,
    setProjectId
  };
};

export default useProjectInfo;
