import React, { FC } from "react";
import { Card, Tabs } from "antd";
import { router } from 'dva';
import type { RouteProps } from 'react-router-dom';
import asyncComponent from "../../../../util/asyncComponent";
// import IntlMessages from "../../../../util/IntlMessages";
import useProjectInfo from './hook/useProjectInfo';
import type { ProjectProps, IChecklistInfo } from './interface';

interface ChecklistDetailRouteProps extends RouteProps {
  component: React.Component | any
  projectId: string | number;
  projectInfo?: ProjectProps | null | {};
  checklistInfo?: IChecklistInfo | null | {};
}

interface ClassInSideProps {
  match: any;
  location: any;
  history: any;
}

const TabPane = Tabs.TabPane;
const { Redirect, Switch, Route, Link } = router;

/* const configMenu = (match: any) => (
  <Menu>
    <Menu.Item>
      <Link to={`${match.url}/groupJobs`}><GroupOutlined />{`Nhóm nhiệm vụ`}</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to={`${match.url}/relationGroupJob`}><GlobalOutlined />{`Nhóm nhiệm vụ liên quan`}</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to={`${match.url}/tags`}><TagOutlined />{`Từ khóa`}</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to={`${match.url}/employees`}><UserOutlined />{`Nhân viên dự án`}</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to={`${match.url}/checklistRole`}><ReadOutlined />{`Thiết lập quyền`}</Link>
    </Menu.Item>
  </Menu>
); */

const ChecklistDetailRoute: FC<ChecklistDetailRouteProps> = ({ component: Component, projectId, projectInfo = {}, checklistInfo = {}, ...rest }: ChecklistDetailRouteProps) =>
  <Route
    {...rest}
    render={props => <Component
      {...props}
      projectId={Number(projectId)}
      projectInfo={projectInfo}
      checklistInfo={checklistInfo}
    />}
  />;

const ClassInSide: FC<ClassInSideProps> = ({ match, location, history, ...rest }: ClassInSideProps) => {
  const projectId = match.params.projectId;
  const { projectInfo: { projectInfo = {}, tabList = [], ...checklistInfo } } = useProjectInfo({ id: projectId, match });

  function callback(key: string) {
    switch (key) {
      case 'jobs':
        history.push({ pathname: `${match.url}/jobs` });
        break;
      case 'approve':
        history.push({ pathname: `${match.url}/approve` });
        break;
      case 'deadline':
        history.push({ pathname: `${match.url}/deadline` });
        break;
      case 'config':
        // history.push({ pathname: `${match.url}/config` });
        break;
      default:
        break;
    }
  }

  const getTabKey = () => {
    const url = match.url === '/' ? '' : match.url;
    const tabKey = location.pathname.replace(`${url}`, '');
    if (tabKey && tabKey !== '') {
      const tkey = tabKey.replace(/\//g, "");
      if (tkey == 'groupJobs' || tkey == 'relationGroupJob' || tkey == 'tags' || tkey == 'employees' || tkey == 'checklistRole' || tkey == 'jobsRole') {
        return 'config';
      }
      return tkey;
    }
    return 'jobs';
  };

  return (
    <Card
      className="gx-card"
      title={<Link to={`/project-detail/`}>{`Checklist dự án ${projectId} - ${projectInfo?.name ?? ""} `}</Link>}
    >
      <Tabs
        // defaultActiveKey="1"
        onChange={callback}
        activeKey={getTabKey()}
      >
        {tabList.map((item: any, index: number) => (
          <TabPane {...item} tab={item.tab} key={item.key || index} />
        ))}
      </Tabs>
      <Switch>
        {location?.state ? <Redirect exact from={`${match.url}/`} to={{
          pathname: `${match.url}/${getTabKey()}`,
          state: { name: location?.state }
        }} />
          : <Redirect exact from={`${match.url}/`} to={`${match.url}/${getTabKey()}`} />}

        <ChecklistDetailRoute
          path={`${match.url}/jobs`}
          projectId={projectId}
          projectInfo={projectInfo}
          checklistInfo={checklistInfo}
          component={asyncComponent(() => import("./jobs"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/deadline`}
          projectId={projectId}
          component={asyncComponent(() => import("./requireChange/list/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/relationgroupJob`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/relatedTasks/list/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/relationgroupJob/:id`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/relatedTasks/crud/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/employees`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/employees/list/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/employees/:id`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/employees/crud/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/checklistRole`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/roleProjectJob/list/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/checklistRole/:id`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/roleProjectJob/crud/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/tags`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/tagJob/list/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/tags/:id`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/tagJob/crud/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/groupjobs`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/groupJob/list/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/groupjobs/:id`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/groupJob/crud/index"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/jobsRole`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/jobs"))}
          exact={true}
        />
        <ChecklistDetailRoute
          path={`${match.url}/historyChangeJob`}
          projectId={projectId}
          component={asyncComponent(() => import("./config/historyChangeJob/list"))}
          exact={true}
        />
      </Switch>
    </Card>
  );
};

export default ClassInSide;
