import React, { FC, memo, useEffect, useState } from "react";
import { router } from "dva";
import { Row, Col, Divider, Button } from "antd";
import * as checklistServices from "../../../../services/checklist";
import {

  PlusOutlined
} from "@ant-design/icons";

interface ListProjectProps {
  match: any,
  history : any
}

const { Link } = router;
const ListProject: FC<ListProjectProps> = (props: ListProjectProps) => {
  const { match, history } = props;
  const [listProject, setListProject] = useState([]);
  const [loading, setLoading] = useState(false);
  const [disableCreate, setDisableCreate] = useState(true);
  useEffect(() => {
    setLoading(true);
    (async () => {
      const response = await checklistServices.getListProject();
      setListProject(response.data.data);
      setDisableCreate(response.data.canCreate)
      setLoading(false);
    })();
  }, []);

  if(loading) {
    return (
      <div className="loader loading">
        <img src="/loading.gif" alt="loader" />
      </div>
    );
  }

  return (
    <>
      <div style={{display : "flex", justifyContent : "space-between"}}>
        <span style={{ fontSize: "1.2em" }}>DANH SÁCH DỰ ÁN</span>
        <Button
            disabled={!disableCreate}
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => {history.push({ pathname: `/manage-project/project/add` });}}
          >
            {`Thêm mới`}
          </Button>
      </div>
      <Divider />
      <Row gutter={[32, 24]}>
        {listProject?.map((item: any, index: number) => (
          <Col
            style={{ marginTop: 30 }}
            key={index}
            xl={6}
            lg={6}
            md={8}
            sm={8}
            xs={12}
          >
            <Link to={`${match.url}/${item.type || "resort"}/${item.id}`}>
              <div
                style={{
                  // border: "solid 1px #545454",
                  padding: "4px"
                }}
                className="card-project"
              >
                <div
                  style={{
                    width: "100%",
                    height: "300px"

                    // backgroundImage: `url("${item.thumbnail}")`,
                    // backgroundSize : "cover",
                    // textAlign : "center",
                    // padding: "5%"
                  }}
                >
                  <img
                    src={item.thumbnail}
                    style={{ objectFit: "cover", height: "100%", borderRadius:"15px" }}
                    className="imgproject"
                  />
                  <span
                    style={{
                      position: "absolute",
                      // top: "50%",
                      // left: "50%",
                      // transform: "translate(-50%, -50%)",
                      // fontSize: "2em",
                      // color: "white"
                      fontSize:"1em",
                      bottom:"8%",
                      left:"15%",
                      right:"27%"
                    }}
                  >
                    {item.name}
                  </span>
                </div>
              </div>
            </Link>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default memo(ListProject);
