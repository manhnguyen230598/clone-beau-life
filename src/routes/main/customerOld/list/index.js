import React, { useState, useEffect, useRef } from "react";
import { Form, Input, Button, Space, Tooltip, Avatar, Modal, Upload } from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  FileExcelOutlined,
  ImportOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import ProTable from "packages/pro-table/Table";
import { get, union } from "lodash";
import CustomerOldDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
const { Dragger } = Upload;
const tranformCustomerOldParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
    if (initFitler.projectName) {
      initFitler.projectName = {
        contains: initFitler.projectName
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
    if (initFormValues.projectName) {
      initFormValues.projectName = {
        contains: initFormValues.projectName
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};

const RESOURCE = "customerOld";
const CustomerOldList = props => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();
  const [formInput] = Form.useForm();
  const [isModalView, setIsModalView] = useState(false);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  // const [skip,] = useState(0);

  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [columnsStateMap, setColumnsStateMap] = useState({
    name: { fixed: "left" },
    phone: { fixed: "left" },
    gender: { fixed: "left" },
    address: { fixed: "left" },
    email: { fixed: "left" },
    "provinceId ": { fixed: "left" },
    districtId: { fixed: "left" },
    wardId: { fixed: "left" }
  });
  const [, setError] = useState([]);
  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history
  } = props;
  const draggerCheckCode = {
    accept: ".csv,.xlsx",
    multiple: false,
    beforeUpload(file) {
      // setDataFile(file)
      formInput.setFieldsValue({
        file
      });
      console.log(file);
      return false;
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
    onRemove(file) { }
  };
  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign({}, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranformCustomerOldParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize || 0,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: initParams.sort
          ? initParams.sort.length <= 0
            ? [{ createdAt: "desc" }]
            : initParams.sort
          : [{ createdAt: "desc" }],
        queryInput: {
          ...initParams.filters
        }
      }
    });
  }, []);
  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };
  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const initParams = tranformCustomerOldParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }
    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize)
    };
    if (sorter.field) {
      params.sort = [
        { [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }
      ];
      // params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        console.log("handleSearch -> fieldsValue", fieldsValue);
        let values = {};
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });

        if (sessionStorage.getItem("isReset") === "true") {
          sessionStorage.setItem("isReset", "false");
          values = {};
        }
        const initParams = tranformCustomerOldParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );

        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }])
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleAddClick = () => {
    history.push({ pathname: `${props.match.url}/add` });
  };
  const handleCancel = () => {
    setIsModalView(false);
  };
  const handleEditClick = item => {
    history.push({ pathname: `${props.match.url}/${item.id}` });
  };
  const onFinish = values => {
    let params = { ...values };
    console.log(values)
    dispatch({
      type: `${RESOURCE}/importUser`,
      payload: params,
      callback: data => {
        Modal.success({
          width: "46vw",
          title: "Kết quả import dữ liệu",
          content: (
            <div>
              <p style={{ color: "green" }}>
                Thành công:{data?.createPhoneSuccess.length}
              </p>
              <ul>
                {data?.createPhoneSuccess.map(e => {
                  return <li>{e.phone}</li>;
                })}
              </ul>
              <p style={{ color: "red" }}>
                Thất bại:{data?.createPhoneFail.length}
              </p>
              <ul>
                {data?.createPhoneFail.map(e => {
                  return (
                    <li>
                      {e.phone} : {e.status}
                    </li>
                  );
                })}
              </ul>
            </div>
          )
        });
        let initParams = {};
        let pagi = Object.assign(pagination, tableChange?.pagination || {});
        const defaultCurrent = pagi.current || 1;
        initParams = tranformCustomerOldParams(
          tableChange?.filtersArg,
          tableChange?.formValues,
          tableChange?.sort
        );

        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            page: tableChange?.pagination?.current || current,
            current: tableChange?.pagination?.current || current,
            skip: (defaultCurrent - 1) * pagi.pageSize,
            limit: pagi.pageSize || limit,
            pageSize: pagi.pageSize || limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }]),
            queryInput: {
              ...initParams.filters
            },
          }
        });
      }
    });
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Dự án",
      dataIndex: "projectName",
      width: 60,
      fixed: "left",
      hideInSearch: false
    },
    {
      title: "Khách hàng",
      dataIndex: "name",
      width: 60,
    },
    {
      title: "Giới tính",
      dataIndex: "gender",
      width: 60,
    },
    {
      title: "Năm sinh",
      dataIndex: "birthYear",
      width: 60,
    },
    {
      title: "Email",
      dataIndex: "email",
      width: 60,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      width: 60,
      fixed: "left",
      hideInSearch: true
    },
    {
      title: "Giới tính",
      dataIndex: "gender",
      width: 60,
      fixed: "left",
      hideInSearch: true
    },
    {
      title: "Số CMND/CCCD",
      dataIndex: "gender",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Email",
      dataIndex: "email",
      width: 60,
      fixed: "left",
      hideInSearch: true
    },
    {
      title: "Tỉnh/Thành phố",
      dataIndex: "provinceName",
      width: 60,
      fixed: "left",
      sorter: true,
      hideInSearch: true
    },
    {
      title: "Thao tác",
      width: 150,
      fixed: "right",
      render: (text, record) => (
        <>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
        </>
      )
    }
  ];

  return (
    <>
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Người dùng"
        actionRef={actionRef}
        formRef={form}
        form={{ initialValues: formValues }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        columnsStateMap={columnsStateMap}
        onColumnsStateChange={mapCols => {
          setColumnsStateMap(mapCols);
        }}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={() => {
          sessionStorage.setItem("isReset", "true");
          setTableChange(RESOURCE, {
            pagination: {
              current: 1,
              skip: 0,
              limit
            },
            filtersArg: {},
            sorter: {},
            formValues: {}
          });
        }}
        toolBarRender={(action, { selectedRows }) => [
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
          >
            {`Thêm mới`}
          </Button>,
          <Button
            icon={<ImportOutlined />}
            // type="primary"
            onClick={() => setIsModalView(true)}
          >
            {`Nhập từ file`} </Button>
        ]}
      />
      <Modal
        title="Import dữ liệu người dùng cũ"
        visible={isModalView}
        onOk={() => formInput.submit()}
        onCancel={handleCancel}
        width={800}
      >
        <Form
          className="ant-advanced-search-form"
          form={formInput}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          hideRequiredMark
        >
          <Form.Item
            label={"Chọn file"}
            name="file"
            rules={[{ required: true, message: "File không được trống" }]}
          >
            <Dragger {...draggerCheckCode}>
              <p className="ant-upload-drag-icon">
                <FileExcelOutlined />
              </p>
              <p className="ant-upload-text">
                Nhấn hoặc thả tệp tin bạn muốn import
              </p>
              <p className="ant-upload-hint">
                Kiểu tệp tin hỗ trợ{" "}
                <a className="ant-upload-hint" style={{ fontWeight: "bold" }}>
                  {" "}
                  .csv .xlsx
                </a>
              </p>
            </Dragger>
          </Form.Item>
        </Form>
        <p>
          Tải file mẫu{" "}
          <a href="/import-file.xlsx" download>
            tại đây
          </a>
        </p>
      </Modal>
      <CustomerOldDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </>
  );
};

export default connect(({ customerOld, loading }) => ({
  customerOld,
  loading: loading.models.customerOld
}))(CustomerOldList);
