import React, { useEffect, useState } from 'react';
import { Card, Button, Form, Col, Row, Popover, Select, DatePicker } from 'antd';
import _ from 'lodash';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import * as enums from 'util/enums';
import { camelCaseToDash } from 'util/helpers';
import ProvincesSelectName from "../../../../components/Select/Provinces/SelectName";
const { Option } = Select;
const RESOURCE = "customerOld";
const fieldLabels = {
  name: "Tên khách hàng",

  phone: "Số điện thoại",
  gender: "Giới tính",
  birthYear: "Ngày sinh",
  ageRange: "Khoảng tuổi",

  indentityNumber: "Số CCCD",
  indentityIssueDate: "Ngày cấp CCCD",
  indentityProvince: "Nơi cấp CCCD",
  email: "Email",
  address: "Địa chỉ",
  projectName: "Tên dự án",
  wardId: "Phường/Xã",
  districtName: "Quận/Huyện",
  provinceName: "Tỉnh/Thành phố",
  purpose: "Mục đích",
  productType: "Loại căn hộ"
};
const CustomerOldForm = ({ customerOld: { formTitle, formData }, dispatch, submitting, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');
  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    if (data.provinceName && typeof data.provinceName == "object") {
      data.provinceName = data.provinceName.label;
    }
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
      callback: (res) => {
        if (res && !res.error) {
          history.goBack();
        }
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  }, [])

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  })

  if (params.id === 'add' || (formData && formData.id && formData.id !== 'add')) {
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin khác" className="gx-card" bordered={false}>
          <Row gutter={16}>
            <Col xl={8} lg={8} md={8} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["projectName"]}
                name="projectName"
                rules={[{ required: true, message: "Bắt buộc điền tên" }]}
              >
                <FormInputRender
                  item={{ title: "Tên căn hộ", dataIndex: "name" }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["name"]}
                name="name"
                rules={[{ required: true, message: "Bắt buộc điền tên" }]}
              >
                <FormInputRender
                  item={{ title: "Tên căn hộ", dataIndex: "name" }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["phone"]}
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Bắt buộc điền số điện thoại"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["gender"]}
                name="gender"
                required={true}
              >
                <Select>
                  <Option value="Nam">Nam</Option>
                  <Option value="Nữ">Nữ</Option>
                  <Option value="Khác">Khác</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["birthYear"]} name="birthYear">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["email"]} name="email">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["address"]} name="address">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "address",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["indentityNumber"]}
                name="indentityNumber"
                rules={[
                  {
                    required: false,
                    message: "Số CCCD phải là số",
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["indentityIssueDate"]}
                name="indentityIssueDate"
              >
                <DatePicker
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["indentityProvince"]}
                name="indentityProvince"
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["provinceName"]}
                name="provinceName"
                rules={[
                  {
                    required: true,
                    message: "Tỉnh/thành phố không được trống"
                  }
                ]}
              >
                <ProvincesSelectName mode="simple" />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["districtName"]}
                name="districtName"
                rules={[
                  {
                    required: true,
                    message: "Tỉnh/thành phố không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["purpose"]} name="purpose">
                <FormInputRender
                  item={{
                    title: "Tên khách hàng",
                    dataIndex: "name",
                    width: 100,
                    valueEnum: {
                      "Để ở": { text: "Để ở" },
                      "Để đầu tư": { text: "Để đầu tư" }
                    }
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["productType"]}
                name="productType"
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 100
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack() }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ customerOld, loading, router }) => ({
  submitting: loading.effects['customerOld/submit'],
  customerOld,
  router
}))(CustomerOldForm);
