import React, { useState } from "react";
import { Card, Form, Row, Col, Select, Button, Modal, message } from "antd";
import _, { values } from "lodash";
import ListProjectSelect from "src/components/Select/Project/ListProject";
import ListUser from "src/components/Select/User/ListUser";
import ListAgency from "src/components/Select/Agency/ListAgency";
import FooterToolbar from "packages/FooterToolbar";
import * as authServices from "src/services/auth";
const { Option } = Select;
function Tools(props) {
  const { history } = props;
  const [form] = Form.useForm();
  const [action, setAction] = useState();
  const onFinish = values => {
    const data = _.cloneDeep(values);
    if (data.data) {
      data.data = data.data?.id;
    }
    authServices.resetTools(data).then(rs => {
      message.success(rs.data?.message);
    });
  };
  const renderData = () => {
    if (action === "remove-agency") {
      return <ListAgency />;
    }
    if (action === "remove-user") {
      return <ListUser />;
    }
    return <ListProjectSelect />;
  };
  return (
    <Form
      className="ant-advanced-search-form"
      form={form}
      layout="vertical"
      onFinish={onFinish}
      //   onFinishFailed={onFinishFailed}
      hideRequiredMark
      //   initialValues={{ ...formData }}
    >
      <Card title={"Công cụ pro admin"} className="gx-card" bordered={false}>
        <Row gutter={16}>
          <Col xl={12} lg={12} md={12} sm={24} xs={24}>
            <Form.Item label={"Hành động"} name="action" required={true}>
              <Select
                onChange={values => {
                  setAction(values);
                  form.setFieldsValue({ data: undefined });
                }}
              >
                <Option value="remove-project">Xóa bỏ dự án</Option>
                <Option value="reset-checklist">
                  Reset checklist dự án về rỗng
                </Option>
                <Option value="remove-user">Xóa bỏ người dùng hệ thống</Option>
                <Option value="remove-agency">Xóa bỏ đại lý</Option>
                <Option value="reset-payment">Reset hóa đơn thanh toán</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col xl={8} lg={8} md={8} sm={8} xs={8}>
            <Form.Item label={"Dữ liệu"} name="data" required={true}>
              {renderData()}
            </Form.Item>
          </Col>
        </Row>
      </Card>
      <FooterToolbar style={{ width: "100%" }}>
        <Button
          type="primary"
          //   onClick={() => form.submit()}
          onClick={() => {
            Modal.confirm({
              title: "Thực hiện hành động",
              content:
                "Bạn chắc chắc muốn thực hiện hành động này, lưu ý hành động này sẽ không thể hoàn tác!",
              onOk: () => form.submit()
            });
          }}
          //   loading={submitting}
        >
          Thực hiện
        </Button>
        <Button
          type="default"
          style={{ color: "#fa5656" }}
          onClick={() => {
            history.goBack();
          }}
        >
          {`Quay lại`}
        </Button>
      </FooterToolbar>
    </Form>
  );
}

export default Tools;
