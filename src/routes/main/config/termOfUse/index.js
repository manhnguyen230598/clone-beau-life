import React, { useEffect, useState } from "react";
import { Form, Row, Button, Col, message } from "antd";
import UploadFile from "components/UploadFile";
import * as configServices from "src/services/config";
import { get } from "lodash";
function TermOfUse(props) {
  const [form] = Form.useForm();
  const [data, setData] = useState({});
  const handleSubmit = values => {
    let url = values.url?.at(0).url;
    console.log(url);
    configServices
      .update(data.id, {
        val: url
      })
      .then(() => {
        message.success("Cập nhật thành công");
      });
  };
  useEffect(() => {
    configServices
      .getList({
        queryInput: {
          keys: "URL_TERM_OF_USE"
        }
      })
      .then(rs => {
        let data = get(rs, "data.data", []);
        let record = data[0];
        if (record.val && typeof record.val === "string") {
          record.url = [
            {
              uid: `${record.id}`,
              name: `Điều khoản sử dụng`,
              url: record.val,
              status: "done"
            }
          ];
        }
        form.resetFields();
        form.setFieldsValue({
          ...record
        });
        setData(record);
      });
  }, []);
  return (
    <Form form={form} onFinish={handleSubmit} initialValues={{ ...data }}>
      <Row>
        <Col md={8}>
          <Form.Item label={"Đường link"} name="url">
            <UploadFile />
          </Form.Item>
        </Col>
        <Button
          onClick={() => {
            form.submit();
          }}
          type="primary"
        >
          Lưu thay đổi
        </Button>
      </Row>
    </Form>
  );
}

export default TermOfUse;
