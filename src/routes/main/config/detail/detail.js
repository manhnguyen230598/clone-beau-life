import React, { useEffect, useState } from "react";
import { Card, Tabs } from "antd";
import { router } from "dva";
import get from "lodash/get";
import asyncComponent from "util/asyncComponent";
import IntlMessages from "util/IntlMessages";
import * as projectServices from "services/bannerHome";

const TabPane = Tabs.TabPane;
const { Redirect, Switch, Route } = router;
const ProjectDetailRoute = ({
  component: Component,
  projectId,
  templateCustomerFile,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => (
      <Component
        {...props}
        templateCustomerFile={templateCustomerFile}
        projectId={Number(projectId)}
      />
    )}
  />
);

const Config = ({ projectId, match, location, history, ...rest }) => {
  const [projectInfo, setProjectInfo] = useState(null);
  useEffect(() => {
    projectServices.get(Number(match.params.id), {}, false).then(res => {
      setProjectInfo(get(res, "data.data[0]", {}));
    });
  }, [match.params.id]);

  if (!projectInfo) {
    return null;
  }

  function callback(key) {
    switch (key) {
      case "bannerHome":
        history.push({ pathname: `${match.url}/bannerHome` });
        break;
      case "termOfUse":
        history.push({ pathname: `${match.url}/termOfUse` });
        break;
      default:
        break;
    }
  }

  const tabList = [
    {
      key: "bannerHome",
      tab: <IntlMessages id="customProject.bannerHome" />
    },
    {
      key: "termOfUse",
      tab: <IntlMessages id="config.termOfUse" />
    }
  ];

  const getTabKey = () => {
    const url = match.url === "/" ? "" : match.url;
    const tabKey = location.pathname.replace(`${url}`, "");
    if (tabKey && tabKey !== "") {
      return tabKey.replace(/\//g, "");
    }
    return "bannerHome";
  };
  return (
    <Card className="gx-card">
      <Tabs
        // defaultActiveKey="1"
        onChange={callback}
        activeKey={getTabKey()}
      >
        {tabList.map((item, index) => (
          <TabPane {...item} tab={item.tab} key={item.key || index} />
        ))}
      </Tabs>
      <Switch>
        <Redirect
          exact
          from={`${match.url}/`}
          to={`${match.url}/${getTabKey()}`}
        />
        <ProjectDetailRoute
          path={`${match.url}/bannerHome`}
          projectId={projectId}
          component={asyncComponent(() => import("../../bannerHome/list"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/bannerHome/:id`}
          projectId={projectId}
          component={asyncComponent(() => import("../../bannerHome/crud"))}
          exact={true}
        />
        <ProjectDetailRoute
          path={`${match.url}/termOfUse`}
          component={asyncComponent(() => import("../termOfUse"))}
          exact={true}
        />
        {/* <ProjectDetailRoute path={`${match.url}/chartproject/:id`} projectId={match.params.id} component={asyncComponent(() => import("../../chartProject/list"))} exact={true} /> */}
      </Switch>
    </Card>
  );
};

export default Config;
