import React from "react";
import { router } from 'dva';
import RoleList from "./list";
import RoleCrud from "./crud";
import ListUserRole from "./list/listUser"
const { Route, Switch, Redirect } = router;
const Main = ({ match }) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/roles`} />
    <Route path={`${match.url}/roles`} component={RoleList} exact />
    <Route path={`${match.url}/roles/user/:id`} component={ListUserRole} />
    <Route path={`${match.url}/roles/:id`} component={RoleCrud} />
    
  </Switch>
);

export default Main;
