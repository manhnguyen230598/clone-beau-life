import React from 'react';
import { Card } from 'antd';
import { connect } from 'dva';
import Roles from './Roles';
import { useEffect } from 'react';

/* const data = [
  { id: 1, name: 'Lop hoc', c: true, r: true, u: true, d: true },
  { id: 2, name: 'Du an', c: false, r: true, u: true, d: false }
]; */
/* const data = [
  {
    id: 1,
    name: 'John Brown sr.',
    c: true, r: true, u: true, d: true,
    children: [
      {
        id: 11,
        name: 'John Brown',
        c: true, r: true, u: true, d: true,
      },
      {
        id: 12,
        name: 'John Brown jr.',
        c: true, r: true, u: true, d: true,
        children: [
          {
            id: 121,
            name: 'Jimmy Brown',
            c: true, r: true, u: true, d: true,
          },
        ],
      },
      {
        id: 13,
        name: 'Jim Green sr.',
        c: true, r: true, u: true, d: true,
        children: [
          {
            id: 131,
            name: 'Jim Green',
            c: true, r: true, u: true, d: true,
            children: [
              {
                id: 1311,
                name: 'Jim Green jr.',
                c: true, r: true, u: true, d: true,
              },
              {
                id: 1312,
                name: 'Jimmy Green sr.',
                c: true, r: true, u: true, d: true,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 2,
    name: 'Joe Black',
    c: true, r: true, u: true, d: true,
  },
]; */
const RoleCrud = (props) => {
  const {
    dispatch,
    match,
    roleMenu
  } = props;
  console.log(`🚀 ~ file: index.js ~ line 72 ~ RoleCrud ~ roleMenu`, roleMenu);

  useEffect(() => {
    if (match.params.id) {
      dispatch({
        type: 'role/fetchRoleMenu',
        payload: {
          roleId: Number(match.params.id)
        }
      });
    }
  }, []);

  return (
    <Card title="Phân quyền" className="gx-card">
      <Roles roles={{ data: roleMenu }} />
    </Card>
  );
};

export default connect(({ role }) => {
  const { roleMenu } = role;
  return { roleMenu };
})(RoleCrud);
