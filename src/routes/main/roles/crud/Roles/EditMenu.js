import React, { useEffect } from 'react';
import { Drawer, Form, Row, Col } from "antd";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import get from "lodash/get";

const fieldLabels = {
  name: "Tên",
  icon: "Icon",
  parent: "Menu cha",
  isParent: "Là cha",
  sequence: "Thứ tự",
  model: "Model",
  active: "Trạng thái",
  url: "Đường dẫn",
  alias: "Alias",
  key: "Key"
};

const EditMenu = ({ record, drawerVisible, onChange }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const triggerChange = v => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...record
    });
  }, [record]);

  return (
    <Drawer
      className="nv-drawer-detail"
      title={
        <span style={{ color: "#f09b1b" }}>{`Chi tiết ${get(
          record,
          "name",
          "-"
        ).toUpperCase()}`}</span>
      }
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Form
        form={form}
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        // initialValues={{
        //   ...record
        // }}
      >
        <Row gutter={[16, 16]} style={{ marginTop: '10px' }}>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["name"]}
              name="name"
              rules={[{ required: true, message: `${fieldLabels["name"]} không được trống` }]}
            >
              <FormInputRender
                item={{
                  title: fieldLabels["name"],
                  dataIndex: "name"
                }}
                placeHolder={"Tên menu"}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["icon"]}
              name="icon"
              rules={[{ required: false, message: `${fieldLabels["icon"]} không được trống` }]}
            >
              <FormInputRender
                item={{
                  title: fieldLabels["icon"],
                  dataIndex: "icon"
                }}
                placeHolder={"Icon"}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["parent"]}
              name="parent"
              rules={[{ required: false, message: `${fieldLabels["parent"]} không được trống` }]}
            >
              <FormInputRender
                item={{
                  title: fieldLabels["parent"],
                  dataIndex: "parent"
                }}
                placeHolder={"menu cha"}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["isParent"]}
              name="isParent"
              rules={[{ required: false, message: `${fieldLabels["isParent"]} không được trống` }]}
              valuePropName="checked"
            >
              <FormInputRender
                item={{
                  title: fieldLabels["isParent"],
                  dataIndex: "isParent",
                  valueType: 'switch'
                }}
                placeHolder={"Là thư mục cha"}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["sequence"]}
              name="sequence"
              rules={[{ required: false, message: `${fieldLabels["sequence"]} không được trống` }]}
            >
              <FormInputRender
                item={{
                  title: fieldLabels["sequence"],
                  dataIndex: "sequence"
                }}
                placeHolder={"Thứ tự"}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["model"]}
              name="model"
              rules={[{ required: false, message: `${fieldLabels["model"]} không được trống` }]}
            >
              <FormInputRender
                item={{
                  title: fieldLabels["model"],
                  dataIndex: "model"
                }}
                placeHolder={"Model"}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["active"]}
              name="active"
              rules={[{ required: false, message: `${fieldLabels["active"]} không được trống` }]}
              valuePropName="checked"
            >
              <FormInputRender
                item={{
                  title: fieldLabels["active"],
                  dataIndex: "active",
                  valueType: "switch"
                }}
                placeHolder={"menu cha"}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["url"]}
              name="url"
              rules={[{ required: false, message: `${fieldLabels["url"]} không được trống` }]}
            >
              <FormInputRender
                item={{
                  title: fieldLabels["url"],
                  dataIndex: "url"
                }}
                placeHolder={"Đường dẫn"}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["alias"]}
              name="parent"
              rules={[{ required: false, message: `${fieldLabels["alias"]} không được trống` }]}
            >
              <FormInputRender
                item={{
                  title: fieldLabels["alias"],
                  dataIndex: "alias"
                }}
                placeHolder={"ID cho đa ngôn ngữ"}
                intl={intl}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label={fieldLabels["key"]}
              name="key"
              rules={[{ required: false, message: `${fieldLabels["key"]} không được trống` }]}
            >
              <FormInputRender
                item={{
                  title: fieldLabels["key"],
                  dataIndex: "key"
                }}
                placeHolder={"Key"}
                intl={intl}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};

export default EditMenu;
