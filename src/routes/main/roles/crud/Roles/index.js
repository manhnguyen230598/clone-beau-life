import React, { useState, useEffect, useCallback } from 'react';
/* import intersectionBy from 'lodash/intersectionBy';
import cloneDeep from 'lodash/cloneDeep'; */
import forIn from 'lodash/forIn';
import { Table, Row, Col, Button, Tooltip } from 'antd';
import { SaveOutlined, EditOutlined } from '@ant-design/icons';
import EditMenu from './EditMenu';
import CheckBox from './CheckBox';

const arrToObj = (arr, key) => {
  return arr.reduce((acc, cur) => {
    if (cur) {
      return {
        ...acc,
        [cur[key]]: cur
      };
    }
  }, {});
};

const objToArr = (obj) => {
  let arr = [];
  forIn(obj, function (value, key) {
    arr.push(value);
  });
  return arr;
};

const ListRoles = ({
  value,
  roles: { data = [] },
  permissions,
  onChange
}) => {
  const [roles, setRoles] = useState([]);
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });

  useEffect(() => {
    if (onChange && data.length) {
      onChange(data);
    }
  }, []);

  useEffect(() => {
    setRoles(origin => {
      const objOrigin = arrToObj(origin || [], 'id');
      const objValues = arrToObj((value && value.data) || [], 'id');
      const objData = arrToObj(data || [], 'id');

      const curObj = Object.assign({}, objValues, objOrigin);
      let finalObj = {};
      forIn(objData, (value, key) => {
        if (curObj[key])
          finalObj[key] = curObj[key];
        else {
          finalObj[key] = value;
        }
      });
      const newroles = objToArr(finalObj);
      return newroles;
      /* let interIds = intersectionBy(origin, data, 'id');
      const newData = data.filter(i => !interIds.map(j => j.id).includes(i.key));
      return [...newData, ...interIds]; */
    });
  }, [data, value]);

  /* componentDidMount() {
    const { dispatch, query } = this.props;
    log("role list query ", query)
    setTimeout(() => {
      const params = {
        filter: {
          UserId: query.userId
        },
        range: [0, 100]
      };
      dispatch({
        type: 'roles/fetch',
        payload: params,
      });
      this.setState({ loading: false });
    }, 2000);
    this.setState({ loading: true });
  } */

  /* const updateRole = (e) => {
    e.preventDefault();
    const { dispatch, query } = this.props;
    const params = {
      userId: query.userId,
      roles
    };
    dispatch({
      type: 'roles/add',
      payload: params,
      callback: (result) => {
        log("result ", result);
        // if (result.status && result.status === 200) {
        message.success('Cập nhật thành công');
        setTimeout(() => {
          // const { match } = this.props;
          // router.push(`/roles/${params.userId}`);
          window.location.reload();
        }, 3000);
        // } else {
        //   message.error('Cập nhật thông tin thất bại');
        // }
      }
    });
  }; */

  const handleEditMenu = (record) => {
    setDrawerDetail({
      visible: true,
      record
    });
  };

  const roleChange = useCallback((e, name, cell, row) => {
    console.log("roleChange e %o, cell %o, row %o", e.target.checked, cell, row);
    setRoles(origin => {
      let newRoles = [];
      const findRole = origin.filter(item => item.id === row.id).length > 0;
      if (findRole) {
        newRoles = origin.map(item => {
          if (item.id === row.id) {
            return {
              ...item,
              [name]: e.target.checked
            };
          }
          return item;
        });
      } else {
        newRoles = [
          ...origin,
          {
            ...row,
            [name]: e.target.checked
          }
        ];
      }
      if (onChange) {
        onChange(newRoles);
      }
      return newRoles;
    });
  }, []);

  const columns = [
    {
      dataIndex: 'name',
      title: 'Chức năng',
      align: 'left',
    },
    {
      dataIndex: 'r',
      title: 'Xem',
      align: 'center',
      render: (cell, row) => (
        <React.Fragment>
          <CheckBox value={cell} cell={cell} row={row} name="r" onChange={roleChange} />
        </React.Fragment>
      ),
    },
    {
      dataIndex: 'c',
      title: 'Thêm',
      align: 'center',
      render: (cell, row) => (
        <React.Fragment>
          <CheckBox value={cell} cell={cell} row={row} name="c" onChange={roleChange} />
        </React.Fragment>
      ),
    },
    {
      dataIndex: 'u',
      title: 'Sửa',
      align: 'center',
      render: (cell, row) => (
        <React.Fragment>
          <CheckBox value={cell} cell={cell} row={row} name="u" onChange={roleChange} />
        </React.Fragment>

      ),
    },
    {
      dataIndex: 'd',
      title: 'Xóa',
      align: 'center',
      render: (cell, row) => (
        <React.Fragment>
          <CheckBox value={cell} cell={cell} row={row} name="d" onChange={roleChange} />
        </React.Fragment>
      ),
    },
    {
      dataIndex: '',
      title: 'Thao tác',
      align: 'center',
      render: (cell, row) => (
        <Tooltip type="Sửa menu">
          <EditOutlined onClick={() => { handleEditMenu(row); }} />
        </Tooltip>
      ),
    },
  ];

  /* if (!permissions || !permissions.isView) return <div>
    Bạn không có quyền truy cập trang này.
  </div>; */
  // const { isUpdate } = permissions;
  return (
    <React.Fragment>
      <Row gutter={6}>
        <Col span={16}>&nbsp;</Col>
        <Col span={4}>&nbsp;</Col>
        <Col span={4}>
          <Button
            type="primary"
            icon={<SaveOutlined />}
            // onClick={updateRole}
            style={{ marginBottom: 10, float: 'right' }}
          >
            {`Lưu`}
          </Button>
        </Col>
      </Row>
      <Table
        rowKey="id"
        scroll={{ x: true, y: false }}
        dataSource={roles}
        columns={columns}
        // onTableChange={this.handleTableChange}
        pagination={false}
      />
      <EditMenu
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
    </React.Fragment>
  );
};

export default ListRoles;
