import React, { useState, useEffect, useRef } from 'react';
import {
  Form,
  Input,
  Button,
  Space,
  Tooltip
} from 'antd';
import { SearchOutlined, PlusOutlined, EditOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import ProTable from 'packages/pro-table/Table';
import { getValue } from 'util/helpers';

const RESOURCE = "role";
const RoleList = (props) => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  const [formValues, setFormValues] = useState({});
  const [skip,] = useState(0);
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ? Number(process.env.REACT_APP_PAGESIZE) : 10);
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});

  const {
    dispatch,
    [RESOURCE]: { data },
    loading,
    history, match
  } = props;

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        current,
        skip,
        limit
      }
    });
  }, []);

  const handleTableChange = (pagination, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    if (filters) {
      if (filters.category) {
        let filCate = filters.category;
        filters.categories = filCate;
        delete filters.category;
      }
    }

    const params = {
      ...formValues,
      ...filters,
      current: Number(pagination.current),
      skip: (Number(pagination.current) - 1) * Number(pagination.pageSize),
      limit: Number(pagination.pageSize)
    };
    if (sorter.field) {
      // params.sorter = { [sorter.field]: sorter.order === 'ascend' ? "ASC" : "DESC" };
      params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/'fetch'`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current.validateFields().then(fieldsValue => {
      console.log('handleSearch -> fieldsValue', fieldsValue);
      const values = {
        current: 1,
        skip: 0,
        limit,
      };
      Object.keys(fieldsValue).forEach(key => {
        if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
          values[key] = fieldsValue[key];
        }
      });

      if (fieldsValue.fromDate) {
        const fromDate = fieldsValue.fromDate.set({ 'hour': 0, 'minute': 0, 'second': 0 }).valueOf();
        values.start_at = [fromDate];
      }
      if (fieldsValue.toDate) {
        const toDate = fieldsValue.toDate.set({ 'hour': 23, 'minute': 59, 'second': 59 }).valueOf();
        values.end_at = toDate;
      }
      delete values.fromDate;
      delete values.toDate;

      const quantity = values.quantity;
      delete values.quantity;
      if (fieldsValue.quantity) {
        values.typeQuantity = quantity.typeQuantity;
        if (quantity.typeQuantity !== 'range') {
          values.quantity = Number(quantity.fromQuantity);
        } else {
          values.quantity = [Number(quantity.fromQuantity), Number(quantity.toQuantity)];
        }
      }

      setFormValues(formValues);
      dispatch({
        type: `${RESOURCE}/'fetch'`,
        payload: {
          ...values
        }
      });
    }).catch(err => {
      if (err) return;
    });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0],
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: '',
    });
    confirm();
  };

  const handleAddClick = () => {
    dispatch({
      type: `${RESOURCE}/'loadForm'`,
      payload: {
        type: 'A',
      }
    });
  };

  const handleEditClick = item => {
    /* dispatch({
      type: `${RESOURCE}/'loadForm'`,
      payload: {
        type: 'E',
        id: item.id,
        item
      }
    }); */
    history.push({ pathname: `${match.url}/${item.id}` });
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 80,
      fixed: 'left',
      ellipsis: true,
      hideInSearch: false,
      hideInTable: false,
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
      hideInSearch: false,
      hideInTable: false,
      ...getColumnSearchProps('name')
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
      width: 150,
      sorter: true,
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updatedAt',
      valueType: 'dateTime',
      width: 150,
      sorter: true,
    },
    {
      title: 'Thao tác',
      width: 50,
      fixed: 'right',
      render: (text, record) => (
        <Tooltip title={`Danh sách người dùng`}><MenuUnfoldOutlined onClick={() => history.push({ pathname: `${match.url}/user/${record.id}` })} /></Tooltip>
        
      ),
    },
  ];

  const pagination = {
    ...(data && data.pagination)
  };

  return (
    <>
      {/* <IntlProvider value={viVNIntl}> */}
      <ProTable
        // className="gx-table-responsive"
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Nhóm quyền"
        actionRef={actionRef}
        formRef={form}
        dataSource={data && data.list}
        pagination={pagination}
        columns={columns}
        loading={loading}
        scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        toolBarRender={(action, { selectedRows }) => [
          <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
            {`Thêm mới`}
          </Button>
        ]}
      />
      {/* </IntlProvider> */}
    </>
  );
};

export default connect(({ role, loading }) => ({
  role,
  loading: loading.models.group
}))(RoleList);
