import React, { useEffect, useState } from 'react';
import { Card, Button, Form, Col, Row, Popover } from 'antd';
import _ from 'lodash';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import Upload from 'components/Upload';
import * as enums from 'util/enums';
import { camelCaseToDash } from 'util/helpers';

const RESOURCE = "bannerHome";
// const fieldLabels = {"image":"Ảnh","isActive":"Trạng thái"};
const fieldLabels = {image: "Hình ảnh","isActive":"Trạng thái", "sequence" : "Thứ tự"};
const BannerHomeForm = ({ bannerHome: { formTitle, formData }, dispatch, submitting, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState('100%');
  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };
  console.log(formData,123456)
  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    // console.log(data,1234)
    if (data.image) {
      data.image =
        data.image && data.image.length > 0
          ? data.image
            .map(
              (i) =>
                i.url ||
                (i.response 
                  ? i.response.url
                  : ''),
            )
            .toString()
          : '';
    }
    
    dispatch({
      type: `${RESOURCE}/submit`,
      payload: data,
			callback: (res) => {
				if (res && !res.error) {
          history.goBack();
        }
			}
    });
    
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  },[])

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  },[formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  })

  if(params.id === 'add' || (formData && formData.id && formData.id !== 'add')){
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin khác" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
                <Form.Item label={fieldLabels["image"]} name="image">
                  <Upload />
                </Form.Item>
              </Col>
              <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
                <Form.Item
                  label={fieldLabels['isActive']}
                  name="isActive"
                >
                  <FormInputRender
                    item={ {"title":"Trạng thái","dataIndex":"isActive","width":200,"valueEnum":{"true":{"text":"Hoạt động","status":"Processing","color":"#ec3b3b","isText":true},"false":{"text":"Dừng","status":"Default","color":"#ec3b3b","isText":true}}} }
                    intl={intl}
                  />
                </Form.Item>
              </Col>
              <Col xxl={6} xl={6} lg={6} md={12} sm={24} xs={24}>
                <Form.Item
                  label={fieldLabels['sequence']}
                  name="sequence"
                >
                  <FormInputRender
                    item={ {"title":"Trạng thái","dataIndex":"isActive","width":200,} }
                    intl={intl}
                  />
                </Form.Item>
              </Col>
          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack() }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ bannerHome, loading, router }) => ({
  submitting: loading.effects['bannerHome/submit'],
  bannerHome,
	router
}))(BannerHomeForm);
