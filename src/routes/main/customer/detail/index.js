
import React from 'react';
import { Image, Drawer, Row, Col } from 'antd';
import TableStatus from 'packages/pro-table/component/status';
import get from 'lodash/get';
import * as enums from 'util/enums';
import { formatDate } from 'util/helpers';
import provinceData from "../../../../components/DataMapVN/province.json";
import districtData from "../../../../components/DataMapVN/district.json";
import wardData from "../../../../components/DataMapVN/ward.json";
import moment from 'moment'

const DescriptionItem = ({ title, content }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const ImagesItem = ({ title, images }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <div>
      {images.map(item => (
        <Image width={80} src={item.url} />
      ))}
    </div>
  </div>
);

const CustomerDetail = ({ record, drawerVisible, onChange }) => {
  console.log("$$record",record)
  const ward=  wardData.RECORDS.find(i=>i.id == get(record, "wardId" , "1" )) || {"name":"Không có"}
  const district=  districtData.RECORDS.find(i=>i.id == get(record, "districtId" , "1" )) || {"name":"Không có"}
  const province=  provinceData.RECORDS.find(i=>i.id == get(record, "provinceId" , "1" )) || {"name":"Không có"}
  console.log("$$ward",ward)
  const triggerChange = (v) => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };
/*   const genderStatus = get(enums.genderEnum[get(record, "gender", "-")], "status", null);
  const Status = TableStatus[genderStatus || 'Init'];
  const genderText = get(enums.genderEnum[get(record, "gender", "-")], "text", "-"); */

  return (
    <Drawer
      width={640}
      style={{ width: "640px !important" }}
      className="nv-drawer-detail"
      title={<span style={{ color: "#f09b1b" }}>{`Chi tiết ${get(record, "name", "-").toUpperCase()}`}</span>}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Tên" content={get(record, "name" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Số điện thoai" content={get(record, "phone" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Giới tính" content={get(record, "gender" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          {/* <DescriptionItem title="Ngày sinh" content={moment(get(record, "birthYear" , "-" )).format("DD-YY-YYYY")} /> */}
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Email" content={get(record, "email" , "-" )} />
        </Col>
        <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
          <DescriptionItem title="Địa chỉ" content={get(record, "address" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
          <DescriptionItem title="Số CMND" content={get(record, "indentityNumber" , "-" )} />
        </Col>
        <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
          {/* <DescriptionItem title="Ngày hết hạn" content={get(record, "indentityIssueDate" , "-" )} /> */}
        </Col>
        <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
          <DescriptionItem title="Nơi cấp" content={get(record, "indentityProvince" , "-" )} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
          <DescriptionItem title="Tỉnh/Thành phố" content={district.name} />
        </Col>
        <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
          <DescriptionItem title="Quận/Huyện" content={province.name} />
        </Col>
        <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={8}>
          <DescriptionItem title="Phường/Xã" content={ward.name} />
        </Col>
      </Row>

    </Drawer>
  );
};

export default CustomerDetail;
