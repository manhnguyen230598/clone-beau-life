import React, { useEffect, useState } from "react";
import {
  Card,
  Button,
  Form,
  Col,
  Row,
  Popover,
  DatePicker,
  Select,
  message
} from "antd";
import _ from "lodash";
import { CloseCircleOutlined } from "@ant-design/icons";
import { connect, routerRedux } from "dva";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import FooterToolbar from "packages/FooterToolbar";
import Upload from "components/Upload";
import * as enums from "util/enums";
import DistrictsSelect from "../../../../components/Select/Districts";
import WardSelect from "../../../../components/Select/Wards";
import ProvincesSelect from "../../../../components/Select/Provinces";
import { camelCaseToDash } from "util/helpers";
import moment from "moment";

const RESOURCE = "customer";
const { Option } = Select;
// const fieldLabels = {"image":"Ảnh","isActive":"Trạng thái"};
const fieldLabels = {
  name: "Tên khách hàng",

  phone: "Số điện thoại",
  gender: "Giới tính",
  birthYear: "Ngày sinh",
  ageRange: "Khoảng tuổi",

  indentityNumber: "Số CCCD",
  indentityIssueDate: "Ngày cấp CCCD",
  indentityProvince: "Nơi cấp CCCD",
  email: "Email",
  address: "Địa chỉ",

  wardId: "Phường/Xã",
  districtId: "Quận/Huyện",
  provinceId: "Tỉnh/Thành phố"
};
const CustomerForm = ({
  customer: { formTitle, formData },
  dispatch,
  submitting,
  match: { params },
  history,
  ...rest
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState("100%");
  const getErrorInfo = errors => {
    const errorCount = errors.filter(item => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = fieldKey => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map(err => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li
          key={key}
          className="nv-error-list-item"
          onClick={() => scrollToField(key)}
        >
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={trigger => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };
  const onFinish = values => {
    setError([]);
    const data = _.cloneDeep(values);
    if (data.birthYear) {
      data.birthYear = +moment(data.birthYear).valueOf();
    }
    if (data.indentityIssueDate) {
      data.indentityIssueDate = moment(data.indentityIssueDate).valueOf();
    }
  
    if (data.wardId) {
      data.wardId = data.wardId.key;
    }
    if (data.districtId) {
      data.districtId = data.districtId.key;
    }
    if (data.provinceId) {
      data.provinceId = data.provinceId.key;
    }
    dispatch({
      type: `customer/submit`,
      payload: data,
      
    });
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/loadForm`,
      payload: {
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll(".ant-layout-sider")[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    }
    window.addEventListener("resize", resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener("resize", resizeFooterToolbar);
    };
  });
  const layout = {
    labelCol: { span: 12 },
    wrapperCol: { span: 16 }
  };
  if (
    params.id === "add" ||
    (formData && formData.id && formData.id !== "add")
  ) {
    return (
      <Form
        {...layout}
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin khác" className="gx-card" bordered={false}>
          <Row>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels["name"]}
                name="name"
                rules={[{ required: true, message: "Bắt buộc điền tên" }]}
              >
                <FormInputRender
                  item={{ title: "Tên căn hộ", dataIndex: "name" }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["phone"]}
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Bắt buộc điền số điện thoại"
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["gender"]}
                name="gender"
                required={true}
              >
                <Select>
                  <Option value="Nam">Nam</Option>
                  <Option value="Nữ">Nữ</Option>
                  <Option value="Khác">Khác</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["birthYear"]} name="birthYear">
                <DatePicker
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["ageRange"]}
                name="ageRange"
                rules={[
                  { required: true, message: "Bắt buộc điền khoảng tuổi" }
                ]}
              >
                <Select>
                  <Option value="Dưới 25">Dưới 25</Option>
                  <Option value="Từ 25-35">Từ 25-35</Option>
                  <Option value="Từ 35-45">Từ 35-45</Option>
                  <Option value="Từ 45-55">Từ 45-55</Option>
                  <Option value="Trên 55">Trên 55</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["email"]} name="email">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item label={fieldLabels["address"]} name="address">
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "address",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["indentityNumber"]}
                name="indentityNumber"
                rules={[
                  {
                    required: false,
                    message: "Số CCCD phải là số",
                    pattern: /^\d+$/
                  }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["indentityIssueDate"]}
                name="indentityIssueDate"
              >
                <DatePicker
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["indentityProvince"]}
                name="indentityProvince"
              >
                <FormInputRender
                  item={{
                    title: "Tên căn hộ",
                    dataIndex: "name",
                    width: 200
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["provinceId"]}
                name="provinceId"
                rules={[
                  {
                    required: true,
                    message: "Tỉnh/thành phố không được trống"
                  }
                ]}
              >
                <ProvincesSelect mode="simple" />
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["districtId"]}
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.provinceId !== curValues.provinceId
                }
                required={true}
              >
                {() => {
                  return (
                    <Form.Item
                      name="districtId"
                      rules={[
                        {
                          required: true,
                          message: "Quận/huyện không được trống"
                        }
                      ]}
                      noStyle
                    >
                      <DistrictsSelect
                        mode="simple"
                        province={
                          (form.getFieldValue("provinceId") || {}).key || ""
                        }
                      />
                    </Form.Item>
                  );
                }}
              </Form.Item>
            </Col>
            <Col xl={8} lg={8} md={8} sm={8} xs={8}>
              <Form.Item
                label={fieldLabels["wardId"]}
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.districtId !== curValues.districtId
                }
                required={true}
              >
                {() => {
                  return (
                    <Form.Item
                      name="wardId"
                      rules={[
                        {
                          required: true,
                          message: "Phường không được trống"
                        }
                      ]}
                      noStyle
                    >
                      <WardSelect
                        mode="simple"
                        district={
                          (form.getFieldValue("districtId") || {}).key || ""
                        }
                      />
                    </Form.Item>
                  );
                }}
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={submitting}
          >
            {params.id === "add" ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button
            type="default"
            style={{ color: "#fa5656" }}
            onClick={() => {
              history.goBack();
            }}
          >
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ customer, loading, router }) => ({
  submitting: loading.effects["customer/submit"],
  customer,
  router
}))(CustomerForm);
