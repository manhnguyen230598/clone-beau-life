import React, { useEffect, useState } from "react";
import {
  Card,
  Button,
  Form,
  Col,
  Row,
  Popover,
  message,
  DatePicker, Input
} from "antd";
import _ from "lodash";
import { injectIntl } from "react-intl";
import { CloseCircleOutlined } from "@ant-design/icons";
import { connect } from "dva";
import { FormInputRender } from "packages/pro-table/form";
import { useIntl } from "packages/pro-table/component/intlContext";
import FooterToolbar from "packages/FooterToolbar";
import Upload from "components/Upload";
import IntlMessages from "src/util/IntlMessages";
import ProvincesSelect from "../../../../components/Select/Provinces";
import DistrictsSelect from "../../../../components/Select/Districts";
import WardSelect from "../../../../components/Select/Wards";
import RoleSelect from "../../../../components/Select/Role/ListRole";
import AgencySelect from "../../../../components/Select/Agency/ListAgency";

const genderEnum = {
  "Nam": { text: "Nam" },
  "Nữ": { text: "Nữ" },
  "Khác": { text: "Khác" }
};

const fieldLabels = {
  name: "Tên",
  brandId: "Thương hiệu",
  categoryId: "Danh mục",
  thumbnail: "Ảnh đại diện",
  images: "Thư viện ảnh",
  quantities: "Kho",
  originPrice: "Giá gốc",
  price: "Giá bán",
  valueAccumulatePoints: "Điểm tặng",
  description: "Mô tả",
  status: "Trạng thái",
  position : "Chức vụ",
  projectJoining : "Dự án đang tham gia"
};
const UserCrud = ({
  user: { formTitle, formData },
  dispatch,
  submitting,
  match: { params },
  history,
  ...rest
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width, setWidth] = useState("100%");
  const getErrorInfo = errors => {
    const errorCount = errors.filter(item => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = fieldKey => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map(err => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li
          key={key}
          className="nv-error-list-item"
          onClick={() => scrollToField(key)}
        >
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={trigger => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = values => {
    setError([]);
    console.log(values);
    const data = _.cloneDeep(values);
    if (data.brandId && typeof data.brandId == "object") {
      data.brandId = data.brandId.key;
    }
    if (data.avatar && Array.isArray(data.avatar)) {
      data.avatar = data.avatar[0].url || data.avatar[0].response.url;
    }
    if (data.images && typeof data.images == "object") {
      data.images = _.map(data.images, item => {
        return _.pick(item, ["id", "uid", "path", "url"]);
      });
    }
    if (data.birth) {
      data.birth = new Date(data.birth).getTime();
    }
    if (data.cmnd_issue_date) {
      data.cmnd_issue_date = new Date(data.cmnd_issue_date).getTime();
    }
    if (data.provinceId) {
      data.provinceId = data.provinceId.value;
    }
    if (data.districtId) {
      data.districtId = data.districtId.value;
    }
    if (data.wardId) {
      data.wardId = data.wardId.value;
    }
    if (data.roleId && typeof data.roleId === "object") {
      data.roleId = data.roleId.id;
    }
    if (data.agencyId && typeof data.agencyId === "object") {
      data.agencyId = data.agencyId.id;
    }
    console.log(data)
    dispatch({
      type: "user/submit",
      payload: data,
      callback: res => {
        if (res.error) {
          message.error(res.error.message);
        } else {
          // dispatch(
          //   routerRedux.push({
          //     pathname: `/manage-user/${camelCaseToDash(RESOURCE)}`
          //   })
          // );
          dispatch({
            type: "user/loadForm",
            payload: {
              type: params.id !== "add" ? "E" : "A",
              id: params.id !== "add" ? params.id : null
            }
          });
        }
      }
    });
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: "user/loadForm",
      payload: {
        type: params.id !== "add" ? "E" : "A",
        id: params.id !== "add" ? params.id : null
      }
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  });

  if (
    params.id === "add" ||
    (formData && formData.id && formData.id !== "add")
  ) {
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card
          title={<IntlMessages id="card.title" />}
          className="gx-card"
          bordered={false}
        >
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={<IntlMessages id="user.name" />}
                name="name"
                rules={[{ required: true, message: "Tên không được trống" }]}
              >
                <FormInputRender
                  item={{ title: "Tên", dataIndex: "name" }}
                  placeHolder={rest.intl.formatMessage({
                    id: "user.name.placeholder"
                  })}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={<IntlMessages id="user.phone" />}
                name="phone"
                rules={[
                  { required: true, message: "Số điện thoại không được trống" }
                ]}
              >
                <FormInputRender
                  item={{ title: "Số điện thoại", dataIndex: "phone" }}
                  placeHolder={rest.intl.formatMessage({
                    id: "user.phone.placeholder"
                  })}
                  intl={intl}
                  disabled={params.id !== "add" ? true : false}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={<IntlMessages id="user.email" />}
                name="email"
                rules={[
                  { required: true, message: "Email không được để trống" }
                ]}
              >
                <FormInputRender
                  item={{ title: "Tên", dataIndex: "name" }}
                  placeHolder={`Nhập email người dùng`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col
              xxl={6}
              xl={{ span: 6, offset: 0 }}
              lg={{ span: 6 }}
              md={24}
              sm={24}
              xs={24}
            >
              <Form.Item
                label={<IntlMessages id="user.birth" />}
                name="birth"
                rules={[
                  { required: false, message: "Ngày sinh không được trống" }
                ]}
              >
                <DatePicker
                  placeHolder={`Nhập ngày sinh người dùng`}
                  style={{ width: "100%" }}
                />
              </Form.Item>
            </Col>
            <Col
              xxl={6}
              xl={{ span: 6, offset: 0 }}
              lg={{ span: 6 }}
              md={24}
              sm={24}
              xs={24}
            >
              <Form.Item
                label={<IntlMessages id="user.gender" />}
                name="gender"
                rules={[
                  { required: false, message: "Giới tính không được trống" }
                ]}
              >
                <FormInputRender
                  item={{
                    title: "Giới tính",
                    dataIndex: "gender",
                    width: 120,
                    filters: "true",
                    valueEnum: genderEnum,
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={<IntlMessages id="user.avatar" />}
                name="avatar"
                rules={[
                  { required: false, message: "Ảnh đại diện không được trống" }
                ]}
              >
                <Upload multipe />
              </Form.Item>
            </Col>
            {params.id === "add" ? (
              <Col xxl={6} xl={6} lg={12} md={24} sm={24} xs={24}>
                <Form.Item
                  label={<IntlMessages id="user.password" />}
                  name="password"
                  rules={[
                    { required: true, message: "Mật khẩu không được để trống" }
                  ]}
                  extra={<small>(*)Mật khẩu tối thiểu 8 ký tự, bao gồm một hoa một thường một số, một ký tự đặc biệt</small>}
                >
                  <Input.Password
                    item={{ title: "Tên", dataIndex: "name", type: "password" }}
                    placeHolder={`Nhập mật khẩu người dùng`}
                    intl={intl}
                  />
                </Form.Item>
              </Col>
            ) : (
              ""
            )}
            {params.id === "add" ? (
              <Col xxl={6} xl={6} lg={12} md={24} sm={24} xs={24}>
                <Form.Item
                  label={<IntlMessages id="user.repeatPass" />}
                  name="repeatPassword"
                  rules={[
                    {
                      required: true,
                      message: "Nhắc lại mật khẩu không được để trống"
                    }
                  ]}
                >
                  <Input.Password
                    item={{ title: "Tên", dataIndex: "name", type: "password" }}
                    placeHolder={`Nhắc lại mật khẩu`}
                    intl={intl}
                  />
                </Form.Item>
              </Col>
            ) : (
              ""
            )}
          </Row>
          <Row gutter={24}>
            <Col xxl={8} xl={8} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={<IntlMessages id="user.cmnd_number" />}
                name="cmnd_number"
                rules={[
                  { required: false, message: "Email không được để trống" }
                ]}
              >
                <FormInputRender
                  item={{ title: "Tên", dataIndex: "name" }}
                  placeHolder={`Nhập số CCCD người dùng`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={8} xl={8} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={<IntlMessages id="user.cmnd_issue_date" />}
                name="cmnd_issue_date"
                rules={[
                  { required: false, message: "Email không được để trống" }
                ]}
              >
                <DatePicker
                  placeHolder={`Nhập ngày cấp cmnd`}
                  style={{ width: "100%" }}
                />
              </Form.Item>
            </Col>
            <Col xxl={8} xl={8} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={<IntlMessages id="user.cmnd_province" />}
                name="cmnd_province"
                rules={[
                  { required: false, message: "Email không được để trống" }
                ]}
              >
                <FormInputRender
                  item={{ title: "Tên", dataIndex: "name" }}
                  placeHolder={`Nhập nơi cấp CCCD`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
          <Col xxl={3} xl={3} lg={3} md={12} sm={12} xs={12}>
              <Form.Item
                label={"Trạng thái"}
                name="isApprove"
                rules={[{ required: false }]}
                valuePropName="checked"
              >
                <FormInputRender
                  item={{
                    title: "Hot",
                    dataIndex: "isHot",
                    valueType: "switch"
                  }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Địa chỉ" className="gx-card" bordered={false}>
          <Row gutter={16}>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={<IntlMessages id="user.province" />}
                name="provinceId"
                rules={[
                  {
                    required: false,
                    // required: requiredMode,
                    message: "Tỉnh/thành phố không được trống"
                  }
                ]}
              >
                <ProvincesSelect
                  mode="simple"
                // onSelect={(value) => {
                //   console.log('value', value);
                // }}
                />
              </Form.Item>
            </Col>
            <Col
              xl={{ span: 6, offset: 2 }}
              lg={{ span: 8 }}
              md={{ span: 12 }}
              sm={24}
            >
              <Form.Item
                label={<IntlMessages id="user.district" />}
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.provinceId !== curValues.provinceId
                }
              >
                {() => {
                  return (
                    <Form.Item
                      name="districtId"
                      rules={[
                        {
                          required: false,
                          message: "Quận/huyện không được trống"
                        }
                      ]}
                      noStyle
                    >
                      <DistrictsSelect
                        mode="simple"
                        province={
                          (form.getFieldValue("provinceId") || {}).key ||
                          formData.provinceId ||
                          ""
                        }
                      />
                    </Form.Item>
                  );
                }}
              </Form.Item>
            </Col>
            <Col
              xl={{ span: 8, offset: 2 }}
              lg={{ span: 10 }}
              md={{ span: 24 }}
              sm={24}
            >
              <Form.Item
                label={<IntlMessages id="user.ward" />}
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.districtId !== curValues.districtId
                }
              >
                {() => {
                  return (
                    <Form.Item
                      name="wardId"
                      rules={[
                        {
                          required: false,
                          message: "Phường không được trống"
                        }
                      ]}
                      noStyle
                    >
                      <WardSelect
                        mode="simple"
                        district={
                          (form.getFieldValue("districtId") || {}).key ||
                          formData.districtId ||
                          ""
                        }
                      />
                    </Form.Item>
                  );
                }}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col lg={8} md={12} sm={24}>
              <Form.Item
                label={<IntlMessages id="user.address" />}
                name="address"
                rules={[
                  { required: false, message: "Địa chỉ không được trống" }
                ]}
              >
                <FormInputRender
                  item={{ title: "Địa chỉ", valueType: "textarea" }}
                  type="form"
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Thông tin khác" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={<IntlMessages id="user.role" />}
                name="roleId"
                rules={[{ required: false, message: "Quyền không được trống" }]}
              >
                <RoleSelect mode="radio" />
              </Form.Item>
            </Col>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={<IntlMessages id="user.agency" />}
                name="agencyId"
                rules={[{ required: false, message: "Đại lý không được trống" }]}
              >
                <AgencySelect mode="radio" />
              </Form.Item>
            </Col>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={fieldLabels["position"]}
                name="position"
                rules={[{ required: false, message: "Đại lý không được trống" }]}
              >
                <FormInputRender
                  item={{ title: "Địa chỉ" }}
                  type="form"
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={<IntlMessages id="user.experiene" />}
                name="experienceYear"
                rules={[{ required: false, message: "Quyền không được trống" }]}
              >
                <FormInputRender
                  item={{ title: "Địa chỉ" }}
                  type="form"
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={<IntlMessages id="user.countbought" />}
                name="soldSuccessCount"
                rules={[{ required: false, message: "Quyền không được trống" }]}
              >
                <FormInputRender
                  item={{ title: "Địa chỉ" }}
                  type="form"
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={<IntlMessages id="user.projectjoin" />}
                name="projectJoin"
                rules={[
                  {
                    required: false,
                    message: "Dự án đã tham gia không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{ title: "Địa chỉ", valueType: "textarea" }}
                  type="form"
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col lg={9} md={12} sm={24}>
              <Form.Item
                label={fieldLabels["projectJoining"]}
                name="projectJoining"
                rules={[
                  {
                    required: false,
                    message: "Dự án đã tham gia không được trống"
                  }
                ]}
              >
                <FormInputRender
                  item={{ title: "Địa chỉ", valueType: "textarea" }}
                  type="form"
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={submitting}
          >
            {params.id === "add" ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button
            type="default"
            style={{ color: "#fa5656" }}
            onClick={() => {
              history.goBack();
            }}
          >
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ user, loading, router }) => ({
  submitting: loading.effects["user/submit"],
  user,
  router
}))(injectIntl(UserCrud));
