import React from "react";
import { Image, Drawer, Row, Col } from "antd";
import dayjs from "dayjs";
import TableStatus from "packages/pro-table/component/status";
import get from "lodash/get";
import * as enums from "util/enums";
import { formatDate } from "util/helpers";

const DescriptionItem = ({ title, content }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const ImagesItem = ({ title, images }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <div>
      {images?.map(item => (
        <Image width={80} src={item.url} />
      ))}
    </div>
  </div>
);

const UserDetail = ({ record, drawerVisible, onChange }) => {
  const triggerChange = v => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };
  const genderStatus = get(
    enums.genderEnum[get(record, "gender", "-")],
    "status",
    null
  );
  const Status = TableStatus[genderStatus || "Success"];
  const genderText = get(
    enums.genderEnum[get(record, "gender", "-")],
    "text",
    "-"
  );

  return (
    <Drawer
      width={840}
      title={
        <span style={{ color: "#f09b1b" }}>{`Hồ sơ chi tiết ${get(
          record,
          "fullname",
          "-"
        ).toUpperCase()}`}</span>
      }
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Row gutter={16}>
        <Col span={12}>
          <DescriptionItem title="Họ tên" content={get(record, "name", "-")} />
        </Col>
        <Col span={12}>
          <DescriptionItem
            title="Giới tính"
            content={<Status>{genderText}</Status>}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title="Ngày sinh"
            content={formatDate(get(record, "birth", "-"))}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem title="Email" content={get(record, "email", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={12}>
          <DescriptionItem
            title="Số CMND"
            content={get(record, "cmnd_number", "-")}
          />
        </Col>
        <Col span={12}>
          <DescriptionItem
            title="Ngày cấp"
            content={formatDate(get(record, "cmnd_issue_date", "-"))}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title="Nơi cấp"
            content={get(record, "cmnd_province", "-")}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title="Số điện thoại"
            content={get(record, "phone", "-")}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <ImagesItem
            title={`Ảnh đại diện`}
            images={get(record, "avatar", [])}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title={`Dự án đã tham gia`}
            content={get(record, "projectJoin", "")}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title={`Dự án muốn tham gia`}
            content={get(record, "projectWantJoin", "")}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title={`Đại lý`}
            content={record.agencyId ? record.agencyId.name : ""}
          />
        </Col>
      </Row>
      {record.anatherAgency ? (
        <Row gutter={16}>
          <Col span={24}>
            <DescriptionItem
              title={`Đại lý khác`}
              content={record.anatherAgency}
            />
          </Col>
        </Row>
      ) : (
        ""
      )}
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title={`Số năm kinh nghiệm`}
            content={get(record, "experienceYear", "")}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title={`Số căn bán thành công`}
            content={get(record, "soldSuccessCount", "")}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem
            title={`Lần đăng nhập cuối`}
            content={dayjs(get(record, "lastLogin", "")).format("DD/MM/YYYY HH:mm:ss")}
          />
        </Col>
      </Row>

    </Drawer>
  );
};

export default UserDetail;
