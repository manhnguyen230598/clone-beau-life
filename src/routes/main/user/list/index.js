import React, { useState, useEffect, useRef } from "react";
import {
  Form,
  Tag,
  Input,
  Button,
  Space,
  Tooltip,
  Avatar,
  Switch,
  Modal,
  Upload
} from "antd";
import {
  SearchOutlined,
  PlusOutlined,
  EditOutlined,
  EyeOutlined,
  ImportOutlined,
  FileExcelOutlined,
  ToolOutlined,
  AppleOutlined
} from "@ant-design/icons";
import { connect, routerRedux } from "dva";
import { union } from "lodash";
import { useIntl } from "packages/pro-table/component/intlContext";
import ProTable from "packages/pro-table/Table";
import UserDetail from "../detail";
import { getValue, setTableChange, getTableChange } from "util/helpers";
import AgencySelect from "../../../../components/Select/Agency/ListAgency";
import RoleSelect from "../../../../components/Select/Role/ListRole";
import { FormInputRender } from "packages/pro-table/form";

const { Dragger } = Upload;
const RESOURCE = "user";
const tranforUserParams = (filters, formValues, sorter) => {
  let initFitler = {};
  if (filters) {
    Object.assign(initFitler, filters);
    if (initFitler.name) {
      initFitler.name = {
        contains: initFitler.name
      };
    }
  }
  let initFormValues = {};
  if (formValues) {
    Object.assign(initFormValues, formValues);
    if (initFormValues.name) {
      initFormValues.name = {
        contains: initFormValues.name
      };
    }
  }
  let initSort = [];
  if (sorter && sorter.field) {
    initSort = [{ [sorter.field]: sorter.order === "ascend" ? "asc" : "desc" }];
  }
  return { filters: { ...initFitler, ...initFormValues }, sort: initSort };
};
const UserList = props => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [formInput] = Form.useForm();
  const [formChangePass] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();
  const [isReset, setIsReset] = useState(false);
  const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [skip] = useState(0);
  const [limit] = useState(
    process.env.REACT_APP_PAGESIZE &&
      !Number.isNaN(process.env.REACT_APP_PAGESIZE)
      ? Number(process.env.REACT_APP_PAGESIZE)
      : 10
  );
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });
  const [pagination, setPagination] = useState({
    pageSize: limit
  });
  const tableChange = getTableChange(RESOURCE);
  const [formValues, setFormValues] = useState(origin => {
    return tableChange?.formValues || {};
  });
  const [isModalView, setIsModalView] = useState(false);
  const [isModalChange, setIsModalChange] = useState(false);
  const [, setError] = useState([]);
  const {
    dispatch,
    [RESOURCE]: { data },
    loading
  } = props;

  useEffect(() => {
    let initParams = {};
    let pagi = Object.assign(pagination, tableChange?.pagination || {});
    const defaultCurrent = pagi.current || 1;
    initParams = tranforUserParams(
      tableChange?.filtersArg,
      tableChange?.formValues,
      tableChange?.sort
    );

    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        page: tableChange?.pagination?.current || current,
        current: tableChange?.pagination?.current || current,
        skip: (defaultCurrent - 1) * pagi.pageSize,
        limit: pagi.pageSize || limit,
        pageSize: pagi.pageSize || limit,
        sort: union(initParams.sort || [], [{ createdAt: "desc" }]),
        queryInput: {
          ...initParams.filters
        },
        populate: "agencyId,roleId"
      }
    });
  }, []);

  useEffect(() => {
    let pagi = Object.assign(pagination, (data && data.pagination) || {});
    setPagination(origin => ({
      ...origin,
      ...pagi
    }));
  }, [data]);

  const onFinish = values => {
    let params = { ...values };
    dispatch({
      type: `${RESOURCE}/importUser`,
      payload: params,
      callback: data => {
        // console.log(data)
        Modal.success({
          width : "46vw",
          title: "Kết quả import dữ liệu",
          content: (
            <div>
              <p style={{ color: "green" }}>
                Thành công:{data?.createPhoneSuccess.length}
              </p>
              <ul>
                {data?.createPhoneSuccess.map(e => {
                  return <li>{e.phone}</li>;
                })}
              </ul>
              <p style={{ color: "red" }}>
                Thất bại:{data?.createPhoneFail.length}
              </p>
              <ul>
                {data?.createPhoneFail.map(e => {
                  return (
                    <li>
                      {e.phone} : {e.status}
                    </li>
                  );
                })}
              </ul>
            </div>
          )
        });
        let initParams = {};
        let pagi = Object.assign(pagination, tableChange?.pagination || {});
        const defaultCurrent = pagi.current || 1;
        initParams = tranforUserParams(
          tableChange?.filtersArg,
          tableChange?.formValues,
          tableChange?.sort
        );

        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            page: tableChange?.pagination?.current || current,
            current: tableChange?.pagination?.current || current,
            skip: (defaultCurrent - 1) * pagi.pageSize,
            limit: pagi.pageSize || limit,
            pageSize: pagi.pageSize || limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }]),
            queryInput: {
              ...initParams.filters
            },
            populate: "agencyId,roleId"
          }
        });
      }
    });
  };
  const onFinishChangePass = values => {
    let params = { ...values };
    dispatch({
      type: `${RESOURCE}/changePass`,
      payload: params
    });
  };
  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
    setError(errorInfo.errorFields);
  };

  const handleTableChange = (pagi, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    if (filters) {
      if (filters.isApprove) {
        filters.isApprove = filters.isApprove == "true" ? true : false;
      }
      if (filters.agencyId) {
        filters.agencyId = filters.agencyId?.id;
      }
      if (filters.roleId) {
        filters.roleId = filters.roleId?.id;
      }
    }

    const initParams = tranforUserParams(filters, formValues, sorter);

    if (sessionStorage.getItem("isReset") === "true") {
      sessionStorage.setItem("isReset", false);
    }

    const params = {
      queryInput: {
        ...initParams.filters
      },
      page: Number(pagi.current),
      current: Number(pagi.current),
      skip: (Number(pagi.current) - 1) * Number(pagi.pageSize),
      limit: Number(pagi.pageSize),
      pageSize: Number(pagi.pageSize),
      populate: "agencyId,roleId"
    };
    if (sorter.field) {
      // params.sorter = { [sorter.field]: sorter.order === 'ascend' ? "ASC" : "DESC" };
      params.sort =
        !sorter.order || sorter.order == "ascend"
          ? sorter.field
          : `-${sorter.field}`;
    } else {
      params.sort = union(initParams.sort || [], [{ createdAt: "desc" }]);
    }
    setTableChange(RESOURCE, { pagination: pagi, filters, sorter, formValues });
    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current
      .validateFields()
      .then(fieldsValue => {
        let values = {};
        console.log(
          `🚀 ~ file: index.js ~ line 196 ~ form.current.validateFields ~ values`,
          values
        );
        Object.keys(fieldsValue).forEach(key => {
          if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
            values[key] = fieldsValue[key];
          }
        });
        if (values.isApprove) {
          values.isApprove = values.isApprove == "true" ? true : false;
        }
        if (typeof values.agencyId == "object") {
          values.agencyId = values.agencyId?.id;
        }
        if (typeof values.roleId == "object") {
          values.roleId = values.roleId?.id;
        }
        if (sessionStorage.getItem("isReset") == "true") {
          sessionStorage.setItem("isReset", "false");
          values = {};
        }
        const initParams = tranforUserParams(
          tableChange?.filters,
          values,
          tableChange?.sort
        );
        // props.history.push({
        //   search: "?" + new URLSearchParams(values).toString()
        // });
        setTableChange(RESOURCE, {
          ...tableChange,
          pagination: {
            ...tableChange.pagination,
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit
          },
          formValues: values
        });
        setFormValues(() => values);
        dispatch({
          type: `${RESOURCE}/fetch`,
          payload: {
            queryInput: {
              ...initParams.filters
            },
            page: 1,
            current: 1,
            skip: 0,
            limit,
            pageSize: limit,
            sort: union(initParams.sort || [], [{ createdAt: "desc" }]),
            populate: "agencyId,roleId"
          }
        });
      })
      .catch(err => {
        if (err) return;
      });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0]
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearchFilter(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: ""
    });
    confirm();
  };

  const handleSelectRows = (keys, rows) => {
    setSelectedRows(rows);
    setSelectedRowKeys(keys);
    /* if (keys.length > 0) {
      triggerChange(rows.map(i => i.id));
    } */
  };

  const handleApproved = isApprove => {
    console.log(
      `🚀 ~ file: index.js ~ line 348 ~ handleApproved ~ selectedRowKeys`,
      selectedRowKeys
    );
    dispatch({
      type: `${RESOURCE}/updateMultiApprove`,
      payload: {
        id: selectedRowKeys,
        isApprove
      },
      callback: res => {
        if (res.code == 0) {
          setSelectedRowKeys([]);
          setSelectedRows([]);
          let initParams = {};
          let pagi = Object.assign(pagination, tableChange?.pagination || {});
          const defaultCurrent = pagi.current || 1;
          initParams = tranforUserParams(
            tableChange?.filtersArg,
            tableChange?.formValues,
            tableChange?.sort
          );

          dispatch({
            type: `${RESOURCE}/fetch`,
            payload: {
              page: tableChange?.pagination?.current || current,
              current: tableChange?.pagination?.current || current,
              skip: (defaultCurrent - 1) * pagi.pageSize,
              limit: pagi.pageSize || limit,
              pageSize: pagi.pageSize || limit,
              sort: union(initParams.sort || [], [{ createdAt: "desc" }]),
              queryInput: {
                ...initParams.filters
              },
              populate: "agencyId"
            }
          });
        }
      }
    });
  };

  const handleAddClick = () => {
    dispatch(routerRedux.push({ pathname: `${props.match.url}/add` }));
  };

  const handleEditClick = item => {
    // dispatch({
    //   type: `${RESOURCE}/loadForm`,
    //   payload: {
    //     type: "E",
    //     id: item.id,
    //     item
    //   }
    // });
    dispatch(routerRedux.push({ pathname: `${props.match.url}/${item.id}` }));
  };

  const showModal = () => {
    setIsModalView(true);
  };

  /* const handleOk = () => {
    form.submit();
  }; */

  const handleCancel = () => {
    setIsModalView(false);
  };

  const onReset = () => {
    console.log("reset");
    sessionStorage.setItem("isReset", "true");
    // form.resetFields();
    // setFormValues({})
    setTableChange(RESOURCE, {
      pagination: {
        current: 1,
        skip: 0,
        limit
      },
      filtersArg: {},
      sorter: {},
      formValues: {}
    });
    // setFormValues(() => ({}));
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      width: 50,
      fixed: "left",
      ellipsis: true,
      hideInSearch: false,
      hideInTable: false
    },
    {
      title: "Tên",
      dataIndex: "name",
      width: 200,
      fixed: "left",
      hideInSearch: false,
      hideInTable: false,
      render: (val, record) => {
        return (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start"
            }}
          >
            <div style={{ textTransform: "uppercase" }}>
              <Avatar
                src={
                  Array.isArray(record.avatar)
                    ? record.avatar[0].url
                    : record.avatar
                }
              />
              <span style={{ marginLeft: "5px" }}>{val}</span>
            </div>
            <a
              style={{ fontSize: "12px", marginLeft: "5px" }}
              onClick={() => {
                setDrawerDetail({
                  visible: true,
                  record
                });
              }}
            >
              <Tooltip title="Chi tiết">
                <EyeOutlined />
              </Tooltip>
            </a>
          </div>
        );
      },
      ...getColumnSearchProps("name")
    },
    {
      title: "Email",
      dataIndex: "email",
      width: 60,
      hideInSearch: false,
      hideInTable: false
    },
    {
      title: "Đại lý",
      dataIndex: "agencyId",
      width: 120,
      hideInSearch: false,
      hideInTable: false,
      render: val => <span>{val ? val.name : ""}</span>,
      renderFormItem: (_, { type, defaultRender, ...rest }, form) => {
        if (type === "form") {
          return null;
        }
        return <AgencySelect mode="radio" />;
        // return defaultRender(_);
      }
    },
    {
      title: "Ngày sinh",
      dataIndex: "birth",
      width: 120,
      hideInSearch: true,
      hideInTable: false,
      valueType: "date"
      // ...getColumnSearchProps('name')
    },
    {
      title: "Giới tính",
      dataIndex: "gender",
      width: 120,
      filters: true,
      onFilter: (value, record) => true,
      hideInSearch: false,
      hideInTable: false,
      valueEnum: {
        Nam: {
          text: "Nam",
          status: "Processing",
          color: "#ec3b3b",
          isText: true
        },
        Nữ: {
          text: "Nữ",
          status: "Default",
          color: "#ec3b3b",
          isText: true
        },
        Khác: {
          text: "Khác",
          status: "Default",
          color: "#ec3b3b",
          isText: true
        }
      }
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      width: 120,
      filters: false,
      hideInSearch: true,
      hideInTable: true
      // ...getColumnSearchProps('address')
    },
    {
      title: "SĐT",
      dataIndex: "phone",
      width: 120,
      hideInSearch: false,
      hideInTable: false
    },
    {
      title: "Quyền",
      dataIndex: "roleId",
      width: 50,
      hideInSearch: false,
      hideInTable: false,
      render: (val, record) => {
        let color = "cyan";
        if (val?.id == 1) {
          color = "magenta";
        } else if (val?.id == 14) {
          color = "volcano";
        } else if (val?.id == 15) {
          color = "green";
        } else if (val?.id == 18) {
          color = "orange";
        }
        return <Tag color={color}>{val?.name}</Tag>;
      },
      renderFormItem: (_, { type, defaultRender, ...rest }, form) => {
        if (type === "form") {
          return null;
        }
        return <RoleSelect mode="radio" />;
        // return defaultRender(_);
      }
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdAt",
      valueType: "date",
      width: 150,
      sorter: true,
      hideInTable: true,
      hideInSearch: true
    },
    {
      title: "Ngày sửa",
      dataIndex: "updatedAt",
      valueType: "date",
      width: 150,
      sorter: true,
      hideInTable: true,
      hideInSearch: true
    },
    {
      title: "Hoạt động",
      dataIndex: "isApprove",
      width: 150,
      hideInSearch: false,
      hideInTable: false,
      filters: true,
      valueEnum: {
        true: {
          text: "Có",
          status: "Processing",
          color: "#ec3b3b",
          isText: true
        },
        false: {
          text: "Không",
          status: "Default",
          color: "#ec3b3b",
          isText: true
        }
      }
      // render: (val, record) => {
      //   const onChangeState = checked => {
      //     let data = {
      //       userId: record.id,
      //       check: checked
      //     };
      //     dispatch({
      //       type: `user/updateState`,
      //       payload: data,
      //       callback: () => {
      //         let initParams = {};
      //         let pagi = Object.assign(
      //           pagination,
      //           tableChange?.pagination || {}
      //         );
      //         const defaultCurrent = pagi.current || 1;
      //         initParams = tranforUserParams(
      //           tableChange?.filtersArg,
      //           tableChange?.formValues,
      //           tableChange?.sort
      //         );

      //         dispatch({
      //           type: `${RESOURCE}/fetch`,
      //           payload: {
      //             page: tableChange?.pagination?.current || current,
      //             current: tableChange?.pagination?.current || current,
      //             skip: (defaultCurrent - 1) * pagi.pageSize,
      //             limit: pagi.pageSize || limit,
      //             pageSize: pagi.pageSize || limit,
      //             sort: union(initParams.sort || [], [{ createdAt: "desc" }]),
      //             queryInput: {
      //               ...initParams.filters
      //             },
      //             populate: "agencyId"
      //           }
      //         });
      //       }
      //     });
      //   };
      //   return (
      //     <Switch
      //       defaultChecked={record.isApprove ? true : false}
      //       onChange={e => onChangeState(e)}
      //     />
      //   );
      // }
    },
    {
      title: "Thao tác",
      width: 100,
      fixed: "right",
      render: (text, record) => (
        <div>
          <Tooltip title={`Sửa`}>
            <EditOutlined onClick={() => handleEditClick(record)} />
          </Tooltip>
          <Tooltip title={`Đổi mật khẩu`}>
            <ToolOutlined
              onClick={() => {
                formChangePass.setFieldsValue({ userId: record.id });
                setIsModalChange(true);
              }}
              style={{ marginLeft: "10px" }}
            />
          </Tooltip>
        </div>
      )
    }
  ];

  const draggerCheckCode = {
    accept: ".csv,.xlsx",
    multiple: false,
    beforeUpload(file) {
      // setDataFile(file)
      formInput.setFieldsValue({
        file
      });
      console.log(file);
      return false;
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
    onRemove(file) {}
  };

  return (
    <>
      {/* <IntlProvider value={viVNIntl}> */}
      <ProTable
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Người dùng"
        actionRef={actionRef}
        formRef={form}
        form={{
          // initialValues: sessionStorage.getItem("isReset") == "true" ? {} : formValues,
          initialValues: formValues
          // preserve: false
        }}
        dataSource={data && data.list}
        pagination={data.pagination}
        columns={columns}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        beforeSearchSubmit={searchParams => {
          return {};
        }}
        onReset={onReset}
        toolBarRender={(action, { selectedRows }) => [
          // selectedRows && selectedRows.length > 0 && (
          //   <Button
          //     icon={<AppleOutlined />}
          //     // type="primary"
          //     onClick={() => handleApproved(true)}
          //   >
          //     {`Duyệt`}
          //   </Button>
          // ),
          // selectedRows && selectedRows.length > 0 && (
          //   <Button
          //     icon={<AppleOutlined />}
          //     // type="primary"
          //     onClick={() => handleApproved(false)}
          //   >
          //     {`Bỏ duyệt`}
          //   </Button>
          // ),
          <Button
            type="primary"
            onClick={() =>
              window.open(
                `${
                  process.env.REACT_APP_URL
                }/api/admin/user/export?accesstoken=${localStorage.getItem(
                  "token"
                )}`
              )
            }
          >
            {`Xuất báo cáo`}
          </Button>,
          <Button
            icon={<ImportOutlined />}
            // type="primary"
            onClick={() => showModal()}
          >
            {`Nhập từ file`}
          </Button>,
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => handleAddClick()}
          >
            {`Thêm mới`}
          </Button>
        ]}
        rowSelection={{
          // type: 'radio',
          selectedRowKeys,
          onChange: handleSelectRows,
          renderCell: (checked, record, index, originNode) => {
            return <Tooltip title={"Chọn người dùng"}>{originNode}</Tooltip>;
          }
        }}
      />
      <UserDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={v => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
      {/* </IntlProvider> */}
      <Modal
        title="Import dữ liệu người dùng"
        visible={isModalView}
        onOk={() => formInput.submit()}
        onCancel={handleCancel}
        width={800}
      >
        <Form
          className="ant-advanced-search-form"
          form={formInput}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          hideRequiredMark
        >
          <Form.Item
            label={"Chọn file"}
            name="file"
            rules={[{ required: true, message: "File không được trống" }]}
          >
            <Dragger {...draggerCheckCode}>
              <p className="ant-upload-drag-icon">
                <FileExcelOutlined />
              </p>
              <p className="ant-upload-text">
                Nhấn hoặc thả tệp tin bạn muốn import
              </p>
              <p className="ant-upload-hint">
                Kiểu tệp tin hỗ trợ{" "}
                <a className="ant-upload-hint" style={{ fontWeight: "bold" }}>
                  {" "}
                  .csv .xlsx
                </a>
              </p>
            </Dragger>
          </Form.Item>
          <Form.Item
            label={"Chọn đơn vị"}
            name="agencyId"
            rules={[{ required: true, message: "Mã đại lý không được trống" }]}
          >
            <AgencySelect mode="radio" />
          </Form.Item>
        </Form>
        <p>
          Tải file mẫu{" "}
          <a href="/import-file.xlsx" download>
            tại đây
          </a>
        </p>
      </Modal>
      <Modal
        title="Đổi mật khẩu"
        visible={isModalChange}
        onOk={() => formChangePass.submit()}
        onCancel={() => setIsModalChange(false)}
        width={800}
      >
        <Form
          className="ant-advanced-search-form"
          form={formChangePass}
          layout="vertical"
          onFinish={onFinishChangePass}
          hideRequiredMark
        >
          <Form.Item
            label={"Người dùng"}
            name="userId"
            rules={[{ required: true, message: "Người dùng không được trống" }]}
          >
            <FormInputRender
              item={{ title: "Địa chỉ" }}
              type="form"
              disabled={true}
              intl={intl}
            />
          </Form.Item>
          <Form.Item
            label={"Mật khẩu"}
            name="password"
            rules={[
              { required: true, message: "Mật khẩu không được để trống" }
            ]}
          >
            <Input.Password
              item={{ title: "Tên", dataIndex: "name", type: "password" }}
              placeHolder={`Nhập mật khẩu người dùng`}
            />
          </Form.Item>
          <Form.Item
            label={"Nhắc lại mật khẩu"}
            name="repeatPassword"
            rules={[
              {
                required: true,
                message: "Nhắc lại mật khẩu không được để trống"
              }
            ]}
          >
            <Input.Password
              item={{ title: "Tên", dataIndex: "name", type: "password" }}
              placeHolder={`Nhắc lại mật khẩu`}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default connect(({ user, loading }) => ({
  user,
  loading: loading.models.user
}))(UserList);
