import React from "react";
import { router } from 'dva';
import asyncComponent from "util/asyncComponent";
import Roles from "./roles";
import Dashboard from "./dashboard";
import UserList from "./user/list";
import UserCrud from "./user/crud";
/* PLOP_INJECT_IMPORT */

const { Route, Switch } = router;
const Main = ({ match }) => (
  <Switch>
    <Route path={`${match.url}/dashboard`} component={Dashboard} />
    <Route path={`${match.url}/roles`} component={Roles} />
    <Route path={`${match.url}/user`} component={UserList} exact={true} />
    <Route path={`${match.url}/user/:id`} component={UserCrud} />
    <Route path={`${match.url}/config`} component={asyncComponent(() => import('./config/detail/detail'))} />
    {/* <Route path={`${match.url}/notification`} component={asyncComponent(() => import('./notification'))} /> */}
    {/* PLOP_INJECT_EXPORT */}
<Route path={`${match.url}/customerOld/:id`} component={asyncComponent(() => import('./customerOld/crud'))} />
<Route path={`${match.url}/customerold`} component={asyncComponent(() => import('./customerOld/list'))} exact={true} />
<Route path={`${match.url}/loginImage/:id`} component={asyncComponent(() => import('./loginImage/crud'))} />
<Route path={`${match.url}/loginImage`} component={asyncComponent(() => import('./loginImage/list'))} exact={true} />
    <Route path={`${match.url}/actionLog`} component={asyncComponent(() => import('./actionLog/list'))} exact={true} />
    <Route path={`${match.url}/customer/:id`} component={asyncComponent(() => import('./customer/crud'))} />
    <Route path={`${match.url}/customer`} component={asyncComponent(() => import('./customer/list'))} exact={true} />
    <Route path={`${match.url}/notification/:id`} component={asyncComponent(() => import('./notification/crud'))} />
    <Route path={`${match.url}/notification`} component={asyncComponent(() => import('./notification/list'))} exact={true} />
    <Route path={`${match.url}/agency/:id`} component={asyncComponent(() => import('./agency/crud'))} />
    <Route path={`${match.url}/agency`} component={asyncComponent(() => import('./agency/list'))} exact={true} />
    <Route path={`${match.url}/tools`} component={asyncComponent(() => import('./tools'))} exact={true} />
  </Switch>
);

export default Main;
