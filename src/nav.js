export default [
  {
    "id": 1,
    "name": "Dasboard",
    "icon": "icon-menu",
    "parent": 0,
    "isParent": true,
    "sequence": 0,
    "model": null,
    "active": true,
    "childrens": [
      {
        "id": 2,
        "name": "Tổng quan dự án",
        "icon": "icon-menu",
        "parent": 1,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true,
      }
    ]
  },
  {
    "id": 3,
    "name": "Hệ thống",
    "icon": "icon-menu",
    "parent": 0,
    "isParent": true,
    "sequence": 0,
    "model": null,
    "active": true,
    "childrens": [
      {
        "id": 4,
        "name": "Phân quyền",
        "icon": "icon-menu",
        "parent": 3,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true
      },
      {
        "id": 5,
        "name": "Người dùng",
        "icon": "icon-menu",
        "parent": 3,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true
      },
      {
        "id": 6,
        "name": "Đại lý",
        "icon": "icon-menu",
        "parent": 3,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true
      }
    ]
  },
  {
    "id": 7,
    "name": "Thi online",
    "icon": "icon-menu",
    "parent": 0,
    "isParent": true,
    "sequence": 0,
    "model": null,
    "active": true,
    "childrens": [
      {
        "id": 8,
        "name": "Lớp học",
        "icon": "icon-menu",
        "parent": 7,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true,
      }
    ]
  },
  {
    "id": 9,
    "name": "Quản lý dự án",
    "icon": "icon-menu",
    "parent": 0,
    "isParent": true,
    "sequence": 0,
    "model": null,
    "active": true,
    "childrens": [
      {
        "id": 10,
        "name": "Dự án",
        "icon": "icon-menu",
        "parent": 9,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true,
      },
      {
        "id": 11,
        "name": "Video dự án",
        "icon": "icon-menu",
        "parent": 11,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true,
      }
    ]
  },
  {
    "id": 12,
    "name": "Tình trạng dự án",
    "icon": "icon-menu",
    "parent": 0,
    "isParent": true,
    "sequence": 0,
    "model": null,
    "active": true,
    "childrens": [
      {
        "id": 13,
        "name": "Checklist dự án",
        "icon": "icon-menu",
        "parent": 12,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true,
      }
    ]
  },
  {
    "id": 13,
    "name": "Booking",
    "icon": "icon-menu",
    "parent": 0,
    "isParent": true,
    "sequence": 0,
    "model": null,
    "active": true,
    "childrens": [
      {
        "id": 14,
        "name": "Book căn",
        "icon": "icon-home",
        "parent": 13,
        "isParent": false,
        "sequence": 0,
        "model": null,
        "active": true,
      }
    ]
  }
];
