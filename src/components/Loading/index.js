import React from 'react';
const Loading = props => {
  return (
    <div className="loader loading">
      <img src="/loading.gif" alt="loader" />
    </div>
  );
};
export default Loading