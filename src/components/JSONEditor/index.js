import React, { Component } from 'react';
import { JsonEditor as Editor } from 'jsoneditor-react';
import './editor.min.css';
import ace from 'brace';
import 'brace/mode/json';
import 'brace/theme/solarized_light';

class JSONWidget extends Component {
	onChange(val) {
		this.setState({ checked: val });
		if (this.props.onChange) {
			this.props.onChange(val);
		}
	}
	render() {
		// console.log(JSON.stringify(this.props.value));
		let placeholder = this.props.value || {};
		try {
			placeholder = JSON.parse(this.props.value);
		} catch (err) {
		}
		// console.log('place holder', placeholder);
		return (
				<Editor

					value={placeholder}
					onChange={e => {
						this.onChange(e);
					}}
					ace={ace}
					theme="ace/theme/solarized_light"
					mode={Editor.modes.code}
					allowedModes={[Editor.modes.tree, Editor.modes.code]}
				// schema={yourSchema}
				/>
		
		);
	}
}

export default JSONWidget;
