import React, { useState, useEffect } from 'react';
import JSONInput from 'react-json-ide';
import locale from 'react-json-ide/locale/en';

const ComponentName = ({ value, onChange }) => {
	const [data, setData] = useState({});
	const [error, setError] = useState(null);

	useEffect(() => {
		setData(value || {});
	}, [value]);

	return (
		<React.Fragment>
			{/* {error && <div>
				<span>Line: {error.line}</span>
				<span>{error.reason}</span>
			</div>} */}
			<JSONInput
				placeholder={data} // data to display
				// theme="light_mitsuketa_tribute"
				theme="dark_vscode_tribute"
				locale={locale}
				colors={{
					string: "#DAA520",
					// background: "#dddada"
				}}
				width="100%"
				height="550px"
				onChange={(data) => {
					console.log('FormSchemaForm -> data', data);
					if (!data.error) {
						if (onChange) {
							onChange(data.jsObject || {});
						}
					} else {
						setError(data.error);
					}
				}}
			/>
		</React.Fragment>
	);
};

export default ComponentName;
