import React, { useEffect, useState } from "react";
import {
  Area,
  AreaChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  Legend,
  PieChart,
  Pie,
  Cell
} from "recharts";
import { Row, Col } from "antd";
import { connect } from "dva";
// import dayjs from 'dayjs';
import Widget from "components/Widget/index";
import ListProjectSelect from "src/components/Select/Project/ListProject";
import * as reportclass from "src/services/reportclass";
import Loading from "src/components/Loading";
const RESOURCE = "reportclass";
const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];
const BalanceHistory = props => {
  const {
    dispatch
    // [RESOURCE]: { data },
  } = props;

  const [dataProject, setDataProjectStaff] = useState({});
  const [loading, setLoading] = useState(false);
  const CustomTooltip1 = ({ active, payload, label }) => {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p className="label">{`${payload[0].payload.name} : ${payload[0].value} đại lý`}</p>
          {/* <p className="intro">{getIntroOfPage(label)}</p> */}
          {/* <p className="desc">Anything you want can be displayed here.</p> */}
        </div>
      );
    }

    return null;
  };
  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill="white"
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };
  const style = {
    left: 60,
    bottom: 25,
    lineHeight: 2,
    paddingTop: 5,
    paddingLeft: 5,
    width: "500px"
  };
  const getDataProjectStaff = async (projectId = 1) => {
    setLoading(true);
    let api = await reportclass.dataProjectStaff({ projectId });
    setDataProjectStaff(api.data);
    setLoading(false);
  };

  useEffect(() => {
    getDataProjectStaff();
  }, []);
  const CustomizedLabel = props => {
    const { x, y, stroke, value } = props;

    return (
      <text
        x={x + 10}
        y={y}
        dy={-5}
        fill={stroke}
        fontSize={15}
        textAnchor="middle"
      >
        {value}
      </text>
    );
  };
  // const formatDate = (value) => {
  //   return dayjs(value).format("DD-MM");
  //   /* const date = new Date(value);
  //   return `${date.getMonth()}-${date.getFullYear()}` */
  // };

  return (
    <Widget styleName="gx-card-full">
      <div className="ant-row-flex gx-px-4 gx-pt-4">
        <h2 className="h4 gx-mb-3">Báo cáo theo dự án</h2>
        <div className="gx-ml-auto">
          <ListProjectSelect
            value={1}
            onChange={e => {
              getDataProjectStaff(e.id);
            }}
          />
        </div>
      </div>
      {loading ? (
        <Loading />
      ) : (
        <Row>
          <Col md={10}>
            <ResponsiveContainer width="100%" height={220}>
              <PieChart width={400} height={400}>
                <Pie
                  data={dataProject ? dataProject.data : []}
                  cx={120}
                  cy={100}
                  labelLine={false}
                  label={renderCustomizedLabel}
                  outerRadius={80}
                  fill="#8884d8"
                  dataKey="value"
                >
                  {dataProject
                    ? (dataProject.data || []).map((entry, index) => (
                        <Cell
                          key={`cell-${index}`}
                          fill={COLORS[index % COLORS.length]}
                        />
                      ))
                    : null}
                </Pie>
                <Tooltip content={<CustomTooltip1 />} />
                <Legend wrapperStyle={style} />
              </PieChart>
            </ResponsiveContainer>
          </Col>
          <Col md={14}>
            <div className="nv-build-infochart">
              <div className="nv-info-classroom">
                <span> Tổng số nhân viên </span>
                <span> {dataProject ? dataProject.total : "0"} Người</span>
              </div>
              {dataProject
                ? (dataProject.data || []).map((item, index) => {
                    return (
                      <>
                        <div className="nv-info-classroom">
                          <span key={index}>{item.name}: </span>
                          <span key={index}>{item.value} Người</span>
                        </div>
                      </>
                    );
                  })
                : null}
            </div>
          </Col>
        </Row>
      )}
    </Widget>
  );
};

export default connect(({ reportclass, loading }) => {
  const { data: historyUser } = reportclass;
  return {
    historyUser,
    loading: loading.models.historyUser
  };
})(BalanceHistory);
