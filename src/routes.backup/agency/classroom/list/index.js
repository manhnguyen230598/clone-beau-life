import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Space, Tooltip, Avatar } from 'antd';
import { SearchOutlined, PlusOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons';
import { connect, routerRedux } from 'dva';
import ProTable from 'packages/pro-table/Table';
import ClassRoomDetail from '../detail';
import { getValue } from 'util/helpers';

const RESOURCE = "classroom";
const ClassRoomList = (props) => {
  const [form] = Form.useForm();
  const searchInput = useRef();
  const actionRef = useRef();

  /* const [selectedRows, setSelectedRows] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); */
  const [formValues, setFormValues] = useState({});
  const [skip,] = useState(0);
  const [limit,] = useState((process.env.REACT_APP_PAGESIZE && !Number.isNaN(process.env.REACT_APP_PAGESIZE)) ? Number(process.env.REACT_APP_PAGESIZE) : 10);
  const [current, setCurrent] = useState(1);
  const [listSearch, setListSearch] = useState({});
  const [drawerDetail, setDrawerDetail] = useState({
    visible: false,
    record: {}
  });

  const {
    dispatch,
    [RESOURCE]: { data },
    loading
  } = props;

  useEffect(() => {
    dispatch({
      type: `${RESOURCE}/fetch`,
      payload: {
        current,
        skip,
        limit
      }
    });
  }, []);

  const handleTableChange = (pagination, filtersArg, sorter) => {
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      if (filtersArg[key] !== null) newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    if (filters) {
      if (filters.category) {
        let filCate = filters.category;
        filters.categories = filCate;
        delete filters.category;
      }
    }

    const params = {
      ...formValues,
      ...filters,
      current: Number(pagination.current),
      skip: (Number(pagination.current) - 1) * Number(pagination.pageSize),
      limit: Number(pagination.pageSize)
    };
    if (sorter.field) {
      // params.sorter = { [sorter.field]: sorter.order === 'ascend' ? "ASC" : "DESC" };
      params.sort = (!sorter.order || sorter.order == 'ascend') ? sorter.field : `-${sorter.field}`;
    }

    setCurrent(pagination.current);
    dispatch({
      type: `${RESOURCE}/'fetch'`,
      payload: params
    });
  };

  const handleTableSearch = () => {
    form.current.validateFields().then(fieldsValue => {
      console.log('handleSearch -> fieldsValue', fieldsValue);
      const values = {
        current: 1,
        skip: 0,
        limit,
      };
      Object.keys(fieldsValue).forEach(key => {
        if (fieldsValue[key] !== null && fieldsValue[key] !== undefined) {
          values[key] = fieldsValue[key];
        }
      });

      if (fieldsValue.fromDate) {
        const fromDate = fieldsValue.fromDate.set({ 'hour': 0, 'minute': 0, 'second': 0 }).valueOf();
        values.start_at = [fromDate];
      }
      if (fieldsValue.toDate) {
        const toDate = fieldsValue.toDate.set({ 'hour': 23, 'minute': 59, 'second': 59 }).valueOf();
        values.end_at = toDate;
      }
      delete values.fromDate;
      delete values.toDate;

      const quantity = values.quantity;
      delete values.quantity;
      if (fieldsValue.quantity) {
        values.typeQuantity = quantity.typeQuantity;
        if (quantity.typeQuantity !== 'range') {
          values.quantity = Number(quantity.fromQuantity);
        } else {
          values.quantity = [Number(quantity.fromQuantity), Number(quantity.toQuantity)];
        }
      }

      setFormValues(formValues);
      dispatch({
        type: `${RESOURCE}/'fetch'`,
        payload: {
          ...values
        }
      });
    }).catch(err => {
      if (err) return;
    });
  };

  const handleSearchFilter = (selectedKeys, confirm, dataIndex) => {
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: selectedKeys[0],
    });
    confirm();
  };

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Tìm kiếm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearchFilter(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters, confirm, dataIndex)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    onFilter: (value, record) => record
  });

  const handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    setListSearch({
      ...listSearch,
      [`search_${dataIndex}`]: '',
    });
    confirm();
  };

  const handleAddClick = () => {
    dispatch(routerRedux.push({ pathname: `${props.match.url}/add` }));
  };

  const handleEditClick = item => {
    dispatch(routerRedux.push({ pathname: `${props.match.url}/${item.id}` }));
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 80,
      fixed: 'left',
      ellipsis: true,
      hideInSearch: false,
      hideInTable: false,
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
      hideInSearch: false,
      hideInTable: false,
      render: (val, record) => {
        return (
          <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
            <div style={{ textTransform: 'uppercase' }}>
              <Avatar src={record.thumbnail} />
              <span style={{ marginLeft: '5px' }}>{val}</span>
            </div>
            <a style={{ fontSize: '12px', marginLeft: '5px' }}
              onClick={() => {
                setDrawerDetail({
                  visible: true,
                  record
                });
              }}>
              <Tooltip title="Chi tiết">
                <EyeOutlined />
              </Tooltip>
            </a>
          </div>
        );
      },
      ...getColumnSearchProps('name')
    },
    {
      title: 'Ngày bắt đầu',
      dataIndex: 'startDate',
      width: 120,
      hideInSearch: false,
      hideInTable: false,
      valueType: 'date',
    },
    {
      title: 'Ngày kết thúc',
      dataIndex: 'endDate',
      width: 120,
      hideInSearch: false,
      hideInTable: false,
      valueType: 'date',
    },
    {
      title: 'Giáo viên',
      dataIndex: 'teacherName',
      width: 120,
      filters: true,
      hideInSearch: false,
      hideInTable: false,
      valueEnum: {
        "male": { "text": "Nam", "status": "Custom", "color": "#ec3b3b", "isText": true },
        "female": { "text": "Nữ", "status": "Custom", "color": "#ec3b3b", "isText": true }
      }
    },
    {
      title: 'Mô tả',
      dataIndex: 'description',
      width: 120,
      filters: false,
      hideInSearch: true,
      hideInTable: true,
      // ...getColumnSearchProps('address')
    },
    {
      title: 'Dự án',
      dataIndex: 'projectId',
      width: 120,
      hideInSearch: true,
      hideInTable: true,
      ...getColumnSearchProps('projectId')
    },
    {
      title: 'Môi giới',
      dataIndex: 'agencyId',
      width: 120,
      hideInSearch: true,
      hideInTable: true,
      ...getColumnSearchProps('agencyId')
    },
    {
      title: 'Trạng thái',
      dataIndex: 'isActive',
      width: 120,
      fitlers: true,
      hideInSearch: false,
      hideInTable: false,
      valueEnum: {
        "true": { "text": "Hoạt động", "status": "Processing", "color": "#ec3b3b", "isText": true },
        "false": { "text": "Dừng", "status": "Default", "color": "#ec3b3b", "isText": true }
      }
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'createdAt',
      valueType: 'date',
      width: 150,
      sorter: true,
    },
    {
      title: 'Ngày sửa',
      dataIndex: 'updatedAt',
      valueType: 'date',
      width: 150,
      sorter: true,
    },
    {
      title: 'Thao tác',
      width: 50,
      fixed: 'right',
      render: (text, record) => (
        <Tooltip title={`Sửa`}><EditOutlined onClick={() => handleEditClick(record)} /></Tooltip>
      ),
    },
  ];

  const pagination = {
    ...(data && data.pagination)
  };

  return (
    <>
      {/* <IntlProvider value={viVNIntl}> */}
      <ProTable
        // className="gx-table-responsive"
        tableClassName="gx-table-responsive"
        type="table"
        rowKey="id"
        search={true}
        headerTitle="Người dùng"
        actionRef={actionRef}
        formRef={form}
        dataSource={data && data.list}
        pagination={pagination}
        columns={columns}
        loading={loading}
        // scroll={{ x: 1500, y: 300 }}
        onChange={handleTableChange}
        onSubmit={handleTableSearch}
        toolBarRender={(action, { selectedRows }) => [
          <Button icon={<PlusOutlined />} type="primary" onClick={() => handleAddClick()}>
            {`Thêm mới`}
          </Button>
        ]}
      />
      <ClassRoomDetail
        drawerVisible={drawerDetail.visible}
        record={drawerDetail.record}
        onChange={(v) => {
          setDrawerDetail(origin => ({
            ...origin,
            visible: v
          }));
        }}
      />
      {/* </IntlProvider> */}
    </>
  );
};

export default connect(({ classroom, loading }) => ({
  classroom,
  loading: loading.models.classroom
}))(ClassRoomList);
