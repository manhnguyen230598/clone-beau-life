
import React from 'react';
import { Image, Drawer, Row, Col } from 'antd';
import TableStatus from 'packages/pro-table/component/status';
import get from 'lodash/get';
import * as enums from 'util/enums';
import { formatDate } from 'util/helpers';

const DescriptionItem = ({ title, content }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <span className="nv-detail-item-content">{content}</span>
  </div>
);

const ImagesItem = ({ title, images }) => (
  <div className="nv-detail-item-wrapper">
    <p className="nv-detail-item-label">{title}:</p>
    <div>
      {images.map(item => (
        <Image width={80} src={item.url} />
      ))}
    </div>
  </div>
);

const ClassRoomDetail = ({ record, drawerVisible, onChange }) => {
  const triggerChange = (v) => {
    if (onChange) {
      onChange(v);
    }
  };

  const onClose = () => {
    triggerChange(false);
  };
/*   const genderStatus = get(enums.genderEnum[get(record, "gender", "-")], "status", null);
  const Status = TableStatus[genderStatus || 'Init'];
  const genderText = get(enums.genderEnum[get(record, "gender", "-")], "text", "-"); */

  return (
    <Drawer
      width={640}
      title={<span style={{ color: "#f09b1b" }}>{`Hồ sơ chi tiết ${get(record, "fullname", "-").toUpperCase()}`}</span>}
      placement="right"
      closable={true}
      onClose={onClose}
      visible={drawerVisible}
    >
      <Row gutter={16}>
        <Col span={12}>
          <DescriptionItem title="Họ tên" content={get(record, "fullname", "-")} />
        </Col>
        {/* <Col span={12}>
          <DescriptionItem title="Giới tính" content={<Status>{genderText}</Status>} />
        </Col> */}
      </Row>
      <Row gutter={16}>
        <Col span={12}>
          <DescriptionItem title="Ngày sinh" content={formatDate(get(record, "dateOfBirth", "-"))} />
        </Col>
        <Col span={12}>
          <DescriptionItem title="Nơi sinh" content={get(record, "whereOfBirth", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={12}>
          <DescriptionItem title="Dân tộc" content={get(record, "nation", "-")} />
        </Col>
        <Col span={12}>
          <DescriptionItem title="Tôn giáo" content={get(record, "religion", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <DescriptionItem title="Hộ khẩu thường trú" content={get(record, "houseHold", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={12}>
          <DescriptionItem title="Năm tốt nghiệp" content={get(record, "graduateYear", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={8}>
          <DescriptionItem title="Số CMND/CCCD" content={get(record, "identityCard", "-")} />
        </Col>
        <Col span={8}>
          <DescriptionItem title="Ngày cấp" content={formatDate(get(record, "identityDate", "-"))} />
        </Col>
        <Col span={8}>
          <DescriptionItem title="Nơi cấp" content={get(record, "identityBy", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={12}>
          <DescriptionItem title="Số điện thoại" content={get(record, "phone", "-")} />
        </Col>
        <Col span={12}>
          <DescriptionItem title="Số điện thoại phụ huynh" content={get(record, "phoneParent", "-")} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <ImagesItem title={`Ảnh học bạ`} images={get(record, "imageSchool", [])} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <ImagesItem title={`Ảnh CMND/CCCD`} images={get(record, "imageIdentity", [])} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <ImagesItem title={`Ảnh bằng tốt nghiệp`} images={get(record, "imageCertificate", [])} />
        </Col>
      </Row>
    </Drawer>
  );
};

export default ClassRoomDetail;
