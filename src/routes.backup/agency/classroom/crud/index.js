import React, { useEffect, useState } from 'react';
import { Card,Button, Form, Col, Row,Popover, message } from 'antd';
import _ from 'lodash';
import { routerRedux } from 'dva';
import { CloseCircleOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import { FormInputRender } from 'packages/pro-table/form';
import { useIntl } from 'packages/pro-table/component/intlContext';
import FooterToolbar from 'packages/FooterToolbar';
import Upload from 'components/Upload';
import * as enums from 'util/enums';
import { camelCaseToDash } from 'util/helpers';

const RESOURCE = "classroom";
const fieldLabels = { "name": "Tên", "brandId": "Thương hiệu", "categoryId": "Danh mục", "thumbnail": "Ảnh đại diện", "images": "Thư viện ảnh", "quantities": "Kho", "originPrice": "Giá gốc", "price": "Giá bán", "valueAccumulatePoints": "Điểm tặng", "description": "Mô tả", "status": "Trạng thái" };
const ClassRoomCrud = ({ classroom: { formTitle, formData }, dispatch, submitting, match: { params }, history, ...rest }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [error, setError] = useState([]);
  const [width,] = useState('100%');
  const getErrorInfo = (errors) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }
    const scrollToField = (fieldKey) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };
    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0];
      return (
        <li key={key} className="nv-error-list-item" onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className="nv-error-icon" />
          <div className="nv-error-message">{err.errors[0]}</div>
          <div className="nv-error-field">{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className="nv-error-icon">
        <Popover
          title="Thông tin lỗi"
          content={errorList}
          overlayClassName="nv-error-popover"
          trigger="click"
          getPopupContainer={(trigger) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined /> &nbsp;{errorCount || 0} lỗi
        </Popover>
      </span>
    );
  };

  const onFinish = (values) => {
    setError([]);
    const data = _.cloneDeep(values);
    if (data.brandId && typeof data.brandId == 'object') {
      data.brandId = data.brandId.key;
    }
    if (data.thumbnail && typeof data.thumbnail == 'object') {
      data.thumbnail = _.map(data.thumbnail, (item) => {
        return _.pick(item, ["id", "uid", "path", "url"]);
      });
    }
    if (data.images && typeof data.images == 'object') {
      data.images = _.map(data.images, (item) => {
        return _.pick(item, ["id", "uid", "path", "url"]);
      });
    }
    dispatch({
      type: 'classroom/submit',
      payload: data,
      callback: (res) => {
        if (res.error) {
          message.error(res.error.message);
        } else {
          dispatch(routerRedux.push({ pathname: `agency/classroom/${camelCaseToDash(RESOURCE)}` }));
        }
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setError(errorInfo.errorFields);
  };

  useEffect(() => {
    dispatch({
      type: 'classroom/loadForm',
      payload: {
        type: params.id !== 'add' ? 'E' : 'A',
        id: params.id !== 'add' ? params.id : null
      },
    });
  }, []);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...formData
    });
  }, [formData]);

  /* useEffect(() => {
    function resizeFooterToolbar() {
      requestAnimationFrame(() => {
        const sider = document.querySelectorAll('.ant-layout-sider')[0];
        if (sider) {
          const widthCur = `calc(100% - ${sider.style.width})`;
          if (width !== widthCur) {
            setWidth(width);
          }
        }
      });
    };
    window.addEventListener('resize', resizeFooterToolbar, { passive: true });
    return function cleanup() {
      window.removeEventListener('resize', resizeFooterToolbar);
    };
  }); */

  if (params.id === 'add' || (formData && formData.id && formData.id !== 'add')) {
    return (
      <Form
        className="ant-advanced-search-form"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        hideRequiredMark
        initialValues={{ ...formData }}
      >
        <Card title="Thông tin cơ bản" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['name']}
                name="name"
                rules={[{ "required": true, "message": "Tên không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Tên", "dataIndex": "name" }}
                  placeHolder={`Nhập tên sản phẩm`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['brandId']}
                name="brandId"
                rules={[{ "required": true, "message": "Thương hiệu không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Tên", "dataIndex": "name" }}
                  placeHolder={`Nhập tên sản phẩm`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={{ span: 12, offset: 0 }} lg={{ span: 12 }} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['categoryId']}
                name="categoryId"
                rules={[{ "required": true, "message": "Danh mục không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Tên", "dataIndex": "name" }}
                  placeHolder={`Nhập tên sản phẩm`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={{ span: 12 }} lg={{ span: 8 }} md={{ span: 24 }} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['quantities']}
                name="quantities"
                rules={[{ "required": true, "message": "Kho không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Kho", "dataIndex": "quantities", valueType: "digit" }}
                  placeHolder={`Nhập số lượng kho sản phẩm`}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={{ span: 12, offset: 0 }} lg={{ span: 12 }} md={{ span: 24 }} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['valueAccumulatePoints']}
                name="valueAccumulatePoints"
                rules={[{ "required": false, "message": "Điểm tặng không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Điểm tặng", "dataIndex": "valueAccumulatePoints", valueType: "digit" }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={12} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['originPrice']}
                name="originPrice"
                rules={[{ "required": true, "message": "Giá gốc không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Giá gốc", "dataIndex": "originPrice", valueType: "money" }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={{ span: 12, offset: 0 }} lg={{ span: 8 }} md={{ span: 24 }} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['price']}
                name="price"
                rules={[{ "required": true, "message": "Giá bán không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Giá bán", "dataIndex": "price", valueType: "money" }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Ảnh" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['thumbnail']}
                name="thumbnail"
                rules={[{ "required": true, "message": "Ảnh đại diện không được trống" }]}
              >
                <Upload multiple={false} />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={{ span: 12, offset: 0 }} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['images']}
                name="images"
              // rules={[{ "required": false, "message": "Mô tả không được trống" }]}
              >
                <Upload multiple={true} />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Thông tin khác" className="gx-card" bordered={false}>
          <Row gutter={24}>
            <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['status']}
                name="status"
                rules={[{ "required": true, "message": "Trạng thái không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Trạng thái", "dataIndex": "status", valueEnum: enums.statusEnum }}
                  intl={intl}
                />
              </Form.Item>
            </Col>
            <Col xxl={12} xl={12} lg={{ span: 12, offset: 0 }} md={24} sm={24} xs={24}>
              <Form.Item
                label={fieldLabels['description']}
                name="description"
                rules={[{ "required": true, "message": "Mô tả không được trống" }]}
              >
                <FormInputRender
                  item={{ "title": "Mô tả", "dataIndex": "description", valueType: "textarea" }}
                  intl={intl}
                  type="form"
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <FooterToolbar style={{ width }}>
          {getErrorInfo(error)}
          <Button type="primary" onClick={() => form.submit()} loading={submitting}>
            {params.id === 'add' ? "Thêm mới" : "Chỉnh sửa"}
          </Button>
          <Button type="default" style={{ color: '#fa5656' }} onClick={() => { history.goBack(); }}>
            {`Quay lại`}
          </Button>
        </FooterToolbar>
      </Form>
    );
  }
  return <></>;
};

export default connect(({ classroom, loading, router }) => ({
  submitting: loading.effects['classroom/submit'],
  classroom,
  router
}))(ClassRoomCrud);

