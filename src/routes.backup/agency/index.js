import React from "react";
import { router } from 'dva';
import ClassRoomList from "./classroom/list";
import ClassRoomCrud from "./classroom/crud";

const { Route, Switch } = router;
const Agency = ({ match }) => (
  <Switch>
    <Route path={`${match.url}/classroom`} component={ClassRoomList} />
    <Route path={`${match.url}/classroom/:id`} component={ClassRoomCrud} />
  </Switch>
);

export default Agency;
