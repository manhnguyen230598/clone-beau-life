import React from "react";
import ReactDOM from "react-dom";
import dva, { createHashHistory } from 'dva';
import createLoading from 'dva-loading';

import registerServiceWorker from './registerServiceWorker';
import "assets/vendors/style";
import "styles/weland.less";
import "./firebase/firebase";
// model
import modelGlobal from './models/global';
import modelMenu from './models/menu';
import modelCommon from './models/common';
import modelSetting from './models/settings';
import modelAuth from './models/auth';
import modelRole from './models/role';
import modelUser from './models/user';
/* PLOP_INJECT_IMPORT */
import modelCustomerOld from './models/customerOld';
import modelLoginImage from './models/loginImage';
import modelActionLog from './models/actionLog';
import modelHistoryChangeJob from './models/historyChangeJob';
import modelEventApp from './models/eventApp';
import modelPopupApp from './models/popupApp';
import modelBannerApp from './models/bannerApp';
import modelDealerTarget from './models/dealerTarget';
import modelNotification from './models/notification';
import modelBannerHome from './models/bannerHome';
import modelJob from './models/job';
import modelRoleUserProjectJob from './models/roleUserProjectJob';
import modelRequireChange from './models/requireChange';
import modelRelatedTasks from './models/relatedTasks';
import modelGroupJob from './models/groupJob';
import modelTagJob from './models/tagJob';
import modelRoleProjectJob from './models/roleProjectJob';
import modelEmployees from './models/employees';
import modelTargetsDealer from './models/targetsDealer';
import modelChecklist from './models/checklist';
import modelBusinessStaff from './models/businessStaff';
import modelSalePolicy from './models/salePolicy';
import modelReportFund from './models/reportFund';
import modelProcedureProject from './models/procedureProject';
import modelPaymentTransaction from './models/paymentTransaction';
import modelSaleTarget from './models/saleTarget';
import modelBookRequestPending from './models/bookRequestPending';
import modelCustomer from './models/customer';
import modelStackingPlanData from './models/stackingPlanData';
import modelStackingPlan from './models/stackingPlan';
import modelApartment from './models/apartment';
import modelProjectMediaCustomer from './models/projectMediaCustomer';
import modelProjectVisitingCustomer from './models/projectVisitingCustomer';
import modelReportclass from './models/reportclass';
import modelUserDoExamLog from './models/userDoExamLog';
import modelBookApartment from './models/bookapartment';
import modelBuilding from './models/building';
import modelProjectArea from './models/projectArea';
import modelAgency from './models/agency';
import modelExamination from './models/examination';
import modelFeedbackclass from './models/feedbackclass';
import modelUserclass from './models/userclass';
import modelDocumentProject from './models/documentProject';
import modelVideoProject from './models/videoProject';
import modelDocuments from './models/documents';
import modelClasslesson from './models/classlesson';
import modelQuestionexam from './models/questionexam';
import modelProject from './models/project';
import modelClassroom from './models/classroom';
import modelProjectTarget from "./models/projectTarget"

const app = dva({
  history: createHashHistory()
});

app.use(createLoading('loading'));

app.model(modelGlobal);
app.model(modelMenu);
app.model(modelCommon);
app.model(modelSetting);
app.model(modelAuth);
app.model(modelRole);
app.model(modelUser);
/* PLOP_INJECT_EXPORT */
app.model(modelCustomerOld);
app.model(modelLoginImage);
app.model(modelActionLog);
app.model(modelHistoryChangeJob);
app.model(modelEventApp);
app.model(modelPopupApp);
app.model(modelBannerApp);
app.model(modelDealerTarget);
app.model(modelNotification);
app.model(modelBannerHome);
app.model(modelJob);
app.model(modelRoleUserProjectJob);
app.model(modelRequireChange);
app.model(modelRelatedTasks);
app.model(modelGroupJob);
app.model(modelTagJob);
app.model(modelRoleProjectJob);
app.model(modelEmployees);
app.model(modelTargetsDealer);
app.model(modelChecklist);
app.model(modelBusinessStaff);
app.model(modelSalePolicy);
app.model(modelReportFund);
app.model(modelProcedureProject);
app.model(modelPaymentTransaction);
app.model(modelSaleTarget);
app.model(modelBookRequestPending);
app.model(modelCustomer);
app.model(modelStackingPlanData);
app.model(modelStackingPlan);
app.model(modelApartment);
app.model(modelProjectMediaCustomer);
app.model(modelProjectVisitingCustomer);
app.model(modelReportclass);
app.model(modelUserDoExamLog);
app.model(modelBookApartment);
app.model(modelBuilding);
app.model(modelProjectArea);
app.model(modelAgency);
app.model(modelExamination);
app.model(modelFeedbackclass);
app.model(modelUserclass);
app.model(modelDocumentProject);
app.model(modelVideoProject);
app.model(modelDocuments);
app.model(modelClasslesson);
app.model(modelQuestionexam);
app.model(modelProject);
app.model(modelClassroom);
app.model(modelProjectTarget);


app.router(require('./NextApp').default);
// app.start('#root');
const App = app.start();
export const AppContext = React.createContext();

ReactDOM.render(<AppContext.Provider value={{ store: app._store }}>
  <App />
</AppContext.Provider>, document.getElementById('root'));

// Do this once
registerServiceWorker();

export default app;
