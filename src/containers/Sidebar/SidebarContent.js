import React, { Component } from "react";
import { Menu } from "antd";
import { router, connect } from "dva";

import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";

import Auxiliary from "util/Auxiliary";
import UserProfile from "./UserProfile";
import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const { Link } = router;

class SidebarContent extends Component {

  getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };
  renderMenu = (data = [], level = 0, navStyle) => {
    return data.map(item => {
      let result;
      if (item.isParent && item.children && item.children.length) {
        if (level == 0) {
          result = (
            <MenuItemGroup key={item.key} className="gx-menu-group" title={<IntlMessages id={`${item.alias}`} />}>
              {this.renderMenu(item.children, 1, navStyle)}
            </MenuItemGroup>
          );
        } else {
          result = (
            <SubMenu
              key={item.key}
              className={this.getNavStyleSubMenuClass(navStyle)}
              title={<span> <i className={`icon ${item.icon}`} />
                <IntlMessages id={`${item.alias}`} /></span>}
            >
              {this.renderMenu(item.children, 2, navStyle)}
            </SubMenu>
          );
        }
      } else {
        if (level == 0) {
          /* result = (
            <SubMenu className="gx-menu-group" key={item.key}
              title={<>
                <Link to={item.url} style={{ color: '#ffffff' }}>
                  <IntlMessages id={`${item.alias}`} />
                </Link></>}>
            </SubMenu>
          ); */
          result = (
            <Menu.Item key={item.key}>
              <Link to={item.url} style={{ color: '#ffffff' }}>
                {/* <i className={`icon ${item.icon}`} /> */}
                <IntlMessages id={`${item.alias}`} />
              </Link>
            </Menu.Item>
          );
        } else {
          result = (
            <Menu.Item key={item.key}>
              <Link to={item.url}>
                <i className={`icon ${item.icon}`} />
                <IntlMessages id={`${item.alias}`} />
              </Link>
            </Menu.Item>
          );
        }
      }

      return result;
    });
  };

  render() {
    const { themeType, navStyle, pathname, menus } = this.props;
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split('/')[1];
    return (
      <Auxiliary>
        <SidebarLogo />
        <div className="gx-sidebar-content">
          <div className={`gx-sidebar-notifications ${this.getNoHeaderClass(navStyle)}`}>
            <UserProfile />
            <AppsNavigation />
          </div>
          <CustomScrollbars className="gx-layout-sider-scrollbar">
            <Menu
              defaultOpenKeys={[defaultOpenKeys]}
              selectedKeys={[selectedKeys]}
              theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
              mode="inline">
              {this.renderMenu(menus, 0, navStyle)}
              {/* <MenuItemGroup key="main" className="gx-menu-group" title={<IntlMessages id="sidebar.main" />}>
                <SubMenu
                  key="dashboard"
                  className={this.getNavStyleSubMenuClass(navStyle)}
                  title={<span> <i className="icon icon-dasbhoard" />
                    <IntlMessages id="sidebar.dashboard" /></span>}
                >
                  <Menu.Item key="main/dashboard/report-agency">
                    <Link to="/main/dashboard/report-agency">
                      <i className="icon icon-crypto" />
                      <IntlMessages id="sidebar.dashboard.crypto" />
                    </Link>
                  </Menu.Item>
                </SubMenu>

                <Menu.Item key="main/roles">
                  <Link to="/main/roles">
                    <i className="icon icon-widgets" />
                    <IntlMessages id="sidebar.roles" />
                  </Link>
                </Menu.Item>

                <Menu.Item key="main/user">
                  <Link to="/main/user">
                    <i className="icon icon-user" />
                    <IntlMessages id="sidebar.user" />
                  </Link>
                </Menu.Item>

                <Menu.Item key="main/agency">
                  <Link to="/main/agency">
                    <i className="icon icon-crm" />
                    <IntlMessages id="sidebar.agency" />
                  </Link>
                </Menu.Item>

              </MenuItemGroup>

              <MenuItemGroup key="in-built-apps" className="gx-menu-group" title={<IntlMessages id="sidebar.inBuiltApp" />} >
                <Menu.Item key="in-built-apps/mail">
                  <Link to="/agency/classroom">
                    <i className="icon icon-email" />
                    <IntlMessages id="sidebar.manage_class" />
                  </Link>
                </Menu.Item>
              </MenuItemGroup>

              <MenuItemGroup key="manage-project" className="gx-menu-group" title={<IntlMessages id="sidebar.customViews" />} >

                <Menu.Item key="manage-project/project">
                  <Link to="/manage-project/project">
                    <i className="icon icon-auth-screen" />
                    <IntlMessages id="sidebar.manage_project" />
                  </Link>
                </Menu.Item>
                <Menu.Item key="manage-project/video-project">
                  <Link to="/manage-project/videoproject">
                    <i className="icon icon-menu-right" />
                    <IntlMessages id="sidebar.video_project" />
                  </Link>
                </Menu.Item>

              </MenuItemGroup>

              {/* <MenuItemGroup className="gx-menu-group" key="state-project"
                title={<IntlMessages id="sidebar.stateProject" />}>

                <Menu.Item key="state-project/checklist">
                  <Link to="/state-project/checklist">
                    <i className="icon icon-anchor" />
                    <IntlMessages
                      id="sidebar.stateProject.checklist" /></Link>
                </Menu.Item>

              </MenuItemGroup> */}
            </Menu>
          </CustomScrollbars>
        </div>
      </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({ settings, menu }) => {
  const { menus } = menu;
  const { navStyle, themeType, locale, pathname } = settings;
  return { navStyle, themeType, locale, pathname, menus };
};
export default connect(mapStateToProps)(SidebarContent);

