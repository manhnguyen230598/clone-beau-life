import React, { Component } from "react";
import { connect, router } from "dva";
import { Menu } from "antd";
import IntlMessages from "../../util/IntlMessages";
import {
  NAV_STYLE_ABOVE_HEADER,
  NAV_STYLE_BELOW_HEADER,
  NAV_STYLE_DEFAULT_HORIZONTAL,
  NAV_STYLE_INSIDE_HEADER_HORIZONTAL
} from "../../constants/ThemeSetting";

const { Link } = router;
const SubMenu = Menu.SubMenu;
// const MenuItemGroup = Menu.ItemGroup;

class HorizontalNav extends Component {

  getNavStyleSubMenuClass = (navStyle) => {
    switch (navStyle) {
      case NAV_STYLE_DEFAULT_HORIZONTAL:
        return "gx-menu-horizontal gx-submenu-popup-curve";
      case NAV_STYLE_INSIDE_HEADER_HORIZONTAL:
        return "gx-menu-horizontal gx-submenu-popup-curve gx-inside-submenu-popup-curve";
      case NAV_STYLE_BELOW_HEADER:
        return "gx-menu-horizontal gx-submenu-popup-curve gx-below-submenu-popup-curve";
      case NAV_STYLE_ABOVE_HEADER:
        return "gx-menu-horizontal gx-submenu-popup-curve gx-above-submenu-popup-curve";
      default:
        return "gx-menu-horizontal";

    }
  };

  renderMenu = (data = [], level = 0, navStyle) => {
    return data.map(item => {
      let result;
      if (item.isParent && item.children && item.children.length) {
        if (level == 0) {
          result = (
            <SubMenu className={this.getNavStyleSubMenuClass(navStyle)} key={item.key}
              title={<IntlMessages id={`${item.alias}`} />}>
              {this.renderMenu(item.children, 1, navStyle)}
            </SubMenu>
          );
        } else {
          result = (<SubMenu className="gx-menu-horizontal" key={item.key}
            title={<span> <i className={`icon ${item.icon}`} />
              <IntlMessages id={`${item.alias}`} /></span>}>
            {this.renderMenu(item.children, 2, navStyle)}
          </SubMenu>
          );
        }
      } else {
        if (level == 0) {
          result = (
            <Menu.Item key={item.key}>
              <Link to={item.url} >
                {/* <i className={`icon ${item.icon}`} /> */}
                <IntlMessages id={`${item.alias}`} />
              </Link>
            </Menu.Item>
          );
          /* result = (
            <SubMenu className="gx-menu-horizontal" key={item.key}
              title={<>
                <Link to={item.url} style={{ color: '#ffffff' }}>
                  <IntlMessages id={`${item.alias}`} />
                </Link></>}>
            </SubMenu>
          ); */
        } else {
          result = (
            <Menu.Item key={item.key}>
              <Link to={item.url}>
                <i className={`icon ${item.icon}`} />
                <IntlMessages id={`${item.alias}`} />
              </Link>
            </Menu.Item>
          );
        }
      }

      return result;
    });
  };

  render() {
    const { pathname, navStyle, menus } = this.props;
    let selectedKeys = pathname.substr(1);
    let defaultOpenKeys = selectedKeys.indexOf('/') !== -1 ? selectedKeys.split('/')[1] : selectedKeys;
    if (selectedKeys === "apartment-site"){
      selectedKeys = "booking-apartment";
      defaultOpenKeys = "booking-apartment";
    }
    if (selectedKeys.indexOf("manage-project/project-detail") !== -1){
      selectedKeys = "manage-project/project";
    }
    if (selectedKeys.indexOf("agency/classroom-detail") !== -1) {
      selectedKeys = "agency/classroom";
    }
    return (

      <Menu
        defaultOpenKeys={[defaultOpenKeys]}
        selectedKeys={[selectedKeys]}
        mode="horizontal"
        disabledOverflow={true}
        onClick={({ item, key, keyPath, domEvent }) => {
          console.log(`🚀 ~ file: HorizontalNav.js ~ line 106 ~ HorizontalNav ~ render ~ key, keyPath`, key, keyPath);
        }}
      >
        {this.renderMenu(menus, 0, navStyle)}
        {/* <SubMenu className={this.getNavStyleSubMenuClass(navStyle)} key="main"
          title={<IntlMessages id="sidebar.main" />}>

          <SubMenu className="gx-menu-horizontal" key="dashboard"
            title={<span> <i className="icon icon-dasbhoard" />
              <IntlMessages id="sidebar.dashboard" /></span>}>
            <Menu.Item key="main/dashboard/report-agency">
              <Link to="/main/dashboard/report-agency">
                <i className="icon icon-crypto" />
                <IntlMessages id="sidebar.dashboard.crypto" />
              </Link>
            </Menu.Item>
          </SubMenu>

          <Menu.Item key="main/roles">
            <Link to="/main/roles">
              <i className="icon icon-widgets" />
              <IntlMessages id="sidebar.roles" />
            </Link>
          </Menu.Item>

          <Menu.Item key="main/user">
            <Link to="/main/user">
              <i className="icon icon-user" />
              <IntlMessages id="sidebar.user" />
            </Link>
          </Menu.Item>

          <Menu.Item key="main/agency">
            <Link to="/main/agency">
              <i className="icon icon-crm" />
              <IntlMessages id="sidebar.agency" />
            </Link>
          </Menu.Item>

        </SubMenu>

        <SubMenu className={this.getNavStyleSubMenuClass(navStyle)} key="in-built-apps"
          title={<IntlMessages id="sidebar.inBuiltApp" />}>

          <Menu.Item key="in-built-apps/classroom">
            <Link to="/agency/classroom">
              <i className="icon icon-email" />
              <IntlMessages id="sidebar.manage_class" />
            </Link>
          </Menu.Item>

        </SubMenu>

        <SubMenu className={this.getNavStyleSubMenuClass(navStyle)} key="manage-project"
          title={<IntlMessages id="sidebar.customViews" />}>

          <Menu.Item key="manage-project/project">
            <Link to="/manage-project/project">
              <i className="icon icon-auth-screen" />
              <IntlMessages
                id="sidebar.manage_project" /></Link>
          </Menu.Item>
          <Menu.Item key="manage-project/videoproject">
            <Link to="/manage-project/videoproject">
              <i className="icon icon-menu-right" />
              <IntlMessages
                id="sidebar.video_project" /></Link>
          </Menu.Item>

        </SubMenu>

        {/* <SubMenu className={this.getNavStyleSubMenuClass(navStyle)} key="state-project"
          title={<IntlMessages id="sidebar.stateProject" />}>

          <Menu.Item key="state-project/checklist">
            <Link to="/state-project/checklist">
              <i className="icon icon-anchor" />
              <IntlMessages
                id="sidebar.stateProject.checklist" /></Link>
          </Menu.Item>

        </SubMenu> */}
      </Menu>

    );
  }
}

HorizontalNav.propTypes = {};
const mapStateToProps = ({ settings, menu }) => {
  const { menus } = menu;
  const { themeType, navStyle, pathname, locale } = settings;
  return { themeType, navStyle, pathname, locale, menus };
};
export default connect(mapStateToProps)(HorizontalNav);

