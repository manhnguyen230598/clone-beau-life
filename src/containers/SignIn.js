import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import {
  GoogleOutlined,
  FacebookOutlined,
  GithubOutlined,
  TwitterOutlined
} from "@ant-design/icons";
import { connect } from "dva";
import { router } from "dva";

import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";
import * as authServices from "src/services/auth";
import Loading from "src/components/Loading";
const { Link } = router;
const FormItem = Form.Item;

class SignIn extends React.Component {
  form = React.createRef();
  state = {
    data : undefined,
    loading : false
  }
  handleSubmit = values => {
    const { dispatch } = this.props;
    dispatch({
      type: "auth/showAuthLoader"
    });
    dispatch({
      type: "auth/userSignIn",
      payload: values
    });
  };
  componentDidUpdate() {
    const { dispatch } = this.props;
    if (this.props.showMessage) {
      setTimeout(() => {
        dispatch({
          type: "auth/hideMessage"
        });
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push("/");
    }
  }
  componentDidMount() {
    this.init()
  }
  init = async ()=>{
    try {
      this.setState({loading :  true})
      let results = await authServices.getLogin();
      this.setState({
        data : results?.data?.data
      })
      this.setState({loading :  false})
    } catch (error) {
      
    }
  }
  render() {
    const { showMessage, loader, alertMessage } = this.props;
    if(this.state.loading) return <Loading />
    return (
      <div
        className=""
        style={{
          backgroundImage: `url(${this.state.data?.image})`,
          height: "100%",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover"
        }}
      >
        <div
          className=""
          style={{
            position: "absolute",
            maxWidth: "680px",
            width: "94%",
            bottom : Number(this.state.data?.bottom) > 0 ? this.state.data?.bottom + "px" : this.state.data?.bottom ,
            right : Number(this.state.data?.right) > 0 ? this.state.data?.bottom + "px" : this.state.data?.bottom ,
            top : Number(this.state.data?.top) > 0 ? this.state.data?.bottom + "px" : this.state.data?.bottom ,
            left : Number(this.state.data?.left) > 0 ? this.state.data?.bottom + "px" : this.state.data?.bottom ,
          }}
        >
          <div className="gx-app-login-main-content">
            <div className="gx-app-logo-content">
              <div className="gx-app-logo-content-bg">
                {/* <img src={require('assets/images/signin.png')} alt='MediaOne' /> */}
              </div>
              <div className="gx-app-logo-wid">
                <h1>
                  <IntlMessages id="app.userAuth.signIn" />
                </h1>
                <p>
                  <IntlMessages id="app.userAuth.bySigning" />
                </p>
                <p>
                  <IntlMessages id="app.userAuth.getAccount" />
                </p>
              </div>
              <div className="gx-app-logo">
                <img alt="example" src={require("assets/images/w-logo.png")} />
                <span
                  style={{ margin: "0 auto", verticalAlign: "center" }}
                >{` `}</span>
              </div>
            </div>
            <div className="gx-app-login-content">
              <Form
                ref={this.form}
                onFinish={this.handleSubmit}
                className="gx-signin-form gx-form-row0"
              >
                <FormItem
                  name="username"
                  initialValue=""
                  rules={[
                    { required: true, message: "Vui lòng nhập tên đăng nhập!" }
                  ]}
                >
                  <Input placeholder="Số điện thoại" />
                </FormItem>
                <FormItem
                  name="password"
                  initialValue=""
                  rules={[
                    { required: true, message: "Vui lòng nhập mật khẩu!" }
                  ]}
                >
                  <Input type="password" placeholder="Mật khẩu" />
                </FormItem>
                {/* <FormItem
                  name="remember"
                  valuePropName="checked"
                  initialValue={true}
                >
                  <Checkbox><IntlMessages id="appModule.iAccept" /></Checkbox>
                  <span className="gx-signup-form-forgot gx-link"><IntlMessages id="appModule.termAndCondition" /></span>
                </FormItem> */}
                <FormItem>
                  <Button type="primary" className="gx-mb-0" htmlType="submit">
                    <IntlMessages id="app.userAuth.signIn" />
                  </Button>
                  {/* <span><IntlMessages id="app.userAuth.or" /></span> <Link to="/signup"><IntlMessages
                    id="app.userAuth.signUp" /></Link> */}
                </FormItem>
                {/* <div className="gx-flex-row gx-justify-content-between">
                  <span>or connect with</span>
                  <ul className="gx-social-link">
                    <li>
                      <GoogleOutlined onClick={() => {
                        // this.props.showAuthLoader();
                        // this.props.userGoogleSignIn();
                      }} />
                    </li>
                    <li>
                      <FacebookOutlined onClick={() => {
                        // this.props.showAuthLoader();
                        // this.props.userFacebookSignIn();
                      }} />
                    </li>
                    <li>
                      <GithubOutlined onClick={() => {
                        // this.props.showAuthLoader();
                        // this.props.userGithubSignIn();
                      }} />
                    </li>
                    <li>
                      <TwitterOutlined onClick={() => {
                        // this.props.showAuthLoader();
                        // this.props.userTwitterSignIn();
                      }} />
                    </li>
                  </ul>
                </div>
                <span
                  className="gx-text-light gx-fs-sm"> demo user email: 'demo@example.com' and password: 'demo#123'</span> */}
              </Form>
            </div>

            {loader ? (
              <div className="gx-loader-view">
                <CircularProgress />
              </div>
            ) : null}
            {showMessage && alertMessage
              ? message.error(alertMessage.toString(), 10)
              : null}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser };
};

export default connect(mapStateToProps)(SignIn);
