import React from "react";
import { routerRedux, router } from 'dva';
import { AppContext } from './index';

import App from "./containers/App/index";

const { Router, Route, Switch } = router;
const { ConnectedRouter } = routerRedux;

export default ({ history, app }) => {
  return (
    <>
      <ConnectedRouter history={history} context={AppContext} omitRouter>
        <Router history={history}>
          {/* <React.Suspense fallback={"fff"}> */}
          <Switch>
            {/* <Route exact path="/login" name="Login Page" render={props => <UserLayout {...props} />} />
            <Route exact path="/exception/404" name="Page 404" render={props => <Page404 {...props} />} />
            <Route exact path="/exception/500" name="Page 500" render={props => <Page500 {...props} />} /> */}
            <Route path="/" name="Home" render={props => <App {...props} />} />
            {/* <Route path="/" component={App} /> */}
          </Switch>
          {/* </React.Suspense> */}
        </Router>
      </ConnectedRouter>
    </>
  );
};

