# [WELAND - React Redux Admin based on Ant Framework]
Wieldy is a react redux based admin template. 
 
# Installation

**Note: Weland is using [yarn] instead npm**

Installing all the dependencies of project, run following command:

``` $yarn ```

# Run dev

``` $yarn dev```

# Run prod

Build

```$yarn build```

Run

```$yarn start```

## Branches
| Branch                           | Description   |
| -------------------------------- | ------------- |
| `master`                         | This is the main production branch. You should pull from here to update your local repo. |
| `dev`               | This branch is for you to kickstart your project with the starter template with auth (Firebase). This way you can start building your project from ground-up. |
