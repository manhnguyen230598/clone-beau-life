# FROM mhart/alpine-node as builder
FROM node:12.22.3-alpine as builddev
WORKDIR /app
COPY . .
# COPY yarn.lock /app/yarn.lock

RUN yarn install
RUN yarn build:dev

# FROM nginx:alpine as builder
FROM nginx:1.20-alpine as builder
COPY --from=builddev /app/build /usr/share/nginx/html
