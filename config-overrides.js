const path = require('path');
const { override, addLessLoader, addBabelPlugins, /* addBabelPresets  fixBabelImports , addWebpackPlugin */ } = require('customize-cra');

const dev = process.env.NODE_ENV == 'development' ? true : false;

const overrideProcessEnv = value => config => {
  config.resolve.modules = [
    path.join(__dirname, 'src')
  ].concat(config.resolve.modules);

  /* config.resolve.modules.rules.push({
    test: /\.tsx?$/,
    exclude: /(node_modules|bower_components)/,
    use: {
      loader: 'babel-loader',
      options: {
        presets: [
          '@babel/typescript',
          [
            '@babel/env',
            {
              loose: true,
              modules: false,
            },
          ],
          '@babel/react',
        ],
        plugins: [
          ['@babel/plugin-proposal-decorators', { legacy: true }],
          ['@babel/plugin-proposal-class-properties', { loose: true }],
          '@babel/proposal-object-rest-spread',
        ],
      },
    },
  }); */

  return config;
};

module.exports = override(
  /* fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true
  }), */
  /* ...addBabelPresets(
    '@babel/typescript',
    [
      '@babel/env',
      {
        loose: true,
        modules: false,
      },
    ],
    '@babel/react'
  ), */
  ...addBabelPlugins(
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    '@babel/proposal-object-rest-spread'
  ),
  addLessLoader({
    sourceMap: dev,
    lessOptions: {
      modifyVars: {
        '@primary-color': '#038fde',
        '@secondary-color': '#fa8c16',
        '@text-color': '#545454',
        '@heading-color': '#535353',
        '@nav-dark-bg': '#003366',
        '@header-text-color': '#262626',
        '@layout-header-background': '#fefefe',
        '@layout-footer-background': '#fffffd',
        '@nav-dark-text-color': '#038fdd',
        '@hor-nav-text-color': '#fffffd',
        '@nav-header-selected-text-color': '#fdbe33'
      },
      javascriptEnabled: true
    }
  }),
  overrideProcessEnv({
    VERSION: JSON.stringify(require('./package.json').version),
  })
);
